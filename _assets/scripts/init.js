
/////////////////////////////////
// initialized google analytics
/////////////////////////////////

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-4006696-1']);
_gaq.push(['_setDomainName', 'halifax.ca']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();


////////////////////////////
// initialize data pickers
////////////////////////////

$(function() {

	// date picker on create and edit form page
	$( "#date" ).datepicker({
	  showWeek: true,
	  firstDay: 1,
	  dateFormat: "yy-mm-dd",
	  //minDate:-400,
	  maxDate:0,
	  numberOfMonths:4,
	  showWeek:false
	  //showAnim: "slideDown"
	});

	// these are the date range pickers on the news listing pages
	$( "#from" ).datepicker({
		//defaultDate: "+1w",
		//changeMonth: true,
		numberOfMonths: 3,
		maxDate:0,
		onClose: function( selectedDate ) {
			if(selectedDate)	$( "#to" ).datepicker( "option", "minDate", selectedDate );
			if(selectedDate)	$( "#to" ).datepicker( "option", "defaultDate", selectedDate );
		}
	});
	$( "#to" ).datepicker({
		//defaultDate: "+1w",
		//changeMonth: true,
		numberOfMonths: 3,
		maxDate:0,
		onClose: function( selectedDate ) {
			if(selectedDate)	$( "#from" ).datepicker( "option", "maxDate", selectedDate );
			if(selectedDate)	$( "#from" ).datepicker( "option", "defaultDate", selectedDate );
		}
	});
				
});


///////////////////////////////////////
// initialized tinyMCE WYSIWYG editor
///////////////////////////////////////

tinyMCE.init({
	mode : "textareas",
	theme : "advanced",   //(n.b. no trailing comma, this will be critical as you experiment later)
	oninit : "setPlainText",	// defaults paste to plain text
	plugins : "autolink,lists,paste",
	paste_auto_cleanup_on_paste : true,
	paste_remove_spans: true,
	theme_advanced_resize_vertical : true,
	//theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,link,unlink,|,undo,redo",
	theme_advanced_buttons1 : "bold,italic,|,bullist,numlist,|,link,unlink,|,cut,copy,pastetext,|,undo,redo",
	theme_advanced_toolbar_location : "top",
	theme_advanced_statusbar_location:"none"		// hides the status bar at the bottom of the text-area
});


///////////////////////////////
// delete confirmation prompt
///////////////////////////////

function confirm_delete() {
	var answer = confirm("Are you sure you want to delete this article?");
	if (answer)
	{
		return true;
	}
	else 
	{
		return false;
	}
}




///////////////////////////////
// Accordian test
///////////////////////////////
$(function() {

	var heading		= $('.theHeading');
	var content 	= $('.theContent');
	var duration	= 300;

	heading.click(function(){
		content.toggle("slow");
	});

});