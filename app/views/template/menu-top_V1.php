

<?php

/* ======================================================================
 *  * 
 * 		MODULE:		menu-top.php
 * 		CREATED:	Unknown
 * 		AUTHOR:		Unknown
 * 
 * 		This contains the Search Filters used within the AReporting
 * 		software on the show_deliverables() portion.  From this
 * 		main deliverables screen the users can select filtering of
 * 		what data is displayed.  This is also further effected by
 * 		Different types of views that use the "DeliverablesViews2016"
 * 		Table to get the collective necessary information shown
 * 		from across several tables (PRSC).
 * 
 * 		MODIFIED:
 * 		201602		PRSC		Altered to add extra fields for the
 * 								2016 changes requested by J.Spekkens.
 * =======================================================================
 */

//		 echo "<br><br><br><br>";

//	 echo "<pre>";
//	 print_r($serviceareas);
//	 echo "</pre>";
?>

<br><br><br>

	<div id="filter_options">
		<div class="container">
		<?= form_open(base_url()) ?>
			<div class="row">
				<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" id="DeliverableTypeID" name="DeliverableTypeID">
							<option value="3" <?= set_select('DeliverableTypeID', 3) ?>>All Types</option>
							<?php foreach ($deliverable_types as $deliverable_type): ?>
							<option value="<?= $deliverable_type['DeliverableTypeID'] ?>" <?= set_select('DeliverableTypeID', $deliverable_type['DeliverableTypeID']) ?>><?= $deliverable_type['DeliverableTypeName'] ?>s</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="filter_option">			
						<select class="form-control" id="BusinessUnitID" name="BusinessUnitID">
							<option value="">All Business Units</option>
							<?php foreach ($business_units as $business_unit): ?>
							<option name="<?= $business_unit['BusinessUnitCode'] ?>" value="<?= $business_unit['BusinessUnitID'] ?>" <?= set_select('BusinessUnitID', $business_unit['BusinessUnitID']) ?>><?= $business_unit['BusinessUnitShortName'] ?> (<?= $business_unit['BusinessUnitCode'] ?>)</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				
				
			<?php 
			
				/*------------------------------------------------------------------
				 * 	This block handles filling the Service Area dropdown box 
				 * 	populating. Depending on the Business unit that the user selects
				 *	this dropdown will be repopulated to reflect the Service areas
				 *  associated with that (BU) Business Unit. 
				*--------------------------------------------------------------PRSC  
 				*/
 			
			?>
	

				<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control service_area_selection _BU_dependency">
							<option value="">All Service Areas</option>
						</select>
						<?php
							$business_unit_id = '';
							foreach ($business_unit_service_areas as $business_unit_service_area)
							{
								if(empty($business_unit_id)) 
								{
									$business_unit_id  = $business_unit_service_area['BusinessUnitID'];
						?>
									<select class="form-control business_unit_dependency <?= $business_unit_id ?>_BU_dependency" name="ServiceArea<?= $business_unit_id ?>">
										<option value="">All <?= $business_unit_service_area['BusinessUnitCode'] ?> Service Areas</option>
										<option value="<?= $business_unit_service_area['ServiceAreaID'] ?>" <?= set_select('ServiceArea'.$business_unit_id, $business_unit_service_area['ServiceAreaID']) ?>><?= $business_unit_service_area['ServiceAreaName'] ?></option>
						<?php
								}
								elseif($business_unit_id != $business_unit_service_area['BusinessUnitID']) 
								{
									$business_unit_id = $business_unit_service_area['BusinessUnitID'];
						?>
									</select>
									<select class="form-control business_unit_dependency <?= $business_unit_id ?>_BU_dependency" name="ServiceArea<?= $business_unit_id ?>">
										<option value="">All <?= $business_unit_service_area['BusinessUnitCode'] ?> Service Areas</option>
										<option value="<?= $business_unit_service_area['ServiceAreaID'] ?>" <?= set_select('ServiceArea'.$business_unit_id, $business_unit_service_area['ServiceAreaID']) ?>><?= $business_unit_service_area['ServiceAreaName'] ?></option>
						<?php
								}
								else
								{
						?>
									<option value="<?= $business_unit_service_area['ServiceAreaID'] ?>" <?= set_select('ServiceArea'.$business_unit_id, $business_unit_service_area['ServiceAreaID']) ?>><?= $business_unit_service_area['ServiceAreaName'] ?></option>
						<?php
								}
							}
						?>

						</select>
					</div>
					<script type="text/javascript">
						$(function() {
							$('.business_unit_dependency').hide();
							<?php if($this->input->post('BusinessUnitID') != ''): ?> 
							$('.service_area_selection').hide();
							$('.' + <?php echo $this->input->post('BusinessUnitID') ?>+'_BU_dependency').show();
							<?php endif ?>

					        $('#BusinessUnitID').change(function(){
					            $('.business_unit_dependency').hide();
					            $('.service_area_selection').hide();
					            $('.' + $(this).val()+'_BU_dependency').show();
					        });
					    });
					</script>
				</div>

			
				<?php 
						if($this->session->userdata('UserAdminFlag')):

				/*------------------------------------------------------------------
				 * 	This block handles filling the Service FOR dropdown box 
				 * 	populating. Depending on the Business unit that the user selects
				 *	this dropdown will be repopulated to reflect the Service areas
				 *  associated with that (BU) Business Unit.
				 *  
				 *   This is duplication of code from the 2015 version, and must
				 *   be changed to support multiple Service Area FOR options in
				 *   the future.
				 *   
				 *   MODIFIED:	
				 *   
				 *   20160114	PRSC	Code block for this added to support FOR
				*--------------------------------------------------------------PRSC  
 				*/
 			
			
			/* OLD VERSION TEMP REMOVED 
			 				
				<div class="col-sm-4">
					<div class="filter_option">			
						<select class="form-control" id="ServiceBY" name="ServiceBY">
							<option value="">Service Delivering this Initiative</option>
							<?php foreach ($services as $dt): ?>
							<option name="<?= $dt['ServiceID'] ?>
							" value="<?= $dt['ServiceID'] ?>"
                              <?= set_select('ServiceID', $dt['ServiceID']) ?>>
                              <?= $dt['ServiceShortNM'] ?> 
                              (<?= $dt['ServiceID'] ?>)</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
	
				<div class="col-sm-4">
					<div class="filter_option">			
						<select class="form-control" id="ServiceFOR" name="ServiceFOR">
							<option value="">Service Impacted by Initiative</option>
							<?php foreach ($services as $dt): ?>
							<option name="<?= $dt['ServiceID'] ?>
							" value="<?= $dt['ServiceID'] ?>"
                              <?= set_select('ServiceID', $dt['ServiceID']) ?>>
                              <?= $dt['ServiceShortNM'] ?> 
                              (<?= $dt['ServiceID'] ?>)</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
			*/ ?>
			
			
			
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Service  BY 
		 * -----------------------------------------------------PRSC 201601
		 */	?>
	
		<div class="col-sm-4">
		<div class="filter_option">			
			<select class="form-control" id="ServiceBY" name="ServiceBY">

				    <?php 
				    $real = '';
				    
					foreach ($services as $rt) 
						{
						if($rt['ServiceID'] == $ddfilters['ServiceBY'])
							$real = $rt['ServiceShortNM'];
						}
						
				    if($real)
				    	echo '<option value="' . $ddfilters['ServiceBY'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Service Delivering the Initiative </option>';
					?>	
					
					 		 
					 <?php 

					 /* --------------------------------------------------------------
					  *		This code builds the drop down Services Box for both the
					  *		Create Screen and the Edit Screen 
					  * ------------------------------------------------20160217 PRSC
					  */
					 	 

				 // In a simple Create just show all the Services */   	
				 foreach ($services as $dt)
					{ 

				/*		<option name="<?= $dt['ServiceID'] ?>"
						 value="<?= $dt['ServiceID'] ?>"
					 		 <?= set_select('ServiceID', $dt['ServiceID'],
					 		 ((!empty($service) && $deliverable['ServiceID']
					 		 	 == $dt['ServiceID'])? TRUE : '')) ?>>
					}				    	
				*/
				    	
					//  Lookup the actual Service Area Code to show to user PRSC					
					$vdt = $dt['ServiceAreaID'];
					foreach ($serviceareas as $st)
					{
						if($vdt == $st['ServiceAreaID'])
						{
							$saCD = $st['ServiceAreaShortCD'];
						}
					}
						 		 
					//  Lookup the actual Business Unit Code to show to user PRSC					
					$vdt = $dt['BusinessUnitID'];
					foreach ($business_units as $st)
					{
					if($vdt == $st['BusinessUnitID'])
						{
							$buCD = $st['BusinessUnitCode'];
						}
					}
							 
					// Add each option to the drop down box
					$vl = $dt['ServiceID'];
//					print '<option value="' .  $vl  . '">' . "(" . $buCD . ")(" . $saCD . ') >' . $dt['ServiceShortNM'] . '</option>';;
					print '<option value="' .  $vl  . '">' . "( " . $buCD . " - " . $saCD . ' ) ' . $dt['ServiceShortNM'] . '</option>';;
					
				    	
				   
				 }		 
					 		 
				?>

					
				</select>
			</div>
		</div>




		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Service   FOR 
		 * -----------------------------------------------------PRSC 201601
		 */	?>
	
		<div class="col-sm-4">
		<div class="filter_option">			
			<select class="form-control" id="ServiceFOR" name="ServiceFOR">

				    <?php 
				    $real = '';
				    
					foreach ($services as $rt) 
						{
						if($rt['ServiceID'] == $ddfilters['ServiceFOR'])
							$real = $rt['ServiceShortNM'];
						}
						
				    if($real)
				    	echo '<option value="' . $ddfilter['ServiceFOR'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Service Impacted by Initiative </option>';
					?>	
					
					 		 
					 <?php 

					 /* --------------------------------------------------------------
					  *		This code builds the drop down Services Box for both the
					  *		Create Screen and the Edit Screen 
					  * ------------------------------------------------20160217 PRSC
					  */
					 	 

				 // In a simple Create just show all the Services */   	
				 foreach ($services as $dt)
					{ 

				/*		<option name="<?= $dt['ServiceID'] ?>"
						 value="<?= $dt['ServiceID'] ?>"
					 		 <?= set_select('ServiceID', $dt['ServiceID'],
					 		 ((!empty($service) && $deliverable['ServiceID']
					 		 	 == $dt['ServiceID'])? TRUE : '')) ?>>
					}				    	
				*/
				    	
					//  Lookup the actual Service Area Code to show to user PRSC					
					$vdt = $dt['ServiceAreaID'];
					foreach ($serviceareas as $st)
					{
						print "TEST A [" . $vdt . "][" . $st['ServiceAreaID'] . "]";
						if($vdt == $st['ServiceAreaID'])
						{
							$saCD = $st['ServiceAreaShortCD'];
						}
					}
						 		 
					//  Lookup the actual Business Unit Code to show to user PRSC					
					$vdt = $dt['BusinessUnitID'];
					foreach ($business_units as $st)
					{
					if($vdt == $st['BusinessUnitID'])
						{
							$buCD = $st['BusinessUnitCode'];
						}
					}
							 
					// Add each option to the drop down box
					$vl = $dt['ServiceID'];
//					print '<option value="' .  $vl  . '">' . "(" . $buCD . ")(" . $saCD . ') >' . $dt['ServiceShortNM'] . '</option>';;
					print '<option value="' .  $vl  . '">' . "( " . $buCD . " - " . $saCD . ' ) ' . $dt['ServiceShortNM'] . '</option>';;
					
				    	
				   
				 }		 
					 		 
				?>

					
				</select>
			</div>
		</div>











		<?php   
		
		/*-------------------------------------------------------------------------
		 * 		Temporarily Retired at J.Spekken request as it was thought to be
		 * 		confusing the users.
		 * ----------------------------------------------------20160218 PRSC

		
				<div class="col-sm-4">
					<div class="filter_option">			
						<select class="form-control" id="Performance" name="Performance">
							<option value="">Key Performance Indicator</option>
							<?php foreach ($performances as $dt): ?>
							<option name="<?= $dt['PerformanceID'] ?>" 
							value="<?= $dt['PerformanceID'] ?>"
                             <?= set_select('PerformanceID', $dt['PerformanceID']) ?>>
                             <?= $dt['PerformanceShortName'] ?> (<?= $dt['PerformanceID'] ?>)</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>

   			*/
		?>


				<div class="col-sm-4">
					<!-- <div class="filter_option">
						<select class="form-control" name="OutcomeID">
							<option value="">All Priority Outcomes</option>
							<?php foreach ($priority_outcomes as $priority_outcome): ?>
								<?php if($priority_outcome['OutcomeID'] == 1): ?>
								<option value="1" <?= set_select('OutcomeID', $priority_outcome['OutcomeID']) ?>>No Associated Outcome</option>
								<?php elseif($priority_outcome['OutcomeID'] != 1): ?>							
								<option value="<?= $priority_outcome['OutcomeID'] ?>" <?= set_select('OutcomeID', $priority_outcome['OutcomeID']) ?>><?= $priority_outcome['OutcomeName'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div> -->

	<?php endif     //*-----------------------------------------------------------( EO If Admin users ) ?>



					<div class="filter_option">
						<select class="form-control" name="FocusAreaID">
							<option value="">All Focus Areas</option>
							<option value="1" <?= set_select('FocusAreaID', 1) ?>>None</option>
							<?php foreach ($focus_areas as $focus_area): ?>
								<?php if($focus_area['FocusAreaID'] == 1): ?>
								<?php elseif($focus_area['FocusAreaID'] != 1): ?>							
								<option value="<?= $focus_area['FocusAreaID'] ?>" 
									<?= set_select('FocusAreaID', $focus_area['FocusAreaID']) ?>>
									<?= $focus_area['FocusAreaName'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="AdminPillarID">
							<option value="">All Administrative Pillars</option>
							<option value="1" <?= set_select('AdminPillarID', 1) ?>>None</option>
							<?php foreach ($admin_pillars as $admin_pillar): ?>
								<?php if($admin_pillar['AdminPillarID'] == 1): ?>
								<?php elseif($admin_pillar['AdminPillarID'] != 1): ?>							
								<option value="<?= $admin_pillar['AdminPillarID'] ?>" <?= set_select('AdminPillarID', $admin_pillar['AdminPillarID']) ?>><?= $admin_pillar['AdminPillarName'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="RiskID">
							<option value="">All Risks</option>
							<option value="1" <?= set_select('RiskID', 1) ?>>None</option>
							<?php foreach ($risks as $risk): ?>
								<?php if($risk['RiskID'] != 1): ?>
								<option value="<?= $risk['RiskID'] ?>" <?= set_select('RiskID', $risk['RiskID']) ?>><?= $risk['RiskDescShort'] ?> (<?= $risk['RiskCode'] ?>)</option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="PriorityID">
							<option value="" >All Priorities</option>
							<option value="2" <?= set_select('PriorityID', 2) ?>>Priority</option>
							<option value="1" <?= set_select('PriorityID', 1) ?>>Not a Priority</option>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="StatusID">
							<option value="">Any Status</option>
							<?php foreach ($statuses as $status): ?>
							<option value="<?= $status['StatusID'] ?>" 
								<?= set_select('StatusID', $status['StatusID']) ?>>
								<?= $status['StatusDescription'] ?></option>
							<?php endforeach ?>
							<option value="4" <?= set_select('StatusID', 4) ?>>At Risk & Failing</option>
						</select>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="ViewType">
							<option value="1" <?= set_select('ViewType', 1) ?>>Default View</option>
							<option value="2" <?= set_select('ViewType', 2) ?>>Focus Area View</option>
							<option value="3" <?= set_select('ViewType', 3) ?>>Risk View</option>
							<option value="4" <?= set_select('ViewType', 4) ?>>Business Unit View</option>
							<option value="5" <?= set_select('ViewType', 5) ?>>Administrative Pillar View</option>
						</select>
					</div>
				</div>	

<?php 
      /*----------------------------------------------------------------------------------
       * 		Left out temporarily until multi-year support enabled.
       * --------------------------------------------------------------------20160217 PRSC
       */

      /*
       			<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="WorkingYear">
							<option value="2016"> Viewing Year</option>
							<option value="2014"> 2014</option>
							<option value="2015"> 2015</option>
							<option value="2016"> 2016</option>
							<option value="2017"> 2017</option>
							<option value="2018"> 2018</option>
							<option value="2019"> 2019</option>
							<option value="2020"> 2020</option>
							<option value="2021"> 2021</option>
						</select>
					</div>
				</div>	
		*/
		?>


		<?php   
		
		/*-------------------------------------------------------------------------
		 * 		Temporarily Retired at J.Spekken request as it was thought to be
		 * 		confusing the users.
		 * ----------------------------------------------------20160218 PRSC

		<div class="col-sm-4">
					<div class="filter_option">			
						<select class="form-control" id="Standard" name="Standard">
							<option value="">Service Standard Level</option>
							<?php foreach ($standards as $dt): ?>
							<option name="<?= $dt['StandardID'] ?>" 
							value="<?= $dt['StandardID'] ?>"
                             <?= set_select('StandardID', $dt['StandardID']) ?>>
                             <?= $dt['StandardShortNM'] ?> (<?= $dt['StandardID'] ?>)</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
		*/ ?>					
				
		<?php 
		/*------------------------------------------------------------------
		 * 		ELEMENT - SOURCE Selection
		 * ----------------------------------------------------20160218 PRSC
		 */
		 ?>
		 
		<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="Source">

						<?php 
							
								$realID = '';
								$realVL	= "";
								if(!empty($ddfilters['Source']))
											$realID = $ddfilters['Source'];
								
								if($realID > 0)
									{
										foreach ($sources as $rt) 
										{
										if($rt['SourceID'] == $realID)
												$realVL = $rt['SourceShortName'];
										}
									}
								
								// Fill the Default Field 
								if($realID > 0)		
								{
									$uchoice = '<option value="' . $realID . '"> ' . $realVL . '</option>';
								}
								else
								{
									$uchoice = '<option value="">Select Source</option>'; 
								}
								print $uchoice;	  
								
							?>


							<?php foreach ($sources as $dt): ?>
								 <option value="<?= $dt['SourceID'] ?>" 
								 <?= set_select('SourceID', $dt['SourceID']) ?>>
								 <?= $dt['SourceShortName'] ?></option>
								
							<?php endforeach ?>
					</select>
					</div>
				</div>	

			<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="PublishedYN">
						<?php 
							
								$realID = '';
								$realVL	= "";
								if(!empty($ddfilters['PublishedYN']))
											$realID = $ddfilters['PublishedYN'];
								
								if($realID > 0)
									{
										foreach ($publishedinfo as $rt) 
										{
										if($rt['PublishedID'] == $realID)
												$realVL = $rt['PublishedShortName'];
										}
									}
								
								// Fill the Default Field 
								if($realID > 0)		
								{
									$uchoice = '<option value="' . $realID . '"> ' . $realVL . '</option>';
								}
								else
								{
									$uchoice = '<option value="">Published Y/N</option>'; 
								}
								print $uchoice;	  
								
							?>

							<option value=""> Published Y/N?</option>
							<?php foreach ($publishedinfo as $dt): ?>
								 <option value="<?= $dt['PublishedID'] ?>" 
								 <?= set_select('PublishedID', $dt['PublishedID']) ?>>
								 <?= $dt['PublishedShortName'] ?></option>
								
							<?php endforeach ?>
						</select>
					</div>
				</div>	


	</div>





			<div class="row">
				<div class="col-sm-12 text-center" style="padding-top:10px;">
					<!-- <div class="filter_buttons text-right"> -->
					<a href="<?= base_url() ?>" type="reset" value="Reset" class="btn btn-danger"><i class="fa fa-refresh"></i> Reset</a> 
					<button type="submit" value="Submit" class="btn btn-info">Filter <i class="fa fa-filter"></i></button>
					<!-- </div> -->
				</div>
			</div>

		</form>

	</div>


</div>

