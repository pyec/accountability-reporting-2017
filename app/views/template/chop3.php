
/*           
============================================================================
 
 		FUNCTION:	download_standards()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		Allows users to export the data into a spreadsheet for this table.
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	
	
	public function download_standards()
	{
		// load file and download helper classes
		$this->load->helper('file');
		$this->load->helper('download');

		// Create a temporary file and open it with write permissions
		$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/test.csv';
		$file = fopen($path, 'w');

		// fetch data
		$filter 				= $this->input->post(null,true);
		$standards 				= $this->main_model->md_get_standards();
		$services				= $this->main_model->md_get_services();
		
		// set default header columns
		$headers = array('Standard ID','Standard Code','Standard Short Description',
									'Service',
									'LastModBy','LastModDate'
									);
		fputcsv($file, $headers);	// add headers to file

		// loop through standards
		foreach ($standards as $dt) 
		{
			$values[0] = $dt['StandardID'];
			$values[1] = $dt['StandardCD'];
			$values[2] = $dt['StandardShortNM'];
			$values[3] = strip_tags($dt['StandardDesc']);
		
		/*
			 * -----------------------------------------------------
			 *     Find Literal for Service Rating
			 * -----------------------------------------------------
			 */
			
								$realID = 0;
								$realVL	= "";
								$realID = $dt['ServiceID'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($services as $rt) 
										{
										if($rt['ServiceID'] == $realID)
											$realVL = $rt['ServiceShortName'];
										}
									}
								}
								else 
									{ $realVL = ""; }
								 $realVL;	  
			
								$values[5] = $realVL;
			
			
			
			
			$values[7] = $dt['LastModBy'];
			$values[8] = $dt['LastModDate'];
			
			fputcsv($file, $values); // add values to CSV file
//			fputcsv($file, $filter); // diag only

//			$fdest = '/inetpub/wwwroot/AccountabilityReporting2015/temp/ftest.csv';
//		write_file($fdest, $filter);	
		
		}

		fclose($file); // close file for editing
		$data = utf8_decode(file_get_contents($path));  // get data and encode it as UTF8
		$filename = 'Accountability-Reporting-2016-Standards-Export-'.date('M-d-Y').'.csv';
		
		
		force_download($filename, $data);	

	}	

	
	
