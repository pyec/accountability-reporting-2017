<!DOCTYPE html>
<html lang="en">

	<head>
	
		<title>Accountability Reporting</title>
		
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/jquery/custom-theme/jquery-ui-1.10.0.custom.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/styles/app.css">

		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto'>
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto+Condensed'>
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300'>
		
		<script src="<?= base_url() ?>_assets/plugins/jquery/jquery.min.js"></script>
		<script src="<?= base_url() ?>_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?= base_url() ?>_assets/plugins/jquery/jquery-ui-1.9.2.custom.min.js"></script>
		<script src="<?= base_url() ?>_assets/plugins/ckeditor/ckeditor.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

		<script>

			$(document).ready(function () {

				$('#tooltip').tooltip('toggle');
			    
			    //Prevent jumping to the top of the page when the href is an #
			  	$('#nav a').each(function() {
   		  			if($(this).attr('href') == "#"){
	   		  			$(this).click(function(event){
	   		  				event.preventDefault();
	   		  			});
	   		  		}
   		  		});
			   
			    $('.back_to_top').click(function(){
			        $("html,body").animate({ scrollTop: 0 }, 500);
			        return false;
			    })


				// var heading		= $('.theHeading');
				// var content 	= $('.theContent');
				// var duration	= 300;

				// heading.click(function(){
				// 	// $('.theContent').toggle("slow");
				// 	// $('.theContent').hide(duration);
				// 	content.toggle(duration);
				// });

				$('.group_content').hide();
				$('.group_heading').click(function() {
			        // $(this).parent().parent().nextAll('.theContent').first().toggle('slow');
			        $(this).parent().siblings().toggle();
			    });


			});
		</script>
		
	</head>
	
	<body>
		<a name="back_to_top"></a>
		
		
		<?php 
		/*---------------------------------------------------------
		 * 		The New Admin Menu at the top of the Header page
		 * --------------------------------------------------------
		 */?>
		
		
		<div class="navbar navbar-inverse navbar-fixed-top">
	        <div class="container">
	         	<div class="navbar-header">
					<?php if($this->session->userdata('UserName')): ?>
					<a type="button" class="btn navbar-toggle" data-toggle="collapse"
							 data-target=".navbar-collapse"><i class="fa fa-bars" style="color:white"></i></a>
					<a type="button" class="btn navbar-toggle back_to_top"
							 href="#back_to_top" style="color:white"><i class="fa fa-arrow-up"></i></a>
					<?php endif ?>
					<a class="navbar-brand" href="<?= base_url() ?>">
						<div class="logo pull-left">Accountability <span>Reporting</span> 2016</div>
					</a>
				</div>

				<?php if($this->session->userdata('UserName')): ?>
				<div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li class="hidden-xs"><a href="#back_to_top"
												 class="back_to_top" title="Back to top">Back to Top</a></li>
							<li><a href="<?= base_url() ?>">Deliverables</a></li>
								
							<!-- <li><a href="<?= base_url() ?>logout">Logout <?= $this->session->userdata('UserNameShort') ?>
							 (<?= $this->session->userdata('UserBusinessUnitCode') ?>
							 <?= $this->session->userdata('UserAdminFlag') ? '/Admin' : '' ?>)</a></li> -->
							<li><a href="<?= base_url() ?>logout">Logout 
										<?= $this->session->userdata('UserNameShort') ?></a></li>
		
						</ul>
					</div>
				</div>
				<?php endif ?>
				
									
				
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">

						<?php 
						$user_warn = '
						<font color="#FFEE00">
						[WARNING] Debugging in progress.
						Please do not use this software without notifying the developer. (chafes@halifax.ca)"
						</font>';
//						echo $user_warn;
						?>
						
						<?php 
						/* -------------------------------------------------------
						 * 			Show Lists Menu
						 * -------------------------------------------------------
						 */
						?>
						
						<ul class="nav navbar-nav navbar-right">
							<!-- And app specific Nav items would be here in the form of a <ul> -->
							<li class="dropdown">

								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								
								List Options <b class="caret"></b></a>
	
								<!-- Here is where we include or app list resource. -->
								<?php 
								
								//include_once("app/views/templates/app_list.php");

								?>
	
							<ul class="dropdown-menu">
								<?php 
								echo '<li><a href="' . base_url() . '">Show Deliverables</a></li>';
								echo '<li><a href="' . base_url() . 'show-risks">Show Risks</a></li>';
								echo '<li><a href="' . base_url() . 'show-services">Show Services</a></li>';
								echo '<li><a href="' . base_url() . 'show-serviceareas">Show Service Areas</a></li>';
								echo '<li><a href="' . base_url() . 'show-performances">Show KPIs</a></li>';
								echo '<li><a href="' . base_url() . 'show-standards">Show Service Standards</a></li>';
								echo '<li><a href="' . base_url() . 'show-businessunits">Show Business Units</a></li>';
								echo '<li><a href="' . base_url() . 'show-focusareas">Show Focus Areas</a></li>';
								echo '<li><a href="' . base_url() . 'show-priorityoutcomes">Show Priority Outcomes</a></li>';
								echo '<li><a href="' . base_url() . 'show-adminpillars">Show Administrative Pillars</a></li>';
								echo '<li><a href="' . base_url() . 'show-adminpriorities">Show Admin Priorities</a></li>';
								echo '<li><a href="' . base_url() . 'show-statusratings">Show Statuses</a></li>';
								echo '<li><a href="' . base_url() . 'show-sources">Show Sources</a></li>';
								?>
							</ul>
							</li>
				
						<?php 
							/*
							 *  --------------------------------------------------------------
							 * 		This displays the Admin user option choices in the upper
							 * 		header bar.  This is being expanded to allow for adding
							 * 		a Performance KPI Entry and Service Standard Entries.
							 * 
							 * 		At some point in the future 2016B changes this may have
							 * 		support for multiple entries being required in the general
							 * 		deliverable.
							 * 
							 * 		Prepare this for future drop down list and admin screens.
							 * 
							 * -------------------------------------------------PRSC 20160116
							 */
							?>				
				
				
						<?php if($this->session->userdata('UserAdminFlag')): ?>
							
							
						<li class="dropdown">

								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Create Options <b class="caret"></b></a>
	
								<!-- Here is where we include or app list resource. -->
								<?php 
								
								//include_once("app/views/templates/app_list.php");

								?>
	
							<ul class="dropdown-menu">
								<?php 
								echo '<li><a href="' . base_url() . 'add-deliverable">Create Deliverable</a></li>';
								echo '<li><a href="' . base_url() . 'add-risk">Create Risk</a></li>';
								echo '<li><a href="' . base_url() . 'add-service">Create Service</a></li>';
								echo '<li><a href="' . base_url() . 'add-servicearea">Create Service Area</a></li>';
								echo '<li><a href="' . base_url() . 'add-performance">Create KPI</a></li>';
								echo '<li><a href="' . base_url() . 'add-standard">Create Service Standard</a></li>';
								echo '<li><a href="' . base_url() . 'add-businessunit">Create Business Unit</a></li>';
								echo '<li><a href="' . base_url() . 'add-focusarea">Create Focus Area</a></li>';
								echo '<li><a href="' . base_url() . 'add-priorityoutcome">Create Priority Outcome</a></li>';
								echo '<li><a href="' . base_url() . 'add-adminpillar">Create Admin Pillar</a></li>';
								echo '<li><a href="' . base_url() . 'add-adminpriority">Create Admin Priority</a></li>';
								echo '<li><a href="' . base_url() . 'add-statusrating">Create Status</a></li>';
								echo '<li><a href="' . base_url() . 'add-source">Create Source</a></li>';
								?>
							</ul>


							</li>
						<?php endif ?>

							
						</ul>
	
	

						&nbsp &nbsp	
						
	


					</div><!-- /.navbar-collapse -->
			
</div></div>	
	
	
												
