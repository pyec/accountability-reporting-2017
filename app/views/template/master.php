<?php
	// Display the page if a valid session exists and the requested page is not the login page.
	// Otherwise redirect to the login page.
	if($page == 'pages/add-deliverable' || 
       $page == 'pages/add-servicelevel'  ||
       $page == 'pages/add-performance' ||
	$page == 'pages/edit' || $page == 'pages/deliverable') 
	{
		// $this->load->view('template/header', $data);
		$this->load->view('template/header');
		$this->load->view($page);
		$this->load->view('template/footer');
	}
	elseif($this->session->userdata('UserName') && $page != 'pages/login') 
	{
		// $this->load->view('template/header', $data);
		$this->load->view('template/header');
		// $this->load->view('template/menu-top', $data);
		$this->load->view('template/menu-top');
		$this->load->view($page);
		$this->load->view('template/footer');
	}
	
	else
	{
		// $this->session->set_flashdata('error', 'You must be logged in to see this content.');
		// $this->load->view('template/header');
		$this->load->view('pages/login');
		// $this->load->view('template/footer');
	}
 ?>