<?php

	
 /*=====================================================================
  * 
  * 	MODULE:		tpl_LIST_All
  * 	AUTHOR:		R.Stephen Chafe
  * 	CREATED:	20160118
  * 
  * 	This template module is the outer shell for ALL the Add/List
  * 	information used in the Accountability Project.
  * 
  * 	This is designed to add an extra layer of security protesction
  * 	to the template display, so that if the user does not show as
  * 	being logged in, or if another issue then force back to login
  * 	page.
  * 
  * 	ARGS:	$page		Name of the underlying core display PAGE file
  * 
  * ====================================================================
  */

	
		$this->load->view($page);
 ?>
