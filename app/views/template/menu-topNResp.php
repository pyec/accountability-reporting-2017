<?php 
/* =================================================================================
 * 
 * 			MODULE:		menu-top.php  (views->templates)
 * 			CREATED:	Unknown
 * 			Author:		Unknown
 * 
 * 			This contains the drop down boxes display for the Accountability2016
 * 			Application vieiwing Filter in CI. It appears at the top of any of the
 * 			listing pages used with the five different types of view trees.
 * 
 *   		This module is set up to use Responsive design principles when 
 *   		displaying the filter selections.  Do not use a standard table
 *   		and beware of the set_selection option as it is not portable even
 *   		though it was commonly used by the original developer (PRSC 201601)
 *   
 *  		Modifications:
 *  		20160114	PRSC	Added header and layou changes for 2016 display fields.
 * 
 * =================================================================================
 */
?>

		
	<div id="filter_options">
		<div class="container">
		<?= form_open(base_url()) ?>
	
	
	<table width="100%" cellpadding="0" cellspacing="0" border="1">
	<tr>
	<td width="33%" valign="top" align="center">
	
	
					<div class="filter_option">
						<select class="form-control" id="DeliverableTypeID" name="DeliverableTypeID">
							<option value="3" <?= set_select('DeliverableTypeID', 3) ?>>All Types</option>
							<?php foreach ($deliverable_types as $deliverable_type): ?>
							<option value="<?= $deliverable_type['DeliverableTypeID'] ?>" <?= set_select('DeliverableTypeID', $deliverable_type['DeliverableTypeID']) ?>><?= $deliverable_type['DeliverableTypeName'] ?>s</option>
							<?php endforeach ?>
						</select>
					</div>
	</td>
</tr><tr>	
	<td width="33%" valign="top" align="center">
	
					<div class="filter_option">			
						<select class="form-control" id="BusinessUnitID" name="BusinessUnitID">
							<option value="">All Business Units</option>
							<?php foreach ($business_units as $business_unit): ?>
							<option name="<?= $business_unit['BusinessUnitCode'] ?>" value="<?= $business_unit['BusinessUnitID'] ?>" <?= set_select('BusinessUnitID', $business_unit['BusinessUnitID']) ?>><?= $business_unit['BusinessUnitShortName'] ?> (<?= $business_unit['BusinessUnitCode'] ?>)</option>
							<?php endforeach ?>
						</select>
					</div>
				
	</td>
</tr><tr>	
	<td width="33%" valign="top" align="center">
                            
                            
	
					<div class="filter_option">
						<select class="form-control service_area_selection _BU_dependency">
							<option value="">All Service Areas</option>
						</select>
						<?php
							$business_unit_id = '';
							foreach ($business_unit_service_areas as $business_unit_service_area)
							{
								if(empty($business_unit_id)) 
								{
									$business_unit_id  = $business_unit_service_area['BusinessUnitID'];
						?>
									<select class="form-control business_unit_dependency <?= $business_unit_id ?>_BU_dependency" name="ServiceArea<?= $business_unit_id ?>">
										<option value="">All <?= $business_unit_service_area['BusinessUnitCode'] ?> Service Areas</option>
										<option value="<?= $business_unit_service_area['ServiceAreaID'] ?>" <?= set_select('ServiceArea'.$business_unit_id, $business_unit_service_area['ServiceAreaID']) ?>><?= $business_unit_service_area['ServiceAreaName'] ?></option>
						<?php
								}
								elseif($business_unit_id != $business_unit_service_area['BusinessUnitID']) 
								{
									$business_unit_id = $business_unit_service_area['BusinessUnitID'];
						?>
									</select>
									<select class="form-control business_unit_dependency <?= $business_unit_id ?>_BU_dependency" name="ServiceArea<?= $business_unit_id ?>">
										<option value="">All <?= $business_unit_service_area['BusinessUnitCode'] ?> Service Areas</option>
										<option value="<?= $business_unit_service_area['ServiceAreaID'] ?>" <?= set_select('ServiceArea'.$business_unit_id, $business_unit_service_area['ServiceAreaID']) ?>><?= $business_unit_service_area['ServiceAreaName'] ?></option>
						<?php
								}
								else
								{
						?>
									<option value="<?= $business_unit_service_area['ServiceAreaID'] ?>" <?= set_select('ServiceArea'.$business_unit_id, $business_unit_service_area['ServiceAreaID']) ?>><?= $business_unit_service_area['ServiceAreaName'] ?></option>
						<?php
								}
							}
						?>

						</select>
					</div>


					<script type="text/javascript">
						$(function() {
							$('.business_unit_dependency').hide();
							<?php if($this->input->post('BusinessUnitID') != ''): ?> 
							$('.service_area_selection').hide();
							$('.' + <?php echo $this->input->post('BusinessUnitID') ?>+'_BU_dependency').show();
							<?php endif ?>

					        $('#BusinessUnitID').change(function(){
					            $('.business_unit_dependency').hide();
					            $('.service_area_selection').hide();
					            $('.' + $(this).val()+'_BU_dependency').show();
					        });
					    });
					</script>
				
	
	</td>
</tr><tr>	
	<td width="33%" valign="top" align="center">


					<!-- <div class="filter_option">
						<select class="form-control" name="OutcomeID">
							<option value="">All Priority Outcomes</option>
							<?php foreach ($priority_outcomes as $priority_outcome): ?>
								<?php if($priority_outcome['OutcomeID'] == 1): ?>
								<option value="1" <?= set_select('OutcomeID', $priority_outcome['OutcomeID']) ?>>No Associated Outcome</option>
								<?php elseif($priority_outcome['OutcomeID'] != 1): ?>							
								<option value="<?= $priority_outcome['OutcomeID'] ?>" <?= set_select('OutcomeID', $priority_outcome['OutcomeID']) ?>><?= $priority_outcome['OutcomeName'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div> -->
					<div class="filter_option">
						<select class="form-control" name="FocusAreaID">
							<option value="">All Focus Areas</option>
							<option value="1" <?= set_select('FocusAreaID', 1) ?>>None</option>
							<?php foreach ($focus_areas as $focus_area): ?>
								<?php if($focus_area['FocusAreaID'] == 1): ?>
								<?php elseif($focus_area['FocusAreaID'] != 1): ?>							
								<option value="<?= $focus_area['FocusAreaID'] ?>" <?= set_select('FocusAreaID', $focus_area['FocusAreaID']) ?>><?= $focus_area['FocusAreaName'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>
	</td>
</tr><tr>	
	<td width="33%" valign="top" align="center">

				<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="AdminPillarID">
							<option value="">All Administrative Pillars</option>
							<option value="1" <?= set_select('AdminPillarID', 1) ?>>None</option>
							<?php foreach ($admin_pillars as $admin_pillar): ?>
								<?php if($admin_pillar['AdminPillarID'] == 1): ?>
								<?php elseif($admin_pillar['AdminPillarID'] != 1): ?>							
								<option value="<?= $admin_pillar['AdminPillarID'] ?>" <?= set_select('AdminPillarID', $admin_pillar['AdminPillarID']) ?>><?= $admin_pillar['AdminPillarName'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>
	</td>
</tr><tr>	
	<td width="33%" valign="top" align="center">

				<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="RiskID">
							<option value="">All Risks</option>
							<option value="1" <?= set_select('RiskID', 1) ?>>None</option>
							<?php foreach ($risks as $risk): ?>
								<?php if($risk['RiskID'] != 1): ?>
								<option value="<?= $risk['RiskID'] ?>" <?= set_select('RiskID', $risk['RiskID']) ?>><?= $risk['RiskDescShort'] ?> (<?= $risk['RiskCode'] ?>)</option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>
	</td>
</tr><tr>	
	<td width="33%" valign="top" align="center">

				<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="PriorityID">
							<option value="" >All Priorities</option>
							<option value="2" <?= set_select('PriorityID', 2) ?>>Priority</option>
							<option value="1" <?= set_select('PriorityID', 1) ?>>Not a Priority</option>
						</select>
					</div>
				</div>
	</td>
</tr><tr>	
	<td width="33%" valign="top" align="center">

					<div class="filter_option">
						<select class="form-control" name="StatusID">
							<option value="">Any Status</option>
							<?php foreach ($statuses as $status): ?>
							<option value="<?= $status['StatusID'] ?>" <?= set_select('StatusID', $status['StatusID']) ?>><?= $status['StatusDescription'] ?></option>
							<?php endforeach ?>
							<option value="4" <?= set_select('StatusID', 4) ?>>At Risk & Failing</option>
						</select>
					</div>
	
<br>

	
					<div class="filter_option">
						<select class="form-control" name="ViewType">
							<option value="1" <?= set_select('ViewType', 1) ?>>Default View</option>
							<option value="2" <?= set_select('ViewType', 2) ?>>Focus Area View</option>
							<option value="3" <?= set_select('ViewType', 3) ?>>Risk View</option>
							<option value="4" <?= set_select('ViewType', 4) ?>>Business Unit View</option>
							<option value="5" <?= set_select('ViewType', 5) ?>>Administrative Pillar View</option>
						</select>
					</div>
			

</td>
</tr></table>

	
				<div class="col-sm-12 text-center" style="padding-top:10px;">
					<!-- <div class="filter_buttons text-right"> -->
					<a href="<?= base_url() ?>" type="reset" value="Reset" class="btn btn-danger"><i class="fa fa-refresh"></i> Reset</a> 
					<button type="submit" value="Submit" class="btn btn-info">Filter <i class="fa fa-filter"></i></button>
					<!-- </div> -->
				</div>
		

		</form>

	</div>


</div>
