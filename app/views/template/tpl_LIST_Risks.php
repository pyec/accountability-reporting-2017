<?php

	
 /*=====================================================================
  * 
  * 	MODULE:		tpl_LIST_Risks
  * 	AUTHOR:		R.Stephen Chafe
  * 	CREATED:	20160118
  * 
  * 	This template module is the outer shell for the Add/List
  * 	Risks information.
  * 
  * 	This is designed to add an extra layer of security protesction
  * 	to the template display, so that if the user does not show as
  * 	being logged in, or if another issue then force back to login
  * 	page.
  * 
  * 	ARGS:	$page		Name of the underlying core display PAGE file
  * 
  * ====================================================================
  */


	// Display the page if a valid session exists and the requested page is not the login page.
	// Otherwise redirect to the login page.
	
	// WARNING - This is not working as it selects the second block even when page is defined.
	
	if($page == 'pages/add-deliverable' || 
       $page == 'pages/add-servicelevel'  ||
       $page == 'pages/add-performance' ||
       $page == 'pages/add-risk' ||
		$page == 'pages/edit' ||
		$page == 'pages/deliverable') 
	{
		// $this->load->view('template/header', $data);
		$this->load->view('template/header');
		$this->load->view($page);
		$this->load->view('template/footer');
	}
	elseif($this->session->userdata('UserName') && $page != 'pages/login') 
	{
		// $this->load->view('template/header', $data);
		$this->load->view('template/header');
		// $this->load->view('template/menu-top', $data);
//		$this->load->view('template/menu-top');
		$this->load->view($page);
		$this->load->view('template/footer');
	}
	
	else
	{
		// $this->session->set_flashdata('error', 'You must be logged in to see this content.');
		// $this->load->view('template/header');
		$this->load->view('pages/login');
		// $this->load->view('template/footer');
	}
 ?>
