<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>



<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			if(empty($businessunit)) 
				{
				echo form_open('create-businessunit');
				}
			else
				{
				echo form_open('update-businessunit');
				}
				echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Priority Outcome ID;
			if(!empty($businessunit)) 
			         echo form_hidden('BusinessUnitID',$businessunit['BusinessUnitID']);
			?>
 
     			<h1>
 				 <?= (!empty($businessunit)) ? 'Edit' : 'Create' ?> Business Unit</button>
                </h1>              
                                
              	<?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
	 

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Business Unit Code
		 * 
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="BusinessUnitCode">Business Unit Code (20-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="BusinessUnitCode" class="form-control"
						 value="<?= set_value('BusinessUnitCode', (!empty($businessunit) ?
						  $businessunit['BusinessUnitCode'] : ''))
						 ?>" maxlength="20">
					</div>
				</div>
 
	


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Business Unit ShorttName Desc
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="BusinessUnitShortName">Business Unit  Short Name (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="BusinessUnitShortName" class="form-control"
						 value="<?= set_value('BusinessUnitShortName', (!empty($businessunit) ?
						  $businessunit['BusinessUnitShortName'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Outcome Name Desc
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="BusinessUnitName">Business Unit Name (60-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="BusinessUnitName" class="form-control"
						 value="<?= set_value('BusinessUnitName', (!empty($businessunit) ?
						  $businessunit['BusinessUnitName'] : ''))
						 ?>" maxlength="60">
					</div>
				</div>

	
	
	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Outcome Description (FULL)
		 * -----------------------------------------------------PRSC 201601
		 */	?>

		<label for="BusinessUnitDesc">Business Unit Description (3000 digits)</label>
		<textarea name="BusinessUnitDesc" rows="15"><?= set_value('BusinessUnitDesc',
		 (!empty($businessunit) ? $businessunit['BusinessUnitDesc'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'BusinessUnitDesc', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($businessunit))
					 ? 'Update' : 'Create' ?> Business Unit</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>


