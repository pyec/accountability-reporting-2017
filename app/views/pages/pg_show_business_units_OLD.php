<?php 
	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($businessunits);
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>


	
<?php if (count($businessunits)>0): ?>

<?php 

	$business_unit_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;


	foreach ($businessunits as $business_unitDT): 
	

	
	?>

<div class="col-sm-12">

	<div class="results">    
	
	<!-- ------------------------------------------------------------------EO-->  
	


		<div class="pull-right hidden-xs">
			<?php if (count($businessunits)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	 		
			?>
			<?php endif ?>
		</div>
		
		
		
		<div class="result_count">
			<p>
				<?= count($businessunits) ?>
				<?php 								/* TITLES */
					echo 'BusinessUnits List List';
					echo (count($businessunits) == 1 ? '' : 's'); 
				?>
			</p>
		</div>
	
	</div>
	
 
    <!-- ------------------------------------------------------------------EO-->  
   
	<?php if (count($businessunits)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	If there are any SERVICES data in the Array then loop through
	 		*	and display each one in a easy to read format.
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	?>


	<?php 
	$business_unit_id 		= '';
	$business_unit_parent_id 	= '';
	$count	= 0;

	?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed Business Unit Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
	?>




<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-businessunit/<?= $business_unitDT['BusinessUnitID'] ?>" 
							title="Edit Business Unit details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						 <?= $business_unitDT['BusinessUnitID'] ?> - <?= $business_unitDT['BusinessUnitShortName'] ?></a></h1>
						
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						/* CNIU - but leave in as it will be used in future version PRSC */
						$label_color = 'success';
						//if($business_unitDT['StatusID'] == 1 ) $label_color = 'success';
						//if($business_unitDT['StatusID'] == 2 ) $label_color = 'warning';
						//if($business_unitDT['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
					</div>
				</div>

	
			<div class="row">

				<div class="col-sm-4">
								<p><strong>Business Unit Description: &nbsp;</strong></p>
				</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $business_unitDT['BusinessUnitDesc'] ?>
				</div>	
			
			</div>


			<div class="row">

				<div class="col-sm-4">
								<p><strong>Business Unit Full Name: &nbsp;</strong></p>
				</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $business_unitDT['BusinessUnitName'] ?>
				</div>	
			
			</div>


		<div class="row">

				<div class="col-sm-4">
								<p><strong>Business Unit Code: &nbsp;</strong></p>
				</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $business_unitDT['BusinessUnitCode'] ?>
				</div>	
			
			</div>
	
				
	
		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>


	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>








