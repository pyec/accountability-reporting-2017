<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($adminpillar);
// echo "</pre>";

?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($adminpillar)) 
				{
				echo form_open('create-adminpillar');
				}
			else
				{
				echo form_open('update-adminpillar');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the AdminPillar ID;
			if(!empty($adminpillar)) 
			         echo form_hidden('AdminPillarID',$adminpillar['AdminPillarID']);
			?>
 
  				<h1>
 				 <?= (!empty($adminpillar)) ? 'Edit' : 'Create' ?> Admin Pillar</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M AdminPillar ShortName Desc
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="AdminPillarName">Admin Pillar Name (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="AdminPillarName" class="form-control"
						 value="<?= set_value('AdminPillarName', (!empty($adminpillar) ?
						  $adminpillar['AdminPillarName'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M AdminPillar Short CD
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="AdminPillarCode">Admin Pillar Code (20-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="AdminPillarCode" class="form-control"
						 value="<?= set_value('AdminPillarCode', (!empty($adminpillar) ?
						  $adminpillar['AdminPillarCode'] : ''))
						 ?>" maxlength="20">
					</div>
				</div>





		

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M AdminPillar Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<label for="AdminPillarDesc">Admin Pillar Description (300 digits)</label>
		<textarea name="AdminPillarDesc" rows="15"><?= set_value('AdminPillarDesc',
		 (!empty($adminpillar) ? $adminpillar['AdminPillarDesc'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'AdminPillarDesc', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($adminpillar))
					 ? 'Update' : 'Create' ?> Admin Pillar</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>


