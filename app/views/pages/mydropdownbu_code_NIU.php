
			<div class="row">
					<div class="col-sm-4">
						<label for="BusinessUnitID2">Business Unit</label>
					</div>
										
					<div class="col-sm-8">			
						<select class="form-control" id="BusinessUnitID2" name="BusinessUnitID2">
							<option value="">Select a business unit</option>
							<?php foreach ($business_units as $rt): ?>
							<option name="<?= $rt['BusinessUnitCode'] ?>" 
								value="<?= $rt['BusinessUnitID'] ?>"
								<?= set_select('BusinessUnitID', $rt['BusinessUnitID'], 
								   ((!empty($service) && $service['BusinessUnitID'] == $rt['BusinessUnitID']) ? TRUE : '')) ?>>
								   (<?= $rt['BusinessUnitCode'] ?>) <?= $rt['BusinessUnitShortName'] ?> 
								   </option>
							<?php endforeach ?>
						</select>
					</div>
					
				</div>


			<?php 
				
					/*----------------------------------------------------------------
					 * 		This will look at the Business Unittype the user enters 
					 * 		and modify whether to show the "Service Areas" that are
					 * 		specific to the BU to choose from.
					 * 
					 * 		Normally this box is set to "hidden" via jquery.
					 * -----------------------------------------------------PRSC 201601
					 */
				
				?>


			<div class="row">
					<div class="col-sm-4">
						<label for="ServiceAreaID">Service Area</label>
					</div>
					
					<div class="col-sm-8">
						<em class="service_area_description _BU_dependency">Select a business unit above</em>


					<select class="form-control dependency BU_dependency"
						 name="ServiceArea">
							<option value="">Select a service area</option>

							<?php 
							    /*--------------------------------------------------------
							     * 		Find the chosen Service Area and make it default
							     * ----------------------------------------------------PRSC
							     */

								$realID 	= '';
								$realVL		= "";
								$realCD  	= "";
								if(!empty($service['ServiceAreaID']))
											$realID = $service['ServiceAreaID'];
								
								if($realID > 0)
									{
										foreach ($serviceareas as $rt) 
										{
										if($rt['ServiceAreaID'] == $realID)
												{
												$realVL = $rt['ServiceAreaName'];
												$realCD = $rt['ServiceAreaShortCD'];
												}
										}
									}
								
								// Fill the Default Field 
								if($realID > 0)		
								{
									$uchoice = '<option value="' . $realID . '"> ' . $realVL . '(' . $realCD . ')</option>';
								}
								else
								{
									$uchoice = '<option value="">Please Select Service Area</option>'; 
								}
								print $uchoice;	  
							
							
							    /*--------------------------------------------------------
							     * 		Then fill in the rest of the Service Areas by BU
							     *---------------------------------------------------PRSC
							     */
							
								$SAID = '';
								$BUID  = $service['BusinessUnitID'];
								if($BUID >0 )
								{
									foreach ($serviceareas as $rt)
									{
										$SAID  = $rt['BusinessUnitID'];
										if($SAID == $BUID)
										{						// Show this BU in the list
										$choice = '<option value="' . $rt['ServiceAreaID'] . '>' .  $rt['ServiceAreaName'] . '</option>';	
										}
									}					
								}		
							?>
							</select>
					</div>
				</div>
	
				<script type="text/javascript">
					$(function() {
						$('.dependency').hide();
						<?php 
							//$id = $this->input->post('BusinessUnitID');
							if($this->input->post('BusinessUnitID') != ''): 
						?> 
						//$('.service_area_description').hide();
						//$('.' + <?php
						echo $this->input->post('BusinessUnitID') ?>+'_BU_dependency').show();
						<?php endif ?>

						if($('#BusinessUnitID').val() != '') {
							$('.service_area_description').hide();
							$('.' + $('#BusinessUnitID').val() + '_BU_dependency').show();
				        }
				        
				        $('#BusinessUnitID').change(function(){
				            $('.dependency').hide();
				            $('.service_area_description').hide();
				            $('.' + $(this).val()+'_BU_dependency').show();
				        });
				    });
				</script>

	
