<?php $this->session->set_flashdata('uri', uri_string()) ?>

<?php
//	 echo "<pre>";
//	 print_r($risks);
//	 echo "</pre>";
	
 /*=====================================================================
  * 
  * 	MODULE:		LIST_Risks
  * 	AUTHOR:		R.Stephen Chafe
  * 	CREATED:	20160118
  * 
  * 	This module will list all of the Risks as stored in the database.
  * 
  * ====================================================================
  */
?>



<div class="col-sm-12">

	<div class="results">
		<div class="pull-right hidden-xs">
			<?php if (count($risks)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	 		
			?>
			<?= form_open(base_url().'download') ?>
				<input type="hidden" name="RiskID" value="<?= set_value('RiskID') ?>">
				<input type="hidden" name="PriorityID" value="<?= set_value('PriorityID') ?>">
				<input type="hidden" name="StatusID" value="<?= set_value('StatusID') ?>">
				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>
			<?php endif ?>
		</div>
		
		
		
		<div class="result_count">
			<p>
				<?= count($risks) ?>
				<?php 								/* TITLES */
					echo 'RISKS List';
					echo (count($risks) == 1 ? '' : 's'); 
				?>
			<p>
		</div>
	</div>


	<?php if (count($risks)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	If there are any RISKS data in the Array then loop through
	 		*	and display each one in a easy to read format.
	 		* -----------------------------------------------------PRSC 201601
	 		>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 		*/  
	?>


	<?php 
	$risk_id 		= '';
	$risk_parent_id 	= '';
	$count	= 0;

	
		// echo "<pre>";
		// print_r($related_supporting_risks);
		// echo "</pre>";
	?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed Risk Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
			?>


	<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-risk/<?= $risk['RiskID'] ?>" title="Update risk details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1><a href="<?= base_url() ?>risk/<?= $risk['RiskID'] ?>" title="Update risk status"><?= $risk['BusinessUnitCode'] ?>
						 <?= $risk['RiskCode'] ?> - <?= $risk['RiskDescShort'] ?></a></h1>
						<p class="text-muted"><?= $risk['RisckDescShort'] ?></p>
						<?= date('M d, Y', strtotime(substr($risk['ApproxEndDate'],0,10))) ?></p> -->
					</div>
				</div>

				<!-- <div class="col-xs-5">
					<?php
						$label_color = '';
						if($risk['StatusID'] == 1 ) $label_color = 'success';
						if($risk['StatusID'] == 2 ) $label_color = 'warning';
						if($risk['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
						<p class="approximate_complete"><?= ($risk['ApproxComplete'] == 100 ? "Complete " : $risk['ApproxComplete']."%")?></p>
						<p style="font-size:1.2em;" class="label label-<?= $label_color ?>"><?= $risk['StatusDescription'] ?></p>
					</div>
				</div> -->

			</div>

			<div class="row">

				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
						<?= $risk['RiskDesc'] ?>
						<div class="row">
							<div class="col-sm-4">
								<p><strong>Statement: &nbsp; <?= $risk['RiskStatement'] ?></strong></p>
								<p><?= $risk['OutcomeDesc'] ?></p>
							</div>
							
							<div class="col-sm-4">
								<p><strong>Risk Description: &nbsp; <?= $risk['RiskDesc'] ?></strong></p>
								<p><?= $risk['RiskDesc'] ?></p>
							</div>
							</div>
						<p class="text-muted" style="">Target Completion Date: <?= date('M d, Y', strtotime(substr($risk['ApproxEndDate'],0,10))) ?></p>
					</div>
				</div>

			</div>

		</div>

	
	
	<?php 
		$count++;
		endforeach;
	?>

	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>

</div>


