<?php $this->session->set_flashdata('uri', uri_string()) ?>

<?php
	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";
?>



<!-- Page -->
<div id="page">
	<div class="container">
	<div class="row">

<div class="col-sm-12">

	<div class="results">
		<div class="pull-right hidden-xs">
			<?php if (count($deliverables)>0): ?>
			<?= form_open(base_url().'download-deliverables') ?>
			    <?php 
			    	$src_1 = '';
			     	$src_1 = $ddfilters['DeliverableTypeID'];
			    ?>
				<input type="hidden" name="DeliverableTypeID" value="<?php print $src_1 ?>">
				<input type="hidden" name="BusinessUnitID" value="<?php print $ddfilters['BusinessUnitID']; ?>">
				<input type="hidden" name="FocusAreaID" value="<?php print $ddfilters['FocusAreaID']; ?>">
				<input type="hidden" name="AdminPillarID" value="<?php print $ddfilters['AdminPillarID']; ?>">
				<input type="hidden" name="RiskID" value="<?php print $ddfilters['RiskID']; ?>">
				<input type="hidden" name="PriorityID" value="<?php print $ddfilters['PriorityID']; ?>">
				<input type="hidden" name="StatusID" value="<?php print $ddfilters['StatusID']; ?>">
				<input type="hidden" name="PublishedYN" value="<?php print $ddfilters['PublishedYN']; ?>">
				<input type="hidden" name="Source" value="<?php print $ddfilters['Source']; ?>">
				<input type="hidden" name="ServiceFOR" value="<?php print $ddfilters['ServiceFOR']; ?>">
				<input type="hidden" name="ServiceBY" value="<?php print $ddfilters['ServiceBY']; ?>">

				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>
			<?php endif ?>
		</div>
		<div class="result_count">
			<p>
				<?= count($deliverables) ?>
				<?php 
					if(set_value('DeliverableTypeID') == 1) echo 'Strategic Initiative';
					elseif (set_value('DeliverableTypeID') == 2) echo 'Operational Deliverable';
					else echo 'Deliverable';
					echo (count($deliverables) == 1 ? '' : 's'); 
					echo " by Risk";
				?>
			<p>
		</div>
	</div>

	<?php if (count($deliverables)>0): ?>

	<?php 

	$deliverable_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;
	$current_risk = '';
	

	foreach ($deliverables as $deliverable): 
	
		$supporting_deliverable_count = 0;
		$related_supporting_deliverables = [];

		if(!empty($supporting_deliverables))
		{
			foreach ($supporting_deliverables as $supporting_deliverable)
			{
				// if operational
				if(set_value('DeliverableTypeID') == 2)
				{
					if($supporting_deliverable['DeliverableID'] == $deliverable['DeliverableParentID'])
					{
						$related_supporting_deliverables[$supporting_deliverable_count]['title'] = $supporting_deliverable['BusinessUnitCode'].' '.$supporting_deliverable['DeliverableCode'].' - '.$supporting_deliverable['DeliverableDescShort'];
						$related_supporting_deliverables[$supporting_deliverable_count]['id'] = $supporting_deliverable['DeliverableID'];
						$related_supporting_deliverables[$supporting_deliverable_count]['percent'] = $supporting_deliverable['ApproxComplete'];
						$related_supporting_deliverables[$supporting_deliverable_count]['status'] = $supporting_deliverable['StatusDescription'];
						$related_supporting_deliverables[$supporting_deliverable_count]['statusid'] = $supporting_deliverable['StatusID'];
						$supporting_deliverable_count++;
					}
				}
				// otherwise assume strategic
				else
				{
					if($deliverable['DeliverableID'] == $supporting_deliverable['DeliverableParentID'])
					{
						$related_supporting_deliverables[$supporting_deliverable_count]['title'] = $supporting_deliverable['BusinessUnitCode'].' '.$supporting_deliverable['DeliverableCode'].' - '.$supporting_deliverable['DeliverableDescShort'];
						$related_supporting_deliverables[$supporting_deliverable_count]['id'] = $supporting_deliverable['DeliverableID'];
						$related_supporting_deliverables[$supporting_deliverable_count]['percent'] = $supporting_deliverable['ApproxComplete'];
						$related_supporting_deliverables[$supporting_deliverable_count]['status'] = $supporting_deliverable['StatusDescription'];
						$related_supporting_deliverables[$supporting_deliverable_count]['statusid'] = $supporting_deliverable['StatusID'];
						$supporting_deliverable_count++;
					}
				}
			}
		}

	?>

	<?php
		// Risk Headings
		if(empty($current_risk) || $current_risk != $deliverable['RiskDescShort']) 
		{
			// close block
			if(!empty($current_risk) && $current_risk != $deliverable['RiskDescShort']) 
			{
				echo "</div></div>";
			}
			$current_risk = $deliverable['RiskDescShort'];
		?>
			<div class="col-sm-12">
				<div class="row">
					<a class="group_heading">
						<div class="col-sm-12" style="background:#333;">
							<h1><?= $current_risk ?></h1>
							<p><?= $deliverable['RiskDesc'] ?></p>
						</div>
					</a>
				</div>
				<div class="row group_content">
		<?php 
		}

		
	?>
	<div class="col-sm-11 col-sm-offset-1">
	<div class="row">
	<div class="block">

		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<!-- <a href="<?= base_url() ?>edit-status/<?= $deliverable['DeliverableID'] ?>" title="Update this deliverable"><h1><?= $deliverable['BusinessUnitCode'] ?> <?= $deliverable['DeliverableCode'] ?> - <?= $deliverable['DeliverableDescShort'] ?></h1></a> -->
						<?php if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>edit-deliverable/<?= $deliverable['DeliverableID'] ?>" title="Update deliverable details"><i class="fa fa-pencil"></i></a></span>
						<?php endif ?>
						<h1><a href="<?= base_url() ?>edit-status/<?= $deliverable['DeliverableID'] ?>" title="Update deliverable status"><?= $deliverable['BusinessUnitCode'] ?> <?= $deliverable['DeliverableCode'] ?> - <?= $deliverable['DeliverableDescShort'] ?></a></h1>
						<?= $deliverable['DeliverableDesc'] ?>
						<!-- <p class="text-muted"><?= $deliverable['ServiceAreaName'] ?></p>
						<p class="text-muted" style="">Target Completion Date: <?= date('M d, Y', strtotime(substr($deliverable['ApproxEndDate'],0,10))) ?></p> -->
					</div>
				</div>

				<!-- <div class="col-sm-3" style="padding-bottom:20px;">
					<?php
						$label_color = '';
						if($deliverable['StatusID'] == 1 ) $label_color = 'success';
						if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
						if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
						<p class="approximate_complete"><?= ($deliverable['ApproxComplete'] == 100 ? "Complete " : $deliverable['ApproxComplete']."%")?></p>
						<p style="font-size:1.2em;" class="label label-<?= $label_color ?>"><?= $deliverable['StatusDescription'] ?></p>
					</div>
				</div> -->

			</div>

			<!-- <div class="row">

				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $deliverable['DeliverableDesc'] ?>
					<p><strong>Outcome: &nbsp; <?= $deliverable['OutcomeName'] ?></strong></p>
					<p><?= $deliverable['OutcomeDesc'] ?></p>
					<p><strong>Risk: &nbsp; <?= $deliverable['RiskDescShort'] ?></strong></p>
					<p><?= $deliverable['RiskDesc'] ?></p>
					</div>
				</div>

			</div> -->

		</div>
		<div class="col-sm-12">
			<div class="row">
				<?php
					$label_color = '';
					if($deliverable['StatusID'] == 1 ) $label_color = 'success';
					if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
					if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
				?>
				<div class="progress_bg" title="<?= $deliverable['StatusDescription'] ?>">
					<div class="completion_label label label-<?= $label_color ?>" style="width:<?= $deliverable['ApproxComplete'] ?>%">
						<span class="label_text"><?= $deliverable['StatusDescription'] ?> - <?= ($deliverable['ApproxComplete'] == 100 ? "Completed" : $deliverable['ApproxComplete']."% complete")?><span>
					</div>
				</div>
			</div>
		</div>


		<?php if($this->session->userdata('UserAdminFlag') || $deliverable['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode') || $deliverable['DeliverableTypeID'] == 1): ?>

		<div class="col-sm-12">

			<div class="row" style="background:#f2f2f2;">

				<div class="col-sm-12">

					<div class="text_block ">
						
						<h1>Status</h1>
						<?php if(empty($deliverable['StatusUpdate'])): ?>
						<p class="muted"><em>No update at this time.</em></p>
						<?php else: ?>
						<?= $deliverable['StatusUpdate'] ?>
						<?php endif ?>

					</div>

					<div class="text_block">

						<?php if(!empty($deliverable['StatusUpdate'])): ?>
						<p class="muted text-right">
							<em>
								Updated <?= date('M d, Y', strtotime(substr($deliverable['LastModDate'],0,10))) ?> 
								<br class="visible-xs"> by <?= $deliverable['LastModBy'] ?>.
							</em>
						</p>
						<?php endif ?>
						
					</div>

				</div>

			</div>

		</div>

		<?php endif ?>


		<?php if(set_value('DeliverableTypeID') == 1 || set_value('DeliverableTypeID') == 2): ?>
		<div class="col-sm-12 supporting_deliverables">
			<p class=""><?= $supporting_deliverable_count ?> Related <?= (set_value('DeliverableTypeID') == 2) ? 'Strategic Initiative' : 'Operational Deliverable'; ?><?= (($supporting_deliverable_count==1) ? '' : 's') ?></p>
			<ul>
				<?php foreach ($related_supporting_deliverables as $related_supporting_deliverable): ?>
				<!-- <li><a href="<?= base_url() ?>edit-status/<?= $related_supporting_deliverable['id'] ?>"><?= $related_supporting_deliverable['title'] ?></a></li> -->
				<?php
					$label_color = '';
					if($related_supporting_deliverable['statusid'] == 1 ) $label_color = 'success';
					if($related_supporting_deliverable['statusid'] == 2 ) $label_color = 'warning';
					if($related_supporting_deliverable['statusid'] == 3 ) $label_color = 'danger';
				?>
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $related_supporting_deliverable['id'] ?>">
						<?= $related_supporting_deliverable['title'] ?>
					</a>
					
					 - <?= $related_supporting_deliverable['percent'] ?>% completed <?= ($related_supporting_deliverable['percent'] < 100) ? '('.$related_supporting_deliverable['status'].')' : '' ?> 
				</li>
				<?php endforeach ?>
			</ul>
		</div>
		<?php endif ?>

	</div>
	</div>
	</div>

	<?php 
		$count++;
		endforeach;
	?>

	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>

</div>

