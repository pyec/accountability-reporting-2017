<?php $this->session->set_flashdata('uri', uri_string()) 


/* ---------------------------------------------------------------------
 * 
 *		MODULE:			pg_show_MAIN_risk-results.php
 *		AUTHOR:			Stephen Chafe (Zen River Software)
 *		CREATED:		Unknown
 *
 * 		This module is modified to display the Deliverables in a special
 * 		format for the results sorted and ordered by Risks.
 * 
 * 
 * 		MODIFICATION HISTORY
 * 
 * 		20160425		PRSC		Added in new Strategic/Operational
 * 									display logic to include both.
 * 
 * ----------------------------------------------------------------------
 */



?>

<?php
	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";
?>



<!-- Page -->
<div id="page">
	<div class="container">
	<div class="row">

<div class="col-sm-12">

	<div class="results">
		<div class="pull-right hidden-xs">
			<?php if (count($deliverables)>0): ?>
			<?= form_open(base_url().'download-deliverables') ?>
			    <?php 
			    	$src_1 = '';
			     	$src_1 = $ddfilters['DeliverableTypeID'];
			    ?>
				<input type="hidden" name="DeliverableTypeID" value="<?php print $src_1 ?>">
				<input type="hidden" name="BusinessUnitID" value="<?php print $ddfilters['BusinessUnitID']; ?>">
				<input type="hidden" name="FocusAreaID" value="<?php print $ddfilters['FocusAreaID']; ?>">
				<input type="hidden" name="AdminPillarID" value="<?php print $ddfilters['AdminPillarID']; ?>">
				<input type="hidden" name="RiskID" value="<?php print $ddfilters['RiskID']; ?>">
				<input type="hidden" name="PriorityID" value="<?php print $ddfilters['PriorityID']; ?>">
				<input type="hidden" name="StatusID" value="<?php print $ddfilters['StatusID']; ?>">
				<input type="hidden" name="PublishedYN" value="<?php print $ddfilters['PublishedYN']; ?>">
				<input type="hidden" name="Source" value="<?php print $ddfilters['Source']; ?>">
				<input type="hidden" name="ServiceFOR" value="<?php print $ddfilters['ServiceFOR']; ?>">
				<input type="hidden" name="ServiceBY" value="<?php print $ddfilters['ServiceBY']; ?>">

				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>
			<?php endif ?>
		</div>
		<div class="result_count">
			<p>
				<?= count($deliverables) ?>
				<?php 
					if(set_value('DeliverableTypeID') == 1) echo 'Strategic Initiative';
					elseif (set_value('DeliverableTypeID') == 2) echo 'Operational Deliverable';
					else echo 'Deliverable';
					echo (count($deliverables) == 1 ? '' : 's'); 
					echo " by Risk";
				?>
			<p>
		</div>
	</div>

	<?php if (count($deliverables)>0): ?>

	<?php 

	$deliverable_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;
	$current_risk = '';
	

	foreach ($deliverables as $deliverable): 
	
		$supporting_deliverable_count = 0;

/*============================================================================
 * 		This ugly chunk is run with every Deliverable.  
 * 		The goal of this code is to build the list of associated Deliverables that
 * 		relates as Strategic (Parent) or Operational (child) to the current one that
 * 		is being shown. [PRSC]
 * ===========================================================================
 */

	
		// This was designed so that if the USER did not select the OPERATIONAL "AllTypes"
		// View then the assumption is that they want the STRATEGIC "AllTypes" Default View.
		//
		// THEREFORE as a default the Deliverables view should be showing the list of
		// related Deliverables below each Deliverable that is displayed even in simple
		// normal mode. [PRSC]

		// These are also used later with the display of the Related Deliverables - PRSC
		$CNT_childrenxx		= 0;
		$CNT_parentsxx		= 0;

		$CHLD_Deliverables   = [];
		$PRNT_Deliverables   = [];
		
		// These variables are used with the Related Deliverables Display at the bottom - PRSC
	    $CNT_total_parents 		= count($parent_deliverables);
 		$CNT_total_kids 		= count($supporting_deliverables);
		

		$XCOND_ShowChildDeliverables 		= FALSE;
		$XCOND_ShowParentDeliverables		= FALSE;
		
//		$XDeliveryType = $ddfilters['DeliverableTypeID']; 
//		print "DELIVERTYPE [" . $XDeliveryType . "]<br>"; 
		$XDeliveryType = 3; 
		
		if($XDeliveryType  ==  1)		$XCOND_ShowChildDeliverables  = TRUE;
		if($XDeliveryType  ==  2)		$XCOND_ShowParentDeliverables  = TRUE;
		if($XDeliveryType  ==  3)
			{
					$XCOND_ShowChildDeliverables  = TRUE;
					$XCOND_ShowParentDeliverables  = TRUE;
			}
		$XCOND_ShowChildDeliverables  = TRUE;
		$XCOND_ShowParentDeliverables  = TRUE;

		
		

		/*------------------------------------------------------------------
		 *            If required then calculate the Parent Deliverables
		 *-----------------------------------------------------------------*/

		
		
//		print "HERE30<br>";
			
		if($XCOND_ShowParentDeliverables == TRUE)
			{

			if($CNT_total_parents   > 0)
			{

//			print "HERE30B<br>";
			
			$CNT_parentsxx		= 0;
			$PRNT_Deliverables   = [];
	
			foreach ($parent_deliverables as $rpdt)
			{

//					 echo "<pre>-------------------------------[PARENT]";
//					 print_r($rpdt);
//					 echo "</pre>";
					$DPID = $deliverable['DeliverableParentID'];

					if($DPID == $rpdt['DeliverableID'])
					{
//					print "HERE30S3  [" . $DPID . "][" . $rpdt['DeliverableID'] . "] <br>";
					$PRNT_Deliverables[$CNT_parentsxx]['title'] = 
								$rpdt['BusinessUnitCode'].' '.$rpdt['DeliverableCode'].' - ' 
								.$rpdt['DeliverableDescShort'];
						$PRNT_Deliverables[$CNT_parentsxx]['id'] = $rpdt['DeliverableID'];
						$PRNT_Deliverables[$CNT_parentsxx]['percent'] = $rpdt['ApproxComplete'];
						$PRNT_Deliverables[$CNT_parentsxx]['status'] = $rpdt['StatusDescription'];
						$PRNT_Deliverables[$CNT_parentsxx]['statusid'] = $rpdt['StatusID'];
						$CNT_parentsxx++;
					}							// EO If Parent for sure	
				}								// EO for each Parent deliverable
	
			}									/* EO If Total Parents greater then zero */
			
//				echo "XSTRAT CNT [" . $CNT_parentsxx . "]<br>";		
			
			
		}										/* EO Kids should be shown at all */

	
		/*------------------------------------------------------------------
		 *            If required then calculate the Child Deliverables
		 *-----------------------------------------------------------------*/
		
		
		 if($XCOND_ShowChildDeliverables == TRUE)
			{
			$CNT_childrenxx		= 0;
			$CHLD_Deliverables   = [];
				
			if($CNT_total_kids   > 0)
			{

//			print "HERE30S<br>";
			
			foreach ($supporting_deliverables as $rcdt)
			{


//					 echo "<pre>-------------------------------[CHILD]";
//					 print_r($rcdt);
//					 echo "</pre>";

					$DCID = $deliverable['DeliverableID'];
//					print "HERE30-XX  [" . $DCID . "][" . $rcdt['DeliverableParentID'] . "] <br>";
					
					if($rcdt['DeliverableParentID'] == $DCID)
						{
//						print "HERE30-CH  [" . $DCID . "][" . $rcdt['DeliverableParentID'] . "] <br>";
						$CHLD_Deliverables[$CNT_childrenxx]['title'] = 
								$rcdt['BusinessUnitCode'].' '.$rcdt['DeliverableCode'].' - ' 
								.$rcdt['DeliverableDescShort'];
						$CHLD_Deliverables[$CNT_childrenxx]['id'] = $rcdt['DeliverableID'];
						$CHLD_Deliverables[$CNT_childrenxx]['percent'] = $rcdt['ApproxComplete'];
						$CHLD_Deliverables[$CNT_childrenxx]['status'] = $rcdt['StatusDescription'];
						$CHLD_Deliverables[$CNT_childrenxx]['statusid'] = $rcdt['StatusID'];
						$CNT_childrenxx++;
//						echo "FCN-CHILD CNT [" . $CNT_childrenxx . "]<br>";		
						
					}								// EO If we should show

				}									// EO for each kid deliverable
	

			}									/* EO If Total Kids greater then zero */

//			echo "XOPER CNT [" . $CNT_childrenxx . "]<br>";		
			
		}										/* EO Parents should be shown at all */
								
		
	/*-------------------------------------------------( EO Ugly Header Block )	*/
	
	?>

	<?php
		// Risk Headings
		if(empty($current_risk) || $current_risk != $deliverable['RiskDescShort']) 
		{
			// close block
			if(!empty($current_risk) && $current_risk != $deliverable['RiskDescShort']) 
			{
				echo "</div></div>";
			}
			$current_risk = $deliverable['RiskDescShort'];
		?>
			<div class="col-sm-12">
				<div class="row">
					<a class="group_heading">
						<div class="col-sm-12" style="background:#333;">
							<h1><?= $current_risk ?></h1>
							<p><?= $deliverable['RiskDesc'] ?></p>
						</div>
					</a>
				</div>
				<div class="row group_content">
		<?php 
		}

		
	?>
	<div class="col-sm-11 col-sm-offset-1">
	<div class="row">
	<div class="block">

		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<!-- <a href="<?= base_url() ?>edit-status/<?= $deliverable['DeliverableID'] ?>" title="Update this deliverable"><h1><?= $deliverable['BusinessUnitCode'] ?> <?= $deliverable['DeliverableCode'] ?> - <?= $deliverable['DeliverableDescShort'] ?></h1></a> -->
						<?php if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>edit-deliverable/<?= $deliverable['DeliverableID'] ?>" title="Update deliverable details"><i class="fa fa-pencil"></i></a></span>
						<?php endif ?>
						<h1><a href="<?= base_url() ?>edit-status/<?= $deliverable['DeliverableID'] ?>" title="Update deliverable status"><?= $deliverable['BusinessUnitCode'] ?> <?= $deliverable['DeliverableCode'] ?> - <?= $deliverable['DeliverableDescShort'] ?></a></h1>
						<?= $deliverable['DeliverableDesc'] ?>
						<!-- <p class="text-muted"><?= $deliverable['ServiceAreaName'] ?></p>
						<p class="text-muted" style="">Target Completion Date: <?= date('M d, Y', strtotime(substr($deliverable['ApproxEndDate'],0,10))) ?></p> -->
					</div>
				</div>

				<!-- <div class="col-sm-3" style="padding-bottom:20px;">
					<?php
						$label_color = '';
						if($deliverable['StatusID'] == 1 ) $label_color = 'success';
						if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
						if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
						<p class="approximate_complete"><?= ($deliverable['ApproxComplete'] == 100 ? "Complete " : $deliverable['ApproxComplete']."%")?></p>
						<p style="font-size:1.2em;" class="label label-<?= $label_color ?>"><?= $deliverable['StatusDescription'] ?></p>
					</div>
				</div> -->

			</div>

			<!-- <div class="row">

				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $deliverable['DeliverableDesc'] ?>
					<p><strong>Outcome: &nbsp; <?= $deliverable['OutcomeName'] ?></strong></p>
					<p><?= $deliverable['OutcomeDesc'] ?></p>
					<p><strong>Risk: &nbsp; <?= $deliverable['RiskDescShort'] ?></strong></p>
					<p><?= $deliverable['RiskDesc'] ?></p>
					</div>
				</div>

			</div> -->

		</div>
		<div class="col-sm-12">
			<div class="row">
				<?php
					$label_color = '';
					if($deliverable['StatusID'] == 1 ) $label_color = 'success';
					if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
					if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
				?>
				<div class="progress_bg" title="<?= $deliverable['StatusDescription'] ?>">
					<div class="completion_label label label-<?= $label_color ?>" style="width:<?= $deliverable['ApproxComplete'] ?>%">
						<span class="label_text"><?= $deliverable['StatusDescription'] ?> - <?= ($deliverable['ApproxComplete'] == 100 ? "Completed" : $deliverable['ApproxComplete']."% complete")?><span>
					</div>
				</div>
			</div>
		</div>


		<?php if($this->session->userdata('UserAdminFlag') || $deliverable['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode') || $deliverable['DeliverableTypeID'] == 1): ?>

		<div class="col-sm-12">

			<div class="row" style="background:#f2f2f2;">

				<div class="col-sm-12">

					<div class="text_block ">
						
						<h1>Status</h1>
						<?php if(empty($deliverable['StatusUpdate'])): ?>
						<p class="muted"><em>No update at this time.</em></p>
						<?php else: ?>
						<?= $deliverable['StatusUpdate'] ?>
						<?php endif ?>

					</div>

					<div class="text_block">

						<?php if(!empty($deliverable['StatusUpdate'])): ?>
						<p class="muted text-right">
							<em>
								Updated <?= date('M d, Y', strtotime(substr($deliverable['LastModDate'],0,10))) ?> 
								<br class="visible-xs"> by <?= $deliverable['LastModBy'] ?>.
							</em>
						</p>
						<?php endif ?>
						
					</div>

				</div>

			</div>

		</div>

		<?php endif ?>


 <!-- ------------------------- START -->      
        
        
 <?php
 /*---------------------------------------------------------------------
  * 
  * 	This is used with the Strategic Listings to build a list of 
  * 	links that allows users to update the status information on 
  * 	each of the related Deliverables to this parent Strategic one.
  * 
  * 	NOTE:		Delivery Types are
  * 	1 - Strategic (Show only related children )
  * 	2 - Children (Show only related parents)
  * 	3 - All
  * -----------------------------------------------------20160222 PRSC
  */
?>
		<?php 
		$XCOND_ShowRelatedParents 		= FALSE;
		$XCOND_ShowRelatedChildren		= FALSE;
//		$XDeliveryType					= $ddfilters['DeliverableTypeID'];
		$XDeliveryType					= 3;					// Over ride in this screen to all - PRSC
		
		// Check if Show the Strategic Values
		if($XDeliveryType == 2 && $CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedParents 	= TRUE; }
				
		// Check if Show the Operational Values		
		if($XDeliveryType == 1 && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedChildren	= TRUE; }
				
		// Check if base view requires showing both		
		if($XDeliveryType == 3 && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedChildren 	= TRUE; }
		if($XDeliveryType == 3 && $CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedParents	= TRUE; }
		// Check if base view requires showing both		
		if($XDeliveryType == '' && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedChildren 	= TRUE; }
		if($XDeliveryType == '' && $CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedParents	= TRUE; }
				
//		print "TYPE [" . $XDeliveryType . "]<br>";		
//		print "CHILD CNT [" . $CNT_childrenxx . "]<br>";		
//		print "STRAT CNT [" . $CNT_parentsxx . "]<br>";		
		
//		$XCOND_ShowRelatedParents 		= TRUE;
//		$XCOND_ShowRelatedChildren		= TRUE;
		
		?>
	
					
		<?php		
		/*-----------------------------------------------
		 * 		Strategics Header
		 * -----------------------------------------------
		 */
		?>
		<?php if($XCOND_ShowRelatedParents == TRUE ): ?>		
		<div class="row">	
		<div class="col-sm-12 supporting_deliverables">
			<p class="">

			<?php 
				echo 'Related Strategic Initiatives';
			?>

			</p>
		<ul>
		<?php
		/* -------------------------------------------------------------
		 * 			Check if Parent Deliverables to show
		 * -------------------------------------------------------------
		 */
		
		foreach ($PRNT_Deliverables as $rcdt): ?>
				<?php
					$label_color = '';
					if($rcdt['statusid'] == 1 ) $label_color = 'success';
					if($rcdt['statusid'] == 2 ) $label_color = 'warning';
					if($rcdt['statusid'] == 3 ) $label_color = 'danger';
					if($deliverable['StatusID'] > 3 ) $label_color = 'info';
					?>
					
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $rcdt['id'] ?>">
						<?= $rcdt['title'] ?>
					</a>
					
					 <!-- - <?= $rcdt['percent'] ?>% completed 
					    <?= ($rcdt['percent'] < 100) ? '('.$rcdt['status'].
					            ')' : '' ?>  -->
					 - <span title="<?= $rcdt['status'] ?> ">
					          <?= $rcdt['percent'] ?>% completed</span>
				</li>
				<?php endforeach ?>

			</ul>
		</div>
		</div>
		
		<?php endif ?>
		
		
		
		<!-- EO Display ALL Related Supporting Deliverables - Strategic or Operational -->
	    
	
	
		<?php		
		/*-----------------------------------------------
		 * 		Operationals(Child) Header
		 * -----------------------------------------------
		 */
		if($XCOND_ShowRelatedChildren == TRUE):
		?>		
		<div class="row">	
		<div class="col-sm-12 supporting_deliverables">
			<p class="">

			<?php 
				echo 'Related Operational Initiatives';
			?>

			</p>
		<ul>
		<?php
		/* -------------------------------------------------------------
		 * 			Check if Child Deliverables to show
		 * -------------------------------------------------------------
		 */
		
		foreach ($CHLD_Deliverables as $rcdt): ?>
				<?php
					$label_color = '';
					if($rcdt['statusid'] == 1 ) $label_color = 'success';
					if($rcdt['statusid'] == 2 ) $label_color = 'warning';
					if($rcdt['statusid'] == 3 ) $label_color = 'danger';
					if($deliverable['StatusID'] > 3 ) $label_color = 'info';
					?>
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $rcdt['id'] ?>">
						<?= $rcdt['title'] ?>
					</a>
					
					 <!-- - <?= $rcdt['percent'] ?>% completed 
					    <?= ($rcdt['percent'] < 100) ? '('.$rcdt['status'].
					            ')' : '' ?>  -->
					 - <span title="<?= $rcdt['status'] ?> ">
					          <?= $rcdt['percent'] ?>% completed</span>
				</li>
				<?php endforeach ?>
          
			</ul>
		</div>
		</div>
		<?php endif ?><!-- EO Display ALL Related Operationals -->
 
   <!--  EO Show Related Deliverables -->

	</div>
	</div>
	</div>

	<?php 
		$count++;
		endforeach;
	?>

	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>

</div>

