		/*
		 * 		This ugly chunk is run with every Deliverable.  
		 * 		The goal of this code is to build the list of associated Deliverables that
		 * 		relates as Strategic (Parent) or Operational (child) to the current one that
		 * 		is being shown. [PRSC]
		 */

	
		// This was designed so that if the USER did not select the OPERATIONAL "AllTypes"
		// View then the assumption is that they want the STRATEGIC "AllTypes" Default View.
		//
		// THEREFORE as a default the Deliverables view should be showing the list of
		// related Deliverables below each Deliverable that is displayed even in simple
		// normal mode. [PRSC]


		$XCOND_ShowChildDeliverables 		= FALSE;
		$XCOND_ShowParentDeliverables		= FALSE;
		
		if($filter[DeliverableTypeID   ==  1)		$XCOND_ShowChildDeliverables  = TRUE;
		if($filter[DeliverableTypeID   ==  2)		$XCOND_ShowParentDeliverables  = TRUE;
		if($filter[DeliverableTypeID   ==  3)
			{
					$XCOND_ShowChildDeliverables  = TRUE;
					$XCOND_ShowParentDeliverables  = TRUE;
			}
		if(!empty($supporting_deliverables))
		{

			$CNT_childrenxx		= 0;
			$CNT_parentxxxx		= 0;

			foreach ($supporting_deliverables as $rcdt)
			{

				/*------------------------------------------------------------------
				 *            If required then calculate the Child Deliverables
				 *-----------------------------------------------------------------*/

				if($XCOND_ShowChildDeliverables)
				{
//					 echo "<pre>-------------------------------[CHILD]";
//					 print_r($rcdt);
//					 echo "</pre>";

					$DCID = $deliverable['DeliverableParentID];
	
					if($DCID == $rcdt['DeliverableID'])
					{
						$CHLD_Deliverables[$CNT_childrenxx]['title'] = 
								$rcdt['BusinessUnitCode'].' '.$rcdt['DeliverableCode'].' - ' 
								.$rcdt['DeliverableDescShort'];
						$CHLD_Deliverables[$CNT_childrenxx]['id'] = $rcdt['DeliverableID'];
						$CHLD_Deliverables[$CNT_childrenxx]['percent'] = $rcdt['ApproxComplete'];
						$CHLD_Deliverables[$CNT_childrenxx]['status'] = $rcdt['StatusDescription'];
						$CHLD_Deliverables[$CNT_childrenxx]['statusid'] = $rcdt['StatusID'];
						$CNT_childrenxx++;
					}

				}
	

				/*------------------------------------------------------------------
				 *            If required then calculate the Parent Deliverables
				 *-----------------------------------------------------------------*/

				if($XCOND_ShowParentDeliverables)
				{
//					 echo "<pre>-------------------------------[PARENT]";
//					 print_r($rcdt);
//					 echo "</pre>";

					$DPID = $deliverable['DeliverableParentID];
	
					if($DPID == $rcdt['DeliverableID'])
					{
						$PRNT_Deliverables[$CNT_parentsxx]['title'] = 
								$rcdt['BusinessUnitCode'].' '.$rcdt['DeliverableCode'].' - ' 
								.$rcdt['DeliverableDescShort'];
						$PRNT_Deliverables[$CNT_parentsxx]['id'] = $rcdt['DeliverableID'];
						$PRNT_Deliverables[$CNT_parentsxx]['percent'] = $rcdt['ApproxComplete'];
						$PRNT_Deliverables[$CNT_parentsxx]['status'] = $rcdt['StatusDescription'];
						$PRNT_Deliverables[$CNT_parentsxx]['statusid'] = $rcdt['StatusID'];
						$CNT_parentsxx++;
					}
				}


			}									/* EO For Each Supporting Deliverable */
		}										/* EO Supporting Deliverable Array Defined */
