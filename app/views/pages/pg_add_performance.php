<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($performance);
// echo "</pre>";

//	 echo "<pre>";
//	 print_r($business_units);
//	 echo "</pre>";

?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($performance)) 
				{
				echo form_open('create-performance');
				}
			else
				{
				echo form_open('update-performance');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Performance ID;
			if(!empty($performance)) 
			         echo form_hidden('PerformanceID',$performance['PerformanceID']);
			?>
 
  				<h1>
 				 <?= (!empty($performance)) ? 'Edit' : 'Create' ?> KPI</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
	 
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Performance ShortName Desc
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="PerformanceShortNM">KPI Short Name (250-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="PerformanceShortNM" class="form-control"
						 value="<?= set_value('PerformanceShortNM', (!empty($performance) ?
						  $performance['PerformanceShortNM'] : ''))
						 ?>" maxlength="250">
					</div>
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Performance Short CD
		 * -----------------------------------------------------PRSC 201602
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="PerformanceCD">KPI Code (20-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="PerformanceCD" class="form-control"
						 value="<?= set_value('PerformanceCD', (!empty($performance) ?
						  $performance['PerformanceCD'] : ''))
						 ?>" maxlength="20">
					</div>
				</div>





		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Service 
		 * -----------------------------------------------------PRSC 201601
		 */	?>
	
			<div class="row">
			<div class="col-sm-4">
				<label for="Service">Service</label>
			</div>
			<div class="col-sm-8">			
				<select class="form-control" id="ServiceID" name="ServiceID">

				    <?php 
				    $real = '';
				    
				    if(!empty($performance))
				    {
				    	foreach ($services as $rt) 
				    	{
						if($rt['ServiceID'] == $performance['ServiceID'])
							$real = $rt['ServiceShortNM'];
						}
				    }	
						
				    if($real)
				    	echo '<option value="' . $performance['ServiceID'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Select Associated Service</option>';
					?>	
					
					 		 
					 <?php 

					 /* --------------------------------------------------------------
					  *		This code builds the drop down Services Box for both the
					  *		Create Screen and the Edit Screen 
					  * ------------------------------------------------20160217 PRSC
					  */
					 	 
					if(!empty($services))
				    {
					foreach ($services as $dt)
					{
				    	
					//  Lookup the actual Service Area Code to show to user PRSC					
					$vdt = $dt['ServiceAreaID'];
					foreach ($serviceareas as $st)
					{
						if($vdt == $st['ServiceAreaID'])
						{
							$saCD = $st['ServiceAreaName'];
						}
					}
						 		 
					//  Lookup the actual Business Unit Code to show to user PRSC					
					$vdt = $dt['BusinessUnitID'];
					foreach ($business_units as $st)
					{
					if($vdt == $st['BusinessUnitID'])
						{
							$buCD = $st['BusinessUnitCode'];
						}
					}
					 		 
				//	 print "(" . $buID . "-" . $buCD . ")(" . $saID . "-" . $saCD . ') >' . $dt['ServiceShortNM'];
					$vl = $dt['ServiceID'];
					print '<option value="' .  $vl  . '">' . "( " . $buCD . ' , ' . $saCD . ' ) ' . $dt['ServiceShortNM'] . '</option>';;
					 
					}												// EO For Each Service
				    }												// EO If Services defined
			   else 
				    {								/* Using just the Create Screen */

				 // In a simple Create just show all the Services */   	
				 foreach ($services as $dt)
					{ 

				/*	Old base method for research purposes
				 * 	<option name="<?= $dt['ServiceID'] ?>"
						 value="<?= $dt['ServiceID'] ?>"
					 		 <?= set_select('ServiceID', $dt['ServiceID'],
					 		 ((!empty($service) && $performance['ServiceID']
					 		 	 == $dt['ServiceID'])? TRUE : '')) ?>>
					}				    	
				*/
				    	
					//  Lookup the actual Service Area Code to show to user PRSC					
					$vdt = $dt['ServiceAreaID'];
					foreach ($serviceareas as $st)
					{
						if($vdt == $st['ServiceAreaID'])
						{
							$saCD = $st['ServiceAreaName'];
						}
					}
						 		 
					//  Lookup the actual Business Unit Code to show to user PRSC					
					$vdt = $dt['BusinessUnitID'];
					foreach ($business_units as $st)
					{
					if($vdt == $st['BusinessUnitID'])
						{
							$buCD = $st['BusinessUnitCode'];
						}
					}
							 
					// Add each option to the drop down box
					$vl = $dt['ServiceID'];
//					print '<option value="' .  $vl  . '">' . "(" . $buCD . ")(" . $saCD . ') >' . $dt['ServiceShortNM'] . '</option>';;
				print '<option value="' .  $vl  . '">' . "( " . $buCD . ' , ' . $saCD . ' ) ' . $dt['ServiceShortNM'] . '</option>';;
											
				    	
				    }
				 }		 
					 		 
				?>

					
				</select>
			</div>
		</div>
	

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Performance Description (FULL)
		 * -----------------------------------------------------PRSC 201601
		 */	?>

		<label for="PerformanceDesc">KPI Description (1500 digits)</label>
		<textarea name="PerformanceDesc" rows="15"><?= set_value('PerformanceDesc',
		 (!empty($performance) ? $performance['PerformanceDesc'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'PerformanceDesc', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($performance))
					 ? 'Update' : 'Create' ?> KPI Performance</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>

