<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($adminpriority);
// echo "</pre>";

?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($adminpriority)) 
				{
				echo form_open('create-adminpriority');
				}
			else
				{
				echo form_open('update-adminpriority');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the AdminPriority ID;
			if(!empty($adminpriority)) 
			         echo form_hidden('AdminPriorityID',$adminpriority['AdminPriorityID']);
			?>
 
  				<h1>
 				 <?= (!empty($adminpriority)) ? 'Edit' : 'Create' ?> Admin Priority</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M AdminPriority ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="AdminPriorityName">Admin Priority Name (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="AdminPriorityName" class="form-control"
						 value="<?= set_value('AdminPriorityName', (!empty($adminpriority) ?
						  $adminpriority['AdminPriorityName'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>



		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Admin Pillar
		 * -----------------------------------------------------PRSC 201601
		 */	?>
	
			<div class="row">
			<div class="col-sm-4">
				<label for="AdminPillar">AdminPillar</label>
			</div>
			<div class="col-sm-8">			
				<select class="form-control" id="AdminPillarID" name="AdminPillarID">

				    <?php 
				    $real = '';
				    
				    if(!empty($adminpriority))
				    {
				    	foreach ($adminpillars as $rt) 
				    	{
						if($rt['AdminPillarID'] == $adminpriority['AdminPillarID'])
							$real = $rt['AdminPillarName'];
						}
				    }	
						
				    if($real)
				    	echo '<option value="' . $adminpriority['AdminPillarID'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Select Associated Admin Pillar</option>';
					?>	
					
					 		 
					 <?php 

					 /* --------------------------------------------------------------
					  *		This code builds the drop down AdminPillars Box for both the
					  *		Create Screen and the Edit Screen 
					  * ------------------------------------------------20160217 PRSC
					  */
					 	 
					if(!empty($adminpillars))
				    {
				 // In a simple Create just show all the AdminPillars */   	
					 foreach ($adminpillars as $dt)
						{ 
						// Add each option to the drop down box
						$vl = $dt['AdminPillarID'];
						print '<option value="' .  $vl  . '">' . " " . $dt['AdminPillarName'] . '</option>';;
					    }
				 	}		 
					 		 
				?>

					
				</select>
			</div>
		</div>



		

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M AdminPriority Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<label for="AdminPriorityDesc">Admin Priority Description (300 digits)</label>
		<textarea name="AdminPriorityDesc" rows="15"><?= set_value('AdminPriorityDesc',
		 (!empty($adminpriority) ? $adminpriority['AdminPriorityDesc'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'AdminPriorityDesc', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($adminpriority))
					 ? 'Update' : 'Create' ?> Admin Priority</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>



