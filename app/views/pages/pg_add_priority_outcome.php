<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>



<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			if(empty($priorityoutcome)) 
				{
				echo form_open('create-priorityoutcome');
				}
			else
				{
				echo form_open('update-priorityoutcome');
				}
				echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Priority Outcome ID;
			if(!empty($priorityoutcome)) 
			         echo form_hidden('OutcomeID',$priorityoutcome['OutcomeID']);
			?>
 
     			<h1>
 				 <?= (!empty($priorityoutcome)) ? 'Edit' : 'Create' ?> Priority Outcome</button>
                </h1>              
                                
              	<?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
	 
 
	


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Outcome ShortName Desc
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="OutcomeName">Outcome Short Name (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="OutcomeName" class="form-control"
						 value="<?= set_value('OutcomeName', (!empty($priorityoutcome) ?
						  $priorityoutcome['OutcomeName'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Focus Area 
		 * -----------------------------------------------------PRSC 201601
		 */	?>
	
			<div class="row">
			<div class="col-sm-4">
				<label for="FocusAreaID">Focus Area </label>
			</div>
			<div class="col-sm-8">			
				<select class="form-control" id="FocusAreaID" name="FocusAreaID">

				    <?php 
				    $real = '';
					foreach ($focusareas as $rt) 
						{
						if($rt['FocusAreaID'] == $priorityoutcome['FocusAreaID'])
							$real = $rt['FocusAreaName'];
						}
						
				    if($real)
				    	echo '<option value="' . $priorityoutcome['FocusAreaID'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Select Associated Focus Area</option>';
					?>	
					
					<?php foreach ($focusareas as $dt): ?>
					<option name="<?= $dt['FocusAreaID'] ?>"
						 value="<?= $dt['FocusAreaID'] ?>"
					 		 <?= set_select('FocusAreaID', $dt['FocusAreaID'],
					 		 ((!empty($priorityoutcome) && 
					 		 		$priorityoutcome['FocusAreaID'] == $dt['FocusAreaID'])? TRUE : '')) ?>>
					 		 <?= $dt['FocusAreaName'] ?></option>
					<?php endforeach ?>

					
				</select>
			</div>
		</div>
		

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Outcome Description (FULL)
		 * -----------------------------------------------------PRSC 201601
		 */	?>

		<label for="OutcomeDesc">Priority Outcome Description (3000 digits)</label>
		<textarea name="OutcomeDesc" rows="15"><?= set_value('OutcomeDesc',
		 (!empty($priorityoutcome) ? $priorityoutcome['OutcomeDesc'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'OutcomeDesc', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($priorityoutcome))
					 ? 'Update' : 'Create' ?> Priority Outcome</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>

