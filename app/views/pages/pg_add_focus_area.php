<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>



<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			if(empty($focusarea)) 
				{
				echo form_open('create-focusarea');
				}
			else
				{
				echo form_open('update-focusarea');
				}
				echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Priority Outcome ID;
			if(!empty($focusarea)) 
			         echo form_hidden('FocusAreaID',$focusarea['FocusAreaID']);
			?>
 
     			<h1>
 				 <?= (!empty($focusarea)) ? 'Edit' : 'Create' ?> Focus Area</button>
                </h1>              
                                
              	<?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
	 

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Focus Area Code
		 * 
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="FocusAreaCode">Focus Area Code (20-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="FocusAreaCode" class="form-control"
						 value="<?= set_value('FocusAreaCode', (!empty($focusarea) ?
						  $focusarea['FocusAreaCode'] : ''))
						 ?>" maxlength="20">
					</div>
				</div>
 
	


	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Outcome Name Desc
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="FocusAreaName">Focus Area Name (60-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="FocusAreaName" class="form-control"
						 value="<?= set_value('FocusAreaName', (!empty($focusarea) ?
						  $focusarea['FocusAreaName'] : ''))
						 ?>" maxlength="60">
					</div>
				</div>

<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Focus Area Type
		 * -----------------------------------------------------PRSC 201601
		 */
	?>
			<div class="row">
			<div class="col-sm-4">
				<label for="FocusAreaTypeID">FocusAreaTypeID</label>
			</div>
			<div class="col-sm-8">			
				<select class="form-control" id="FocusAreaTypeID" name="FocusAreaTypeID">

				    <?php 
				    $real = '';
					foreach ($focusareatypes as $rt) 
						{
						if($rt['FocusAreaTypeID'] == $focusarea['FocusAreaTypeID'])
							$real = $rt['FocusAreaTypeName'];
						}
						
				    if($real)
				    	echo '<option value="' . $focusarea['FocusAreaTypeID'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Select Focus Area Type</option>';
					?>	
						
						
					<?php foreach ($focusareatypes as $dt): ?>
					<option name="<?= $dt['FocusAreaTypeID'] ?>"
						 value="<?= $dt['FocusAreaTypeID'] ?>"
					 		 <?= set_select('FocusAreaTypeID', $dt['FocusAreaTypeID'],
					 		 ((!empty($focusarea) && $focusarea['FocusAreaTypeID'] == $dt['FocusAreaTypeID'])? TRUE : '')) ?>>
					 		 <?= $dt['FocusAreaTypeName'] ?></option>
					<?php endforeach ?>
					
		
				</select>
			</div>
		</div>
	
	
	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Outcome Description (FULL)
		 * -----------------------------------------------------PRSC 201601
		 */	?>

		<label for="FocusAreaDesc">Focus Area Description (3000 digits)</label>
		<textarea name="FocusAreaDesc" rows="15"><?= set_value('FocusAreaDesc',
		 (!empty($focusarea) ? $focusarea['FocusAreaDesc'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'FocusAreaDesc', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($focusarea))
					 ? 'Update' : 'Create' ?> Focus Area</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>


