	
/*           
============================================================================
 
 		FUNCTION:	Sources
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-source" view file.
 		This will allow users to create and enter a new ServiceArea
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_source($id = NULL)
	{	
		$data['data_state'] = "";
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		
	// boxes in the "add-service" web page.
		if($id)	$data['source'] = $this->main_model->md_get_source($id);
		
		$data['page'] = "pages/pg_add_source";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
	
	
	public function show_sources($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		// This shows a list of the sources for users to reference
		$data['sources'] 			= $this->main_model->md_get_sources();
		$data['page']				= "pages/pg_show_sources";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	

public function update_source()
	{
			
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data = $this->input->post(NULL,TRUE);
		$data['data_state'] = "";
		
		$id = $this->input->post('SourceID',TRUE);

		if($id > 0)
		{
			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_source($data))	
			{
					$data['data_state'] = "Source Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Source Record";
			}
			
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_sourcek" web page - PRSC
		
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['source'] 		= $this->main_model->md_get_source($id);
			$data['page'] = "pages/pg_add_source";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		}
	}
	
	public function create_source()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data = $this->input->post(NULL,TRUE);
		$data['data_state'] = "";
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */
			
		$fresp = 1;
		
		if($fresp > 0)
		{
			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_source($data))	
			{
					$data['data_state'] = "Source Record Created Successfully";
					$this->session->set_flashdata('success', 'Source was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Source Record.');
					$data['data_state'] = "Error Creatng Source Record";
			}
		
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_source" web page.
		
			
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['source'] 		= $this->main_model->md_get_source($id);
			$data['page'] = "pages/pg_add_source";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		}	
	else 
	{
//		print "Error in Key <br>";
	}	
		
		
}
		
		
//------------------------------------------------( EO Function Block )


		
