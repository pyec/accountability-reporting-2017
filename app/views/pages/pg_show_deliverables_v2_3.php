<?php $this->session->set_flashdata('uri', uri_string()) ?>

<?php
//	 echo "<pre>";
//	 print_r($business_units);
//	 echo "</pre>";
?>

<br><br><br><br>
<?php
//	 echo "<pre>";
//	 print_r($ddfilters);
//	 echo "</pre>";
?>


<!-- Page -->
<div id="page">
	<div class="container">
	<div class="row">

<div class="col-sm-12">

	<div class="results">
		<div class="pull-right hidden-xs">
			<?php if (count($deliverables)>0): ?>
			<?= form_open(base_url().'download-deliverables') ?>
			    <?php 
			    	$src_1 = '';
			     	$src_1 = $ddfilters['DeliverableTypeID'];
			    ?>
				<input type="hidden" name="DeliverableTypeID" value="<?php print $src_1 ?>">
				<input type="hidden" name="BusinessUnitID" value="<?php print $ddfilters['BusinessUnitID']; ?>">
				<input type="hidden" name="FocusAreaID" value="<?php print $ddfilters['FocusAreaID']; ?>">
				<input type="hidden" name="AdminPillarID" value="<?php print $ddfilters['AdminPillarID']; ?>">
				<input type="hidden" name="RiskID" value="<?php print $ddfilters['RiskID']; ?>">
				<input type="hidden" name="PriorityID" value="<?php print $ddfilters['PriorityID']; ?>">
				<input type="hidden" name="StatusID" value="<?php print $ddfilters['StatusID']; ?>">
				<input type="hidden" name="PublishedYN" value="<?php print $ddfilters['PublishedYN']; ?>">
				<input type="hidden" name="Source" value="<?php print $ddfilters['Source']; ?>">
				<input type="hidden" name="ServiceFOR" value="<?php print $ddfilters['ServiceFOR']; ?>">
				<input type="hidden" name="ServiceBY" value="<?php print $ddfilters['ServiceBY']; ?>">

				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>
			<?php endif ?>
		</div>
		<div class="result_count">
			<p>
				<?= count($deliverables) ?>
				<?php 
					if(set_value('DeliverableTypeID') == 1) echo 'Strategic Initiative';
					elseif (set_value('DeliverableTypeID') == 2) echo 'Operational Deliverable';
					else echo 'Deliverable';
					echo (count($deliverables) == 1 ? '' : 's'); 
				?>
			<p>
		</div>
	</div>

	<?php if (count($deliverables)>0): ?>

	<?php 

	$deliverable_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;

	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";

//	 echo "<pre>";
//	 print_r($services);
//	 echo "</pre>";
	?>
	
	<?php 
	foreach ($deliverables as $deliverable): 
	
		$supporting_deliverable_count = 0;
		$related_supporting_deliverables = [];

		if(!empty($supporting_deliverables))
		{

			foreach ($supporting_deliverables as $supporting_deliverable)
			{
				// if operational
				// if(set_value('DeliverableTypeID') == 2)
				if($deliverable['DeliverableTypeID'] == 2)
				{
//					 echo "<pre>-------------------------------[SUPDEL]";
//					 print_r($supporting_deliverable);
//					 echo "</pre>";

					if($deliverable['DeliverableParentID'] == $supporting_deliverable['DeliverableID'])
					{
						$related_supporting_deliverables[$supporting_deliverable_count]['title'] = 
								$supporting_deliverable['BusinessUnitCode'].' '.$supporting_deliverable['DeliverableCode'].' - ' 
								.$supporting_deliverable['DeliverableDescShort'];
						$related_supporting_deliverables[$supporting_deliverable_count]['id'] = $supporting_deliverable['DeliverableID'];
						$related_supporting_deliverables[$supporting_deliverable_count]['percent'] = $supporting_deliverable['ApproxComplete'];
						$related_supporting_deliverables[$supporting_deliverable_count]['status'] = $supporting_deliverable['StatusDescription'];
						$related_supporting_deliverables[$supporting_deliverable_count]['statusid'] = $supporting_deliverable['StatusID'];
						$supporting_deliverable_count++;
					}

				}
				// This was designed so that if the USER did not select the OPERATIONAL "AllTypes" View then
				// the assumption is that they want the STRATEGIC "AllTypes" Default View.
				//
				// THEREFORE as a default the Deliverables view should be showing the list of related Deliverables
				//  below each Deliverable that is displayed even in simple normal mode.

				else
				{
//					 echo "<pre>-------------------------------[SUPDEL- B]";
//					 print_r($supporting_deliverable);
//					 echo "</pre>";
					 
					 
					if($deliverable['DeliverableID'] == $supporting_deliverable['DeliverableParentID'])
					{
						$related_supporting_deliverables[$supporting_deliverable_count]['title'] = 
								$supporting_deliverable['BusinessUnitCode'].' '.$supporting_deliverable['DeliverableCode'].' - ' 
								.$supporting_deliverable['DeliverableDescShort'];
						$related_supporting_deliverables[$supporting_deliverable_count]['id'] = $supporting_deliverable['DeliverableID'];
						$related_supporting_deliverables[$supporting_deliverable_count]['percent'] = $supporting_deliverable['ApproxComplete'];
						$related_supporting_deliverables[$supporting_deliverable_count]['status'] = $supporting_deliverable['StatusDescription'];
						$related_supporting_deliverables[$supporting_deliverable_count]['statusid'] = $supporting_deliverable['StatusID'];
						$supporting_deliverable_count++;
					}
				}

			}
		
		}
//		 echo "<pre>";
//		 print_r($related_supporting_deliverables);
//		 echo "</pre>";
	?>

	<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right">
							<a href="<?= base_url() ?>edit-deliverable/<?= $deliverable['DeliverableID'] ?>"
							 title="Edit deliverable details"><i class="fa fa-pencil"></i></a></span>
						<?php endif ?>
						
				<?php
			   /*
    	 		* ----------------------------------------------------------------
	 			*	This is the Link embedded at a title in the show deliverables
	 			*	screen.  Clicking on this goes to the Edit Status Screen
	 			*	which is now separate from the regular delivable update.
	 			* -----------------------------------------------------PRSC 201601
	 			*/ ?>  
						
						
				<h1>
					<a href="<?= base_url() ?>edit-status/<?= $deliverable['DeliverableID'] ?>
							" title="Edit deliverable status">
							<?= $deliverable['BusinessUnitCode'] ?>
							<?= $deliverable['DeliverableCode'] ?> -
							<?= $deliverable['DeliverableDescShort'] ?>
					</a>
				</h1>
							<p class="text-muted"><?= $deliverable['ServiceAreaName'] ?></p>
						<!-- <p class="text-muted" style="">Target Completion Date: <?= date('M d, Y', strtotime(substr($deliverable['ApproxEndDate'],0,10))) ?></p> -->
					</div>
				</div>

				<!-- <div class="col-xs-5">
					<?php
						$label_color = '';
						if($deliverable['StatusID'] == 1 ) $label_color = 'success';
						if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
						if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
						if($deliverable['StatusID'] > 3 ) $label_color = 'info';
						?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
						<p class="approximate_complete"><?= ($deliverable['ApproxComplete'] == 100 ? "Complete " : $deliverable['ApproxComplete']."%")?></p>
						<p style="font-size:1.2em;" class="label label-<?= $label_color ?>"><?= $deliverable['StatusDescription'] ?></p>
					</div>
				</div> -->

			</div>

			<div class="row">

				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
						<?= $deliverable['DeliverableDesc'] ?>
						<div class="row">
							<div class="col-sm-4">
								<p><strong>Outcome: &nbsp; <?= $deliverable['OutcomeName'] ?> <!-- (<?= $deliverable['FocusAreaName'] ?>) --></strong></p>
								<p><?= $deliverable['OutcomeDesc'] ?></p>
							</div>
							<div class="col-sm-4">
								<p><strong>Risk: &nbsp; <?= $deliverable['RiskDescShort'] ?></strong></p>
								<p><?= $deliverable['RiskDesc'] ?></p>
							</div>
							
							<div class="col-sm-4">
								<p><strong>Admin Priority: &nbsp; <?= $deliverable['AdminPriorityName'] ?></strong></p>
								
								<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverable['AdminPriorityID'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($adminpriorities as $rt) 
										{
										if($rt['AdminPriorityID'] == $realID)
										{
										$realVL = $rt['AdminPriorityDesc'];
										}
										}
									}
								}
								else 
									{ $realVL = "N/A"; }

								echo '<p>' . $realVL . '</p>';
								?>	  
								
								
							</div>
						</div>
						
				<?php 
					/*
					 * ------------------------------------------------------------------------
					 * 		This section below was added with the 201601 Changes in order
					 * 		to display the extra fields now shown on the list and detail 
					 * 		deliverable display.
					 * 
					 * 		Future versions of this may show a list in columns as the desire
					 * 		by finance is to have this as a zero to many capability.
					 * -----------------------------------------------------------20160126 PRSC
					 */
					
					?>

					<div class="row">
			
					<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service FOR (Impacted)
					 * =-------------------------------------------------  PRSC
					 */
					?>
					
						<div class="col-sm-4">
								<p><strong>Service Impacted by Initiative: &nbsp; </strong></p>
								<p><?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverable['ServiceFOR'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($services as $rt) 
										{
										if($rt['ServiceID'] == $deliverable['ServiceFOR'])
										{
											$biu_Name = "";
											$servarea_Name = "";
										    foreach ($business_units as $st) 
											{
												
											if($st['BusinessUnitID'] == $rt['BusinessUnitID'])
											$biu_Name = $st['BusinessUnitName'];
											}
											
											foreach ($serviceareas as $st) 
											{
											if($st['ServiceAreaID'] == $rt['ServiceAreaID'])
											$servarea_Name = $st['ServiceAreaName'];
											}
									
//										$realVL = $biu_Name . "->" . $servarea_Name . '->' . 
//													$rt['ServiceShortNM'];
										$realVL = $rt['ServiceShortNM'] . " (" . $biu_Name . "," . $servarea_Name . ")";
										}
										}
									}
								}
								else 
									{ $realVL = "N/A"; }

								echo $realVL;	  
								?></p>
	
							</div>
						
					<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service BY
					 * =-------------------------------------------------  PRSC
					 */
					?>
						
							<div class="col-sm-8">
							<p><strong>Service Performing this Initiative: &nbsp; </strong></p>
							<p><?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverable['ServiceBY'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($services as $rt) 
										{
										$biu_Name = "";
										$servarea_Name = "";
										
										if($rt['ServiceID'] == $deliverable['ServiceBY'])
										{
										    foreach ($business_units as $st) 
											{
												
											if($st['BusinessUnitID'] == $rt['BusinessUnitID'])
											$biu_Name = $st['BusinessUnitName'];
											}
											
											foreach ($serviceareas as $st) 
											{
											if($st['ServiceAreaID'] == $rt['ServiceAreaID'])
											$servarea_Name = $st['ServiceAreaName'];
											}
										
//										$realVL = $biu_Name . "->" . $servarea_Name . '->' . 
//													$rt['ServiceShortNM'];
										$realVL = $rt['ServiceShortNM'] . " (" . $biu_Name . "," . $servarea_Name . ")";
										}
										}
									}
								}
								else 
									{ $realVL = "N/A"; }
								echo $realVL;	  
								?></p>
							</div>

		
						
						
						
					<?php 
					/*
					 * ------------------------------------------------------------------------
					 * 		This section below was added with the 201601 Changes in order
					 * 		to display the extra fields now shown on the list and detail 
					 * 		deliverable display.
					 * 
					 * 		Future versions of this may show a list in columns as the desire
					 * 		by finance is to have this as a zero to many capability.
					 * -----------------------------------------------------------20160126 PRSC
					 */
					
					?>

							<div class="col-sm-4">
								<p><strong>Source: &nbsp; </strong>
							
							<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverable['Source'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($sources as $rt) 
										{
										if($rt['SourceID'] == $deliverable['Source'])
										$realVL = $rt['SourceShortName'];
										}
									}
								}
								else 
									{ $realVL = "N/A"; }
								echo $realVL;	  
								?></p>
							</div>
	
						<div class="col-sm-8">
								<p><strong>Published (Yes/No): &nbsp; </strong>
								
							<?php 
								$realID = 0;
								$realVL	= 'N/A';
								$realID = $deliverable['PublishedYN'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($publishedinfo as $rt) 
										{
										if($rt['PublishedID'] == $deliverable['PublishedYN'])
										$realVL = $rt['PublishedShortName'];
										}
									}
								}
								else 
									{ $realVL = 'N/A'; }
								echo $realVL;	  
								?></p>								
						</div>			




					</div>
		


						<p class="text-muted" style="">Target Completion Date: <?= date('M d, Y', strtotime(substr($deliverable['ApproxEndDate'],0,10))) ?></p>
					</div>
				</div>

			</div>

		</div>

		<div class="col-sm-12">
			<div class="row">
				<?php
					$label_color = '';
					if($deliverable['StatusID'] == 1 ) $label_color = 'success';
					if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
					if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
					if($deliverable['StatusID'] > 3 ) $label_color = 'info';
					?>
				<div class="progress_bg" title="<?= $deliverable['StatusDescription'] ?>">
					<div class="completion_label label label-<?= $label_color ?>" style="width:<?= $deliverable['ApproxComplete'] ?>%">
						<span class="label_text"><?= $deliverable['StatusDescription'] ?> - <?= ($deliverable['ApproxComplete'] == 100 ? "Completed" : $deliverable['ApproxComplete']."% complete")?><span>
					</div>
				</div>
			</div>
		</div>

		<?php if($this->session->userdata('UserAdminFlag') || $deliverable['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode') || $deliverable['DeliverableTypeID'] == 1): ?>

		<div class="col-sm-12">

			<div class="row" style="background:#f2f2f2;">

				<div class="col-sm-12">

					<div class="text_block ">
						
						<h1>Status</h1>
						<?php if(empty($deliverable['StatusUpdate'])): ?>
						<p class="muted"><em>No update at this time.</em></p>
						<?php else: ?>
						<?= $deliverable['StatusUpdate'] ?>
						<?php endif ?>

					</div>

					<div class="text_block">

						<?php if(!empty($deliverable['StatusUpdate'])): ?>
						<p class="muted text-right">
							<em>
								Updated <?= date('M d, Y', strtotime(substr($deliverable['LastModDate'],0,10))) ?> 
								<br class="visible-xs"> by <?= $deliverable['LastModBy'] ?>.
							</em>
						</p>
						<?php endif ?>
						
					</div>

				</div>

			</div>

		</div>

<?php
 /*---------------------------------------------------------------------
  * 
  * 	This is used with the Strategic Listings to build a list of 
  * 	links that allows users to update the status information on 
  * 	each of the related Deliverables to this parent Strategic one.
  * 
  * -----------------------------------------------------20160222 PRSC
  */
?>

<?php 
/*------------------------------------------------------( CHECK if anything to Display) */
?>

	<?php 
//		 echo "<pre>";
//		 print_r($related_supporting_deliverables);
//		 echo "</pre>";
		 
     ?>
		
		<?php 
		$XCOND_ShowDeliverables 		= 0;
//		echo "THIS STATE [" . $XCOND_ShowDeliverables . "]<br>";      
		
		$CNT_related_deliverables 		= count($related_supporting_deliverables);
		
		if($ddfilters['DeliverableTypeID'] == 1 && $CNT_related_deliverables > 0)
				{ $XCOND_ShowDeliverables 	= 1; }
		if($ddfilters['DeliverableTypeID'] == 2 && $CNT_related_deliverables > 0)
				{ $XCOND_ShowDeliverables 	= 1; }
	    ?>
	    
	    <?php 	      
//		echo "THIS COUNT [" . $CNT_related_deliverables . "]<br>";      
//		echo "THIS STATE [" . $XCOND_ShowDeliverables . "]<br>";      
		//if(set_value('DeliverableTypeID') == 1 || set_value('DeliverableTypeID') == 2):
		if($XCOND_ShowDeliverables		== 1):
		
		?>
		
		<div class="col-sm-12 supporting_deliverables">
			<p class="">
			<?= $supporting_deliverable_count ?> Related <?= (set_value('DeliverableTypeID') == 2) ? 
			     'Strategic Initiative' : 'Operational Deliverable'; ?><?= (($supporting_deliverable_count==1) ? '' : 's') ?>
			     </p>
			<ul>
			
			
		<?php foreach ($related_supporting_deliverables as $related_supporting_deliverable): ?>
				<?php
					$label_color = '';
					if($related_supporting_deliverable['statusid'] == 1 ) $label_color = 'success';
					if($related_supporting_deliverable['statusid'] == 2 ) $label_color = 'warning';
					if($related_supporting_deliverable['statusid'] == 3 ) $label_color = 'danger';
					if($deliverable['StatusID'] > 3 ) $label_color = 'info';
					?>
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $related_supporting_deliverable['id'] ?>">
						<?= $related_supporting_deliverable['title'] ?>
					</a>
					
					 <!-- - <?= $related_supporting_deliverable['percent'] ?>% completed 
					    <?= ($related_supporting_deliverable['percent'] < 100) ? '('.$related_supporting_deliverable['status'].
					            ')' : '' ?>  -->
					 - <span title="<?= $related_supporting_deliverable['status'] ?> ">
					          <?= $related_supporting_deliverable['percent'] ?>% completed</span>
				</li>

				<?php endforeach ?>
			</ul>
		</div>
		<?php endif ?><!-- EO Display ALL Related Supporting Deliverables - Strategic or Operational -->
	
 
	</div>


	
		<?php endif ?><!-- EO Main Display Deliverable Record -->
	
 
   
	<?php 
		$count++;
		endforeach;
	?>

	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>

</div>


