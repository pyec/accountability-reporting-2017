<?php $this->session->set_flashdata('uri', uri_string()) ?>

<?php
	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";
?>


<!-- Page -->
<div id="page">
	<div class="container">
	<div class="row">

<div class="col-sm-12">

	<div class="results">
		<div class="pull-right hidden-xs">
			<?php if (count($deliverables)>0): ?>
			<?= form_open(base_url().'download') ?>
				<input type="hidden" name="DeliverableTypeID" value="<?= set_value('DeliverableTypeID') ?>">
				<input type="hidden" name="BusinessUnitID" value="<?= set_value('BusinessUnitID') ?>">
				<input type="hidden" name="ServiceArea<?= set_value('BusinessUnitID') ?>" value="<?= set_value('ServiceArea'.set_value('BusinessUnitID')) ?>">
				<input type="hidden" name="FocusAreaID" value="<?= set_value('FocusAreaID') ?>">
				<input type="hidden" name="AdminPillarID" value="<?= set_value('AdminPillarID') ?>">
				<input type="hidden" name="RiskID" value="<?= set_value('RiskID') ?>">
				<input type="hidden" name="PriorityID" value="<?= set_value('PriorityID') ?>">
				<input type="hidden" name="StatusID" value="<?= set_value('StatusID') ?>">
				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>
			<?php endif ?>
		</div>
		<div class="result_count">
			<p>
				<?= count($deliverables) ?>
				<?php 
					if(set_value('DeliverableTypeID') == 1) echo 'Strategic Initiative';
					elseif (set_value('DeliverableTypeID') == 2) echo 'Operational Deliverable';
					else echo 'Deliverable';
					echo (count($deliverables) == 1 ? '' : 's'); 
				?>
			<p>
		</div>
	</div>

	<?php if (count($deliverables)>0): ?>

	<?php 

	$deliverable_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;

	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";

	// echo "<pre>";
	// print_r($supporting_deliverables);
	// echo "</pre>";

	foreach ($deliverables as $deliverable): 
	
		$supporting_deliverable_count = 0;
		$related_supporting_deliverables = [];

		if(!empty($supporting_deliverables))
		{

			foreach ($supporting_deliverables as $supporting_deliverable)
			{
				// if operational
				// if(set_value('DeliverableTypeID') == 2)
				if($deliverable['DeliverableTypeID'] == 2)
				{
					// echo "<pre>";
					// print_r($supporting_deliverable);
					// echo "</pre>";

					if($deliverable['DeliverableParentID'] == $supporting_deliverable['DeliverableID'])
					{
						$related_supporting_deliverables[$supporting_deliverable_count]['title'] = $supporting_deliverable['BusinessUnitCode'].' '.$supporting_deliverable['DeliverableCode'].' - '.$supporting_deliverable['DeliverableDescShort'];
						$related_supporting_deliverables[$supporting_deliverable_count]['id'] = $supporting_deliverable['DeliverableID'];
						$related_supporting_deliverables[$supporting_deliverable_count]['percent'] = $supporting_deliverable['ApproxComplete'];
						$related_supporting_deliverables[$supporting_deliverable_count]['status'] = $supporting_deliverable['StatusDescription'];
						$related_supporting_deliverables[$supporting_deliverable_count]['statusid'] = $supporting_deliverable['StatusID'];
						$supporting_deliverable_count++;
					}

				}
				// otherwise assume strategic
				else
				{
					if($deliverable['DeliverableID'] == $supporting_deliverable['DeliverableParentID'])
					{
						$related_supporting_deliverables[$supporting_deliverable_count]['title'] = $supporting_deliverable['BusinessUnitCode'].' '.$supporting_deliverable['DeliverableCode'].' - '.$supporting_deliverable['DeliverableDescShort'];
						$related_supporting_deliverables[$supporting_deliverable_count]['id'] = $supporting_deliverable['DeliverableID'];
						$related_supporting_deliverables[$supporting_deliverable_count]['percent'] = $supporting_deliverable['ApproxComplete'];
						$related_supporting_deliverables[$supporting_deliverable_count]['status'] = $supporting_deliverable['StatusDescription'];
						$related_supporting_deliverables[$supporting_deliverable_count]['statusid'] = $supporting_deliverable['StatusID'];
						$supporting_deliverable_count++;
					}
				}

			}
		
		}
		// echo "<pre>";
		// print_r($related_supporting_deliverables);
		// echo "</pre>";
	?>

	<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>edit-deliverable/<?= $deliverable['DeliverableID'] ?>" title="Update deliverable details"><i class="fa fa-pencil"></i></a></span>
						<?php endif ?>
						<h1><a href="<?= base_url() ?>test-status/<?= $deliverable['DeliverableID'] ?>" title="Update deliverable status"><?= $deliverable['BusinessUnitCode'] ?> <?= $deliverable['DeliverableCode'] ?> - <?= $deliverable['DeliverableDescShort'] ?></a></h1>
						<p class="text-muted"><?= $deliverable['ServiceAreaName'] ?></p>
						<!-- <p class="text-muted" style="">Target Completion Date: <?= date('M d, Y', strtotime(substr($deliverable['ApproxEndDate'],0,10))) ?></p> -->
					</div>
				</div>

				<!-- <div class="col-xs-5">
					<?php
						$label_color = '';
						if($deliverable['StatusID'] == 1 ) $label_color = 'success';
						if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
						if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
						<p class="approximate_complete"><?= ($deliverable['ApproxComplete'] == 100 ? "Complete " : $deliverable['ApproxComplete']."%")?></p>
						<p style="font-size:1.2em;" class="label label-<?= $label_color ?>"><?= $deliverable['StatusDescription'] ?></p>
					</div>
				</div> -->

			</div>

			<div class="row">

				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
						<?= $deliverable['DeliverableDesc'] ?>
						<div class="row">
							<div class="col-sm-4">
								<p><strong>Outcome: &nbsp; <?= $deliverable['OutcomeName'] ?> <!-- (<?= $deliverable['FocusAreaName'] ?>) --></strong></p>
								<p><?= $deliverable['OutcomeDesc'] ?></p>
							</div>
							<div class="col-sm-4">
								<p><strong>Risk: &nbsp; <?= $deliverable['RiskDescShort'] ?></strong></p>
								<p><?= $deliverable['RiskDesc'] ?></p>
							</div>
							<div class="col-sm-4">
								<p><strong>Admin Priority: &nbsp; <?= $deliverable['AdminPriorityName'] ?></strong></p>
								<p><?= $deliverable['AdminPriorityDesc'] ?></p>
							</div>
						</div>
						<p class="text-muted" style="">Target Completion Date: <?= date('M d, Y', strtotime(substr($deliverable['ApproxEndDate'],0,10))) ?></p>
					</div>
				</div>

			</div>

		</div>

		<div class="col-sm-12">
			<div class="row">
				<?php
					$label_color = '';
					if($deliverable['StatusID'] == 1 ) $label_color = 'success';
					if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
					if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
				?>
				<div class="progress_bg" title="<?= $deliverable['StatusDescription'] ?>">
					<div class="completion_label label label-<?= $label_color ?>" style="width:<?= $deliverable['ApproxComplete'] ?>%">
						<span class="label_text"><?= $deliverable['StatusDescription'] ?> - <?= ($deliverable['ApproxComplete'] == 100 ? "Completed" : $deliverable['ApproxComplete']."% complete")?><span>
					</div>
				</div>
			</div>
		</div>

		<?php if($this->session->userdata('UserAdminFlag') || $deliverable['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode') || $deliverable['DeliverableTypeID'] == 1): ?>

		<div class="col-sm-12">

			<div class="row" style="background:#f2f2f2;">

				<div class="col-sm-12">

					<div class="text_block ">
						
						<h1>Status</h1>
						<?php if(empty($deliverable['StatusUpdate'])): ?>
						<p class="muted"><em>No update at this time.</em></p>
						<?php else: ?>
						<?= $deliverable['StatusUpdate'] ?>
						<?php endif ?>

					</div>

					<div class="text_block">

						<?php if(!empty($deliverable['StatusUpdate'])): ?>
						<p class="muted text-right">
							<em>
								Updated <?= date('M d, Y', strtotime(substr($deliverable['LastModDate'],0,10))) ?> 
								<br class="visible-xs"> by <?= $deliverable['LastModBy'] ?>.
							</em>
						</p>
						<?php endif ?>
						
					</div>

				</div>

			</div>

		</div>

		<?php endif ?>


		<?php if(set_value('DeliverableTypeID') == 1 || set_value('DeliverableTypeID') == 2): ?>
		<div class="col-sm-12 supporting_deliverables">
			<p class=""><?= $supporting_deliverable_count ?> Related <?= (set_value('DeliverableTypeID') == 2) ? 'Strategic Initiative' : 'Operational Deliverable'; ?><?= (($supporting_deliverable_count==1) ? '' : 's') ?></p>
			<ul>
				<?php foreach ($related_supporting_deliverables as $related_supporting_deliverable): ?>
				<?php
					$label_color = '';
					if($related_supporting_deliverable['statusid'] == 1 ) $label_color = 'success';
					if($related_supporting_deliverable['statusid'] == 2 ) $label_color = 'warning';
					if($related_supporting_deliverable['statusid'] == 3 ) $label_color = 'danger';
				?>
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>test-status/<?= $related_supporting_deliverable['id'] ?>">
						<?= $related_supporting_deliverable['title'] ?>
					</a>
					
					 <!-- - <?= $related_supporting_deliverable['percent'] ?>% completed <?= ($related_supporting_deliverable['percent'] < 100) ? '('.$related_supporting_deliverable['status'].')' : '' ?>  -->
					 - <span title="<?= $related_supporting_deliverable['status'] ?> "><?= $related_supporting_deliverable['percent'] ?>% completed</span>
				</li>

				<?php endforeach ?>
			</ul>
		</div>
		<?php endif ?>

	</div>

	<?php 
		$count++;
		endforeach;
	?>

	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>

</div>

