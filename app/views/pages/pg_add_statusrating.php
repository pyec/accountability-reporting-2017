<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($statusrating);
// echo "</pre>";

?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($statusrating)) 
				{
				echo form_open('create-statusrating');
				}
			else
				{
				echo form_open('update-statusrating');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Status ID;
			if(!empty($statusrating)) 
			         echo form_hidden('StatusID',$statusrating['StatusID']);
			?>
 
  				<h1>
 				 <?= (!empty($statusrating)) ? 'Edit' : 'Create' ?> Status Rating</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Status Rating Name/Desc 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="StatusDescription">Status Rating Name (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="StatusDescription" class="form-control"
						 value="<?= set_value('StatusDescription', (!empty($statusrating) ?
						  $statusrating['StatusDescription'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>


			



				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($statusrating))
					 ? 'Update' : 'Create' ?> Status Rating</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>




