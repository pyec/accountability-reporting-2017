<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>


<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			// open the form and pass the problem code in a hidden field
			if(empty($risk)) 
				{
				echo form_open('create-risk');
				}
			else
				{
				echo form_open('update-risk');
				}
				
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Risk ID;
			if(!empty($risk)) 
			         echo form_hidden('RiskID',$risk['RiskID']);
			?>
 
  				<h1>
 				 <?= (!empty($risk)) ? 'Edit' : 'Create' ?> Risk</button>
                </h1>              
 			<?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 ?> 
	                               
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201601
	 */ ?>


	
	 
	 <?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Risk Code
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="RiskCode">Risk Code (5-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="RiskCode" class="form-control"
						 value="<?= set_value('RiskCode', (!empty($risk)?  $risk['RiskCode'] : ''))
						 ?>" maxlength="5">
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Risk ShortName Desc
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="RiskDescShort">Risk Short Name (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="RiskDescShort" class="form-control"
						 value="<?= set_value('RiskDescShort', (!empty($risk) ?
						  $risk['RiskDescShort'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Likelihood
		 * -----------------------------------------------------PRSC 201601
		 */
		
		?>

	
		<div class="row">
			<div class="col-sm-4">
				<label for="Likelihood">Likelihood</label>
			</div>
			<div class="col-sm-8">			
				<select class="form-control" id="Likelihood" name="Likelihood">

				    <?php 
				    $rt = $risk['Likelihood'];
				    $real = '';
					foreach ($likelihoods as $rt) 
						{
						if($rt['LikelihoodID'] == $risk['Likelihood'])
							$real = $rt['LikelihoodShortName'];
						}
						
				    if($real)
				    	echo '<option value="' . $risk['Likelihood'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Select Likelihood</option>';
					?>	
					<?php foreach ($likelihoods as $dt): ?>
					<option name="<?= $dt['LikelihoodID'] ?>"
						 value="<?= $dt['LikelihoodID'] ?>"
					 		 <?= set_select('LikelihoodID', $dt['LikelihoodID'],
					 		 ((!empty($likelihood) && $risk['LikelihoodID'] == $dt['LikelihoodID'])? TRUE : '')) ?>>
					 		 <?= $dt['LikelihoodShortName'] ?> (<?= $dt['LikelihoodShortName'] ?>)</option>
					<?php endforeach ?>
				</select>
			</div>
		</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Impact 
		 * -----------------------------------------------------PRSC 201601
		 */	?>
	
			<div class="row">
			<div class="col-sm-4">
				<label for="Impact">Impact</label>
			</div>
			<div class="col-sm-8">			
				<select class="form-control" id="Impact" name="Impact">

				    <?php 
				    $rt = $risk['Impact'];
				    $real = '';
					foreach ($impacts as $rt) 
						{
						if($rt['ImpactID'] == $risk['Impact'])
							$real = $rt['ImpactShortName'];
						}
						
				    if($real)
				    	echo '<option value="' . $risk['Impact'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Select Net Impact</option>';
					?>	
						
					<?php foreach ($impacts as $dt): ?>
					<option name="<?= $dt['ImpactID'] ?>"
						 value="<?= $dt['ImpactID'] ?>"
					 		 <?= set_select('ImpactID', $dt['ImpactID'],
					 		 ((!empty($impact) && $risk['ImpactID'] == $dt['ImpactID'])? TRUE : '')) ?>>
					 		 <?= $dt['ImpactShortName'] ?> (<?= $dt['ImpactShortName'] ?>)</option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	
			<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	HeatMapRate
		 * -----------------------------------------------------PRSC 201601
		 */	?>
	
		<div class="row">
			<div class="col-sm-4">
				<label for="HeatMapRating">HeatMapRating</label>
			</div>
			<div class="col-sm-8">			
				<select class="form-control" id="HeatMapRating" name="HeatMapRating">	
				    <?php 
				    $rt = $risk['HeatMapRating'];
				    $real = '';
					foreach ($heatmapratings as $rt) 
						{
						if($rt['HeatMapRateID'] == $risk['HeatMapRating'])
							$real = $rt['HeatMapRateShortName'];
						}
						
				    if($real)
				    	echo '<option value="' . $risk['HeatMapRating'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Select Heat Map Rating </option>';
					?>		
		
		
					<?php foreach ($heatmapratings as $dt): ?>
					<option name="<?= $dt['HeatMapRateID'] ?>"
						 value="<?= $dt['HeatMapRateID'] ?>"
					 		 <?= set_select('HeatMapRateID', $dt['HeatMapRateID'],
					 		 ((!empty($heatmaprating) && $risk['HeatMapRateID'] == $dt['HeatMapRateID'])? TRUE : '')) ?>>
					 		 <?= $dt['HeatMapRateShortName'] ?> (<?= $dt['HeatMapRateShortName'] ?>)</option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Risk Owner (Should be username?)
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="RiskOwner">Risk Owner (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="RiskOwner" class="form-control"
						 value="<?= set_value('RiskOwner', (!empty($risk) ?
						  $risk['RiskOwner'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>



		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Risk Statement
		 * -----------------------------------------------------PRSC 201601
		 */	?>

		<label for="RiskStatement">Risk Statement (500 digits)</label>
		<textarea name="RiskStatement" rows="15"><?= set_value('RiskStatement',
		 (!empty($risk) ? $risk['RiskStatement'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'RiskStatement', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Risk Description (FULL)
		 * -----------------------------------------------------PRSC 201601
		 */	?>

		<label for="RiskDesc">Risk Description (1500 digits)</label>
		<textarea name="RiskDesc" rows="15"><?= set_value('RiskDesc',
		 (!empty($risk) ? $risk['RiskDesc'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'RiskDesc', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($risk))
					 ? 'Update' : 'Create' ?> Risk</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>

