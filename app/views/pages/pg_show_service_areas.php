<?php 

 	/*================================================================
	 * 
	 * 	MODULE:			pg_show_service_areas.php
	 * 	AUTHOR:			R.Stephen Chafe (Zen River Software)
	 * 	CREATED:		2016_03_06
	 * 
	 * 	This module contains the LIST display for all of the 
	 *  service_areas data.
	 *  
	 * ==============================================================
	 */



	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($service_areas);
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>


	
<?php if (count($service_areas)>0): ?>


<div class="col-sm-12">

	<div class="results">    
	
	<!-- ------------------------------------------------------------------EO-->  
	
		<div class="pull-right hidden-xs">
			<?php if (count($service_areas)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	 		
			?>
			<?= form_open(base_url().'download-service-areas') ?>
					<input type="hidden" name="ServiceAreaID" value="<?= set_value('ServiceAreaID') ?>">
				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>
			
			
		</div>
		
		
		
		<div class="result_count">
			<p>
				<?php 								/* TITLES */
					echo count($service_areas);
					echo ' SERVICE AREA List';
					echo (count($service_areas) == 1 ? '' : 'ings'); 
				?>
			</p>
		</div>

			<?php endif ?>
		</div>
	
	</div>


<?php 

	$service_area_id 		= '';
	$service_area_parent_id 	= '';
	$count	= 0;



	foreach ($service_areas as $service_areaDT): 
	

	?>

<div class="col-sm-12">

	<?php if (count($service_areas)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	If there are any SERVICES data in the Array then loop through
	 		*	and display each one in a easy to read format.
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	?>


	<?php 
	$service_area_id 		= '';
	$service_area_parent_id 	= '';
	$count	= 0;

	
	?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed ServiceArea Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
	?>




<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

					<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service Area Name
					 * =-------------------------------------------------  PRSC
					 */
					?>

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-service_area/<?= $service_areaDT['ServiceAreaID'] ?>"
									 title="Update Service_Area details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						 <?php /* service_areaDT['ServiceAreaID'] */ ?>
						 <?= $service_areaDT['ServiceAreaName'] ?>
						 <?php 
						  if( !empty($service_areaDT['ServiceAreaShortCD']))
						  {
						  	echo "[";
						  	echo $service_areaDT['ServiceAreaShortCD'];
						  	echo "]";
						  } 
						  ?>
						</h1>
						
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						/* CNIU - but leave in as it will be used in future version PRSC */
						$label_color = 'success';
						//if($service_areaDT['StatusID'] == 1 ) $label_color = 'success';
						//if($service_areaDT['StatusID'] == 2 ) $label_color = 'warning';
						//if($service_areaDT['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
					</div>
				</div>

			</div>

			<div class="row">

					<div class="col-sm-4">
								<p><strong>Service Area Description: &nbsp;</strong></p>
					</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $service_areaDT['ServiceAreaDesc'] ?>
				</div>	
			</div>
			
			<div class="row">
	


					<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Business Unit
					 * =-------------------------------------------------  PRSC
					 */
					?>
				
				
				<div class="col-sm-4">
								<p><strong>&nbsp &nbsp Business Unit: &nbsp; </strong></p>
								<p>&nbsp &nbsp
															
							<?php 
							//  Lookup the actual literal to show to user PRSC					
							$vdt = $service_areaDT['BusinessUnitID'];
							foreach ($business_units as $dt)
							{
								if($vdt == $dt['BusinessUnitID'])
								{
									$value = $dt['BusinessUnitName'];
									echo $value;	
								}
							}
							?>
								
							</p>
				</div>
	
					<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service Area Short Code
					 * =-------------------------------------------------  PRSC
					 */
					?>
	
				<div class="col-sm-4">
								<p><strong>&nbsp &nbsp Service Area Short Code: &nbsp; </strong></p>
								<p>&nbsp &nbsp
															
							<?php 
							//  Lookup the actual literal to show to user PRSC					
							$value = $service_areaDT['ServiceAreaShortCD'];
							echo $value;	
							?>
								
							</p>
						</div>	
	
	
			</div>
				
	
		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>


	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>






