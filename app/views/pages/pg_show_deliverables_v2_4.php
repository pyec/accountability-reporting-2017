<?php $this->session->set_flashdata('uri', uri_string()) 

/* ---------------------------------------------------------------------
 * 
 *		MODULE:			pg_show_deliverables2
 *		AUTHOR:			Stephen Chafe (Zen River Software)
 *		CREATED:		Unknown
 *
 * 		This is the common template for displaying the inner list of 
 * 		"Deliverables" data to the user.
 * 
 * 		This is used in multiple modes, primarily within the "Default View" types.
 * 
 * 		The "ddfilter" variable and (older) "filter" variable impact what is
 * 		displayed as well.  For some reason the original developer did that within
 * 		the body of this code, rather then in the data model where it belongs.
 * 
 * 
 * 		MODIFICATION HISTORY
 * 
 * 		20160413		PRSC		Changing to seperate how DeliveryTypeID
 * 									is handled within the display. Wrote all
 * 									new code sections for Strategic/Oper display.
 * 
 * ----------------------------------------------------------------------
 */
?>

<br>  <!-- Header Spacer -->


<?php
//	 echo "<pre>";
//	 print_r($business_units);
//	 echo "</pre>";
?>
<?php
//echo "<br><br><br><br>";

//	echo "<pre> ----------------DDFF";
//	 print_r($ddfilters);
//	 echo "</pre>";
$BILink = '';

if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'production':
            $BILink = BI_PROD_URL;
            break;
        default:
            $BILink = BI_QA_URL;
    }
}

?>


<!-- Page -->
<div id="page">
	<div class="container">

	<div class="row">

	<div class="col-sm-12">

	<div class="results">

		<div class="pull-right hidden-xs">
			<?php if (count($deliverablesARR)>0): ?>
			<?= form_open(base_url().'download-deliverables') ?>
			    <?php 
			    	$src_1 = '';
			     	$src_1 = $ddfilters['DeliverableTypeID'];
			    ?>
				<input type="hidden" name="DeliverableTypeID" value="<?php print $src_1 ?>">
				<input type="hidden" name="BusinessUnitID" value="<?php print $ddfilters['BusinessUnitID']; ?>">
				<input type="hidden" name="FocusAreaID" value="<?php print $ddfilters['FocusAreaID']; ?>">
				<input type="hidden" name="AdminPillarID" value="<?php print $ddfilters['AdminPillarID']; ?>">
				<input type="hidden" name="RiskID" value="<?php print $ddfilters['RiskID']; ?>">
				<input type="hidden" name="PriorityID" value="<?php print $ddfilters['PriorityID']; ?>">
				<input type="hidden" name="StatusID" value="<?php print $ddfilters['StatusID']; ?>">
				<input type="hidden" name="PublishedYN" value="<?php print $ddfilters['PublishedYN']; ?>">
				<input type="hidden" name="Source" value="<?php print $ddfilters['Source']; ?>">
				<input type="hidden" name="ServiceFOR" value="<?php print $ddfilters['ServiceFOR']; ?>">
				<input type="hidden" name="ServiceBY" value="<?php print $ddfilters['ServiceBY']; ?>">

                <a target="_blank" href="<?php echo $BILink ?>/BOE/BI?startFolder=AXhYLy13NL9Mv69MfAMIzww&isCat=false" class="btn btn-info" style="margin-right: 0.25em">Reports</a>
				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>
			<?php endif ?>
		</div><!-- EO Pull Right -->
		
		<div class="result_count">
			<p>
				<?= count($deliverablesARR) ?>
				<?php 
					if(set_value('DeliverableTypeID') == 1) echo 'Strategic Initiative';
					elseif (set_value('DeliverableTypeID') == 2) echo 'Operational Deliverable';
					else echo 'Deliverable';
					echo (count($deliverablesARR) == 1 ? '' : 's'); 
				?>
			<p>
		</div><!-- EO Result Count -->

	</div><!-- EO Results -->
	</div><!-- EO cm12 -->  
    </div><!-- EO Row -->

	<?php 

	// These variables are used with the Related Deliverables Display at the bottom - PRSC
    $CNT_total_parents 		= count($parent_deliverables);
    $CNT_total_kids 		= count($supporting_deliverables);
    
//	print "FULLVIEWS - Parent [" . $CNT_total_parents . "][" . $CNT_total_kids . "]<br>";

	
	if (count($deliverablesARR)>0): ?>

	<?php 

	$deliverable_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;

	// echo "<pre>";
	// print_r($deliverablesARR);
	// echo "</pre>";

//	 echo "<pre>";
//	 print_r($services);
//	 echo "</pre>";
	?>
	
	
	
	<?php 
	// These are also used later with the display of the Related Deliverables - PRSC
	$CNT_childrenxx		= 0;
	$CNT_parentsxx		= 0;

	$CHLD_Deliverables   = [];
	$PRNT_Deliverables   = [];
	
//	print "HERE10<br>";
	
	
	/* 
	 * BASIC FUNCTIONAL DESIGN
	 * 
	 * This is the beginning of the overall loop that takes all Deliverables
	 * one at a time and builds the display block for each.
	 * 
	 * The first step requires identifying the sub-Deliverables that are all
	 * linked to the primary one.  These are both Strategic and Operational
	 * in how they are displayed.
	 * 
	 * There are sub categories of AD associated with each Business Unit.
	 * These other 13 AD groups are considered General End users. 
	 * 
	 * If an End user is in one of these 13 sub active-directory groups 
	 * then they should not be allowed to see the Status screens
	 * or information associated with Operational Deliverables from other
	 * Business Units.
	 * 
	 * 
	 */
	
	$glb_showDeliverableYN	= true;					// Default Display Deliverable YN
	$glb_showStatusLinkYN	= true;					// Show Deliverable Title as Status Screen Link YN
	$glb_showStatusDetailsYN	= true;
	
	
	
	foreach ($deliverablesARR as $deliverableREC): 
	
		$supporting_deliverable_count = 0;
//		$related_supporting_deliverables = [];

		
//		 echo "<pre>";
//		 print_r($parent_deliverables);
//		 echo "</pre>";
	

//		echo "<pre>";
//		print_r($deliverableREC);
//	 	echo "</pre>";
		
	 	/* Allow for no Filters Info by setting default search values */
	 	
	 	if(!$ddfilters['DeliverableTypeID'])
	 	{
	 		$glb_filter_DeliverableTypeID	=	3;
	 	}
	 	else
	 	{
	 		$glb_filter_DeliverableTypeID	=	$ddfilters['DeliverableTypeID'];
	 	}
	 					
		 
		/*--------------------------------------------------------------------------
		 * 		If listing Operational Deliverable then skip if the user is not
		 * 		from the same Business Group.
		 *----------------------------------------------------------- PRSC 20160814-
		 */

//		print "BUSCODE [" . $deliverableREC['BusinessUnitCode'] . "]<br>";
//		print "BUSUSER [" . $this->session->userdata('UserBusinessUnitCode') . "]<br>";
		
	 	// Global Base Defines
		$glb_showDeliverableYN			= false;
		$glb_showStatusLinkYN			= false;
		$glb_showStatusDetailsYN		= false;
		$glb_show_percbarYN				= false;
		
		
		
			// Strategics from other groups should be visible.
			if(	$deliverableREC['DeliverableTypeID'] == 1 &&
				($glb_filter_DeliverableTypeID == 1 || $glb_filter_DeliverableTypeID == 3)) 
			{
				$glb_showDeliverableYN 		= true;
				$glb_showStatusLinkYN		= false;
				$glb_showStatusDetailsYN	= false;
				
				if( $deliverableREC['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode'))
				{
				$glb_showStatusLinkYN		= true;
				$glb_showStatusDetailsYN	= true;
				}
				else
				{				
				// Strategic Details from other groups should be visible and linkable
				// This logic is duplicated here in case in the future the FICT folks
				// change their mind about this, so simple flag change below is all 
				// that is required - PRSC
				$glb_showStatusLinkYN		= true;
				$glb_showStatusDetailsYN	= true;
				}
				
//				print "F0_DELV [" . $glb_showDeliverableYN . "]<br>";
//				print "F0_STATL [" . $glb_showStatusLinkYN . "]<br>";
//				print "F0_STATD [" . $glb_showStatusDetailsYN . "]<br>";

				// If user is Extended than allow status to be seen in main list
				if($this->session->userdata('extendedYN'))
				{
				$glb_showStatusDetailsYN	= true;
				}
				
			}
		
	
		// Dont show oper delibables
		
		/*-------------------------------------------------------
		 * 	This next multi-block section is used to set flags
		 * 	on what can be seen and what cant by the End user
		 * 	base on their group and various other factors.
		 * 	This use of YN flags is much cleaner then the open
		 * 	testing method of the array values used by the early
		 * 	developers - which made changes and maintenance 
		 *  a complete nightmare
		 * ----------------------------------------20160823 PRSC- 
		 */
		
		if(	!$this->session->userdata('UserAdminFlag'))
		{
			// If list has Filter for Operationals only show only same business unit operationals CNIU
//			if($deliverableREC['BusinessUnitCode'] != $this->session->userdata('UserBusinessUnitCode') &&
//				$deliverableREC['DeliverableTypeID'] == 2 &&
//				$glb_filter_DeliverableTypeID) 
//			{
//				$glb_showDeliverableYN 		= false;
//				$glb_showStatusLinkYN		= false;
//				$glb_showStatusDetailsYN	= false;
//				print "F1_DELV [" . $glb_showDeliverableYN . "]<br>";
//				print "F1_STAT [" . $glb_showStatusLinkYN . "]<br>";
//				print "F1_STAT [" . $glb_showStatusDetailsYN . "]<br>";
//			}

			
			// If list has Filter for Operationals only
			if(	$deliverableREC['DeliverableTypeID'] == 2 &&
				$glb_filter_DeliverableTypeID == 2) 
			{
				$glb_showDeliverableYN 		= true;
				$glb_showStatusLinkYN		= false;
				$glb_showStatusDetailsYN	= false;
				
				if( $deliverableREC['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode'))
				{
				$glb_showStatusLinkYN		= true;
				$glb_showStatusDetailsYN	= true;
				}
//				print "F2_DELV [" . $glb_showDeliverableYN . "]<br>";
//				print "F2_STATL [" . $glb_showStatusLinkYN . "]<br>";
//				print "F2_STATD [" . $glb_showStatusDetailsYN . "]<br>";
				
			}
			
			
			
			// Filter for ALL Deliverables when Operational is found
			if(	$deliverableREC['DeliverableTypeID'] == 2 &&
				$glb_filter_DeliverableTypeID == 3) 
			{
				$glb_showDeliverableYN = true;
				if( $deliverableREC['BusinessUnitCode'] != $this->session->userdata('UserBusinessUnitCode'))
				{
				$glb_showStatusLinkYN		= false;
				$glb_showStatusDetailsYN	= false;
				}
//				print "F3_DELV [" . $glb_showDeliverableYN . "]<br>";
//				print "F3_STAT [" . $glb_showStatusLinkYN . "]<br>";
//				print "F3_STAT [" . $glb_showStatusDetailsYN . "]<br>";
			}

			// Filter for ALL Deliverables when Operational is found
			if(	$deliverableREC['DeliverableTypeID'] == 2 &&
				$glb_filter_DeliverableTypeID == 3 )
			{
				$glb_showDeliverableYN = true;
				if( $deliverableREC['BusinessUnitCode'] != $this->session->userdata('UserBusinessUnitCode'))
				{
				$glb_showStatusLinkYN		= false;
				$glb_showStatusDetailsYN	= false;
				}
				else
				{							/* Otherwise it is from the same Business Unit so show fully */
				$glb_showStatusLinkYN		= true;
				$glb_showStatusDetailsYN	= true;
					
				}
				
//				print "F4_DELV [" . $glb_showDeliverableYN . "]<br>";
//				print "F4_STATL [" . $glb_showStatusLinkYN . "]<br>";
//				print "F4_STATL [" . $glb_showStatusDetailsYN . "]<br>";
				
			
			
			}

			// [TASK0084832]
			// Filter for ALL Deliverables when Strategic is found
			// If not same business then show no link or status info.
			// This is currently duplication of F0 test in the event
			// that future requirements require this seperating the
			// way non-unit operational works. This works the reverse
			// condition of F0 check but otherwise currently the same.
			if(	$deliverableREC['DeliverableTypeID'] == 1 &&
				($glb_filter_DeliverableTypeID == 3 || $glb_filter_DeliverableTypeID == 1 )) 
			{
				$glb_showDeliverableYN = true;
				if( $deliverableREC['BusinessUnitCode'] != $this->session->userdata('UserBusinessUnitCode'))
				{
				$glb_showStatusLinkYN		= true;     /* Should not view Status if Strategic */
				$glb_showStatusDetailsYN	= true;
				} 
				else
				{										/* If same Business Unit then display Status */
				$glb_showStatusLinkYN		= true;
				$glb_showStatusDetailsYN	= true;
				} 
				
				
//				print "F5_DELV [" . $glb_showDeliverableYN . "]<br>";
//				print "F5_STATL [" . $glb_showStatusLinkYN . "]<br>";
//				print "F5_STATD [" . $glb_showStatusDetailsYN . "]<br>";
				
			}
		
			// [TASK0084832] If user is Extended than allow status to be seen in main list.
			// Also Extended users may also click on link that goes to the STATUS screen
			// where they can then update IF this is in their own business unit. PRSC

			  $glb_extendedYN 			= $this->session->userdata('UserExtendedYN');
				if($glb_extendedYN)
				{
					if( $deliverableREC['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode'))
					{
					$glb_showStatusLinkYN		= true;
					}					

				$glb_showStatusDetailsYN	= true;
//				print "F7_DELV [" . $glb_showDeliverableYN . "]<br>";
//				print "F7_STATL [" . $glb_showStatusLinkYN . "]<br>";
//				print "F7_STATD [" . $glb_showStatusDetailsYN . "]<br>";
				}
//				print "EXTENDED [" . $glb_extendedYN . "]<br>";
				
			
			
		}		//* EO if not Admin view conditions test

		
		
		
		
	/*--------------------------------------------------------------
	 *	 If the user is Admin then let them see everything - PRSC
	 *--------------------------------------------------------------
	 */
		if(	$this->session->userdata('UserAdminFlag'))
		{
			$glb_showDeliverableYN			= true;
			$glb_showStatusLinkYN			= true;
			$glb_showStatusDetailsYN		= true;
			
			
//			print "FX_DELV [" . $glb_showDeliverableYN . "]<br>";
//			print "FX_STAT [" . $glb_showStatusLinkYN . "]<br>";
//			print "FX_STAT [" . $glb_showStatusDetailsYN . "]<br>";
//			print "ADMIN<br>";
		}
	else 	
	{		
//			print "NOT ADMIN<br>";
	}
	

	/*---------------------------------------------------
	 * 		Show percentage done bar
	 * --------------------------------------------------
	 */	
		
		
//	if(	$deliverableREC['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode') &&
//				$deliverableREC['DeliverableTypeID'] == 1)

	$glb_show_percbarYN					= true;				// Show non-edit bar by default
	if($glb_showStatusDetailsYN == true)
		{
		$glb_show_percbarYN		= true;
		}						 
	
	
//	DEBUG ONLY - Show final flag states for each before displaying
	
//	print "SHW_DELV [" . $glb_showDeliverableYN . "]<br>";
//	print "SHW_STAT [" . $glb_showStatusLinkYN . "]<br>";
//	print "SHW_STAT [" . $glb_showStatusDetailsYN . "]<br>";
		
		
		
   /*-------------------------------------------------------
    * 	If Deliverable to be displayed then do so
    *-------------------------------------------------------
    */
		
	if($glb_showDeliverableYN == true): 		
				
?>


<?php  	
	 	
/*============================================================================
 * 		This ugly chunk is run with every Deliverable.  
 * 		The goal of this code is to build the list of associated Deliverables that
 * 		relates as Strategic (Parent) or Operational (child) to the current one that
 * 		is being shown. [PRSC]
 * ===========================================================================
 */

	
		// This was designed so that if the USER did not select the OPERATIONAL "AllTypes"
		// View then the assumption is that they want the STRATEGIC "AllTypes" Default View.
		//
		// THEREFORE as a default the Deliverables view should be showing the list of
		// related Deliverables below each Deliverable that is displayed even in simple
		// normal mode. [PRSC]


		$XCOND_ShowChildDeliverables 		= FALSE;
		$XCOND_ShowParentDeliverables		= FALSE;
		
		$XDeliveryType = $ddfilters['DeliverableTypeID']; 
//		print "DELIVERTYPE [" . $XDeliveryType . "]<br>"; 
		
		if($ddfilters['DeliverableTypeID']   ==  1)		$XCOND_ShowChildDeliverables  = TRUE;
		if($ddfilters['DeliverableTypeID']   ==  2)		$XCOND_ShowParentDeliverables  = TRUE;
		if($ddfilters['DeliverableTypeID']   ==  3)
			{
					$XCOND_ShowChildDeliverables  = TRUE;
					$XCOND_ShowParentDeliverables  = TRUE;
			}
		$XCOND_ShowChildDeliverables  = TRUE;
		$XCOND_ShowParentDeliverables  = TRUE;

		
		

		/*------------------------------------------------------------------
		 *            If required then calculate the Parent Deliverables
		 *-----------------------------------------------------------------*/

		
		
//		print "HERE30<br>";
			
		if($XCOND_ShowParentDeliverables == TRUE)
			{

			if($CNT_total_parents   > 0)
			{

//			print "HERE30B<br>";
			
			$CNT_parentsxx		= 0;
			$PRNT_Deliverables   = [];
	
			foreach ($parent_deliverables as $rpdt)
			{

//					 echo "<pre>-------------------------------[PARENT]";
//					 print_r($rpdt);
//					 echo "</pre>";
					$DPID = $deliverableREC['DeliverableParentID'];

					if($DPID == $rpdt['DeliverableID'])
					{
//					print "HERE30S3  [" . $DPID . "][" . $rpdt['DeliverableID'] . "] <br>";
					$PRNT_Deliverables[$CNT_parentsxx]['title'] = 
								$rpdt['BusinessUnitCode'].' '.$rpdt['DeliverableCode'].' - ' 
								.$rpdt['DeliverableDescShort'];
						$PRNT_Deliverables[$CNT_parentsxx]['id'] = $rpdt['DeliverableID'];
						$PRNT_Deliverables[$CNT_parentsxx]['percent'] = $rpdt['ApproxComplete'];
						$PRNT_Deliverables[$CNT_parentsxx]['status'] = $rpdt['StatusDescription'];
						$PRNT_Deliverables[$CNT_parentsxx]['statusid'] = $rpdt['StatusID'];
						$CNT_parentsxx++;
					}							// EO If Parent for sure	
				}								// EO for each Parent deliverable
	
			}									/* EO If Total Parents greater then zero */
			
//				echo "XSTRAT CNT [" . $CNT_parentsxx . "]<br>";		
			
			
		}										/* EO Kids should be shown at all */

	
		/*------------------------------------------------------------------
		 *            If required then calculate the Child Deliverables
		 *-----------------------------------------------------------------*/
		
		
		 if($XCOND_ShowChildDeliverables == TRUE)
			{
			$CNT_childrenxx		= 0;
			$CHLD_Deliverables   = [];
				
			if($CNT_total_kids   > 0)
			{

//			print "HERE30S<br>";
			
			foreach ($supporting_deliverables as $rcdt)
			{


//					 echo "<pre>-------------------------------[CHILD]";
//					 print_r($rcdt);
//					 echo "</pre>";

					$DCID = $deliverableREC['DeliverableID'];
//					print "HERE30-XX  [" . $DCID . "][" . $rcdt['DeliverableParentID'] . "] <br>";
					
					if($rcdt['DeliverableParentID'] == $DCID)
						{
//						print "HERE30-CH  [" . $DCID . "][" . $rcdt['DeliverableParentID'] . "] <br>";
						$CHLD_Deliverables[$CNT_childrenxx]['title'] = 
								$rcdt['BusinessUnitCode'].' '.$rcdt['DeliverableCode'].' - ' 
								.$rcdt['DeliverableDescShort'];
						$CHLD_Deliverables[$CNT_childrenxx]['id'] = $rcdt['DeliverableID'];
						$CHLD_Deliverables[$CNT_childrenxx]['percent'] = $rcdt['ApproxComplete'];
						$CHLD_Deliverables[$CNT_childrenxx]['status'] = $rcdt['StatusDescription'];
						$CHLD_Deliverables[$CNT_childrenxx]['statusid'] = $rcdt['StatusID'];
						$CNT_childrenxx++;
//						echo "FCN-CHILD CNT [" . $CNT_childrenxx . "]<br>";		
						
					}								// EO If we should show

				}									// EO for each kid deliverable
	

			}									/* EO If Total Kids greater then zero */

//			echo "XOPER CNT [" . $CNT_childrenxx . "]<br>";		
			
		}										/* EO Parents should be shown at all */
								
		
	/*-------------------------------------------------( EO Ugly Header Block )	*/
		
     ?>





	<?php 
	/*-------------------------------------------------------------
	 * 				DISPLAY DELIVERABLE BLOCK
	 * ------------------------------------------------------------
	 */?>


	<div class="block">
	
		<div class="col-sm-12">


			<!-- SO ROW-A -->
			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						
						<?php
						/* 
						 * 		If End user is admin then they can click on this to Edit
						 * 		the actual base Deliverable Data.
						 */
						
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right">
							<a href="<?= base_url() ?>edit-deliverable/<?= $deliverableREC['DeliverableID'] ?>"
							 title="Edit deliverable details"><i class="fa fa-pencil"></i></a></span>
						<?php endif ?>
	
						
				<?php
			   /*
    	 		* ----------------------------------------------------------------
	 			*	This is the Link embedded at a title in the show deliverables
	 			*	screen.  Clicking on this goes to the Edit Status Screen
	 			*	which is now separate from the regular delivable update.
	 			* -----------------------------------------------------PRSC 201601
	 			*/ 
				
				if($glb_showStatusLinkYN	== true):
				?>  
						
				<h1>
					<a href="<?= base_url() ?>edit-status/<?= $deliverableREC['DeliverableID'] ?>
							" title="Edit deliverable status">
							<?= $deliverableREC['BusinessUnitCode'] ?>
							<?= $deliverableREC['DeliverableCode'] ?> -
							<?= $deliverableREC['DeliverableDescShort'] ?>
					</a>
				</h1>
				<?php endif ?>


				<?php 
				/*
				 * 		If Header is not a active Link show this instead.
				 */
				
				if($glb_showStatusLinkYN	== false):
				?>  
						
				<h1>
					
							<?= $deliverableREC['BusinessUnitCode'] ?>
							<?= $deliverableREC['DeliverableCode'] ?> -
							<?= $deliverableREC['DeliverableDescShort'] ?>
					
				</h1>
				<?php endif ?>



					<p class="text-muted"><?= $deliverableREC['ServiceAreaName'] ?></p>
					<!-- <p class="text-muted" style="">Target Completion Date: 
					     <?= date('M d, Y', strtotime(substr($deliverableREC['ApproxEndDate'],0,10))) ?></p> -->
						     
					</div><!-- EO Text Block -->
				</div><!-- EO cm12 -->

				<!-- SO COLOR STAT -->

				<!-- <div class="col-xs-5">
					<?php
						/*----------------------------------------------------------
						 * 	Display the Visibile Percentage Complete
						 * ------------------------------------------ 20160826 PRSC-
						 */
					
						$label_color = '';
						if($deliverableREC['StatusID'] == 1 ) $label_color = 'success';
						if($deliverableREC['StatusID'] == 2 ) $label_color = 'warning';
						if($deliverableREC['StatusID'] == 3 ) $label_color = 'danger';
						if($deliverableREC['StatusID'] > 3 ) $label_color = 'info';
						?>
						
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
						<p class="approximate_complete">
						<?= ($deliverableREC['ApproxComplete'] == 100 ? "Complete " :
						     $deliverableREC['ApproxComplete']."%")?>
						</p>
						     
						<p style="font-size:1.2em;" class="label label-
						<?= $label_color ?>"><?= $deliverableREC['StatusDescription'] ?>
						</p>
						
					</div>
				</div> -->
				<!-- EO COLOR STAT -->
				
			</div>
			<!-- EO ROW-A -->

			<!-- SO ROW B -->
			<div class="row">

				<!-- SOB col-sm-12 BL -->
				<div class="col-sm-12">
				
					<div style="padding-bottom: 15px;">
				
						<?= $deliverableREC['DeliverableDesc'] ?>

                        <div class="row">
                            <!-- SO Col-sm-4 -->
                            <div class="col-sm-4">

                                <p class="" style=""><b>
                                    Completion Target:
                                    <?php
                                    $trueInitialDate 	= '';
                                    $trueInitialDate 	= $deliverableREC['InitialTarget'];
                                    if(!$trueInitialDate)
                                    {
                                        $trueInitialDate = 'TBD';
                                    }
                                    else
                                    {
                                        $trueInitialDate = date('M d, Y', strtotime(substr($trueInitialDate,0,10)));
                                    }
                                    print $trueInitialDate;
                                    ?>
                                    </b>
                                </p>
                            </div>
                        </div>

						<!-- SO ROW C -->
						<div class="row">
							
							<div class="col-sm-4"><!-- SO OutcomeDesc -->
							
								<p><strong>Outcome: &nbsp; <?= $deliverableREC['OutcomeName'] ?>
								<!-- (<?= $deliverableREC['FocusAreaName'] ?>) --></strong></p>
								<p><?= $deliverableREC['OutcomeDesc'] ?></p>
							
							</div><!-- EO Outcome Name - FA - OutcomeDesc -->
							
							<div class="col-sm-4"><!-- SO RiskDesc -->
							
								<p><strong>Risk: &nbsp; <?= $deliverableREC['RiskDescShort'] ?></strong></p>
								<p><?= $deliverableREC['RiskDesc'] ?></p>
							
							</div><!-- EO RiskDesc -->
							
							<!-- SO AdminPrior -->
							<div class="col-sm-4">
							
								<p><strong>Admin Priority: &nbsp; <?= $deliverableREC['AdminPriorityName'] ?></strong></p>
							
								<!-- SOC AdminPrior -->
								<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverableREC['AdminPriorityID'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($adminpriorities as $rt) 
										{
										if($rt['AdminPriorityID'] == $realID)
										{
										$realVL = $rt['AdminPriorityDesc'];
										}
										}
									}
								}
								else 
									{ $realVL = "N/A"; }

								echo '<p>' . $realVL . '</p>';
								?><!-- EOC AdminPrior -->	  
								
							</div><!-- EO AdminPrior -->

						</div><!-- EO ROW- C -->
						
						
				<?php 
					/*
					 * ------------------------------------------------------------------------
					 * 		This section below was added with the 201601 Changes in order
					 * 		to display the extra fields now shown on the list and detail 
					 * 		deliverable display.
					 * 
					 * 		Future versions of this may show a list in columns as the desire
					 * 		by finance is to have this as a zero to many capability.
					 * -----------------------------------------------------------20160126 PRSC
					 */
					
					?>

					<!-- SO ROW D -->
					<div class="row">
			
					<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service FOR (Impacted)
					 * =-------------------------------------------------  PRSC
					 */
					?>
					
						<!-- SO col-sm-4 SF -->					
						<div class="col-sm-4">
								<p><strong>Service Impacted by Initiative: &nbsp; </strong></p>
								<p>
								
								<!-- SOC ServiceFOR -->
								<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverableREC['ServiceFOR'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($services as $rt) 
										{
										if($rt['ServiceID'] == $deliverableREC['ServiceFOR'])
										{
											$biu_Name = "";
											$servarea_Name = "";
										    foreach ($business_units as $st) 
											{
												
											if($st['BusinessUnitID'] == $rt['BusinessUnitID'])
											$biu_Name = $st['BusinessUnitName'];
											}
											
											foreach ($serviceareas as $st) 
											{
											if($st['ServiceAreaID'] == $rt['ServiceAreaID'])
											$servarea_Name = $st['ServiceAreaName'];
											}
									
//										$realVL = $biu_Name . "->" . $servarea_Name . '->' . 
//													$rt['ServiceShortNM'];
										$realVL = $rt['ServiceShortNM'] . " (" . $biu_Name . "," . $servarea_Name . ")";
										}
										}
									}
								}
								else 
									{ $realVL = "N/A"; }

								echo $realVL;	  
								?>
								<!-- EOC ServiceFor -->
								</p>
	
							</div>
							<!-- EO col-sm4 SF -->
						
						
					<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service BY
					 * =-------------------------------------------------  PRSC
					 */
					?>
							<!-- SO Col-sm-8 -->
							<div class="col-sm-8">
							<p><strong>Service Performing this Initiative: &nbsp; </strong></p>
							
							<p>
							
							<!-- SOC ServiceBY -->
							<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverableREC['ServiceBY'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($services as $rt) 
										{
										$biu_Name = "";
										$servarea_Name = "";
										
										if($rt['ServiceID'] == $deliverableREC['ServiceBY'])
										{
										    foreach ($business_units as $st) 
											{
												
											if($st['BusinessUnitID'] == $rt['BusinessUnitID'])
											$biu_Name = $st['BusinessUnitName'];
											}
											
											foreach ($serviceareas as $st) 
											{
											if($st['ServiceAreaID'] == $rt['ServiceAreaID'])
											$servarea_Name = $st['ServiceAreaName'];
											}
										
//										$realVL = $biu_Name . "->" . $servarea_Name . '->' . 
//													$rt['ServiceShortNM'];
										$realVL = $rt['ServiceShortNM'] . " (" . $biu_Name . "," . $servarea_Name . ")";
										}
										}
									}
								}
								else 
									{ $realVL = "N/A"; }
								echo $realVL;	  
								?>
								<!-- EOC ServiceBy -->
								</p>
							</div>
							<!-- EO col-sm8 -->

		
					</div>
					<!-- EO ROW-D -->	
						
					<!-- SO ROW E -->
					<div class="row">
						
						
					<?php 
					/*
					 * ------------------------------------------------------------------------
					 * 		This section below was added with the 201601 Changes in order
					 * 		to display the extra fields now shown on the list and detail 
					 * 		deliverable display.
					 * 
					 * 		Future versions of this may show a list in columns as the desire
					 * 		by finance is to have this as a zero to many capability.
					 * -----------------------------------------------------------20160126 PRSC
					 */
					
					?>

							<!-- SO Col-sm-4 -->
							<div class="col-sm-4">
								<p><strong>Source: &nbsp; </strong>
							
							<!-- SOC Source -->
							<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverableREC['Source'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($sources as $rt) 
										{
										if($rt['SourceID'] == $deliverableREC['Source'])
										$realVL = $rt['SourceShortName'];
										}
									}
								}
								else 
									{ $realVL = "N/A"; }
								echo $realVL;	  
								?>
								<!-- EOC Source -->
								
								</p>
							</div>
							<!-- EO col-sm-4 -->
	
							<!-- SO col-sm-8 -->
							<div class="col-sm-8">
								<p><strong>Published (Yes/No): &nbsp; </strong>
								
							<!-- SOC PublishedYN -->	
							<?php 
								$realID = 0;
								$realVL	= 'N/A';
								$realID = $deliverableREC['PublishedYN'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($publishedinfo as $rt) 
										{
										if($rt['PublishedID'] == $deliverableREC['PublishedYN'])
										$realVL = $rt['PublishedShortName'];
										}
									}
								}
								else 
									{ $realVL = 'N/A'; }
								echo $realVL;	  
								?>
								<!-- EOC Published YN -->
								
								</p>								
						</div><!-- EO col-sm-8 -->			

					</div>
					<!-- EO Row D -->

					
					<!-- SO ROW E -->
					<div class="row">
							<!-- SO Col-sm-4 -->
							<div class="col-sm-4">
                                <?php
                                $style = '';
                                $truedate 	= '';
                                $targ_date 	= $deliverableREC['ApproxEndDate'];
                                if(!$targ_date)
                                {
                                    $truedate = 'TBD';
                                }
                                else
                                {
                                    $truedate = date('M d, Y', strtotime(substr($targ_date,0,10)));

                                    if ($trueInitialDate != 'TBD') {
                                        if (strtotime($truedate) > strtotime($trueInitialDate)) {
                                            $style = 'color:red';
                                        }
                                    }
                                }
                                ?>

							<p class="" style="">
                                <b>
                                    Currently Estimated to Complete:
                                    <span style="<?php echo $style ?>">
                                    <?php
                                        print $truedate;
                                    ?>
                                    </span>
                                    </b>
								</p>
							</div>	
					</div>
					<!-- EO Row E -->


					</div><!-- EO sml-12 BL -->
				</div>
				<!-- EO Row 00 -->

		<?php 

		// These should not be here TODO
		print '</div>';
		print '</div>';
		?>



		<?php 
		/*--------------------------------------------------------------------------------------------------
		 * 		This section is used to show the scroll bar for percentage of complete of each deliverable.
		 *--------------------------------------------------------------------------------------------------
		 */
		?>

		<div class="col-sm-12">
			<div class="row">
				<?php
					$label_color = '';
					if($deliverableREC['StatusID'] == 1 ) $label_color = 'success';
					if($deliverableREC['StatusID'] == 2 ) $label_color = 'warning';
					if($deliverableREC['StatusID'] == 3 ) $label_color = 'danger';
					if($deliverableREC['StatusID'] > 3 ) $label_color = 'info';
					?>
					
				
				<?php 
				/*--------------------------------------------------------------------------
				 * 		This shows the actual Status Details.  This should only display if
				 * 		the current general AD user is from the same AD Business Unit AND
				 * 		This 
				 *----------------------------------------------------------- PRSC 20160814-
				 */

							
				if($glb_show_percbarYN == true):			
				
				?>
					
				<div class="progress_bg" title="<?= $deliverableREC['StatusDescription'] ?>">
					<div class="completion_label label label-<?= $label_color ?>" 
								style="width:<?= $deliverableREC['ApproxComplete'] ?>%">
					
						<span class="label_text"><?= $deliverableREC['StatusDescription'] ?> -
						 <?= ($deliverableREC['ApproxComplete'] == 100 ? "Completed" : 
						 			$deliverableREC['ApproxComplete']."% complete")?><span>
					</div>
				</div>
				<?php endif ?>
				
			</div>
		</div>



	<?php 
	
		/*
		 * 		Show the Status Area if this is a Friendly Operational Deliverable
		 */
	
		if($glb_showStatusDetailsYN  == true ): ?>

		<div class="col-sm-12">

			<div class="row" style="background:#f2f2f2;">

				<div class="col-sm-12">

					<div class="text_block ">
						
						<h1>Status</h1>
						<?php if(empty($deliverableREC['StatusUpdate'])): ?>
						<p class="muted"><em>No update at this time.</em></p>
						<?php else: ?>
						<?= $deliverableREC['StatusUpdate'] ?>
						<?php endif ?>

					</div>

					<div class="text_block">

						<?php if(!empty($deliverableREC['StatusUpdate'])): ?>
						<p class="muted text-right">
							<em>
								Updated <?= date('M d, Y', 
										strtotime(substr($deliverableREC['LastModDate'],0,10))) ?> 
								<br class="visible-xs"> by <?= $deliverableREC['LastModBy'] ?>.
							</em>
						</p>
						<?php endif ?><!-- EO if there is a status update to display -->
						
					</div>

				</div>

			</div>

		</div>
	    <?php endif ?><!-- EO If in Admin mode show this -->
 
  
  
  
  
  <!-- START BLOC -- Show Strategic Linked Deliverables -->      
        
<?php
 	/*---------------------------------------------------------------------
  	* 
  	* 	This is used with the Strategic Listings to build a list of 
  	* 	links that allows users to update the status information on 
  	* 	each of the related Deliverables to this parent Strategic one.
  	* 
  	* 	NOTE:		Delivery Types are
  	* 	1 - Strategic (Show only related children )
  	* 	2 - Children (Show only related parents)
  	* 	3 - All
  	* -----------------------------------------------------20160222 PRSC
  	*/
?>
		<?php 
		$XCOND_ShowRelatedParents 		= FALSE;
		$XCOND_ShowRelatedChildren		= FALSE;
		$XDeliveryType					= $ddfilters['DeliverableTypeID'];
		
		// Check if Show the Strategic Values
		if($XDeliveryType == 2 && $CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedParents 	= TRUE; }
				
		// Check if Show the Operational Values		
		if($XDeliveryType == 1 && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedChildren	= TRUE; }
				
		// Check if base view requires showing both		
		if($XDeliveryType == 3 && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedChildren 	= TRUE; }
		if($XDeliveryType == 3 && $CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedParents	= TRUE; }
		// Check if base view requires showing both		
		if($XDeliveryType == '' && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedChildren 	= TRUE; }
		if($XDeliveryType == '' && $CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedParents	= TRUE; }
				
//		print "TYPE [" . $XDeliveryType . "]<br>";		
//		print "CHILD CNT [" . $CNT_childrenxx . "]<br>";		
//		print "STRAT CNT [" . $CNT_parentsxx . "]<br>";		
		
//		$XCOND_ShowRelatedParents 		= TRUE;
//		$XCOND_ShowRelatedChildren		= TRUE;
		
		?>
	
					
		<?php		
		/*-----------------------------------------------
		 * 		Strategics Header
		 * -----------------------------------------------
		 */
		?>
		<?php if($XCOND_ShowRelatedParents == TRUE ): ?>		
		<div class="row">	
		<div class="col-sm-12 supporting_deliverables">
			<p class="">

			<?php 
				echo 'Related Strategic Initiatives';
			?>

			</p>
		<ul>
		<?php
		/* -------------------------------------------------------------
		 * 			Check if Parent Deliverables to show
		 * -------------------------------------------------------------
		 */
		
		foreach ($PRNT_Deliverables as $rcdt): ?>
				<?php
					$label_color = '';
					if($rcdt['statusid'] == 1 ) $label_color = 'success';
					if($rcdt['statusid'] == 2 ) $label_color = 'warning';
					if($rcdt['statusid'] == 3 ) $label_color = 'danger';
					if($deliverableREC['StatusID'] > 3 ) $label_color = 'info';
					?>
					
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $rcdt['id'] ?>">
						<?= $rcdt['title'] ?>
					</a>
					
					 <!-- - <?= $rcdt['percent'] ?>% completed 
					    <?= ($rcdt['percent'] < 100) ? '('.$rcdt['status'].
					            ')' : '' ?>  -->
					 - <span title="<?= $rcdt['status'] ?> ">
					          <?= $rcdt['percent'] ?>% completed</span>
				</li>
				<?php endforeach ?>

			</ul>
		</div>
		</div>
		
		<?php endif ?>
		
		
		
		<!-- EO Display ALL Related Supporting Deliverables - Strategic or Operational -->
	    
	
	
		<?php		
		/*-----------------------------------------------
		 * 		Operationals(Child) Header
		 * -----------------------------------------------
		 */
		if($XCOND_ShowRelatedChildren == TRUE):
		?>		
		<div class="row">	
		<div class="col-sm-12 supporting_deliverables">
			<p class="">

			<?php 
				echo 'Related Operational Initiatives';
			?>

			</p>
		<ul>
		<?php
		/* -------------------------------------------------------------
		 * 			Check if Child Deliverables to show
		 * -------------------------------------------------------------
		 */
		
		foreach ($CHLD_Deliverables as $rcdt): ?>
				<?php
					$label_color = '';
					if($rcdt['statusid'] == 1 ) $label_color = 'success';
					if($rcdt['statusid'] == 2 ) $label_color = 'warning';
					if($rcdt['statusid'] == 3 ) $label_color = 'danger';
					if($deliverableREC['StatusID'] > 3 ) $label_color = 'info';
					?>
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $rcdt['id'] ?>">
						<?= $rcdt['title'] ?>
					</a>
					
					 <!-- - <?= $rcdt['percent'] ?>% completed 
					    <?= ($rcdt['percent'] < 100) ? '('.$rcdt['status'].
					            ')' : '' ?>  -->
					 - <span title="<?= $rcdt['status'] ?> ">
					          <?= $rcdt['percent'] ?>% completed</span>
				</li>
				<?php endforeach ?>
          
			</ul>
		</div>
		</div>
		<?php endif ?><!-- EO Display ALL Related Operationals -->
 
   <!--  EO Show Related Deliverables -->
     
 <?php
//     print "</div>";
     print "</div>";
	print "<br>";
	?>
        
    <?php endif ?><!-- EO IF show This Deliverable yes no -->    
        
        
   <!--  END OF -->     
	<?php 
//	    print "  <br>";
//	    print ' <font color="#AAAAEE">--</font><br>';
	 	$count++;
		endforeach;
		
	?><!-- EO For Each deliverable -->

	<?php $this->load->view('template/copyright') ?>
	<?php endif ?><!-- EO IF count deliverables greater then 0 -->

</div>
</div>



