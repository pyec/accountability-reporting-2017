<?php 
	/*=====================================================================
	 * 
	 * 		MODULE:		pg_show_preferences
	 * 		AUTHOR:		R.Stephen Chafe (Zen River Software)
	 * 		CREATED:	201602xx
	 * 		
	 * 		This module handles the list format web page for 
	 * 		Performances data records.
	 * 
	 * ====================================================================
	 */
?>

<?php 
	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($performances);
//	 echo "---------------------------------------<br>";
	 
//	 print_r($services);
	 
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>


	
<?php if (count($performances)>0): ?>

<div class="col-sm-12">

	<div class="results">    
	
	<!-- ------------------------------------------------------------------EO-->  
	


		<div class="pull-right hidden-xs">
			<?php if (count($performances)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/  ?>
		<?= form_open(base_url().'download-performances') ?>
					<input type="hidden" name="PerformanceID" value="<?= set_value('PerformanceID') ?>">
				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>
	
		</div>
		
		
		
		<div class="result_count">
			<p>
				<?php 								/* TITLES */
					echo count($performances);
					echo ' KPI PERFORMANCE List';
					echo (count($performances) == 1 ? '' : 'ings'); 
				?>
			</p>
		</div>
			<?php endif ?>
		</div>
	
	</div>




<?php 

	$performance_id 		= '';
	$performance_parent_id 	= '';
	$count	= 0;

	// echo "<pre>";
	// print_r($performances);
	// echo "</pre>";


	foreach ($performances as $performanceDT): 
	
		$supporting_deliverable_count = 0;
	?>

<div class="col-sm-12">

   
	<?php if (count($performances)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	If there are any SERVICES data in the Array then loop through
	 		*	and display each one in a easy to read format.
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	?>


	<?php 
	$performance_id 		= '';
	$performance_parent_id 	= '';
	$count	= 0;

	
	?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed Performance Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
	?>




<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-performance/<?= $performanceDT['PerformanceID'] ?>" 
							title="Update performance details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						<?php /* $performanceDT['PerformanceID'] */ ?>
						<?= $performanceDT['PerformanceShortNM'] ?> [  <?= $performanceDT['PerformanceCD']; ?> ]
						 </h1>
						
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						/* CNIU - but leave in as it will be used in future version PRSC */
						$label_color = 'success';
						//if($performances['StatusID'] == 1 ) $label_color = 'success';
						//if($performances['StatusID'] == 2 ) $label_color = 'warning';
						//if($performances['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
					</div>
				</div>

			</div>

			<div class="row">

					<div class="col-sm-4">
								<p><strong>Performance Description: &nbsp;</strong></p>
					</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $performanceDT['PerformanceDesc'] ?>
				</div>	
			
			</div>

			<div class="row">

					<div class="col-sm-4">
								<p><strong>&nbsp &nbsp Performance Code: &nbsp; </strong></p>
					
					
							<p>&nbsp &nbsp
							<?php 
												
									$value = $performanceDT['PerformanceCD'];
									echo $value;	
							?>
							</p>							
					</div>



				<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service 
					 * =-------------------------------------------------  PRSC
					 */
					?>
					
						<div class="col-sm-4">
								<p><strong>Service: &nbsp; </strong></p>
								<p><?php 
								$realID = 0;
								$realVL	= "";
								$realID = $performanceDT['ServiceID'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($services as $rt) 
										{
										if($rt['ServiceID'] == $performanceDT['ServiceID'])
										{
											$biu_Name = "";
											$servarea_Name = "";
										    foreach ($business_units as $st) 
											{
												
											if($st['BusinessUnitID'] == $rt['BusinessUnitID'])
											$biu_Name = $st['BusinessUnitName'];
											}
											
											foreach ($serviceareas as $st) 
											{
											if($st['ServiceAreaID'] == $rt['ServiceAreaID'])
											$servarea_Name = $st['ServiceAreaName'];
											}
									
										$realVL = $biu_Name . "->" . $servarea_Name . '->' . $rt['ServiceShortNM'];
//										$realVL = '<option value="' .  $vl  . '">' . "( " . $buCD . ' , ' . $saCD . ' ) ' . 	
//				    					$dt['ServiceShortNM'] . '</option>';;
										}
										}
									}
								}
								else 
									{ $realVL = "N/A"; }

								echo $realVL;	  
								?></p>
	
							</div>
					</div>
	
				

		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>


	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>






