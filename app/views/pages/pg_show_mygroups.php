<?php 
	/*=====================================================================
	 * 
	 * 		MODULE:		pg_show_mygroups
	 * 		AUTHOR:		R.Stephen Chafe (Zen River Software)
	 * 		CREATED:	201602xx
	 * 		
	 * 		This module handles the list format web page for 
	 * 		Services data records.
	 * 
	 * 		MODIFICATIONS:
	 * 		20160311		PRSC	Display format changed for K.Couture
	 * 
	 * ====================================================================
	 */
?>


<?php 
	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
	 echo "<pre>";
	 print_r($userinfoAD);
	 echo "</pre>";
?>	
<br><br><br><br><br><br>







