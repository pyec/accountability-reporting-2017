<?php 

 	/*================================================================
	 * 
	 * 	MODULE:			pg_show_focusareas.php
	 * 	AUTHOR:			R.Stephen Chafe (Zen River Software)
	 * 	CREATED:		2016_03_06
	 * 
	 * 	This module contains the LIST display for all of the 
	 *  focusareas data.
	 *  
	 * ==============================================================
	 */



	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($focusareas);
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>


	
<?php if (count($focusareas)>0): ?>


<div class="col-sm-12">

	<div class="results">    
	
	<!-- ------------------------------------------------------------------EO-->  
	
		<div class="pull-right hidden-xs">
			<?php if (count($focusareas)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	 		
			?>
			</form>
			
			
		</div>
		
		
		
		<div class="result_count">
			<p>
				<?php 								/* TITLES */
					echo count($focusareas);
					echo ' FOCUS AREA List';
					echo (count($focusareas) == 1 ? '' : 'ings'); 
				?>
			</p>
		</div>

			<?php endif ?>
		</div>
	
	</div>


<?php 

	$adminpillar_id 		= '';
	$adminpillar_parent_id 	= '';
	$count	= 0;



	foreach ($focusareas as $focusareaDT): 
	

	?>

<div class="col-sm-12">

	<?php if (count($focusareas)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	If there are any SERVICES data in the Array then loop through
	 		*	and display each one in a easy to read format.
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	?>


	<?php 
	$adminpillar_id 		= '';
	$adminpillar_parent_id 	= '';
	$count	= 0;

	
	?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed Focus Area Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
	?>




<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-focusarea/<?= $focusareaDT['FocusAreaID'] ?>" 
							title="Edit Focus Area details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						<?php //<?= $focusareaDT['FocusAreaID'] - PRSC Removed temp?> 
						 <?= $focusareaDT['FocusAreaName'] ?>
						 </h1>
						
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						/* CNIU - but leave in as it will be used in future version PRSC */
						$label_color = 'success';
						//if($focusareas['StatusID'] == 1 ) $label_color = 'success';
						//if($focusareas['StatusID'] == 2 ) $label_color = 'warning';
						//if($focusareas['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
					</div>
				</div>

			</div>

			<div class="row">

					<div class="col-sm-4">
								<p><strong>Focus Area Description: &nbsp;</strong></p>
					</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $focusareaDT['FocusAreaDesc'] ?>
					</div>	
					
				</div>
			</div>
			
				<div class="row">

				<div class="col-sm-4">
								<p><strong>Focus Area Code: &nbsp;</strong></p>
				</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $focusareaDT['FocusAreaCode'] ?>
					</div>	
			
				</div>
			</div>	


		
					<div class="row">
						<div class="col-sm-4">
								<p><strong>Focus Area Type: &nbsp; </strong></p>
								
							<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $focusareaDT['FocusAreaTypeID'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($focusareatypes as $rt) 
										{
										if($rt['FocusAreaTypeID'] == $focusareaDT['FocusAreaTypeID'])
										$realVL = $rt['FocusAreaTypeName'];
										}
									}
								}
								else 
									{ $realVL = ""; }
								echo $realVL;	  
								?></p>								
						</div>			
			
						
	
			
				

		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>


	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>







