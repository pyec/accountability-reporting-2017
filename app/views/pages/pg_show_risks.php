<?php 
	/*=====================================================================
	 * 
	 * 		MODULE:		pg_show_risks
	 * 		AUTHOR:		R.Stephen Chafe (Zen River Software)
	 * 		CREATED:	201602xx
	 * 		
	 * 		This module handles the list format web page for 
	 * 		Risks data records.
	 * 
	 * 		MODIFICATIONS:
	 * 		20160311		PRSC	Display format changed for K.Couture
	 * 
	 * ====================================================================
	 */
?>

<?php 
	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($risks);
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>


	
	


<?php if (count($risks)>0): ?>

<div class="col-sm-12">

	<div class="results">    
	
	<!-- ------------------------------------------------------------------EO-->  
	


		<div class="pull-right hidden-xs">
			<?php if (count($risks)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	 		
			?>

 			<?= form_open(base_url().'download-risks') ?>
					<input type="hidden" name="RiskID" value="<?= set_value('RiskID') ?>">
				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>

		</div>
		
		
		
		<div class="result_count">
			<p>
				<?php 								/* TITLES */
					echo count($risks);
					echo ' RISK List';
					echo (count($risks) == 1 ? '' : 'ings'); 
				?>
			</p>
		</div>

			<?php endif ?>
		</div>
</div>



<?php 

	$risk_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;

	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";

	// echo "<pre>";
	// print_r($supporting_deliverables);
	// echo "</pre>";

	foreach ($risks as $risk): 
	
		$supporting_deliverable_count = 0;

		if(!empty($supporting_deliverables))
		{

			foreach ($supporting_deliverables as $supporting_deliverable)
			{
				// if operational
				// if(set_value('DeliverableTypeID') == 2)
					// otherwise assume strategic
			
			}
		
		}
		// echo "<pre>";
		// print_r($related_supporting_deliverables);
		// echo "</pre>";
	?>

<div class="col-sm-12">

	
 
    <!-- ------------------------------------------------------------------EO-->  
   
	<?php if (count($risks)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	If there are any RISKS data in the Array then loop through
	 		*	and display each one in a easy to read format.
	 		* -----------------------------------------------------PRSC 201601
	 		>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 		*/  
	?>


	<?php 
	$risk_id 		= '';
	$risk_parent_id 	= '';
	$count	= 0;

	
		// echo "<pre>";
		// print_r($related_supporting_risks);
		// echo "</pre>";
	?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed Risk Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
	?>




<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-risk/<?= $risk['RiskID'] ?>" title="Update risk details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						 <?= $risk['RiskCode'] ?> - <?= $risk['RiskDescShort'] ?></a></h1>
						
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						/* CNIU - but leave in as it will be used in future version PRSC */
						$label_color = 'success';
						//if($risk['StatusID'] == 1 ) $label_color = 'success';
						//if($risk['StatusID'] == 2 ) $label_color = 'warning';
						//if($risk['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
					</div>
				</div>

			</div>

			<div class="row">

					<div class="col-sm-4">
								<p><strong>Risk Description: &nbsp;</strong></p>
						<?= $risk['RiskDesc'] ?>
				</div>	
		
				<div class="col-sm-4">
								<p><strong>Statement: &nbsp; </strong></p>
					<div style="padding-bottom: 15px;">
					<p><?= $risk['RiskStatement'] ?> </p>
				</div>	

		</div>			
		<div class="row">

				
							
				<div class="col-sm-4">
								<p><strong> &nbsp Factors: &nbsp; </strong></p>
								<p><strong>&nbsp Likelihood,Net: &nbsp; </strong><?= $risk['Likelihood'] ?></p>
								<p><strong>&nbsp Impact, Net: &nbsp; </strong><?= $risk['Impact'] ?></p>
								<p><strong>&nbsp Heat Map Rating: &nbsp; </strong>
								
								
							<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $risk['HeatMapRating'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($heatmapratings as $rt) 
										{
										if($rt['HeatMapRateID'] == $risk['HeatMapRating'])
										$realVL = $rt['HeatMapRateShortName'];
										}
									}
								}
								else 
									{ $realVL = ""; }
								echo $realVL;	  
								?></p>								
								
								
								<p><strong>&nbsp Risk Owner: &nbsp; </strong><?= $risk['RiskOwner'] ?></p>

							</div>
			</div>
				
				
	
		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>


	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>






