<?php 
	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($priorityoutcomes);
//	 echo "---------------------------------------<br>";
	 
//	 print_r($services);
	 
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>


	
<?php if (count($priorityoutcomes)>0):?>


<div class="col-sm-12">

	<div class="results">    
	
	<!-- ------------------------------------------------------------------EO-->  
	


		<div class="pull-right hidden-xs">
			<?php if (count($priorityoutcomes)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	 		
			?>
		</div>
		
		
		
		<div class="result_count">
			<p>
				<?php 								/* TITLES */
					echo count($priorityoutcomes);
					echo ' PRIORITY OUTCOME List';
					echo (count($priorityoutcomes) == 1 ? '' : 'ings'); 
				?>
			</p>
		</div>

			<?php endif ?>
		</div>
</div>



<?php 

	$priorityoutcome_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;


	foreach ($priorityoutcomes as $priorityoutcomeDT): 
	
	?>

<div class="col-sm-12">
		
	
 
    <!-- ------------------------------------------------------------------EO-->  
   
	<?php if (count($priorityoutcomes)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	If there are any data in the Array then loop through
	 		*	and display each one in a easy to read format.
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	?>


	<?php 
	$priorityoutcome_id 		= '';
	$priorityoutcome_parent_id 	= '';
	$count	= 0;

	?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed Priority Outcome Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
	?>




<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-priorityoutcome/<?= $priorityoutcomeDT['OutcomeID'] ?>" 
							title="Edit Priority Outcome details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						<?php  /* $priorityoutcomeDT['OutcomeID'] */ ?>
						<?= $priorityoutcomeDT['OutcomeName'] ?>
						 </h1>
						
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						/* CNIU - but leave in as it will be used in future version PRSC */
						$label_color = 'success';
						//if($priorityoutcomes['StatusID'] == 1 ) $label_color = 'success';
						//if($priorityoutcomes['StatusID'] == 2 ) $label_color = 'warning';
						//if($priorityoutcomes['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
					</div>
				</div>

			</div>

			<div class="row">

					<div class="col-sm-4">
								<p><strong>Priority Outcome Description: &nbsp;</strong></p>
					</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $priorityoutcomeDT['OutcomeDesc'] ?>
					</div>	
					
				</div>
			</div>
	
	
									
	
	
	
	
			
			<div class="row">

				<div class="col-sm-4">
								<p><strong>Focus Area ID: &nbsp;</strong></p>
				</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?php 
					$realID = 0;
					$realVL	= "";
					$realID = $priorityoutcomeDT['FocusAreaID'];
					if($realID)
					{
						if($realID > 0)
						{
							foreach ($focusareas as $rt) 
							{
							if($rt['FocusAreaID'] == $priorityoutcomeDT['FocusAreaID'])
							$realVL = "(" . $rt['FocusAreaCode'] . ") " . $rt['FocusAreaName'];
							}
						}
					}
					else 
						{ $realVL = ""; }
						echo $realVL;	  
					?></p>								
					</div>	
			
				</div>
			</div>	




	
			
				

		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>


	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>








