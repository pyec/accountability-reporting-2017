<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($service);
// echo "</pre>";

//	 echo "<pre>";
//	 print_r($business_units);
//	 echo "</pre>";

?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($service)) 
				{
				echo form_open('create-service');
				}
			else
				{
				echo form_open('update-service');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Service ID;
			if(!empty($service)) 
			         echo form_hidden('ServiceID',$service['ServiceID']);
			?>
 
  				<h1>
 				 <?= (!empty($service)) ? 'Edit' : 'Create' ?> Service</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
	 
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Service ShortName Desc
		 * -----------------------------------------------------PRSC 201601
		 */	?>


			<div class="row">
					<div class="col-sm-4">
						<label for="ServiceShortNM">Service Name (250-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="ServiceShortNM" class="form-control"
						 value="<?= set_value('ServiceShortNM', (!empty($service) ? 
						 $service['ServiceShortNM'] : '')) ?>" maxlength="250">
					</div>
			</div>

			<hr>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Service Short CD
		 * -----------------------------------------------------PRSC 201602
		 */	?>

			<div class="row">
					<div class="col-sm-4">
						<label for="ServiceShortCD">Service Short Code (20-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="ServiceShortCD" class="form-control"
						 value="<?= set_value('ServiceShortCD', (!empty($service) ? 
						 $service['ServiceShortCD'] : '')) ?>" maxlength="20">
					</div>
			</div>

		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Business Unit (Impacts Serv Area)
		 * -----------------------------------------------------PRSC 201601
		 */	?>


	
				<div class="row">
					<div class="col-sm-4">
						<label for="BusinessUnitID">Business Unit</label>
					</div>
										
					<div class="col-sm-8">			
						<select class="form-control" id="BusinessUnitID" name="BusinessUnitID">
							<option value="">Select a business unit</option>
							<?php foreach ($business_units as $rt): ?>
							<option name="<?= $rt['BusinessUnitCode'] ?>" 
								value="<?= $rt['BusinessUnitID'] ?>"
								<?= set_select('BusinessUnitID', $rt['BusinessUnitID'], 
								   ((!empty($service) && $service['BusinessUnitID'] == $rt['BusinessUnitID']) ? TRUE : '')) ?>>
								   (<?= $rt['BusinessUnitCode'] ?>) <?= $rt['BusinessUnitShortName'] ?> 
								   </option>
							<?php endforeach ?>
						</select>
					</div>
					
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Service Area
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="ServiceAreaID">Service Area</label>
					</div>
					<div class="col-sm-8">
						<em class="service_area_description _BU_dependency">Select a business unit above</em>

					<?php 
						$BUID = '';
						foreach ($business_unit_service_areas as $busa)
						{
						if(empty($BUID)) 
							{
							$BUID  = $busa['BusinessUnitID'];
					?>
		
		
						<select class="form-control business_unit_dependency 
							<?= $BUID ?>_BU_dependency"
						 	name="ServiceArea<?= $BUID ?>">
					
						<option value="">Select a service area</option>
					
						<option value="<?= $busa['ServiceAreaID'] ?>"
								 <?= set_select('ServiceArea'.$BUID,
							 	 $busa['ServiceAreaID'],
							 	  ((!empty($service) && $service['ServiceAreaID']
							 	   == $busa['ServiceAreaID']) ? TRUE : '')) ?>>
								  <?= $busa['ServiceAreaName'] ?></option>
				
						<?php
						}
						elseif($BUID != $busa['BusinessUnitID']) 
						{

						$BUID = $busa['BusinessUnitID'];
						?>
						</select>
			
						<select class="form-control business_unit_dependency <?= $BUID ?>_BU_dependency"
						 name="ServiceArea<?= $BUID ?>">
			
						<option value="<?= $busa['ServiceAreaID'] ?>"
							 <?= set_select('ServiceArea'.$BUID, $busa['ServiceAreaID'],
							  ((!empty($service) && $service['ServiceAreaID'] ==
							   $busa['ServiceAreaID']) ? TRUE : '')) ?>
								  ><?= $busa['ServiceAreaName'] ?></option>
						
						<?php
						}
						else
						{
						?>
				
				
					<option value="<?= $busa['ServiceAreaID'] ?>" 
					<?= set_select('ServiceArea'.$BUID, 
							$busa['ServiceAreaID'], 
							((!empty($service) && $service['ServiceAreaID'] ==
							 $busa['ServiceAreaID']) ? TRUE : '')) ?>>
							 <?= $busa['ServiceAreaName'] ?>
							 
					 </option>
					
					<?php
					}
				}
			?>

		</select>
	</select>
	</div>
	</div>

				<script type="text/javascript">
					$(function() {
						$('.business_unit_dependency').hide();
						<?php 
							//$BUID = $this->input->post('BusinessUnitID');
							if($this->input->post('BusinessUnitID') != ''): 
						?> 
						//$('.service_area_description').hide();
						//$('.' + <?php echo $this->input->post('BusinessUnitID') ?>+'_BU_dependency').show();
						<?php endif ?>

						if($('#BusinessUnitID').val() != '') {
							$('.service_area_description').hide();
							$('.' + $('#BusinessUnitID').val() + '_BU_dependency').show();
				        }
				        
				        $('#BusinessUnitID').change(function(){
				            $('.business_unit_dependency').hide();
				            $('.service_area_description').hide();
				            $('.' + $(this).val()+'_BU_dependency').show();
				        });
				    });
				</script>


	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Service Description (FULL)
		 * -----------------------------------------------------PRSC 201601
		 */	?>


				<label for="ServiceDESC">Service Description (4000-Digits)</label>
				<textarea name="ServiceDESC" rows="15"><?= set_value('ServiceDESC', 
				(!empty($service) ? $service['ServiceDESC'] : '')) ?></textarea>
				<script>
		            CKEDITOR.replace( 'ServiceDESC', {
						toolbar: [
							{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },			// Defines toolbar group without name.
							{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
							{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
						]
					});
		        </script>

				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info"><i class="icon-ok icon-white"></i>
					 <?= (!empty($service)) ? 'Update' : 'Create' ?> Service</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>

