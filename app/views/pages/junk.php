		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Service   FOR 
		 * -----------------------------------------------------PRSC 201601
		 */	?>
	
		<div class="col-sm-4">
		<div class="filter_option">			
			<select class="form-control" id="ServiceFOR" name="ServiceFOR">

				    <?php 
				    $real = '';
				    
/*					foreach ($services as $rt) 
						{
						if($rt['ServiceID'] == $deliverable['ServiceID'])
							$real = $rt['ServiceShortNM'];
						}
*/						
				    if($real)
				    	echo '<option value="' . $deliverable['ServiceID'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Service Impacted by Initiative </option>';
					?>	
					
					 		 
					 <?php 

					 /* --------------------------------------------------------------
					  *		This code builds the drop down Services Box for both the
					  *		Create Screen and the Edit Screen 
					  * ------------------------------------------------20160217 PRSC
					  */
					 	 

				 // In a simple Create just show all the Services */   	
				 foreach ($services as $dt)
					{ 

				/*		<option name="<?= $dt['ServiceID'] ?>"
						 value="<?= $dt['ServiceID'] ?>"
					 		 <?= set_select('ServiceID', $dt['ServiceID'],
					 		 ((!empty($service) && $deliverable['ServiceID']
					 		 	 == $dt['ServiceID'])? TRUE : '')) ?>>
					}				    	
				*/
				    	
					//  Lookup the actual Service Area Code to show to user PRSC					
					$vdt = $dt['ServiceAreaID'];
					foreach ($serviceareas as $st)
					{
						if($vdt == $st['ServiceAreaID'])
						{
							$saCD = $st['ServiceAreaShortCD'];
						}
					}
						 		 
					//  Lookup the actual Business Unit Code to show to user PRSC					
					$vdt = $dt['BusinessUnitID'];
					foreach ($business_units as $st)
					{
					if($vdt == $st['BusinessUnitID'])
						{
							$buCD = $st['BusinessUnitCode'];
						}
					}
							 
					// Add each option to the drop down box
					$vl = $dt['ServiceID'];
//					print '<option value="' .  $vl  . '">' . "(" . $buCD . ")(" . $saCD . ') >' . $dt['ServiceShortNM'] . '</option>';;
					print '<option value="' .  $vl  . '">' . "( " . $buCD . " - " . $saCD . ' ) ' . $dt['ServiceShortNM'] . '</option>';;
					
				    	
				   
				 }		 
					 		 
				?>

					
				</select>
			</div>
		</div>
