<?php $this->session->set_flashdata('uri', uri_string()) ?>

<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">


	<?php if (count($deliverable)>0): ?>

	<?php 

	$deliverable_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;
	// $supporting_deliverable_count = 0;

		
		// // first time through
		// if($count == 0)
		// {
		// 	if($deliverable_id == '')			$deliverable_id = $deliverable['DeliverableID'];
		// 	//if($deliverable_parent_id == '')	$deliverable_parent_id = $deliverable['DeliverableParentID'];
		// }

		// // if this is an supporting deliverable and it's parent id is the same as the previously set deliverable id
		// elseif(($count > 0) && ($deliverable['DeliverableTypeID'] == 2) && ($deliverable['DeliverableParentID'] == $deliverable_id))
		// {
		// 	$supporting_deliverable_count++;
		// 	$count++;
		// 	continue;
		// }
		// elseif($count > 0)
		// {
		// 	echo '<div class="col-sm-12 text-center supporting_deliverable_count">'.$supporting_deliverable_count.' related supporting deliverable'.(($supporting_deliverable_count==1) ? '' : 's').'</div></div>';
		// 	$supporting_deliverable_count = 0;
		// 	$deliverable_id = $deliverable['DeliverableID'];
		// 	$deliverable_parent_id = $deliverable['DeliverableParentID'];

		// }

		$supporting_deliverable_count = 0;
		$related_supporting_deliverables = [];

		foreach ($supporting_deliverables as $supporting_deliverable)
		{
			// if($deliverable['DeliverableID'] == $supporting_deliverable['DeliverableParentID'])
			// {
			// 	$related_supporting_deliverables[$supporting_deliverable_count] = $supporting_deliverable['BusinessUnitCode'].' '.$supporting_deliverable['DeliverableCode'].' - '.$supporting_deliverable['DeliverableDescShort'];
			// 	$supporting_deliverable_count++;
			// }

			// if operational
		
					$related_supporting_deliverables[$supporting_deliverable_count]['title'] = $supporting_deliverable['BusinessUnitCode'].' '.$supporting_deliverable['DeliverableCode'].' - '.$supporting_deliverable['DeliverableDescShort'];
					$related_supporting_deliverables[$supporting_deliverable_count]['id'] = $supporting_deliverable['DeliverableID'];
					$related_supporting_deliverables[$supporting_deliverable_count]['percent'] = $supporting_deliverable['ApproxComplete'];
					$related_supporting_deliverables[$supporting_deliverable_count]['status'] = $supporting_deliverable['StatusDescription'];
					$related_supporting_deliverables[$supporting_deliverable_count]['statusid'] = $supporting_deliverable['StatusID'];
					$supporting_deliverable_count++;

		}
	?>

	<div class="block">
	
		<div class="col-sm-12">
			
			<div class="row">

				<div class="col-xs-7">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<h1><?= $deliverable['BusinessUnitCode'] ?> <?= $deliverable['DeliverableCode'] ?> - <?= $deliverable['DeliverableDescShort'] ?></h1>
						<p class="text-muted"><?= $deliverable['ServiceAreaName'] ?></p>
						<!-- <p class="text-muted" style="">Target Completion Date: <?= date('M d, Y', strtotime(substr($deliverable['ApproxEndDate'],0,10))) ?></p> -->
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						$label_color = '';
						if($deliverable['StatusID'] == 1 ) $label_color = 'success';
						if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
						if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
						<p style="font-size:2.5em; margin:5px 0 5px; padding:0; line-height:1em; font-weight:bold"><?= ($deliverable['ApproxComplete'] == 100 ? "Completed " : $deliverable['ApproxComplete']."%")?></p>
						<p style="font-size:1.2em;" class="label label-<?= $label_color ?>"><?= $deliverable['StatusDescription'] ?></p>
					</div>
				</div>

			</div>

		</div>


		<div class="col-sm-12">
			<div style="padding-bottom: 15px;">
			<?= $deliverable['DeliverableDesc'] ?>
			<div class="row">
				<div class="col-sm-4">
					<p><strong>Outcome: &nbsp; <?= $deliverable['OutcomeName'] ?> <!-- (<?= $deliverable['FocusAreaName'] ?>) --></strong></p>
					<p><?= $deliverable['OutcomeDesc'] ?></p>
				</div>
				<div class="col-sm-4">
					<p><strong>Risk: &nbsp; <?= $deliverable['RiskDescShort'] ?></strong></p>
					<p><?= $deliverable['RiskDesc'] ?></p>
				</div>
				<div class="col-sm-4">
					<p><strong>Admin Priority: &nbsp; <?= $deliverable['AdminPriorityName'] ?></strong></p>
					<p><?= $deliverable['AdminPriorityDesc'] ?></p>
				</div>
			</div>

			</div>
		</div>

		<?php if($this->session->userdata('UserAdminFlag') || $deliverable['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode') || $deliverable['DeliverableTypeID'] == 1): ?>

		<div class="col-sm-12">

			<div class="row" style="background:#f2f2f2;">

				<div class="col-sm-12">

					<div class="text_block ">
					
						<h1>Status</h1>

						<?php if(!empty($success)): ?>
						<div class="alert alert-success"><?= $success ?></div>
						<?php elseif(!empty($fail)): ?>
						<div class="alert alert-error"><?= $fail ?></div>
						<?php endif; ?>

						<?php if(validation_errors() != ""): ?>
						<div class="alert alert-danger">
						<?= validation_errors() ?>
						</div>
						<?php endif ?>
						


						<?php if($this->session->userdata('UserAdminFlag') || $deliverable['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode')): ?>
							
						<?php 
						echo form_open('update');
						echo form_hidden('DeliverableID',$deliverable['DeliverableID']);
						// echo form_hidden('LastModBy',$this->session->userdata('UserID'));
						echo form_hidden('LastModBy',$this->session->userdata('UserName'));
						?>

							<textarea name="StatusUpdate" id="StatusUpdate" rows="10" maxlength="5000">
								<?= set_value('StatusUpdate', $deliverable['StatusUpdate']) ?>
							</textarea>

							<script>
					            CKEDITOR.replace( 'StatusUpdate', {
									toolbar: [
										{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },			// Defines toolbar group without name.
										{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
										{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
									]
								});
					        </script>
						

							<h3>Approximate Completion</h3>
							<p>Slide the bar to adjust the approximate percent complete and update the approximate completion date.</p>
							
							<div id="slider" style="margin:10px 0px 15px 0;"></div>

							<div class="row">
								<div class="col-sm-4">

									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-thumbs-up"></i></span>
										<input type="text" name="ApproxComplete" id="ApproxComplete" class="form-control" readonly> 
									</div>

									<script>
										$(function() {
											$( "#slider" ).slider({
												value:<?php echo $deliverable['ApproxComplete']; ?>,
												min: 0,
												max: 100,
												range: "min",
												step: 5,
												slide: function( event, ui ) {
													$( "#ApproxComplete" ).val( ui.value + "%" );
												}
											});
											$( "#ApproxComplete" ).val( $( "#slider" ).slider( "value" )+"%" );
										});
									</script>

								</div>
								<div class="col-sm-4">

									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input type="text" name="ApproxEndDate" id="ApproxEndDate" class="form-control" value="<?= (!empty($deliverable['ApproxEndDate'])) ? date('m/d/Y', strtotime(substr($deliverable['ApproxEndDate'],0,10))) : date('m/d/Y', mktime(0, 0, 0, 4, 1, date("Y")+1)); ?>" readonly> 
									</div>
									<script>
										$('#ApproxEndDate').datepicker({ 
											minDate: '04/01/2014'
											// defaultDate: "04/01/2015"
										});
									</script>

								</div>
								<div class="col-sm-4">

									<select class="form-control" name="StatusID">
										<?php foreach ($statuses as $status): ?>
										<option value="<?= $status['StatusID'] ?>" <?= set_select('StatusID', $status['StatusID'], $deliverable['StatusID'] == $status['StatusID'] ? TRUE : FALSE) ?> ><?= $status['StatusDescription'] ?></option>
										<?php endforeach ?>
									</select>

								</div>

							</div>

							<div class="row">

								<div class="text-center" style="padding:25px;">
									<input type="hidden" name="id" value="<?= $deliverable['DeliverableID'] ?>">
									<input type="submit" class="btn btn-info" value="Update">
								</div>

							</div>

						</form>

						<?php elseif($deliverable['DeliverableTypeID'] == 1): ?>
							<?php if(empty($deliverable['StatusUpdate'])): ?>
								<p class="muted"><em>No update at this time.</em></p>
							<?php else: ?>
								<?= $deliverable['StatusUpdate'] ?>
							<?php endif ?>
						<?php endif ?>

					</div>

					<div class="text_block">
						
						<?php if(!empty($deliverable['StatusUpdate'])): ?>
						<p class="muted text-right">
							<em>
								Updated <?= date('M d, Y', strtotime(substr($deliverable['LastModDate'],0,10))) ?> 
								<br class="visible-xs"> by <?= $deliverable['LastModBy'] ?>.
							</em>
						</p>
						<?php endif ?>

					</div>

				</div>

			</div>

		</div>

		<?php endif ?>


		<div class="col-sm-12 supporting_deliverables">
			<p class=""><?= $supporting_deliverable_count ?> Related <?= ($deliverable['DeliverableTypeID'] == 2) ? 'Strategic' : 'Operational'; ?> Deliverable<?= (($supporting_deliverable_count==1) ? '' : 's') ?></p>
			<ul>
				<?php foreach ($related_supporting_deliverables as $related_supporting_deliverable): ?>
				<!-- <li><a href="<?= base_url() ?>edit-status/<?= $related_supporting_deliverable['id'] ?>"><?= $related_supporting_deliverable['title'] ?></a></li> -->
				<?php
					$label_color = '';
					if($related_supporting_deliverable['statusid'] == 1 ) $label_color = 'success';
					if($related_supporting_deliverable['statusid'] == 2 ) $label_color = 'warning';
					if($related_supporting_deliverable['statusid'] == 3 ) $label_color = 'danger';
				?>
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $related_supporting_deliverable['id'] ?>">
						<?= $related_supporting_deliverable['title'] ?>
					</a>
					 - <?= $related_supporting_deliverable['percent'] ?>% completed <?= ($related_supporting_deliverable['percent'] < 100) ? '('.$related_supporting_deliverable['status'].')' : '' ?> 
				</li>
				<?php endforeach ?>
			</ul>
		</div>

	</div>

	<?php else: ?>

	<div class="block no_deliverables_found">
		<p>No deliverables found</p>
	</div>

	<?php endif; ?>

	<?php $this->load->view('template/copyright') ?>