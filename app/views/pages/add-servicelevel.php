
<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			echo form_open('create-deliverable');
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update set the deliverable id
			if(!empty($deliverable)) echo form_hidden('DeliverableID',$deliverable['DeliverableID']);
			?>
 
  				<h1>Create a Service Level (Under Construction)</h1>
                                
				<?php if($this->session->flashdata('success')): 
                                //---------( Check for state based on the CI Internal Session Variable settings - PRSC )
                                    
                                ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

				<div class="row">
					<div class="col-sm-4">
						<label for="DeliverableTypeID">Type</label>
					</div>
					<div class="col-sm-8">
						<select class="form-control" id="DeliverableTypeID" name="DeliverableTypeID">
							<option value="">Select a deliverable type</option>
							<?php foreach ($deliverable_types as $deliverable_type): ?>
							<option value="<?= $deliverable_type['DeliverableTypeID'] ?>" <?= set_select('DeliverableTypeID', $deliverable_type['DeliverableTypeID'], ((!empty($deliverable) && $deliverable['DeliverableTypeID'] == $deliverable_type['DeliverableTypeID']) ? TRUE : '')) ?>><?= $deliverable_type['DeliverableTypeName'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>

				<script type="text/javascript">
					$(function() {
						// initialize checkboxes
						<?php if($this->input->post('DeliverableTypeID') == 2): ?>
						//$('.deliverable_dependency').show();
						<?php else: ?>
						//$('.deliverable_dependency').hide();
						<?php endif ?>

				        if($('#DeliverableTypeID').val() == 2) {
				        	$('.deliverable_dependency').show();
				        }
				        else {
				        	$('.deliverable_dependency').hide();
				        }

				        $('#DeliverableTypeID').change(function(){
				            $('.deliverable_dependency').hide();
				            $('.' + $(this).val()+'_dependency').show();
				        });
				    });
				</script>

				<div class="row deliverable_dependency 2_dependency">
					<div class="col-sm-4">
						<label for="DeliverableParentID">Related to</label>
					</div>
					<div class="col-sm-8">
						<select class="form-control" name="DeliverableParentID">
							<option value="">Select a strategic deliverable (optional)</option>
							<?php foreach ($strategic_deliverables as $strategic_deliverable): ?>
							<option value="<?= $strategic_deliverable['DeliverableID'] ?>" <?= set_select('DeliverableParentID', $strategic_deliverable['DeliverableID'], ((!empty($deliverable) && $deliverable['DeliverableParentID'] == $strategic_deliverable['DeliverableID']) ? TRUE : '')) ?>><?= $strategic_deliverable['BusinessUnitCode'] ?> <?= $strategic_deliverable['DeliverableCode'] ?> - <?= $strategic_deliverable['DeliverableDescShort'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
						<label for="BusinessUnitID">Business Unit</label>
					</div>
					<div class="col-sm-8">			
						<select class="form-control" id="BusinessUnitID" name="BusinessUnitID">
							<option value="">Select a business unit</option>
							<?php foreach ($business_units as $business_unit): ?>
							<option name="<?= $business_unit['BusinessUnitCode'] ?>" value="<?= $business_unit['BusinessUnitID'] ?>" <?= set_select('BusinessUnitID', $business_unit['BusinessUnitID'], ((!empty($deliverable) && $deliverable['BusinessUnitID'] == $business_unit['BusinessUnitID']) ? TRUE : '')) ?>><?= $business_unit['BusinessUnitShortName'] ?> (<?= $business_unit['BusinessUnitCode'] ?>)</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>


				<div class="row">
					<div class="col-sm-4">
						<label for="ServiceAreaID">Service Area</label>
					</div>
					<div class="col-sm-8">
						<em class="service_area_description _BU_dependency">Select a business unit above</em>

							<?php 
								$business_unit_id = '';
								foreach ($business_unit_service_areas as $business_unit_service_area)
								{
									if(empty($business_unit_id)) 
									{
										$business_unit_id  = $business_unit_service_area['BusinessUnitID'];
							?>
										<select class="form-control business_unit_dependency <?= $business_unit_id ?>_BU_dependency" name="ServiceArea<?= $business_unit_id ?>">
											<option value="">Select a service area</option>
											<option value="<?= $business_unit_service_area['ServiceAreaID'] ?>" <?= set_select('ServiceArea'.$business_unit_id, $business_unit_service_area['ServiceAreaID'], ((!empty($deliverable) && $deliverable['ServiceAreaID'] == $business_unit_service_area['ServiceAreaID']) ? TRUE : '')) ?>><?= $business_unit_service_area['ServiceAreaName'] ?></option>
							<?php
									}
									elseif($business_unit_id != $business_unit_service_area['BusinessUnitID']) 
									{
										$business_unit_id = $business_unit_service_area['BusinessUnitID'];
							?>
										</select>
										<select class="form-control business_unit_dependency <?= $business_unit_id ?>_BU_dependency" name="ServiceArea<?= $business_unit_id ?>">
											<option value="<?= $business_unit_service_area['ServiceAreaID'] ?>" <?= set_select('ServiceArea'.$business_unit_id, $business_unit_service_area['ServiceAreaID'], ((!empty($deliverable) && $deliverable['ServiceAreaID'] == $business_unit_service_area['ServiceAreaID']) ? TRUE : '')) ?>><?= $business_unit_service_area['ServiceAreaName'] ?></option>
							<?php
									}
									else
									{
							?>
											<option value="<?= $business_unit_service_area['ServiceAreaID'] ?>" <?= set_select('ServiceArea'.$business_unit_id, $business_unit_service_area['ServiceAreaID'], ((!empty($deliverable) && $deliverable['ServiceAreaID'] == $business_unit_service_area['ServiceAreaID']) ? TRUE : '')) ?>><?= $business_unit_service_area['ServiceAreaName'] ?></option>
							<?php
									}
								}
							?>

							</select>
						</select>
					</div>
				</div>

				<script type="text/javascript">
					$(function() {
						$('.business_unit_dependency').hide();
						<?php 
							//$business_unit_id = $this->input->post('BusinessUnitID');
							if($this->input->post('BusinessUnitID') != ''): 
						?> 
						//$('.service_area_description').hide();
						//$('.' + <?php echo $this->input->post('BusinessUnitID') ?>+'_BU_dependency').show();
						<?php endif ?>

						if($('#BusinessUnitID').val() != '') {
							$('.service_area_description').hide();
							$('.' + $('#BusinessUnitID').val() + '_BU_dependency').show();
				        }
				        
				        $('#BusinessUnitID').change(function(){
				            $('.business_unit_dependency').hide();
				            $('.service_area_description').hide();
				            $('.' + $(this).val()+'_BU_dependency').show();
				        });
				    });
				</script>


				<div class="row">
					<div class="col-sm-4">
						<label for="OutcomeID">Outcome</label>
					</div>
					<div class="col-sm-8">
						<select class="form-control" name="OutcomeID">
							<option value="">Select a priority outcome</option>
							<option value="1" <?= set_select('OutcomeID', 1, ((!empty($deliverable) && $deliverable['OutcomeID'] == 1) ? TRUE : '')) ?>>None</option>
							<?php foreach ($priority_outcomes as $priority_outcome): ?>
								<?php if($priority_outcome['OutcomeID'] != 1): ?>							
								<option value="<?= $priority_outcome['OutcomeID'] ?>" <?= set_select('OutcomeID', $priority_outcome['OutcomeID'], ((!empty($deliverable) && $deliverable['OutcomeID'] == $priority_outcome['OutcomeID']) ? TRUE : '')) ?>><?= $priority_outcome['OutcomeName'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
						<label for="AdminPriorityID">Admin Priority</label>
					</div>
					<div class="col-sm-8">
						<select class="form-control" name="AdminPriorityID">
							<option value="">Select an adiministrative priority</option>
							<option value="1" <?= set_select('AdminPriorityID', 1, ((!empty($deliverable) && $deliverable['AdminPriorityID'] == 1) ? TRUE : '')) ?>>None</option>
							<?php foreach ($admin_priorities as $admin_priority): ?>
								<?php if($admin_priority['AdminPriorityID'] != 1): ?>							
								<option value="<?= $admin_priority['AdminPriorityID'] ?>" <?= set_select('AdminPriorityID', $admin_priority['AdminPriorityID'], ((!empty($deliverable) && $deliverable['AdminPriorityID'] == $admin_priority['AdminPriorityID']) ? TRUE : '')) ?>><?= $admin_priority['AdminPriorityName'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>


				<div class="row">
					<div class="col-sm-4">
						<label for="RiskID">Risk</label>
					</div>
					<div class="col-sm-8">
						<select class="form-control" name="RiskID">
							<option value="">Select an associated risk</option>
							<option value="1" <?= set_select('RiskID', 1, ((!empty($deliverable) && $deliverable['RiskID'] == 1) ? TRUE : '')) ?>>None</option>
							<?php foreach ($risks as $risk): ?>
								<?php if($risk['RiskID'] != 1): ?>
								<option value="<?= $risk['RiskID'] ?>" <?= set_select('RiskID', $risk['RiskID'], ((!empty($deliverable) && $deliverable['RiskID'] == $risk['RiskID']) ? TRUE : '')) ?>><?= $risk['RiskDescShort'] ?> (<?= $risk['RiskCode'] ?>)</option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>



				<div class="row">
					<div class="col-sm-4">
						<label for="PriorityID">Is this a priority?</label>
					</div>
					<div class="col-sm-8">
						<select class="form-control" name="PriorityID">
							<?php foreach ($priorities as $priority): ?>
							<option value="<?= $priority['PriorityID'] ?>" <?= set_select('PriorityID', $priority['PriorityID'], ((!empty($deliverable) && $deliverable['PriorityID'] == $priority['PriorityID']) ? TRUE : '')) ?>><?= $priority['PriorityDesc'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>


				<div class="row">
					<div class="col-sm-4">
						<label for="DeliverableCode">Business Plan #</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="DeliverableCode" class="form-control" value="<?= set_value('DeliverableCode', (!empty($deliverable) ? $deliverable['DeliverableCode'] : '')) ?>" maxlength="5">
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
						<label for="DeliverableDescShort">Short Description</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="DeliverableDescShort" class="form-control" value="<?= set_value('DeliverableDescShort', (!empty($deliverable) ? $deliverable['DeliverableDescShort'] : '')) ?>" maxlength="500">
					</div>
				</div>

				<label for="DeliverableDesc">Full Description</label>
				<textarea name="DeliverableDesc" rows="15"><?= set_value('DeliverableDesc', (!empty($deliverable) ? $deliverable['DeliverableDesc'] : '')) ?></textarea>
				<script>
		            CKEDITOR.replace( 'DeliverableDesc', {
						toolbar: [
							{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },			// Defines toolbar group without name.
							{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
							{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
						]
					});
		        </script>

				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info"><i class="icon-ok icon-white"></i> <?= (!empty($deliverable)) ? 'Update' : 'Create' ?> Deliverable</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
