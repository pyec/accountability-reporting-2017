<?php 
	/*=====================================================================
	 * 
	 * 		MODULE:		pg_show_risks
	 * 		AUTHOR:		R.Stephen Chafe (Zen River Software)
	 * 		CREATED:	201602xx
	 * 		
	 * 		This module handles the list format web page for 
	 * 		Risks data records.
	 * 
	 * 		MODIFICATIONS:
	 * 		20160311		PRSC	Display format changed for K.Couture
	 * 
	 * ====================================================================
	 */
?>

<?php 
	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($risks);
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>



<?php if (count($risks)>0): ?>

<div class="col-sm-12">

	<div class="results">    
		<div class="pull-right hidden-xs">
			<?php if (count($risks)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/ ?>
	 		
	 			<?= form_open(base_url().'download-risks') ?>
				<input type="hidden" name="DeliverableTypeID" value="<?= set_value('DeliverableTypeID') ?>">
				<input type="hidden" name="BusinessUnitID" value="<?= set_value('BusinessUnitID') ?>">
				<input type="hidden" name="ServiceArea<?= set_value('BusinessUnitID') ?>" value="<?= set_value('ServiceArea'.set_value('BusinessUnitID')) ?>">
				<input type="hidden" name="FocusAreaID" value="<?= set_value('FocusAreaID') ?>">
				<input type="hidden" name="AdminPillarID" value="<?= set_value('AdminPillarID') ?>">
				<input type="hidden" name="RiskID" value="<?= set_value('RiskID') ?>">
				<input type="hidden" name="PriorityID" value="<?= set_value('PriorityID') ?>">
				<input type="hidden" name="StatusID" value="<?= set_value('StatusID') ?>">
				<input type="hidden" name="PublishedYN" value="<?= set_value('PublishedYN') ?>">
				<input type="hidden" name="SourceID" value="<?= set_value('SourceID') ?>">
				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>
		</div>
		<div class="result_count">
			<p>
				<?php 								/* TITLES */
					echo count($services);
					echo ' RISKS List';
					echo (count($risks) == 1 ? '' : 'ings'); 
				?>
			</p>
		</div>

		<?php endif ?>
	</div>
</div>

<?php 

	$risk_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;


	foreach ($risks as $risk): 
	

		?>
	<?php if (count($risks)>0):
	?>


	<?php 
	$risk_id 		= '';
	$risk_parent_id 	= '';
	$count	= 0;
	?>


	<?php 
	 		
	?>

<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-risk/<?= $risk['RiskID'] ?>" title="Update risk details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						 <?= $risk['RiskCode'] ?> - <?= $risk['RiskDescShort'] ?></a></h1>
						
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						/* CNIU - but leave in as it will be used in future version PRSC */
						$label_color = 'success';
						//if($risk['StatusID'] == 1 ) $label_color = 'success';
						//if($risk['StatusID'] == 2 ) $label_color = 'warning';
						//if($risk['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
					</div>
				</div>

			</div>

			<div class="row">

					<div class="col-sm-4">
								<p><strong>Risk Description: &nbsp;</strong></p>
						<?= $risk['RiskDesc'] ?>
					</div>	
		
				<div class="col-sm-4">
								<p><strong>Statement: &nbsp; </strong></p>
					<div style="padding-bottom: 15px;">
					<p><?= $risk['RiskStatement'] ?> </p>
				</div>	
			</div>			



		<div class="row">

				
							
				<div class="col-sm-4">
								<p><strong> &nbsp Factors: &nbsp; </strong></p>
								<p><strong>&nbsp Likelihood,Net: &nbsp; </strong><?= $risk['Likelihood'] ?></p>
								<p><strong>&nbsp Impact, Net: &nbsp; </strong><?= $risk['Impact'] ?></p>
								<p><strong>&nbsp Heat Map Rating: &nbsp; </strong>
								
								
							<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $risk['HeatMapRating'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($heatmapratings as $rt) 
										{
										if($rt['HeatMapRateID'] == $risk['HeatMapRating'])
										$realVL = $rt['HeatMapRateShortName'];
										}
									}
								}
								else 
									{ $realVL = ""; }
								echo $realVL;	  
								?></p>								
								
								
								<p><strong>&nbsp Risk Owner: &nbsp; </strong><?= $risk['RiskOwner'] ?></p>

							</div>
			</div>
	
		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>


	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>






