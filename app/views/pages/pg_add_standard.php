<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>



<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			if(empty($standard)) 
				{
				echo form_open('create-standard');
				}
			else
				{
				echo form_open('update-standard');
				}
				echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Standard ID;
			if(!empty($standard)) 
			         echo form_hidden('StandardID',$standard['StandardID']);
			?>
 
     			<h1>
 				 <?= (!empty($standard)) ? 'Edit' : 'Create' ?> Standard</button>
                </h1>              
                                
              	<?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201601
	 */  
	 ?>
	 
 
	


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Standard ShortName Desc
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="StandardShortNM">Standard Short Name (250-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="StandardShortNM" class="form-control"
						 value="<?= set_value('StandardShortNM', (!empty($standard) ?
						  $standard['StandardShortNM'] : ''))
						 ?>" maxlength="250">
					</div>
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Standard Short Code
		 * -----------------------------------------------------PRSC 201601
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="StandardCD">Standard Short Code  (20-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="StandardCD" class="form-control"
						 value="<?= set_value('StandardCD', (!empty($standard) ?
						  $standard['StandardCD'] : ''))
						 ?>" maxlength="20">
					</div>
				</div>




		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	Service 
		 * -----------------------------------------------------PRSC 201601
		 */	?>
	
			<div class="row">
			<div class="col-sm-4">
				<label for="Service">Service</label>
			</div>
			<div class="col-sm-8">			
				<select class="form-control" id="ServiceID" name="ServiceID">

				    <?php 
				    $real = '';
				    
				    if(!empty($standard))
				    {
				    	foreach ($services as $rt) 
				    	{
						if($rt['ServiceID'] == $standard['ServiceID'])
							$real = $rt['ServiceShortNM'];
						}
				    }	
						
				    if($real)
				    	echo '<option value="' . $standard['ServiceID'] . '" > ' . $real . '</option>"';
					else
						echo '<option value="">Select Associated Service</option>';
					?>	
					
					 		 
					 <?php 

					 /* --------------------------------------------------------------
					  *		This code builds the drop down Services Box for both the
					  *		Create Screen and the Edit Screen 
					  * ------------------------------------------------20160217 PRSC
					  */
					 	 
					if(!empty($services))
				    {
					foreach ($services as $dt)
					{
				    	
					//  Lookup the actual Service Area Code to show to user PRSC					
					$vdt = $dt['ServiceAreaID'];
					foreach ($serviceareas as $st)
					{
						if($vdt == $st['ServiceAreaID'])
						{
							$saCD = $st['ServiceAreaName'];
						}
					}
						 		 
					//  Lookup the actual Business Unit Code to show to user PRSC					
					$vdt = $dt['BusinessUnitID'];
					foreach ($business_units as $st)
					{
					if($vdt == $st['BusinessUnitID'])
						{
							$buCD = $st['BusinessUnitCode'];
						}
					}
					 		 
				//	 print "(" . $buID . "-" . $buCD . ")(" . $saID . "-" . $saCD . ') >' . $dt['ServiceShortNM'];
					$vl = $dt['ServiceID'];
					print '<option value="' .  $vl  . '">' . "( " . $buCD . ' , ' . $saCD . ' ) ' . $dt['ServiceShortNM'] . '</option>';;
					 
					}												// EO For Each Service
				    }												// EO If Services defined
			   else 
				    {								/* Using just the Create Screen */

				 // In a simple Create just show all the Services */   	
				 foreach ($services as $dt)
					{ 

				/*	Old base method for research purposes
				 * 	<option name="<?= $dt['ServiceID'] ?>"
						 value="<?= $dt['ServiceID'] ?>"
					 		 <?= set_select('ServiceID', $dt['ServiceID'],
					 		 ((!empty($service) && $standard['ServiceID']
					 		 	 == $dt['ServiceID'])? TRUE : '')) ?>>
					}				    	
				*/
				    	
					//  Lookup the actual Service Area Code to show to user PRSC					
					$vdt = $dt['ServiceAreaID'];
					foreach ($serviceareas as $st)
					{
						if($vdt == $st['ServiceAreaID'])
						{
							$saCD = $st['ServiceAreaName'];
						}
					}
						 		 
					//  Lookup the actual Business Unit Code to show to user PRSC					
					$vdt = $dt['BusinessUnitID'];
					foreach ($business_units as $st)
					{
					if($vdt == $st['BusinessUnitID'])
						{
							$buCD = $st['BusinessUnitCode'];
						}
					}
							 
					// Add each option to the drop down box
					$vl = $dt['ServiceID'];
//					print '<option value="' .  $vl  . '">' . "(" . $buCD . ")(" . $saCD . ') >' . $dt['ServiceShortNM'] . '</option>';;
				print '<option value="' .  $vl  . '">' . "( " . $buCD . ' , ' . $saCD . ' ) ' . $dt['ServiceShortNM'] . '</option>';;
											
				    	
				    }
				 }		 
					 		 
				?>

					
				</select>
			</div>
		</div>
	
	

	

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Standard Description (FULL)
		 * -----------------------------------------------------PRSC 201601
		 */	?>

		<label for="StandardDesc">Standard Description (1500 digits)</label>
		<textarea name="StandardDesc" rows="15"><?= set_value('StandardDesc',
		 (!empty($standard) ? $standard['StandardDesc'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'StandardDesc', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($standard))
					 ? 'Update' : 'Create' ?> Standard</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>

