<?php 

 	/*================================================================
	 * 
	 * 	MODULE:			pg_show_sourcess.php
	 * 	AUTHOR:			R.Stephen Chafe (Zen River Software)
	 * 	CREATED:		2016_03_06
	 * 
	 * 	This module contains the LIST display for all of the 
	 *  source data.
	 *  
	 * ==============================================================
	 */


	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($sources);
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>


	
<?php if (count($sources)>0): ?>


<div class="col-sm-12">

	<div class="results">    
	
	<!-- ------------------------------------------------------------------EO-->  
	
		<div class="pull-right hidden-xs">
			<?php if (count($sources)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	 		
			?>
			
			
		</div>
		
		
		
		<div class="result_count">
			<p>
				<?php 								/* TITLES */
					echo count($sources);
					echo ' SOURCE List';
					echo (count($sources) == 1 ? '' : 'ings'); 
				?>
			</p>
		</div>

			<?php endif ?>
		</div>
	
	</div>


<?php 

	$source_id 		= '';
	$source_parent_id 	= '';
	$count	= 0;

	// echo "<pre>";
	// print_r($sources);
	// echo "</pre>";

	foreach ($sources as $dt): 
	

	?>

<div class="col-sm-12">

	
 
    <!-- ------------------------------------------------------------------EO-->  
   
	<?php if (count($sources)>0):	?>


	<?php 
	$source_id 		= '';
	$source_parent_id 	= '';
	$count	= 0;

		?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed Source Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
	?>




<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-source/<?= $dt['SourceID'] ?>" 
							title="Update standard details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						 <?php /* $dt['SourceID'] */ ?> 
						 <?= $dt['SourceShortName'] ?></a></h1>
						
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						/* CNIU - but leave in as it will be used in future version PRSC */
						$label_color = 'success';
						//if($dt['StatusID'] == 1 ) $label_color = 'success';
						//if($dt['StatusID'] == 2 ) $label_color = 'warning';
						//if($dt['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
					</div>
				</div>

			</div>

			<div class="row">

				<div class="col-sm-4">
								<p><strong>Source Description: &nbsp;</strong></p>
				</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $dt['SourceDesc'] ?>
				</div>	
			
			</div>

				
	
	
					</div>
				
	
		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>


	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>








