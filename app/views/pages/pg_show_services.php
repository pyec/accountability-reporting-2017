<?php 
	/*=====================================================================
	 * 
	 * 		MODULE:		pg_show_services
	 * 		AUTHOR:		R.Stephen Chafe (Zen River Software)
	 * 		CREATED:	201602xx
	 * 		
	 * 		This module handles the list format web page for 
	 * 		Services data records.
	 * 
	 * 		MODIFICATIONS:
	 * 		20160311		PRSC	Display format changed for K.Couture
	 * 
	 * ====================================================================
	 */
?>


<?php 
	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($services);
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>




	
<?php if (count($services)>0): ?>

<div class="col-sm-12">

	<div class="results">    
		<div class="pull-right hidden-xs">
			<?php if (count($services)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/ ?>
	 		
	 			<?= form_open(base_url().'download-services') ?>
				<input type="hidden" name="DeliverableTypeID" value="<?= set_value('DeliverableTypeID') ?>">
				<input type="hidden" name="BusinessUnitID" value="<?= set_value('BusinessUnitID') ?>">
				<input type="hidden" name="ServiceArea<?= set_value('BusinessUnitID') ?>" value="<?= set_value('ServiceArea'.set_value('BusinessUnitID')) ?>">
				<input type="hidden" name="FocusAreaID" value="<?= set_value('FocusAreaID') ?>">
				<input type="hidden" name="AdminPillarID" value="<?= set_value('AdminPillarID') ?>">
				<input type="hidden" name="RiskID" value="<?= set_value('RiskID') ?>">
				<input type="hidden" name="PriorityID" value="<?= set_value('PriorityID') ?>">
				<input type="hidden" name="StatusID" value="<?= set_value('StatusID') ?>">
				<input type="hidden" name="PublishedYN" value="<?= set_value('PublishedYN') ?>">
				<input type="hidden" name="SourceID" value="<?= set_value('SourceID') ?>">
				<button type="submit" class="btn btn-info" class="right">Export &nbsp;<i class="fa fa-download"></i></button>
			</form>
		</div>
		<div class="result_count">
			<p>
				<?php 								/* TITLES */
					echo count($services);
					echo ' SERVICE List';
					echo (count($services) == 1 ? '' : 'ings'); 
				?>
			</p>
		</div>

		<?php endif ?>
	</div>
</div>
	

<?php 

	$service_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;

	// echo "<pre>";
	// print_r($deliverables);
	// echo "</pre>";

	// echo "<pre>";
	// print_r($supporting_deliverables);
	// echo "</pre>";

	foreach ($services as $serviceDT): 
	
	
		// echo "<pre>";
		// print_r($related_supporting_deliverables);
		// echo "</pre>";
	?>

<div class="col-sm-12">

	
 
    <!-- ------------------------------------------------------------------EO-->  
   
	<?php if (count($services)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	If there are any SERVICES data in the Array then loop through
	 		*	and display each one in a easy to read format.
	 		* -----------------------------------------------------PRSC 201601
	 		>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 		*/  
	?>


	<?php 
	$service_id 		= '';
	$service_parent_id 	= '';
	$count	= 0;

	
		// echo "<pre>";
		// print_r($related_supporting_services);
		// echo "</pre>";
	?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed Service Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
	?>




<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-service/<?= $serviceDT['ServiceID'] ?>" 
							title="Update service details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						 <?php /* $serviceDT['ServiceID'] */ ?>
						 <?= $serviceDT['ServiceShortNM'] ?> [<?= $serviceDT['ServiceShortCD'] ?>]</h1>
						
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						/* CNIU - but leave in as it will be used in future version PRSC */
						$label_color = 'success';
						//if($serviceDT['StatusID'] == 1 ) $label_color = 'success';
						//if($serviceDT['StatusID'] == 2 ) $label_color = 'warning';
						//if($serviceDT['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
					</div>
				</div>

			</div>

			<div class="row">

					<div class="col-sm-4">
								<p><strong>Service Desc: &nbsp;</strong></p>
					</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $serviceDT['ServiceDESC'] ?>
				</div>	
			
			</div>

			<div class="row">

				<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service Short Code
					 * =-------------------------------------------------  PRSC
					 */
					?>

				<div class="col-sm-4">
								<p><strong>&nbsp &nbsp Service Short Code: &nbsp; </strong></p>
								<p>&nbsp &nbsp
	
							<?php 
							//  Lookup the actual literal to show to user PRSC					
								$value = $serviceDT['ServiceShortCD'];
								echo $value;	
							?>							
								
								</p>
				</div>



					<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Business Unit
					 * =-------------------------------------------------  PRSC
					 */
					?>
			
					<div class="col-sm-4">
								<p><strong>&nbsp &nbsp Business Unit: &nbsp; </strong></p>
								<p>&nbsp &nbsp
															
							<?php 
							//  Lookup the actual literal to show to user PRSC					
							$vdt = $serviceDT['BusinessUnitID'];
							foreach ($business_units as $dt)
							{
								if($vdt == $dt['BusinessUnitID'])
								{
									$value = $dt['BusinessUnitName'];
									echo $value;	
								}
							}
							?>
								
								</p>
					</div>


				<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service Area Name
					 * =-------------------------------------------------  PRSC
					 */
					?>
			
				<div class="col-sm-4">
								<p><strong>&nbsp &nbsp Service Area: &nbsp; </strong></p>
								<p>&nbsp &nbsp
	
							<?php 
							//  Lookup the actual literal to show to user PRSC					
							$vdt = $serviceDT['ServiceAreaID'];
							foreach ($serviceareas as $dt)
							{
								if($vdt == $dt['ServiceAreaID'])
								{
									$value = $dt['ServiceAreaName'];
									echo $value;	
								}
							}
							?>							
								
								</p>
				</div>
	
		
			</div>
				
				

		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>


	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>






