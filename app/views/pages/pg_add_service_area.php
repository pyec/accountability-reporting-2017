<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">
<br><br>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			if(empty($servicearea)) 
				{
				echo form_open('create-servicearea');
				}
			else
				{
				echo form_open('update-servicearea');
				}
				
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Risk ID;
					
			if(!empty($servicearea)) 
			         echo form_hidden('ServiceAreaID',$servicearea['ServiceAreaID']);
			?>
 
  			<h1>
 				 <?= (!empty($servicearea)) ? 'Edit' : 'Create' ?> Service Area
            </h1>              

			<?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 ?> 
				 
 
					                             
	<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework. This also is suppose to
	 *  provide notice to the user that save was successful.  However
	 *  it does not always work.
	 * ----------------------------------------------------PRSC 201601
	 */  
	 ?>
		<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
			<?php elseif($this->session->flashdata('danger')): ?>
			
			<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
		<?php endif; ?>
				
     		
        <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>


			<div class="row">
					<div class="col-sm-4">
						<label for="ServiceAreaName">Service Area Name (50-Digits)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="ServiceAreaName" class="form-control"
						 value="<?= set_value('ServiceAreaName', (!empty($servicearea) ? 
						 $servicearea['ServiceAreaName'] : '')) ?>" maxlength="50">
					</div>
				</div>

				<hr>

	
		
				<?php 
				
					/*----------------------------------------------------------------
					 * 		This will look at the risk type the user enters and modify
					 * 		whether to show the "Related To" Dependency dropdown box
					 * 		for added information to be added to the form. 
					 * 
					 * 		Normally this box is set to "hidden" via jquery.
					 * -----------------------------------------------------PRSC 201601
					 */
				
				?>

				<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Business Unit
					 * ----------------------------------------------------  PRSC
					 */
					?>
	
				<div class="row">
					<div class="col-sm-4">
						<label for="BusinessUnitID">Business Unit</label>
					</div>
					<div class="col-sm-8">			
						<select class="form-control" id="BusinessUnitID" name="BusinessUnitID">
							<option value="">Select a business unit</option>
							<?php foreach ($business_units as $business_unit): ?>
							<option name="<?= $business_unit['BusinessUnitCode'] ?>"
								 value="<?= $business_unit['BusinessUnitID'] ?>"
							 		 <?= set_select('BusinessUnitID', $business_unit['BusinessUnitID'],
							 		 ((!empty($servicearea) && $servicearea['BusinessUnitID'] == $business_unit['BusinessUnitID']) ? TRUE : '')) ?>>
							 		 <?= $business_unit['BusinessUnitShortName'] ?> (<?= $business_unit['BusinessUnitCode'] ?>)</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>

				<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service Area Short Code
					 * ----------------------------------------------------  PRSC
					 */
					?>
	
				<div class="row">
					<div class="col-sm-4">
						<label for="ServiceAreaShortCD">Service Area Code (20-Digits)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="ServiceAreaShortCD" class="form-control"
						 value="<?= set_value('ServiceAreaShortCD', (!empty($servicearea) ? 
						 $servicearea['ServiceAreaShortCD'] : '')) ?>" maxlength="20">
					</div>
			
			</div>

	
		

				<label for="ServiceAreaDesc">Service Area Description (1500-Digits)</label>
				<textarea name="ServiceAreaDesc" rows="15"><?= set_value('ServiceAreaDesc', 
				(!empty($servicearea) ? $servicearea['ServiceAreaDesc'] : '')) ?></textarea>
				<script>
		            CKEDITOR.replace( 'ServiceAreaDesc', {
						toolbar: [
							{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },			// Defines toolbar group without name.
							{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
							{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
						]
					});
		        </script>

				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info"><i class="icon-ok icon-white"></i>
					 <?= (!empty($servicearea)) ? 'Update' : 'Create' ?> ServiceArea</button>
				</div>
			
			</div>
		</form>
	</div>

</div>

