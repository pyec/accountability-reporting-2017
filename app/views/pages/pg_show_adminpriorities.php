<?php 
 	/*================================================================
	 * 
	 * 	MODULE:			pg_show_adminprioritys.php
	 * 	AUTHOR:			R.Stephen Chafe (Zen River Software)
	 * 	CREATED:		2016_03_06
	 * 
	 * 	This module contains the LIST display for all of the 
	 *  adminprioritys data.
	 *  
	 * ==============================================================
	 */



	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($adminprioritys);
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>


	
<?php if (count($adminprioritys)>0): ?>


<div class="col-sm-12">

	<div class="results">    
	
	<!-- ------------------------------------------------------------------EO-->  
	
		<div class="pull-right hidden-xs">
			<?php if (count($adminprioritys)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	 		
			?>
		
			
		</div>
		
		
		
		<div class="result_count">
			<p>
				<?php 								/* TITLES */
					echo count($adminprioritys);
					echo ' ADMIN PRIORITY List';
					echo (count($adminprioritys) == 1 ? '' : 'ings'); 
				?>
			</p>
		</div>

			<?php endif ?>
		</div>
	
	</div>


<?php 

	$standard_id 		= '';
	$standard_parent_id 	= '';
	$count	= 0;

	// echo "<pre>";
	// print_r($adminprioritys);
	// echo "</pre>";

	foreach ($adminprioritys as $adminpriorityDT): 
	
	?>

<div class="col-sm-12">

	<?php if (count($adminprioritys)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	If there are any SERVICES data in the Array then loop through
	 		*	and display each one in a easy to read format.
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	?>


	<?php 
	$standard_id 		= '';
	$standard_parent_id 	= '';
	$count	= 0;
	?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed Source Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
	?>




	<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-adminpriority/<?= $adminpriorityDT['AdminPriorityID'] ?>" 
							title="Update adminprioritys details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						<?php /* $adminpriorityDT['AdminPriorityID'] */ ?>
						<?= $adminpriorityDT['AdminPriorityName'] ?> 
						
						 </h1>
						
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						/* CNIU - but leave in as it will be used in future version PRSC */
						$label_color = 'success';
						//if($adminpriorityDT['StatusID'] == 1 ) $label_color = 'success';
						//if($adminpriorityDT['StatusID'] == 2 ) $label_color = 'warning';
						//if($adminpriorityDT['StatusID'] == 3 ) $label_color = 'danger';
					?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
					</div>
				</div>

			</div>

			<div class="row">

					<div class="col-sm-4">
								<p><strong>AdminPriority Description: &nbsp;</strong></p>
					</div>	
					
							
				<div class="col-sm-12">
					<div style="padding-bottom: 15px;">
					<?= $adminpriorityDT['AdminPriorityDesc'] ?>
				</div>	
			
			</div>
	

			<div class="row">

	
				<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : adminpillar 
					 * =-------------------------------------------------  PRSC
					 */
					?>
					
						<div class="col-sm-4">
								<p><strong>&nbsp &nbsp AdminPillar: &nbsp; </strong></p>
								<p>
								&nbsp &nbsp
								<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $adminpriorityDT['AdminPillarID'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($adminpillars as $rt) 
										{
										if($rt['AdminPillarID'] == $adminpriorityDT['AdminPillarID'])
										{
										
										$realVL = $rt['AdminPillarName'];
										 }			
									  }
									}
								}
								else 
									{ $realVL = "N/A"; }

								echo $realVL;	  
								?></p>
	
							</div>
					</div>
	
				

		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>



	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>





