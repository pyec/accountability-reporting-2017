<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($source);
// echo "</pre>";

?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($source)) 
				{
				echo form_open('create-source');
				}
			else
				{
				echo form_open('update-source');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the Source ID;
			if(!empty($source)) 
			         echo form_hidden('SourceID',$source['SourceID']);
			?>
 
  				<h1>
 				 <?= (!empty($source)) ? 'Edit' : 'Create' ?> Source</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Source ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="SourceShortName">Source Name (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="SourceShortName" class="form-control"
						 value="<?= set_value('SourceShortName', (!empty($source) ?
						  $source['SourceShortName'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>



			

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M Source Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<label for="SourceDesc">Source Description (500 digits)</label>
		<textarea name="SourceDesc" rows="15"><?= set_value('SourceDesc',
		 (!empty($source) ? $source['SourceDesc'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'SourceDesc', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($source))
					 ? 'Update' : 'Create' ?> Source</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>




