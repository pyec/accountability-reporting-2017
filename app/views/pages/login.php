<!DOCTYPE html>
<html lang="en">
    <head>

        <title>Sign in | Accountability Reporting | Halifax</title>

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/styles/app.css">
    
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto+Condensed'>
        <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300'>

        <script src="<?= base_url() ?>_assets/plugins/jquery/jquery.min.js"></script>
        <script src="<?= base_url() ?>_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>_assets/plugins/placeholders/placeholders.min.js"></script>
        
    </head>
    
    <body style="background:#333;">

        <div class="container">          

            <!-- <form class="form-signin" role="form" action="<?= base_url() ?>login" method="post" accept-charset="utf-8"> -->
            <form class="form-signin" role="form" action="<?= base_url() ?>validate" method="post" accept-charset="utf-8">
                
                <div title="Built by Gian Pompilio in ICT Business Applications" style="padding-bottom:15px;">
                    <div class="logo logo-login">Accountability <span>Reporting</span></div>
                    <p class="text-muted text-center">Login using your HRM credentials</p>
                </div>

                <!-- <input type="email" name="email_address" class="form-control" maxlength="50" value="<?= set_value('email_address') ?>" placeholder="Email address" required autofocus> -->
                <input type="text" name="user_name" class="form-control" maxlength="20" value="<?= set_value('user_name') ?>" placeholder="Username (eg. hrm\smithj)" autocomplete="on" required autofocus>
                <input type="password" name="password" class="form-control" value="<?= set_value('password') ?>" placeholder="Password" autocomplete="off" required>
                <button class="btn btn-lg btn-primary btn-block" style="margin:15px 0;" type="submit">Sign in</button>

                <!-- invalid credentials -->
                <?php if($this->session->flashdata('error')): ?>
                <p class="text-danger text-center"><?= $this->session->flashdata('error') ?></p>
                <?php endif ?>

            </form>

        </div>

    </body>
</html>