<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">

<br><br>

<?php 
/*..................................( These are used for diagnostic and debugging) */
// echo "<pre>";
// print_r($generalstatus);
// echo "</pre>";

?>

<div class="col-sm-12">

		<div class="col-sm-12 deliverable create_deliverable">

			<?php 
			// open the form and pass the problem code in a hidden field
			
			if(empty($generalstatus)) 
				{
				echo form_open('create-generalstatus');
				}
			else
				{
				echo form_open('update-generalstatus');
				}
	
			echo form_hidden('LastModBy',$this->session->userdata('UserName'));
			// if this is an update to set the GeneralStatus ID;
			if(!empty($generalstatus)) 
			         echo form_hidden('GeneralStatusID',$generalstatus['GeneralStatusID']);
			?>
 
  				<h1>
 				 <?= (!empty($generalstatus)) ? 'Edit' : 'Create' ?> General Status</button>
                </h1>              
                                
                <?php 
				 print '<font color="FF0000">' . $data_state . '</font>';
			 	?> 
                                
                                
				<?php if($this->session->flashdata('success')): 
				
    /*
     * ----------------------------------------------------------------
	 * 	Check for Error or Alert state based on the CI Internal Session
	 *  Variable settings from the framework.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
				<div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
				<?php elseif($this->session->flashdata('danger')): ?>
				<div class="alert alert-error"><?= $this->session->flashdata('danger') ?></div>
				<?php endif; ?>
				
                <?= (validation_errors() != "") ? '<div class="text-danger">Missing or incorrect information detected.<br>
                Please scroll down and correct the issues identified in red.</div>'.validation_errors() : '' ?>

				<hr>

	
	<?php 
	/*
     * ----------------------------------------------------------------
	 * 		Start Data Entry / Modification fields display.
	 * -----------------------------------------------------PRSC 201603
	 */  
	 ?>
	 
	
		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M GeneralStatus ShortName 
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="GeneralStatusName">General Status Name (50-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="GeneralStatusName" class="form-control"
						 value="<?= set_value('GeneralStatusName', (!empty($generalstatus) ?
						  $generalstatus['GeneralStatusName'] : ''))
						 ?>" maxlength="50">
					</div>
				</div>


		<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M GeneralStatus Short CD
		 * -----------------------------------------------------PRSC 201603
		 */	?>

				<div class="row">
					<div class="col-sm-4">
						<label for="GeneralStatusCD">General Status Code (20-Digit)</label>
					</div>
					<div class="col-sm-8">
						<input type="text" name="GeneralStatusCD" class="form-control"
						 value="<?= set_value('GeneralStatusCD', (!empty($generalstatus) ?
						  $generalstatus['GeneralStatusCD'] : ''))
						 ?>" maxlength="20">
					</div>
				</div>


	
		

	<?php 
		/*----------------------------------------------------------------
		 * 		ELEMENT:	E/M GeneralStatus Description (FULL)
		 * -----------------------------------------------------PRSC 201603
		 */	?>

		<label for="GeneralStatusDesc">General Status Description (300 digits)</label>
		<textarea name="GeneralStatusDesc" rows="15"><?= set_value('GeneralStatusDesc',
		 (!empty($generalstatus) ? $generalstatus['GeneralStatusDesc'] : '')) ?></textarea>
		<script>
		 CKEDITOR.replace( 'GeneralStatusDesc', {
			toolbar: [
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },	
					// Defines toolbar group without name.
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
			]
		});
		</script>

		


				<div class="text-center" style="padding:15px 0;">
					<button type="submit" class="btn btn-info">
					<i class="icon-ok icon-white"></i> <?= (!empty($generalstatus))
					 ? 'Update' : 'Create' ?> General Status</button>
				</div>
			
			</div>
		</form>
	</div>

</div>
</div>
</div>




