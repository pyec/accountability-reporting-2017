<?php $this->session->set_flashdata('uri', uri_string()) 

/* ---------------------------------------------------------------------
 * 
 *		MODULE:			pg_add_status2
 *		AUTHOR:			Stephen Chafe (Zen River Software)
 *		CREATED:		Unknown
 *
 * 		This module allows users to input or modify current status
 * 		information entered for a Deliverable.
 * 
 * 
 * 		MODIFICATION HISTORY
 * 
 * 		20160425		PRSC		Added in new Strategic/Operational
 * 									display logic to include both.
 * 		20160823		PRSC		Added logic for TASK0084832 where
 * 									New extended group can see details
 * 									but not modify them.
 * 		20160826		PRSC		Cleaned up the percentage bar displays
 * 									after errors caused by TASK0084832 tests.
 * 
 * ----------------------------------------------------------------------
 */


?>

<!-- Page -->
<div id="page" style="padding:75px 0 20px;">
	<div class="container">
	<div class="row">


	<?php if (count($deliverable)>0): ?>

	<?php 

	$deliverable_id 		= '';
	$deliverable_parent_id 	= '';
	$count	= 0;
	// $supporting_deliverable_count = 0;

		
		// // first time through
		// if($count == 0)
		// {
		// 	if($deliverable_id == '')			$deliverable_id = $deliverable['DeliverableID'];
		// 	//if($deliverable_parent_id == '')	$deliverable_parent_id = $deliverable['DeliverableParentID'];
		// }

		// // if this is an supporting deliverable and it's parent id is the same as the previously set deliverable id
		// elseif(($count > 0) && ($deliverable['DeliverableTypeID'] == 2) && ($deliverable['DeliverableParentID'] == $deliverable_id))
		// {
		// 	$supporting_deliverable_count++;
		// 	$count++;
		// 	continue;
		// }
		// elseif($count > 0)
		// {
		// 	echo '<div class="col-sm-12 text-center supporting_deliverable_count">'.$supporting_deliverable_count.' related supporting deliverable'.(($supporting_deliverable_count==1) ? '' : 's').'</div></div>';
		// 	$supporting_deliverable_count = 0;
		// 	$deliverable_id = $deliverable['DeliverableID'];
		// 	$deliverable_parent_id = $deliverable['DeliverableParentID'];

		// }

		$supporting_deliverable_count = 0;
//		$related_supporting_deliverables = [];

	// These are also used later with the display of the Related Deliverables - PRSC
	$CNT_childrenxx		= 0;
	$CNT_parentsxx		= 0;

	$CHLD_Deliverables   = [];
	$PRNT_Deliverables   = [];
		
	// These variables are used with the Related Deliverables Display at the bottom - PRSC
    $CNT_total_parents 		= count($parent_deliverables);
    $CNT_total_kids 		= count($supporting_deliverables);
	
/*============================================================================
 * 		This ugly chunk is run with every Deliverable.  
 * 		The goal of this code is to build the list of associated Deliverables that
 * 		relates as Strategic (Parent) or Operational (child) to the current one that
 * 		is being shown. [PRSC]
 * ===========================================================================
 */

	
		// This was designed so that if the USER did not select the OPERATIONAL "AllTypes"
		// View then the assumption is that they want the STRATEGIC "AllTypes" Default View.
		//
		// THEREFORE as a default the Deliverables view should be showing the list of
		// related Deliverables below each Deliverable that is displayed even in simple
		// normal mode. [PRSC]


		$XCOND_ShowChildDeliverables 		= FALSE;
		$XCOND_ShowParentDeliverables		= FALSE;
		
//		$XDeliveryType = $ddfilters['DeliverableTypeID']; 
//		print "DELIVERTYPE [" . $XDeliveryType . "]<br>"; 
		$XDeliveryType = 3; 
		
		if($XDeliveryType  ==  1)		$XCOND_ShowChildDeliverables  = TRUE;
		if($XDeliveryType  ==  2)		$XCOND_ShowParentDeliverables  = TRUE;
		if($XDeliveryType  ==  3)
			{
					$XCOND_ShowChildDeliverables  = TRUE;
					$XCOND_ShowParentDeliverables  = TRUE;
			}
		$XCOND_ShowChildDeliverables  = TRUE;
		$XCOND_ShowParentDeliverables  = TRUE;

		
		

		/*------------------------------------------------------------------
		 *            If required then calculate the Parent Deliverables
		 *-----------------------------------------------------------------*/

		
		
//		print "HERE30<br>";
			
		if($XCOND_ShowParentDeliverables == TRUE)
			{

			if($CNT_total_parents   > 0)
			{

//			print "HERE30B<br>";
			
			$CNT_parentsxx		= 0;
			$PRNT_Deliverables   = [];
	
			foreach ($parent_deliverables as $rpdt)
			{

//					 echo "<pre>-------------------------------[PARENT]";
//					 print_r($rpdt);
//					 echo "</pre>";
					$DPID = $deliverable['DeliverableParentID'];

					if($DPID == $rpdt['DeliverableID'])
					{
//					print "HERE30S3  [" . $DPID . "][" . $rpdt['DeliverableID'] . "] <br>";
					$PRNT_Deliverables[$CNT_parentsxx]['title'] = 
								$rpdt['BusinessUnitCode'].' '.$rpdt['DeliverableCode'].' - ' 
								.$rpdt['DeliverableDescShort'];
						$PRNT_Deliverables[$CNT_parentsxx]['id'] = $rpdt['DeliverableID'];
						$PRNT_Deliverables[$CNT_parentsxx]['percent'] = $rpdt['ApproxComplete'];
						$PRNT_Deliverables[$CNT_parentsxx]['status'] = $rpdt['StatusDescription'];
						$PRNT_Deliverables[$CNT_parentsxx]['statusid'] = $rpdt['StatusID'];
						$CNT_parentsxx++;
					}							// EO If Parent for sure	
				}								// EO for each Parent deliverable
	
			}									/* EO If Total Parents greater then zero */
			
//				echo "XSTRAT CNT [" . $CNT_parentsxx . "]<br>";		
			
			
		}										/* EO Kids should be shown at all */

	
		/*------------------------------------------------------------------
		 *            If required then calculate the Child Deliverables
		 *-----------------------------------------------------------------*/
		
		
		 if($XCOND_ShowChildDeliverables == TRUE)
			{
			$CNT_childrenxx		= 0;
			$CHLD_Deliverables   = [];
				
			if($CNT_total_kids   > 0)
			{

//			print "HERE30S<br>";
			
			foreach ($supporting_deliverables as $rcdt)
			{


//					 echo "<pre>-------------------------------[CHILD]";
//					 print_r($rcdt);
//					 echo "</pre>";

					$DCID = $deliverable['DeliverableID'];
//					print "HERE30-XX  [" . $DCID . "][" . $rcdt['DeliverableParentID'] . "] <br>";
					
					if($rcdt['DeliverableParentID'] == $DCID)
						{
//						print "HERE30-CH  [" . $DCID . "][" . $rcdt['DeliverableParentID'] . "] <br>";
						$CHLD_Deliverables[$CNT_childrenxx]['title'] = 
								$rcdt['BusinessUnitCode'].' '.$rcdt['DeliverableCode'].' - ' 
								.$rcdt['DeliverableDescShort'];
						$CHLD_Deliverables[$CNT_childrenxx]['id'] = $rcdt['DeliverableID'];
						$CHLD_Deliverables[$CNT_childrenxx]['percent'] = $rcdt['ApproxComplete'];
						$CHLD_Deliverables[$CNT_childrenxx]['status'] = $rcdt['StatusDescription'];
						$CHLD_Deliverables[$CNT_childrenxx]['statusid'] = $rcdt['StatusID'];
						$CNT_childrenxx++;
//						echo "FCN-CHILD CNT [" . $CNT_childrenxx . "]<br>";		
						
					}								// EO If we should show

				}									// EO for each kid deliverable
	

			}									/* EO If Total Kids greater then zero */

//			echo "XOPER CNT [" . $CNT_childrenxx . "]<br>";		
			
		}										/* EO Parents should be shown at all */
								
		
	/*-------------------------------------------------( EO Ugly Header Block )	*/
		
     ?>
	
	
	
	
	
<br><br><br><br>
	<div class="block">
	
		<div class="col-sm-12">
			
			<div class="row">

				<div class="col-xs-7">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<h1><?= $deliverable['BusinessUnitCode'] ?> <?= $deliverable['DeliverableCode'] ?> - 
						<?= $deliverable['DeliverableDescShort'] ?> </h1>
						<p class="text-muted"><?= $deliverable['ServiceAreaName'] ?></p>
						<!-- <p class="text-muted" style="">Target Completion Date:
						 <?= date('M d, Y', strtotime(substr($deliverable['ApproxEndDate'],0,10))) ?></p> -->
					</div>
				</div>

				<div class="col-xs-5">
					<?php
						$label_color = '';
						if($deliverable['StatusID'] == 1 ) $label_color = 'success';
						if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
						if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
						if($deliverable['StatusID'] > 3 ) $label_color = 'info';
						
						?>
					<div class="text_block text-right" style="padding-bottom: 0; padding-right: 0">
						<p style="font-size:2.5em; margin:5px 0 5px; padding:0; line-height:1em; font-weight:bold">
						<?= ($deliverable['ApproxComplete'] == 100 ? "Completed " : $deliverable['ApproxComplete']."%")?></p>
						<p style="font-size:1.2em;" class="label label-<?= $label_color ?>">
						<?= $deliverable['StatusDescription'] ?></p>
					</div>
				</div>

			</div>

		</div>


		<div class="col-sm-12">
			<div style="padding-bottom: 15px;">
			<?= $deliverable['DeliverableDesc'] ?>
                <div class="row">
                    <!-- SO Col-sm-4 -->
                    <div class="col-sm-4">

                        <p class="" style=""><b>
                                Completion Target:
                                <?php
                                $trueInitialDate 	= '';
                                $trueInitialDate 	= $deliverable['InitialTarget'];
                                if(!$trueInitialDate)
                                {
                                    $trueInitialDate = 'TBD';
                                }
                                else
                                {
                                    $trueInitialDate = date('M d, Y', strtotime(substr($trueInitialDate,0,10)));
                                }
                                print $trueInitialDate;
                                ?>
                            </b>
                        </p>
                    </div>
                </div>
			<div class="row">
				<div class="col-sm-4">
					<p><strong>Outcome: &nbsp; <?= $deliverable['OutcomeName'] ?> <!-- (<?= $deliverable['FocusAreaName'] ?>) --></strong></p>
					<p><?= $deliverable['OutcomeDesc'] ?></p>
				</div>
				<div class="col-sm-4">
					<p><strong>Risk: &nbsp; <?= $deliverable['RiskDescShort'] ?></strong></p>
					<p><?= $deliverable['RiskDesc'] ?></p>
				</div>
				<div class="col-sm-4">
					<p><strong>Admin Priority: &nbsp; <?= $deliverable['AdminPriorityName'] ?></strong></p>
					<p><?= $deliverable['AdminPriorityDesc'] ?></p>
				</div>
			</div>

			</div>
		


				<?php 
					/*
					 * ------------------------------------------------------------------------
					 * 		This section below was added with the 201601 Changes in order
					 * 		to display the extra fields now shown on the list and detail 
					 * 		deliverable display.
					 * 
					 * 		Future versions of this may show a list in columns as the desire
					 * 		by finance is to have this as a zero to many capability.
					 * -----------------------------------------------------------20160126 PRSC
					 */
					
					?>

					<div class="row">
			
					<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service FOR (Impacted)
					 * =-------------------------------------------------  PRSC
					 */
					?>
					
						<div class="col-sm-4">
								<p><strong>Service Impacted by Initiative: &nbsp; </strong></p>
								<p><?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverable['ServiceFOR'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($services as $rt) 
										{
										if($rt['ServiceID'] == $deliverable['ServiceFOR'])
										{
											$biu_Name = "";
											$servarea_Name = "";
										    foreach ($business_units as $st) 
											{
												
											if($st['BusinessUnitID'] == $rt['BusinessUnitID'])
											$biu_Name = $st['BusinessUnitName'];
											}
											
											foreach ($serviceareas as $st) 
											{
											if($st['ServiceAreaID'] == $rt['ServiceAreaID'])
											$servarea_Name = $st['ServiceAreaName'];
											}
									
//										$realVL = $biu_Name . "->" . $servarea_Name . '->' . 
//													$rt['ServiceShortNM'];
										$realVL = $rt['ServiceShortNM'] . " (" . $biu_Name . "," . $servarea_Name . ")";
										}
										}
									}
								}
								else 
									{ $realVL = "N/A"; }

								echo $realVL;	  
								?></p>
	
							</div>
						
					<?php 
					/*---------------------------------------------------------
					 * 			ELEMENT : Service BY
					 * =-------------------------------------------------  PRSC
					 */
					?>
						
							<div class="col-sm-8">
							<p><strong>Service Performing this Initiative: &nbsp; </strong></p>
							<p><?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverable['ServiceBY'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($services as $rt) 
										{
										$biu_Name = "";
										$servarea_Name = "";
										
										if($rt['ServiceID'] == $deliverable['ServiceBY'])
										{
										    foreach ($business_units as $st) 
											{
												
											if($st['BusinessUnitID'] == $rt['BusinessUnitID'])
											$biu_Name = $st['BusinessUnitName'];
											}
											
											foreach ($serviceareas as $st) 
											{
											if($st['ServiceAreaID'] == $rt['ServiceAreaID'])
											$servarea_Name = $st['ServiceAreaName'];
											}
										
//										$realVL = $biu_Name . "->" . $servarea_Name . '->' . 
//													$rt['ServiceShortNM'];
										$realVL = $rt['ServiceShortNM'] . " (" . $biu_Name . "," . $servarea_Name . ")";
										}
										}
									}
								}
								else 
									{ $realVL = "N/A"; }
								echo $realVL;	  
								?></p>
							</div>

					</div>
					
					
					<div class="row">
						
						
						
					<?php 
					/*
					 * ------------------------------------------------------------------------
					 * 		This section below was added with the 201601 Changes in order
					 * 		to display the extra fields now shown on the list and detail 
					 * 		deliverable display.
					 * 
					 * 		Future versions of this may show a list in columns as the desire
					 * 		by finance is to have this as a zero to many capability.
					 * -----------------------------------------------------------20160126 PRSC
					 */
					
					?>

							<div class="col-sm-4">
								<p><strong>Source: &nbsp; </strong>
							
							<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverable['Source'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($sources as $rt) 
										{
										if($rt['SourceID'] == $deliverable['Source'])
										$realVL = $rt['SourceShortName'];
										}
									}
								}
								else 
									{ $realVL = "N/A"; }
								echo $realVL;	  
								?></p>
							</div>
	
						<div class="col-sm-8">
								<p><strong>Published (Yes/No): &nbsp; </strong>
								
							<?php 
								$realID = 0;
								$realVL	= "";
								$realID = $deliverable['PublishedYN'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($publishedinfo as $rt) 
										{
										if($rt['PublishedID'] == $deliverable['PublishedYN'])
										$realVL = $rt['PublishedShortName'];
										}
									}
								}
								else 
									{ $realVL = "N/A"; }
								echo $realVL;	  
								?></p>								
						</div>			

					</div>
                                <?php
                                $style = '';
                                $truedate 	= '';
                                $targ_date 	= $deliverable['ApproxEndDate'];
                                if(!$targ_date)
                                {
                                    $truedate = 'TBD';
                                }
                                else
                                {
                                    $truedate = date('M d, Y', strtotime(substr($targ_date,0,10)));

                                    if ($trueInitialDate != 'TBD') {
                                        if (strtotime($truedate) > strtotime($trueInitialDate)) {
                                            $style = 'color:red';
                                        }
                                    }
                                }
                                ?>

            <p class="" style="">
                <b>
                    Currently Estimated to Complete:
                    <span style="<?php echo $style ?>">
                                    <?php
                                    print $truedate;
                                    ?>
                                    </span>
                </b>
            </p>
	
	
	
	
	
					</div>
				</div>

			

		


     <?php 
         /*------------------------------------------------------------------
          * 	This next block should handle whether a slider bar is
          * 	displayed or a simple colored bar is displayed.  The latter
          * 	should be the default, while the slider should only display
          * 	if the current user has Admin or Modification rights to 
          * 	this component.
          * 
          * 	LOGIC
          * 	Create flags to check
          * 	{ Display Colored bar if Default state }
          * 	else
          * 	{ Display Slider Bar }
          * 
          * 
          * ---------------------------------------------------20160828 PRSC-
          */
        ?>


		<?php 

			// Default flags
			$cond_show_editableYN							= false;	
			$cond_show_colourbar_perc_completeYN			= false;	
			$cond_allowslider_editYN						= false;
			$cond_enter_status_blockYN						= false;	
			$cond_show_only_statusYN						= false;	
			$cond_show_stat2 								= false;			// Never use or remove completely this block
			$cond_show_whoupdatedYN							= false;			
			

			// If Deliverable Strategic then everyone should see the status in non-edit
			if($deliverable['DeliverableTypeID'] == 1)
				{
				$cond_show_only_statusYN						= true;	
				$cond_show_colourbar_perc_completeYN			= true;	
				$cond_show_whoupdatedYN							= true;				
				}			

			
			// If same business unit then show Strategic and Operational Status as editable
			if($deliverable['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode'))
				{
				$cond_show_only_statusYN						= true;	
				$cond_show_colourbar_perc_completeYN			= true;	
				if($deliverable['DeliverableTypeID'] == 1 ||
					$deliverable['DeliverableTypeID'] == 2 ||
					$deliverable['DeliverableTypeID'] == 3)		// Editable Strategics same unit
						{
						$cond_show_editableYN							= true;	
						$cond_show_colourbar_perc_completeYN			= false;	
						$cond_allowslider_editYN						= true;	
						$cond_enter_status_blockYN						= true;	
						$cond_show_whoupdatedYN							= true;
						}
				else
						{			// Operationals visible non-edit only by default if same business unit
						$cond_show_only_statusYN						= true;	
						$cond_show_colourbar_perc_completeYN			= true;	
						$cond_enter_status_blockYN						= false;	
						$cond_show_whoupdatedYN							= true;
						}	
//				print "F2_EDITYN [" . $cond_show_editableYN . "]<br>";
//				print "F2_SHCBAR [" . $cond_show_colourbar_perc_completeYN . "]<br>";
//				print "F2_ALLOWS [" . $cond_allowslider_editYN . "]<br>";
//				print "F2_ALLEDS [" . $cond_enter_status_blockYN . "]<br>";
//				print "F2_WHOUPD [" . $cond_show_whoupdatedYN . "]<br>";
				}

				
			// [TASK0084832] If user is Extended than allow status to be seen in main list.
			// Also Extended users may also click on link that goes to the STATUS screen
			// where they can then update IF this is in their own business unit. PRSC

			$glb_extendedYN 			= $this->session->userdata('UserExtendedYN');
			if($glb_extendedYN == true)
				{
				  // Extended can edit both Strategic and Operational from its own group	
				  if( $deliverable['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode'))
					{
					$cond_show_editableYN							= true;	
					$cond_show_colourbar_perc_completeYN			= false;	
					$cond_show_only_statusYN						= false;	
					$cond_allowslider_editYN						= true;		
					$cond_enter_status_blockYN						= true;	
					$cond_show_whoupdatedYN							= true;
					}
						
				  // View Operational from other bus units
				  if($deliverable['DeliverableTypeID'] == 2 &&
				  		$deliverable['BusinessUnitCode'] != $this->session->userdata('UserBusinessUnitCode'))	
						{
						$cond_show_editableYN							= false;	
						$cond_show_colourbar_perc_completeYN			= true;	
						$cond_show_only_statusYN						= true;	
						$cond_allowslider_editYN						= false;		
						$cond_enter_status_blockYN						= false;	
						$cond_show_whoupdatedYN							= true;
						}
					
//					print "F7_EDITYN [" . $cond_show_editableYN . "]<br>";
//					print "F7_SHCBAR [" . $cond_show_colourbar_perc_completeYN . "]<br>";
//					print "F7_ALLOWS [" . $cond_allowslider_editYN . "]<br>";
//					print "F7_ALLEDS [" . $cond_enter_status_blockYN . "]<br>";
					}					
					
				

		// Admin Version
		if($this->session->userdata('UserAdminFlag'))
					{
					$cond_show_editableYN							= true;	
					$cond_show_colourbar_perc_completeYN			= false;	
					$cond_show_only_statusYN						= false;	
					$cond_allowslider_editYN						= true;				// Sub Flag to show slider in Form				
					$cond_enter_status_blockYN						= true;	
					$cond_show_whoupdatedYN							= true;
					}
					

		// If not editable scrollbar then make sure percentage always shown

		if($cond_allowslider_editYN		!= true)
				{
					$cond_show_colourbar_perc_completeYN			= true;
				}			
					
		// Determine if	Status is displayed as Editable or just Info version		
					
			//$cond_enter_status_blockYN						= true;			
					
		// Test State Override flags
//			$cond_show_editableYN							= false;	
//			$cond_show_colourbar_perc_completeYN			= true;	
//			$cond_show_only_statusYN						= true;	
//			print "F99_EDITYN [" . $cond_show_editableYN . "]<br>";
//			print "F99_SHCBAR [" . $cond_show_colourbar_perc_completeYN . "]<br>";
//			print "F99_ALLOWS [" . $cond_allowslider_editYN . "]<br>";
//			print "F99_ALLEDS [" . $cond_enter_status_blockYN . "]<br>";
//			print "F99_WHOUPD [" . $cond_show_whoupdatedYN . "]<br>";
			
		?>			
			
		
		<?php 
		/*--------------------------------------------------------------------------------------------------
		 * 		This section is used to show the scroll bar for percentage of complete of each deliverable.
		 *--------------------------------------------------------------------------------------------------
		 */
		if($cond_show_colourbar_perc_completeYN) :		
		?>

		<div class="col-sm-12">
			<div class="row">
				<?php
					$label_color = '';
					if($deliverable['StatusID'] == 1 ) $label_color = 'success';
					if($deliverable['StatusID'] == 2 ) $label_color = 'warning';
					if($deliverable['StatusID'] == 3 ) $label_color = 'danger';
					if($deliverable['StatusID'] > 3 ) $label_color = 'info';
					?>
					
					
				<div class="progress_bg" title="<?= $deliverable['StatusDescription'] ?>">
					<div class="completion_label label label-<?= $label_color ?>" 
								style="width:<?= $deliverable['ApproxComplete'] ?>%">
					
						<span class="label_text"><?= $deliverable['StatusDescription'] ?> -
						 <?= ($deliverable['ApproxComplete'] == 100 ? "Completed" : 
						 			$deliverable['ApproxComplete']."% complete")?><span>
					</div>
				</div>
				<?php endif ?>
				
			</div>
		</div>
		
				
		<?php 
		/*----------------------------------------
		 *    EO Status Color Bar
		 *----------------------------------------    
		 */
		
		endif ?>
		
		
	
		<?php 
		/*-------------------------------------------------------
		 * 		Show Status Field on its own if non-update ver
		 * --------------------------------------- 20160826 PRSC-
		 */
						
		if($cond_show_only_statusYN == true) :		
		?>
		
		<div class="col-sm-12">

			<div class="row" style="background:#f2f2f2;">

				<div class="col-sm-12">

					<div class="text_block ">
					
						<h1>Status</h1>
						
			<?php if(empty($deliverable['StatusUpdate'])): ?>
					<p class="muted">
						<em>No update at this time.</em>
					</p>
			<?php else: ?>
					<?= $deliverable['StatusUpdate'] ?>
			<?php endif ?>
		<?php endif ?>
	
					</div><!-- EO text block -->
				</div><!-- EO col-sm12 -->	
			</div>
		</div><!-- EO col-sm-12 -->		
							
		

		<?php 
		/*---------------------------------------------------------------------------------	
		 * This shows the editable coloured bar that is a reflection of the percentage 
		 * as well as the status entry portion and the drop down selections.
		 * --------------------------------------------------------------------------------
		 */
		
		if($cond_show_editableYN = true) :		

		?>

		<!-- SECTION:	Display Edit Scroll Bar -->
		
		<div class="col-sm-12">

			<div class="row" style="background:#f2f2f2;">

				<div class="col-sm-12">

					<div class="text_block ">


					<?php if($cond_show_stat2 == true):		
					?>					
						<h1>Status S2</h1>

						<?php if(!empty($success)): ?>
							<div class="alert alert-success"><?= $success ?></div>
						<?php elseif(!empty($fail)): ?>
							<div class="alert alert-error"><?= $fail ?></div>
						<?php endif; ?>

						<?php if(validation_errors() != ""): ?>
						<div class="alert alert-danger">
							<?= validation_errors() ?>
						</div>
						<?php endif ?>
						
					<?php endif ?>




					<?php 
						
						/*------------------------------------------------------
						 * 		SO FORM BLOCK: Show Status box area Y/N
						 * -----------------------------------------------------
						 */
						
//						if($this->session->userdata('UserAdminFlag') || 
//								$deliverable['BusinessUnitCode'] == $this->session->userdata('UserBusinessUnitCode')): 
						if($cond_allowslider_editYN):		
						?>
							
						<?php 
						
						/*--------------------------------------------------------
						 * 		Use CI form library to define a data entry form
						 * -------------------------------------------------------
						 */
						
						echo form_open('update-status/' . $deliverable['DeliverableID']);
						echo form_hidden('DeliverableID',$deliverable['DeliverableID']);
						// echo form_hidden('LastModBy',$this->session->userdata('UserID'));
						echo form_hidden('LastModBy',$this->session->userdata('UserName'));
						?>



							<?php 
							/*---------------------------------------------------------
							 * 		Use CKEDIT to enter the Status Information Data
							 * --------------------------------------------------------
							 */
							
							if($cond_enter_status_blockYN	== true):
							?>

							<textarea name="StatusUpdate" id="StatusUpdate" rows="10" maxlength="5000">
								<?= set_value('StatusUpdate', $deliverable['StatusUpdate']) ?>
							</textarea>

							<script>
					            CKEDITOR.replace( 'StatusUpdate', {
									toolbar: [
										{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ] },			// Defines toolbar group without name.
										{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', '-', 'RemoveFormat' ] },
										{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList' ] },
									]
								});
					        </script>
							<?php 
							/*		EO cond_enter_status_blockYN	*/	endif ?>
						
							<?php 
							/*-------------------------------------------------
							 * 		Show Slider Area Titles
							 * ------------------------------------------------
							 */
							?>
						
							<!-- SECT: Slider area Titles -->
							<h3>Approximate Completion</h3>
							<p>Slide the bar to adjust the approximate percent complete and update the approximate completion date.
							</p>



							<?php 
							/*-------------------------------------------------
							 * 		Show the Editable Slider
							 * ------------------------------------------------
							 */
							?>
							
							<!-- SECT: Editable Slider Bar displayed for user to modify percentage  -->
							<div id="slider" style="margin:10px 0px 15px 0;"></div>

							<div class="row">
								<div class="col-sm-4">

									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-thumbs-up"></i></span>
										<input type="text" name="ApproxComplete" id="ApproxComplete" class="form-control" readonly> 
									</div>

									<script>
										$(function() {
											$( "#slider" ).slider({
												value:<?php echo $deliverable['ApproxComplete']; ?>,
												min: 0,
												max: 100,
												range: "min",
												step: 5,
												slide: function( event, ui ) {
													$( "#ApproxComplete" ).val( ui.value + "%" );
												}
											});
											$( "#ApproxComplete" ).val( $( "#slider" ).slider( "value" )+"%" );
										});
									</script>

								</div><!-- EO Show Slider Cell col-sm-4 -->
								
								
								<!-- SECT:	Enter End Date -->
								<div class="col-sm-4">

									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input type="text" name="ApproxEndDate" 
												id="ApproxEndDate" class="form-control" 
												value="<?= (!empty($deliverable['ApproxEndDate'])) ? 
												date('m/d/Y', strtotime(substr($deliverable['ApproxEndDate'],0,10))) : 
												date('m/d/Y', mktime(0, 0, 0, 3, 31, date("Y")+1)); ?>" readonly>
									</div>
									<script>
										$('#ApproxEndDate').datepicker({ 
											minDate: '04/01/2014',
                                            changeMonth: true,
                                            changeYear: true
											// defaultDate: "04/01/2015"
										});
									</script>

								</div><!-- EO Enter End Date col-sm-4 -->
								
								
								<!-- SECT: Create Status Dropdown -->
								<div class="col-sm-4">
									<select class="form-control" name="StatusID">
									
										<?php		
										/*	 Create the Drop down box for available status from DB REF table for Statuses
										 * 	 
										 * 	TODO:	This requires some error testing in case no Status.
										 * 
										 */
									
										foreach ($statuses as $status): ?>
										
										<option value="<?= $status['StatusID'] ?>" 
										<?= set_select('StatusID', $status['StatusID'], 
													$deliverable['StatusID'] == $status['StatusID'] ? TRUE : FALSE) ?> >
										<?= $status['StatusDescription'] ?></option>
										<?php endforeach ?>
										
									</select>

								</div><!-- EO Build Status Dropdown Box col-sm4 -->

							</div><!-- EO Show Slider Block Entry Row -->

							<!-- SECT: Submit button and hidden data fields -->
							<div class="row">

								<div class="text-center" style="padding:25px;">
									<input type="hidden" name="id" value="<?= $deliverable['DeliverableID'] ?>">
									<input type="submit" class="btn btn-info" value="Update">
								</div>

							</div>
							<!-- EO Submit button -->

						</form>
						
						<?php 
						/*-------------------------------------------------
						 * 		EO BLOCK FORM: Show Slider Area Titles
						 * ------------------------------------------------
						 */
						?>
						
						
						

						<?php 
						
						/*-------------------------------------------------------
						 * 		If status is Strategic deal with empty Status
						 * 		Data field  CNIU
						 * --------------------------------------- 20160826 PRSC-
						 */
						
						elseif($deliverable['DeliverableTypeID'] == 1): ?>
						
						
						<?php endif ?>

					</div> <!-- EO ?? -->


					<?php 
					/*--------------------------------------------------------
					 * 		Show the Last Update By section at bottom
					 * ------------------------------------------------- PRSC-
					 */
					?>

					<?php if($cond_show_whoupdatedYN): ?>
	
					<div class="text_block">
						
						<?php if(!empty($deliverable['StatusUpdate'])): ?>
						<p class="muted text-right">
							<em>
								Updated <?= date('M d, Y', strtotime(substr($deliverable['LastModDate'],0,10))) ?> 
								<br class="visible-xs"> by <?= $deliverable['LastModBy'] ?>.
							</em>
						</p>
						<?php endif ?>

					</div><!-- EO Last Modified By text-box --> 

					<?php endif // EO whoupdatedYN ?>


				</div>

			</div>
		
			

		

		<?php endif
		/* <<<<---- EO Show Admin level Editable section */
		
		 ?>


 
        
        
 <?php
 /*---------------------------------------------------------------------
  * 
  * 	This is used with the Strategic Listings to build a list of 
  * 	links that allows users to update the status information on 
  * 	each of the related Deliverables to this parent Strategic one.
  * 
  * 	NOTE:		Delivery Types are
  * 	1 - Strategic (Show only related children )
  * 	2 - Children (Show only related parents)
  * 	3 - All
  * -----------------------------------------------------20160222 PRSC
  */
?>
		<?php 
		$XCOND_ShowRelatedParents 		= FALSE;
		$XCOND_ShowRelatedChildren		= FALSE;
//		$XDeliveryType					= $ddfilters['DeliverableTypeID'];
		$XDeliveryType					= 3;					// Over ride in this screen to all - PRSC
		
		// Check if Show the Strategic Values
		if($XDeliveryType == 2 && $CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedParents 	= TRUE; }
				
		// Check if Show the Operational Values		
		if($XDeliveryType == 1 && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedChildren	= TRUE; }
				
		// Check if base view requires showing both		
		if($XDeliveryType == 3 && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedChildren 	= TRUE; }
		if($XDeliveryType == 3 && $CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedParents	= TRUE; }
		// Check if base view requires showing both		
		if($XDeliveryType == '' && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedChildren 	= TRUE; }
		if($XDeliveryType == '' && $CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedParents	= TRUE; }
				
//		print "TYPE [" . $XDeliveryType . "]<br>";		
//		print "CHILD CNT [" . $CNT_childrenxx . "]<br>";		
//		print "STRAT CNT [" . $CNT_parentsxx . "]<br>";		
		
//		$XCOND_ShowRelatedParents 		= TRUE;
//		$XCOND_ShowRelatedChildren		= TRUE;
		
		?>
	
					
		<?php		
		/*-----------------------------------------------
		 * 		Strategics Header
		 * -----------------------------------------------
		 */
		?>
		<?php if($XCOND_ShowRelatedParents == TRUE ): ?>		
		<div class="row">	
		<div class="col-sm-12 supporting_deliverables">
			<p class="">

			<?php 
				echo 'Related Strategic Initiatives';
			?>

			</p>
		<ul>
		<?php
		/* -------------------------------------------------------------
		 * 			Check if Parent Deliverables to show
		 * -------------------------------------------------------------
		 */
		
		foreach ($PRNT_Deliverables as $rcdt): ?>
				<?php
					$label_color = '';
					if($rcdt['statusid'] == 1 ) $label_color = 'success';
					if($rcdt['statusid'] == 2 ) $label_color = 'warning';
					if($rcdt['statusid'] == 3 ) $label_color = 'danger';
					if($deliverable['StatusID'] > 3 ) $label_color = 'info';
					?>
					
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $rcdt['id'] ?>">
						<?= $rcdt['title'] ?>
					</a>
					
					 <!-- - <?= $rcdt['percent'] ?>% completed 
					    <?= ($rcdt['percent'] < 100) ? '('.$rcdt['status'].
					            ')' : '' ?>  -->
					            
					 - <span title="<?= $rcdt['status'] ?> ">
					          <?= $rcdt['percent'] ?>% completed</span>
				</li>
				<?php endforeach ?>

			</ul>
		</div>
		</div>
		
		<?php endif ?>
		
		
		
		<!-- EO Display ALL Related Supporting Deliverables - Strategic or Operational -->
	    
	
	
		<?php		
		/*-----------------------------------------------
		 * 		Operationals(Child) Header
		 * -----------------------------------------------
		 */
		if($XCOND_ShowRelatedChildren == TRUE):
		?>		
		<div class="row">	
		<div class="col-sm-12 supporting_deliverables">
			<p class="">

			<?php 
				echo 'Related Operational Initiatives';
			?>

			</p>
		<ul>
		<?php
		/* -------------------------------------------------------------
		 * 			Check if Child Deliverables to show
		 * -------------------------------------------------------------
		 */
		
		foreach ($CHLD_Deliverables as $rcdt): ?>
				<?php
					$label_color = '';
					if($rcdt['statusid'] == 1 ) $label_color = 'success';
					if($rcdt['statusid'] == 2 ) $label_color = 'warning';
					if($rcdt['statusid'] == 3 ) $label_color = 'danger';
					if($deliverable['StatusID'] > 3 ) $label_color = 'info';
					?>
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $rcdt['id'] ?>">
						<?= $rcdt['title'] ?>
					</a>
					
					 <!-- - <?= $rcdt['percent'] ?>% completed 
					    <?= ($rcdt['percent'] < 100) ? '('.$rcdt['status'].
					            ')' : '' ?>  -->
					 - <span title="<?= $rcdt['status'] ?> ">
					          <?= $rcdt['percent'] ?>% completed</span>
				</li>
				<?php endforeach ?>
          
			</ul>
		</div>
		</div>
		<?php //endif ?><!-- EO Display ALL Related Operationals -->
 
   <!--  EO Show Related Deliverables -->
     
     </div>
     </div>
        
   <!--  END OF -->     

	</div>


	<?php endif; ?>

	<?php $this->load->view('template/copyright') ?>
