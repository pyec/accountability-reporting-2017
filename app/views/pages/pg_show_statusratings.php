<?php 

 	/*================================================================
	 * 
	 * 	MODULE:			pg_show_statusratingss.php
	 * 	AUTHOR:			R.Stephen Chafe (Zen River Software)
	 * 	CREATED:		2016_03_06
	 * 
	 * 	This module contains the LIST display for all of the 
	 *  statusrating data.
	 *  
	 * ==============================================================
	 */


	/*----------------------------------------------------------------
	 * 		DEBUG ALL
	 * 		This is for debugging the RISKS array to make sure that
	 * 		the data is loaded from the database correctly. Only
	 * 		use this for testing and keep uncommented otherwise.
	 * -----------------------------------------------------PRSC 201601
	 */
//	 echo "<pre>";
//	 print_r($statusratings);
//	 echo "</pre>";
?>	
<br><br><br><br><br><br>


	
<?php if (count($statusratings)>0): ?>


<div class="col-sm-12">

	<div class="results">    
	
	<!-- ------------------------------------------------------------------EO-->  
	
		<div class="pull-right hidden-xs">
			<?php if (count($statusratings)>0):
		   /*
     		* ----------------------------------------------------------------
	 		*	EXPORT SECTION
	 		*
	 		*	Insert the Hidden Fields in Form to create a LIST of the 
	 		*	variables used for knowledge of the EXPORT. 
	 		*
	 		*	THIS SHOULD ONLY BE FOR ADMINS
	 		* -----------------------------------------------------PRSC 201601
	 		*/  
	 		
			?>
		
			
		</div>
		
		
		
		<div class="result_count">
			<p>
				<?php 								/* TITLES */
					echo count($statusratings);
					echo ' STATUS RATING List';
					echo (count($statusratings) == 1 ? '' : 'ings'); 
				?>
			</p>
		</div>

			<?php endif ?>
		</div>
	
	</div>


<?php 

	$statusrating_id 		= '';
	$statusrating_parent_id 	= '';
	$count	= 0;

	// echo "<pre>";
	// print_r($statusratings);
	// echo "</pre>";

	foreach ($statusratings as $dt): 
	

	?>

	<div class="col-sm-12">

	
 
    <!-- ------------------------------------------------------------------EO-->  
   
	<?php if (count($statusratings)>0):	?>


	<?php 
	$statusrating_id 		= '';
	$statusrating_parent_id 	= '';
	$count	= 0;

		?>


	<?php 
	/*
    * ----------------------------------------------------------------
	*	DISPLAY LINE
	*
	*	Show Detailed StatusRating Information for Users to see.
	*
	* -----------------------------------------------------PRSC 201601
	*/  
	 		
	?>




<div class="block">
	
		<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-12">
					<div class="text_block" style="padding-left:0;padding-bottom:0;">
						<?php 
						//*-------------------------------( Show Edit Option only if Admin) */
						if($this->session->userdata('UserAdminFlag')): ?>
							<span class="pull-right"><a href="<?= base_url() ?>
							edit-statusrating/<?= $dt['StatusID'] ?>" 
							title="Update status details">
							<i class="fa fa-pencil"></i></a></span>
						<?php endif ?>

						<h1>
						<?php /* $dt['StatusRatingID'] */ ?>
						<?= $dt['StatusDescription'] ?> 
						 </h1>
						
					</div>
				</div>

				</div>


		</div>

	</div>
</div>

	
	<?php endif ?>
</div> 
	<?php $count++;	endforeach; ?>


	<?php $this->load->view('template/copyright') ?>

	<?php endif; ?>







