		<?php 
		/*------------------------------------------------------------------
		 * 		ELEMENT - SOURCE Selection
		 * ----------------------------------------------------20160218 PRSC
		 */
		 ?>
		 
		<div class="col-sm-4">
					<div class="filter_option">
						<select class="form-control" name="Source">

						<?php 
							
								$realID = '';
								$realVL	= "";
								if(!empty($ddfilters['Source']))
											$realID = $ddfilters['Source'];
								
								if($realID > 0)
									{
										foreach ($sources as $rt) 
										{
										if($rt['SourceID'] == $realID)
												$realVL = $rt['SourceShortName'];
										}
									}
								
								// Fill the Default Field 
								if($realID > 0)		
								{
									$uchoice = '<option value="' . $realID . '"> ' . $realVL . '</option>';
								}
								else
								{
									$uchoice = '<option value="">Select Source</option>'; 
								}
								print $uchoice;	  
								
							?>


							<?php foreach ($sources as $dt): ?>
								 <option value="<?= $dt['SourceID'] ?>" 
								 <?= set_select('SourceID', $dt['SourceID']) ?>>
								 <?= $dt['SourceShortName'] ?></option>
								
							<?php endforeach ?>
					</select>
					</div>
				</div>	
