<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['login'] 							= 'deliverables/login';
$route['validate'] 							= 'deliverables/validate';

$route['logout'] 							= 'deliverables/logout';
$route['deliverable/(:num)'] 				= 'deliverables/deliverable/$1';

$route['download'] 							= 'deliverables/download';

$route['download-deliverables'] 			= 'deliverables/download_deliverables';
$route['download-services'] 				= 'deliverables/download_services';
$route['download-risks'] 					= 'deliverables/download_risks';
$route['download-performances'] 			= 'deliverables/download_performances';
$route['download-standards'] 				= 'deliverables/download_standards';
$route['download-service-areas'] 			= 'deliverables/download_service_areas';



// Edit status of existing deliverables -  changed to below lines PRSC
//$route['edit']		 						= 'deliverables/edit';
//$route['update'] 							= 'deliverables/update';

// Edit status of existing deliverables - PRSC
$route['edit-status/(:num)']				= 'deliverables/edit_status/$1';
$route['update-status/(:num)'] 				= 'deliverables/update_status/$1';

$route['update-status/(:num)'] 				= 'deliverables/update_status/$1';

// Used for testing user membership credentials.
$route['credentials'] 						= 'deliverables/credentials';
$route['disp-groups'] 						= 'deliverables/disp_mygroups';


// Creates new deliverables
$route['edit-deliverable/(:num)'] 			= 'deliverables/add_deliverable/$1';
$route['add-deliverable'] 					= 'deliverables/add_deliverable';
$route['create-deliverable'] 				= 'deliverables/create_deliverable';

// Added for 201601 upgrades - PRSC 20160118

$route['edit-service/(:num)'] 				= 'deliverables/add_service/$1';
$route['edit-service_area/(:num)']			= 'deliverables/add_service_area/$1';
$route['edit-standard/(:num)'] 				= 'deliverables/add_standard/$1';
$route['edit-performance/(:num)'] 			= 'deliverables/add_performance/$1';
$route['edit-risk/(:num)'] 					= 'deliverables/add_risk/$1';
$route['edit-focusarea/(:num)'] 			= 'deliverables/add_focus_area/$1';
$route['edit-priorityoutcome/(:num)'] 		= 'deliverables/add_priority_outcome/$1';
$route['edit-businessunit/(:num)'] 			= 'deliverables/add_business_unit/$1';

$route['add-service'] 						= 'deliverables/add_service';
$route['add-servicearea'] 					= 'deliverables/add_service_area';
$route['add-standard'] 						= 'deliverables/add_standard';
$route['add-performance'] 					= 'deliverables/add_performance';
$route['add-risk'] 							= 'deliverables/add_risk';
$route['add-focusarea'] 					= 'deliverables/add_focus_area';
$route['add-priorityoutcome'] 				= 'deliverables/add_priority_outcome';
$route['add-businessunit'] 					= 'deliverables/add_business_unit';


$route['show-services'] 					= 'deliverables/show_services';
$route['show-serviceareas'] 				= 'deliverables/show_service_areas';
$route['show-standards'] 					= 'deliverables/show_standards';
$route['show-performances'] 				= 'deliverables/show_performances';
$route['show-risks'] 						= 'deliverables/show_risks';
$route['show-deliverables'] 				= 'deliverables/show_deliverables';
$route['show-focusareas'] 					= 'deliverables/show_focus_areas';
$route['show-priorityoutcomes'] 			= 'deliverables/show_priority_outcomes';
$route['show-businessunits'] 				= 'deliverables/show_business_units';

$route['update-servicearea'] 				= 'deliverables/update_service_area';
$route['update-risk'] 						= 'deliverables/update_risk';
$route['update-performance'] 				= 'deliverables/update_performance';
$route['update-standard'] 					= 'deliverables/update_standard';
$route['update-service'] 					= 'deliverables/update_service';
$route['update-focusarea'] 					= 'deliverables/update_focus_area';
$route['update-priorityoutcome']			= 'deliverables/update_priority_outcome';
$route['update-businessunit'] 				= 'deliverables/update_business_unit';

$route['create-servicearea'] 				= 'deliverables/create_service_area';
$route['create-risk'] 						= 'deliverables/create_risk';
$route['create-performance'] 				= 'deliverables/create_performance';
$route['create-standard'] 					= 'deliverables/create_standard';
$route['create-service'] 					= 'deliverables/create_service';
$route['create-focusarea'] 					= 'deliverables/create_focus_area';
$route['create-priorityoutcome']			= 'deliverables/create_priority_outcome';
$route['create-businessunit'] 				= 'deliverables/create_business_unit';


/*...........................adminpillar page routes......................*/

$route['edit-adminpillar/(:num)'] 				= 'deliverables/add_adminpillar/$1';
$route['add-adminpillar'] 						= 'deliverables/add_adminpillar';
$route['update-adminpillar'] 					= 'deliverables/update_adminpillar';
$route['show-adminpillars'] 					= 'deliverables/show_adminpillars';
$route['create-adminpillar'] 					= 'deliverables/create_adminpillar';


/*...........................adminpriority page routes......................*/

$route['edit-adminpriority/(:num)'] 			= 'deliverables/add_adminpriority/$1';
$route['add-adminpriority'] 					= 'deliverables/add_adminpriority';
$route['update-adminpriority'] 					= 'deliverables/update_adminpriority';
$route['show-adminpriorities'] 					= 'deliverables/show_adminpriorities';
$route['create-adminpriority'] 					= 'deliverables/create_adminpriority';


/*...........................source page routes......................*/

$route['edit-source/(:num)'] 					= 'deliverables/add_source/$1';
$route['add-source'] 							= 'deliverables/add_source';
$route['update-source'] 						= 'deliverables/update_source';
$route['show-sources'] 							= 'deliverables/show_sources';
$route['create-source'] 						= 'deliverables/create_source';


/*...........................statusrating page routes......................*/

$route['edit-statusrating/(:num)'] 				= 'deliverables/add_statusrating/$1';
$route['add-statusrating'] 						= 'deliverables/add_statusrating';
$route['update-statusrating'] 					= 'deliverables/update_statusrating';
$route['show-statusratings'] 					= 'deliverables/show_statusratings';
$route['create-statusrating'] 					= 'deliverables/create_statusrating';


// Edit status of existing deliverables
//$route['edit-deliverable']		 			= 'deliverables/add_deliverable';
$route['update-deliverable'] 					= 'deliverables/update_deliverable';

// $route['(:any)/(:any)/(:any)'] 				= 'deliverables/index/$1/$2/$3';
// $route['(:any)/(:any)'] 						= 'deliverables/index/$1/$2';
$route['(:any)'] 								= 'deliverables/index';

$route['default_controller'] 					= 'deliverables';
//$route['default_controller'] 					= 'bizplan/index';


/* End of file routes.php */
/* Location: ./application/config/routes.php */