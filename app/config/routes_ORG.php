<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['login'] 							= 'deliverables/login';
$route['validate'] 							= 'deliverables/validate';

$route['logout'] 							= 'deliverables/logout';
$route['deliverable/(:num)'] 				= 'deliverables/deliverable/$1';

$route['download'] 							= 'deliverables/download';

// Creates new deliverables
$route['edit-deliverable/(:num)'] 			= 'deliverables/add_deliverable/$1';
$route['add-deliverable'] 					= 'deliverables/add_deliverable';
$route['create-deliverable'] 				= 'deliverables/create_deliverable';

$route['add-servicelevel'] 					= 'deliverables/add_servicelevel';


// Edit status of existing deliverables
$route['edit']		 						= 'deliverables/edit';
$route['update'] 							= 'deliverables/update';

// $route['(:any)/(:any)/(:any)'] 				= 'deliverables/index/$1/$2/$3';
// $route['(:any)/(:any)'] 					= 'deliverables/index/$1/$2';
$route['(:any)'] 							= 'deliverables/index';

$route['default_controller'] 				= 'deliverables';
//$route['default_controller'] 				= 'bizplan/index';


/* End of file routes.php */
/* Location: ./application/config/routes.php */