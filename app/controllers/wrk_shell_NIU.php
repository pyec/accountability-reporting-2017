	
	
/*           
============================================================================
 
 		FUNCTION:	add_outcome()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-servicestandard" view file.
 		This will allow users to create and enter a new ServiceArea
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_outcome($id = NULL)
	{	
		$data['data_state'] = "";
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		
	// boxes in the "add-service" web page.
		if($id)	$data['outcome'] = $this->main_model->md_get_outcome($id);
		
		$data['page'] = "pages/pg_add_outcome";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
	
	
	public function show_outcomes($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		// This shows a list of the outcomes for users to reference
		$data['outcomes'] 			= $this->main_model->md_get_outcomes();
		$data['page']				= "pages/pg_show_outcomes";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	

public function update_outcome()
	{
			
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data = $this->input->post(NULL,TRUE);
		$data['data_state'] = "";
		
		$id = $this->input->post('OutcomeID',TRUE);

		if($id > 0)
		{
			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_outcome($data))	
			{
					$data['data_state'] = "Outcome Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Outcome Record";
			}
			
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_outcomek" web page - PRSC
		
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['outcome'] 		= $this->main_model->md_get_outcome($id);
			$data['page'] = "pages/pg_add_outcome";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		}
	}
	
	public function create_outcome()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data = $this->input->post(NULL,TRUE);
		$data['data_state'] = "";
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */
			
		$fresp = 1;
		
		if($fresp > 0)
		{
			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_outcome($data))	
			{
					$data['data_state'] = "Outcome Record Created Successfully";
					$this->session->set_flashdata('success', 'Outcome was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Outcome Record.');
					$data['data_state'] = "Error Creatng Outcome Record";
			}
		
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_outcome" web page.
		
			
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['outcome'] 		= $this->main_model->md_get_outcome($id);
			$data['page'] = "pages/pg_add_outcome";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		}	
	else 
	{
//		print "Error in Key <br>";
	}	
		
		
}
		
		
//------------------------------------------------( EO Function Block )


		