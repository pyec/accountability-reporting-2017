<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*           
============================================================================
 Class: 		Deliverables
 Description:	Main application controller. 
 				Interacts with the deliverables_model and application views.
============================================================================
*/
class Deliverables extends CI_Controller {
/*           
============================================================================
 index
----------------------------------------------------------------------------
 Displays all deliverables for a particular filter
============================================================================
*/
	public function index()
	{
		// check for session
		if($this->session->userdata('UserName'))
		{
			
			
                   /*
                    *------------------------------------------------------
                    *      CI Define the dropdowns for Filter
                    *-------------------------------------------------PRSC 
                    */
			
			
			// ensures select inputs get repopulated
			$this->form_validation->set_rules('DeliverableTypeID', 'type', 'trim');
			$this->form_validation->set_rules('BusinessUnitID', 'business unit', 'trim');
			$this->form_validation->set_rules('ServiceArea'.$this->input->post('BusinessUnitID',TRUE), 'service area', 'trim');
			// $this->form_validation->set_rules('OutcomeID', 'outcome', 'trim');
			$this->form_validation->set_rules('FocusAreaID', 'focus area', 'trim');
			$this->form_validation->set_rules('AdminPillarID', 'administrative pillar', 'trim');
			$this->form_validation->set_rules('RiskID', 'risk', 'trim');
			$this->form_validation->set_rules('PriorityID', 'priority', 'trim');
			$this->form_validation->set_rules('StatusID', 'status', 'trim');
			$this->form_validation->set_rules('ViewType', 'view', 'trim');

			// execute form validation for the purpose of reloading the form options
			$this->form_validation->run();
			// if ($this->form_validation->run() == FALSE){}
			// else{}

                        /*------------------------------------------------------------------
                         *  Call main_model.php DB Access routines that get app data references 
                         *  used to later build the drop down menu lists for search header.
                         *  Based on used by the "FILTER" settings in the web page search parms.
                         *------------------------------------------------------------------PRSC  
                         */
                        
			$filter = $this->input->post(null,true);                // Get the User Search Parms

			$data['deliverables'] 					= $this->main_model->get_deliverables($filter);
			
			if($filter['DeliverableTypeID'] != 3)
				$data['supporting_deliverables']                = $this->main_model->get_supporting_deliverables($filter);

			// for reporting menus
			$data['deliverable_types'] 				= $this->main_model->get_deliverable_types();
			$data['business_units'] 				= $this->main_model->get_business_units();
			$data['business_unit_service_areas']    = $this->main_model->get_business_unit_service_areas();
			$data['service_areas']    				= $this->main_model->get_service_areas();
			//$data['focus_area_outcomes']          = $this->main_model->get_focus_area_outcomes();
			$data['focus_areas'] 					= $this->main_model->get_focus_areas();
			$data['admin_pillars'] 					= $this->main_model->get_admin_pillars();
			//$data['priority_outcomes'] 			= $this->main_model->get_priority_outcomes();
			$data['risks'] 							= $this->main_model->get_risks();
			$data['priorities'] 					= $this->main_model->get_priorities();
			$data['statuses'] 						= $this->main_model->get_statuses();

			
                        /*
                         *------------------------------------------------------
                         *      Determine CI template page to use based on
                         *      View type choice entered by the user
                         *-------------------------------------------------PRSC 
                         */
                        
                        
			if($filter['ViewType'] == 2) // focus area view
			{
				$data['page'] = "pages/focus-area-results";
			}
			elseif($filter['ViewType'] == 3) // risk view
			{
				$data['page'] = "pages/risk-results";
			}
			elseif($filter['ViewType'] == 4) // business unit view
			{
				$data['page'] = "pages/business-unit-results";
			}
			elseif($filter['ViewType'] == 5) // admin pillar view
			{
				$data['page'] = "pages/admin-pillar-results";
			}
			else // default view // $filter['ViewType'] == 1
			{
				$data['page'] = "pages/index";			
			}

                   /*
                    *------------------------------------------------------
                    *      Load selected template as the current master
                    *      and show with header and footer as main.
                    *-------------------------------------------------PRSC 
                    */
   
			$this->load->view('template/master', $data);

		}
		
                   /*
                    *------------------------------------------------------
                    *      IF HERE then user was not defined
                    *      so force user to login screen.
                    *-------------------------------------------------PRSC 
                    */
		
		
		else $this->load->view('pages/login');
	}

/*------------------------------------------------------------------------------
 *      Validate check pass the credentials from the login form against the 
 *      LDAP server. If valid, an array of user information will be returned
 *      to be stored in SESSION. 
 * 
 *      If credentials not successful, we return the user to the login screen
 *       with an error message.
 * 
 * ------------------------------------------------------------------------PRSC
 */
        
	public function validate()
	{
		// Get the user credentials though post
		$credentials = $this->input->post(null, true);

		// modify they user name to add the domain so the user doesn't have to type it.
		$user_name = $credentials['user_name'];

		// HRP
		if (strpos($user_name, 'hrp\\') !== false)
		{
			$user_name = str_replace("hrp\\", "", $user_name);
			
			// ENVIRONMENT is set in index.php at root of application
			if(ENVIRONMENT == 'production') 
			{
				$host_name = '71.7.141.101'; // msrodc11.hrp.halifax.ca
				$base = 'OU=HRP Users,OU=Halifax Regional Police,DC=hrp,DC=halifax,DC=ca';
			}
			// dev environment
			else
			{
				$host_name = 'msaddns05.hrp.halifax.ca'; 	// IP: 172.30.110.95
				$base = 'DC=hrp,DC=halifax,DC=ca';
			}
			 				
		} // HRM & Library
		elseif (strpos($user_name, 'hrm\\') !== false)
		{
			$user_name = str_replace("hrm\\", "", $user_name);

			// ENVIRONMENT is set in index.php at root of application
			if(ENVIRONMENT == 'production') 
			{
				$host_name = '71.7.141.100'; // msrodc01.hrm.halifax.ca
				$base = 'DC=hrm,DC=halifax,DC=ca';
			}
			// dev environment
			else
			{
				$host_name = 'msaddns02.hrm.halifax.ca'; 	// IP: 172.25.210.95
				$base = 'OU=HRM Users,OU=Halifax Regional Municipality,DC=hrm,DC=halifax,DC=ca'; 
			}

		}
		
		// Set up the config array for the LDAP library
		$config = array(
			'port' 		=> '389', // 389 = standard | 636 = secure
			'host_name'     => $host_name, 
			'base' 		=> $base, 
			'rdn' 		=> $credentials['user_name'], 
			'password' 	=> $credentials['password'], 
		);


		// Load LDAP and pass it the configuration array.
		$this->load->library('ldap', $config);

		// Call the validate user function and pass it the filter, in 
                // this instance we want to filter by account name (i.e. the
                //  user name the typed in the login form)
		$user_data = $this->ldap->validate_user("(samaccountname=".$user_name.")");


		// if the user is not validated.
		if (!$user_data)
		{
			$this->session->set_flashdata('error', 'Invalid credentials.');
			redirect('login');
		}
		
		$user_data['rdn'] = $credentials['user_name'];
	
		// Other wise store the user data in session and redirect the 
                // user to the home controller, with the session set the user
                // will now gain access to the web site.
                
		$user_data['user'] = FALSE;
		$user_data['admin'] = FALSE;

		for ($i=0; $i < $user_data['groups']['count']; $i++) 
		{ 
			if (strpos($user_data['groups'][$i], 'AppAccountabilityReportingAdmin') !== false)
			{
				$user_data['user'] = TRUE;
				$user_data['admin'] = TRUE;
				break; // exit for loop
			}
			if (strpos($user_data['groups'][$i],'AppAccountabilityReporting') !== false) 
			{
				$user_data['user'] = TRUE;
				$business_units = $this->main_model->get_business_units();
				foreach ($business_units as $business_unit)
				{
					if (strpos($user_data['groups'][$i], 'AppAccountabilityReporting'.$business_unit['BusinessUnitCode']) !== false)
					{
						$user_data['division'] = $business_unit['BusinessUnitCode'];
						break; // exit for loop
					}
				}
			}
		}

		// user doesn't have pemission
		if (!$user_data['user'])
		{
			$this->session->set_flashdata('error', 'You do not have permission access to this system. Contact the Service Desk for support.');
			redirect('login');
		}

		$data = array(
			// 'UserID'				=> $user_data['user']['UserID'],
			'UserName'  			=> $user_data['first_name']." ".$user_data['last_name'],
			'UserNameShort' 		=> $user_data['first_name'],
			'UserEmail'    			=> $user_data['email_address'],
			'UserBusinessUnitCode'  => $user_data['division'],
			'UserBusinessUnitID' 	=> $this->main_model->get_business_unit_id($user_data['division']),
			'UserAdminFlag'			=> $user_data['admin'],
			'UserFlag'				=> $user_data['user'],
			'logged_in' 			=> TRUE
		);
		$this->session->set_userdata($data);



		// DEBUG VIA EMAIL
		//=============================================
		// $output = "<html><body>\n";
		// $output .=  "<p>".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."</p>";
		// $output .=  "<h1>".$user_name."</h1>";

		// // User array
		// $output .=  "<h2>User Data Array</h2>";
		// foreach ($user_data as $key => $value) {
		//     if(is_array($value))
		//     {
		//     	$output .=  "<p>$key</p>\n<ul>\n";
		//     	foreach ($value as $key2 => $value2) {
		//     		// if (strpos($value2,'AppAccountabilityReporting') !== false) 
		//     			$output .=  "<li>$key2: $value2</li>\n";
		//     	}
		//     	$output .=  "</ul>\n";
		//     }
		//     else $output .=  "<p>$key: $value</p>\n";
		// }

		// // Data array
		// $output .=  "<h2>Data Array</h2>";
		// foreach ($data as $key => $value) {
		//     if(is_array($value))
		//     {
		//     	$output .=  "<p>$key</p>\n<ul>\n";
		//     	foreach ($value as $key2 => $value2) {
		//     		// if (strpos($value2,'AppAccountabilityReporting') !== false) 
		//     			$output .=  "<li>$key2: $value2</li>\n";
		//     	}
		//     	$output .=  "</ul>\n";
		//     }
		//     else $output .=  "<p>$key: $value</p>\n";
		// }
		// // print_r($this->session->all_userdata());
		// $output .= "</body></html>";

		// $config['charset']  = 'utf-8';			// added
		// $config['mailtype'] = 'html';
		// $this->email->set_newline( "\r\n" );	// added
		// $this->email->set_crlf( "\r\n" );		// added
		// $this->email->initialize($config);
		// $this->email->clear();					// added

		// $this->email->from('no-reply@halifax.ca', 'Halifax Regional Municipality');
		// $this->email->to('pompilg@halifax.ca'); 
		// $this->email->subject('Accountability Debuging');
		// $this->email->message($output);	
		// $this->email->send();
		//=============================================


		redirect('');	// loads deliverables() (default function), pointing to root keeps the url clean
	}

/*           
============================================================================
 login
----------------------------------------------------------------------------
 Checks POST data for a valid email and password.
----------------------------------------------------------------------------
 On success: sets session and redirects back to index to display a list of 
 all strategic deliverables.
 On fail: return to login screen.
============================================================================
*/
	public function login()
	{	
		$data['page'] = "pages/login";
		$this->load->view('template/master', $data);

		// // Set validation rules and wrap errors with bootstrap text-error style		
		// $this->form_validation->set_rules('email_address', 'email address', 'required|trim|valid_email|max_length[50]');
		// $this->form_validation->set_rules('password', 'password', 'required|trim|max_length[10]');

		// // If it fails load index().
		// if ($this->form_validation->run() == FALSE)
		// {			
		// 	$data['page'] = "pages/login";
		// 	$this->load->view('template/master', $data);
		// }
		// // Otherwise check that the user exists, create a session and redirect the user to the deliverables page.
		// // If the user doesn't exit send them back to the login screen with the appropriate error
		// else
		// {
		// 	$email			= $this->input->post('email_address',TRUE);
		// 	$password 		= $this->input->post('password',TRUE);
		// 	$data['user'] 	= $this->main_model->is_user($email, $password);
			
		// 	// user exists
		// 	if(!empty($data['user']))
		// 	{
		// 		$user_data = array(
		// 			'UserID'				=> $data['user']['UserID'],
		// 			'UserName'  			=> $data['user']['UserName'],
		// 			'UserEmail'    			=> $email,
		// 			'UserBusinessUnitID' 	=> $data['user']['UserBusinessUnitID'],
		// 			'UserBusinessUnitCode'  => $data['user']['UserBusinessUnitCode'],
		// 			'UserAdminFlag'			=> $data['user']['UserAdminFlag'],
		// 			'logged_in' 			=> TRUE
		// 		);
		// 		$this->session->set_userdata($user_data);
		// 		redirect('', 'refresh');	// loads deliverables() (default function), pointing to root keeps the url clean
		// 	}
		// 	// user doesn't exist
		// 	else
		// 	{
		// 		$this->session->set_flashdata('error', 'Invalid credentials. Please try again.');
		// 		redirect('login', 'refresh');
		// 	}
		// }
	}
/*           
============================================================================
 logout
----------------------------------------------------------------------------
 Destroys session and returns user to the login screen
============================================================================
*/
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}
/*           
============================================================================
 view($id)
----------------------------------------------------------------------------
 Accepts an ID if the deliverable from POST.
----------------------------------------------------------------------------
 Loads the edit deliverable view for users with access to a the deliverable 
 being requested, otherwise it sends them back to the main listings.
============================================================================
*/
	public function deliverable($id)
	{

		$data['deliverable'] 					= $this->main_model->get_deliverable($id);
		$parentID = ($data['deliverable']['DeliverableTypeID'] == 2 ? $data['deliverable']['DeliverableParentID'] : NULL);
		$data['supporting_deliverables'] 		= $this->main_model->get_supporting_deliverables($id, $data['deliverable']['DeliverableTypeID'], $parentID );

		// for side menu
		// $data['deliverable_types'] 				= $this->main_model->get_deliverable_types();
		// $data['business_units'] 				= $this->main_model->get_business_units();
		// $data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
		// // $data['focus_area_outcomes'] 			= $this->main_model->get_focus_area_outcomes();
		// // $data['priority_outcomes'] 				= $this->main_model->get_priority_outcomes();
		// $data['risks'] 							= $this->main_model->get_risks();
		// $data['priorities'] 					= $this->main_model->get_priorities();
		$data['statuses'] 						= $this->main_model->get_statuses();

		$data['page'] = "pages/deliverable";
		$this->load->view('template/master', $data);
	}

/*           
============================================================================
 edit
----------------------------------------------------------------------------
 Accepts an ID if the deliverable from POST.
----------------------------------------------------------------------------
 Loads the edit deliverable view for users with access to a the deliverable 
 being requested, otherwise it sends them back to the main listings.
============================================================================
*/
	// public function edit()
	// {
	// 	$id = $this->input->post('id',true);

	// 	if(!empty($id))
	// 	{
	// 		$data['deliverable'] 					= $this->main_model->get_deliverable($id);
	// 		// $data['focus_area_outcomes'] 			= $this->main_model->get_focus_area_outcomes();
	// 		$data['statuses'] 						= $this->main_model->get_statuses();
	// 		// $data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
					
	// 		$data['page'] = "pages/edit";
	// 		$this->load->view('template/master', $data);
	// 	}
	// 	else $this->index();
	// }
/*           
============================================================================
 update
----------------------------------------------------------------------------
 Accepts an ID if the deliverable from POST.
----------------------------------------------------------------------------
 Validates the submitted data. 
 If valid, updates that deliverable and returns the user to the URL listing 
 view they were on before they clicked edit. 
 If invalid, returns the user to the edit page.
============================================================================
*/
	public function update()
	{
		// Set validation rules and wrap errors with bootstrap text-error style		
		$this->form_validation->set_rules('StatusUpdate', 'status update', 'trim|required|max_length[5000]');
		$this->form_validation->set_rules('ApproxComplete', 'approximate completion', 'trim|required');
		$this->form_validation->set_rules('ApproxEndDate', 'approximate end date', 'trim|required');
		$this->form_validation->set_rules('StatusID', 'status', 'trim|required');

		$data = $this->input->post(NULL,TRUE);

		$id = $this->input->post('DeliverableID',TRUE);

		if ($this->form_validation->run() == FALSE)
		{
			$data['deliverable'] 					= $this->main_model->get_deliverable($id);
			$parentID = ($data['deliverable']['DeliverableTypeID'] == 2 ? $data['deliverable']['DeliverableParentID'] : NULL);
			$data['supporting_deliverables'] 		= $this->main_model->get_supporting_deliverables($id, $data['deliverable']['DeliverableTypeID'], $parentID );
			$data['statuses'] 						= $this->main_model->get_statuses();

			$data['page'] = "pages/deliverable";
			$this->load->view('template/master', $data);
		}
		// Otherwise, save the submitted data and send a confirmation email to user
		else
		{
			// $this->main_model->update_deliverable($data);

			if($this->main_model->update_deliverable_status($data))	$data['success'] = 'Deliverable was updated.';
			else 													$data['fail'] = 'Error updating deliverable.';
			
			$data['deliverable'] 					= $this->main_model->get_deliverable($id);
			$parentID = ($data['deliverable']['DeliverableTypeID'] == 2 ? $data['deliverable']['DeliverableParentID'] : NULL);
			$data['supporting_deliverables'] 		= $this->main_model->get_supporting_deliverables($id, $data['deliverable']['DeliverableTypeID'], $parentID );
			$data['statuses'] 						= $this->main_model->get_statuses();

			$data['page'] = "pages/deliverable";
			$this->load->view('template/master', $data);

			// redirect($this->session->flashdata('uri'));	// set in the view
		}
	}
/*           
============================================================================
 download
----------------------------------------------------------------------------
 $type = sort type (focus-area/business-unit/status)
 $section and $category will depend on if type is focus-area or business-unit.
 $section = focus area code or business unit code (respectfully)
 $categofy = outcome code or service area id (respectfully)
----------------------------------------------------------------------------
 Validates the submitted data. 
 If valid, updates that deliverable and returns the user to the URL listing 
 view they were on before they clicked edit. 
 If invalid, returns the user to the edit page.
============================================================================
*/
	public function download()
	{
		// load file and download helper classes
		$this->load->helper('file');
		$this->load->helper('download');

		// Create a temporary file and open it with write permissions
		$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/test.csv';
		$file = fopen($path, 'w');

		// fetch data
		$filter = $this->input->post(null,true);
		$deliverables = $this->main_model->get_deliverables($filter);

		// set default header columns
		$headers = array('Business Unit', 'Service Area', 'Business Plan Number', 'Business Plan Deliverable', 'Business Plan Deliverable Description', 'Focus Area', 'Priority Outcome', 'Administrative Priority', 'Administrative Pillar', 'Associated Risk', 'Priority Status', 'Status', 'Status Update', 'Percent Complete', 'Expected Completion', 'Last Updated', 'Last Updated By');
		
		// for operational deliverables, fetch supporting strategic data and add additional header columns
		if($filter['DeliverableTypeID'] == 2) 
		{
			$supporting_deliverables = $this->main_model->get_supporting_deliverables($filter);
			array_push($headers, 'Strategic Business Unit', 'Strategic Business Plan Number', 'Strategic Initiative');
		}
		
		fputcsv($file, $headers);	// add headers to file

		// loop through deliverables
		foreach ($deliverables as $deliverable) 
		{
			$values[0] = $deliverable['BusinessUnitCode'];
			$values[1] = $deliverable['ServiceAreaName'];
			$values[2] = $deliverable['DeliverableCode'];
			$values[3] = $deliverable['DeliverableDescShort'];
			$values[4] = strip_tags($deliverable['DeliverableDesc']);
			$values[5] = $deliverable['FocusAreaName'];
			$values[6] = $deliverable['OutcomeName'];
			$values[7] = $deliverable['AdminPriorityName'];
			$values[8] = $deliverable['AdminPillarName'];
			$values[9] = $deliverable['RiskDescShort'];
			$values[10] = $deliverable['PriorityDesc'];
			$values[11] = $deliverable['StatusDescription'];
			$values[12] = strip_tags($deliverable['StatusUpdate']);
			$values[13] = $deliverable['ApproxComplete'];
			$values[14] = substr($deliverable['ApproxEndDate'], 0, -13);	// Remove the timestamp
			$values[15] = substr($deliverable['LastModDate'], 0, -13);		// Remove the timestamp
			$values[16] = $deliverable['LastModBy'];

			
			// for operational deliverables
			if($deliverable['DeliverableTypeID'] == 2)
			{
				// reset values
				$values[17] = '';	// Strategic Business Unit (CODE)
				$values[18] = '';	// Strategic Business Plan Number
				$values[19] = '';	// Strategic Deliverable

				// for non-admin users
				if(!$this->session->userdata('UserAdminFlag')) 
				{
					// clear status updates for busines units that aren't their own. 
					if($deliverable['BusinessUnitCode'] != $this->session->userdata('UserBusinessUnitCode'))  $values[12] = '';
				}

				// for operational deliverables with strategic parents, find the match and capture strategic details
				if($deliverable['DeliverableParentID'] > 0) 
				{
					if(!empty($supporting_deliverables))
					{
						foreach ($supporting_deliverables as $supporting_deliverable) 
						{
							if($deliverable['DeliverableParentID'] == $supporting_deliverable['DeliverableID']) 
							{
								$values[17] = $supporting_deliverable['BusinessUnitCode'];		// Strategic Business Unit (CODE)
								$values[18] = $supporting_deliverable['DeliverableCode'];		// Strategic Business Plan Number
								$values[19] = $supporting_deliverable['DeliverableDescShort'];	// Strategic Deliverable
							}
						}
					}
				}
			}

			fputcsv($file, $values); // add values to CSV file

		}

		fclose($file); // close file for editing
		$data = utf8_decode(file_get_contents($path));  // get data and encode it as UTF8
		$filename = 'Accountability-Reporting-Export-'.date('M-d-Y').'.csv';
		force_download($filename, $data);	

	}
/*           
============================================================================
 add
----------------------------------------------------------------------------
 Loads the add new deliverable view.
============================================================================
*/
	public function add_deliverable($id = NULL)
	{	
		$strategic['DeliverableTypeID'] = 1;
		if($id)	$data['deliverable'] = $this->main_model->get_deliverable($id);

		// load required dropdown data
		$data['deliverable_types'] 				= $this->main_model->get_deliverable_types();
		// $data['strategic_deliverables'] 					= $this->main_model->get_strategic_deliverables();
		$data['strategic_deliverables'] 		= $this->main_model->get_deliverables($strategic);
		$data['business_units'] 				= $this->main_model->get_business_units();
		$data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
		// $data['focus_area_outcomes'] 			= $this->main_model->get_focus_area_outcomes();
		$data['priority_outcomes'] 				= $this->main_model->get_priority_outcomes();
		$data['admin_priorities'] 				= $this->main_model->get_admin_priorities();
		$data['risks'] 							= $this->main_model->get_risks();
		$data['priorities'] 					= $this->main_model->get_priorities();

		$data['page'] = "pages/add-deliverable";
		$this->load->view('template/master', $data);
	}

/*	-----------------------------------------------------------------------------------
	create() - Actually creates new deliverables
	Currently inaccessbile because it's disabled in routes.php
	-----------------------------------------------------------------------------------*/
	public function create_deliverable()
	{	
		// Set validation rules and wrap errors with bootstrap text-error style		
		$this->form_validation->set_rules('DeliverableTypeID', 'type', 'trim|required|xss_clean');
		if($this->input->post('DeliverableTypeID',TRUE) == 2) 
			$this->form_validation->set_rules('DeliverableParentID', 'Related to', 'trim|xss_clean');
		$this->form_validation->set_rules('BusinessUnitID', 'business unit', 'trim|required|xss_clean');
		$this->form_validation->set_rules('ServiceArea'.$this->input->post('BusinessUnitID',TRUE), 'service area', 'trim|required|xss_clean');
		$this->form_validation->set_rules('OutcomeID', 'outcome', 'trim|required|xss_clean');
		$this->form_validation->set_rules('AdminPriorityID', 'admin priority', 'trim|required|xss_clean');
		$this->form_validation->set_rules('RiskID', 'risk', 'trim|xss_clean');
		$this->form_validation->set_rules('PriorityID', 'priority', 'trim|required|xss_clean');
		$this->form_validation->set_rules('DeliverableCode', 'business plan #', 'trim|required|xss_clean|max_length[5]');
		$this->form_validation->set_rules('DeliverableDescShort', 'short description', 'trim|required|xss_clean|max_length[500]');
		$this->form_validation->set_rules('DeliverableDesc', 'full description', 'trim|required|xss_clean|max_length[5000]');
		// $this->form_validation->set_rules('StatusUpdate', 'Update', 'trim|xss_clean');
		// $this->form_validation->set_rules('StatusUpdate', 'Update', 'trim|xss_clean');
		// $this->form_validation->set_rules('StatusUpdate', 'Status Update', 'trim|xss_clean|required|max_length[5000]');
		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

		if ($this->form_validation->run() == FALSE) 	$this->add_deliverable();
		// Otherwise, save the submitted data and send a confirmation email to user
		else
		{
			$data = $this->input->post(NULL,TRUE);

			// update
			if(!empty($data['DeliverableID']))
			{
				if($this->main_model->update_deliverable($data))	$this->session->set_flashdata('success', 'Deliverable was updated.');
				else 												$this->session->set_flashdata('danger', 'Error updating deliverable.');
				redirect('edit-deliverable/'.$data['DeliverableID']);
			}
			// create
			else
			{
				if($this->main_model->create_deliverable($data))	$this->session->set_flashdata('success', 'Deliverable was added.');
				else 												$this->session->set_flashdata('danger', 'Error adding deliverable.');
				redirect('create-deliverable');
			}
		}

	}



}