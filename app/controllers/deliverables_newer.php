<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*           
============================================================================
 Class: 		Deliverables
 Description:	Main application controller. 
 				Interacts with the deliverables_model and application views.
============================================================================
*/
class Deliverables extends CI_Controller {
/*           
============================================================================
 index
----------------------------------------------------------------------------
 
	This is the opening screen for the application. 
	
	At present it calls the Filter display and list of all the base
	deliverables for the current fiscal year. 

============================================================================
*/
	public function index()
	{
	$this->show_deliverables();

	}


	
	
/*------------------------------------------------------------------------------
 *      Validate check pass the credentials from the login form against the 
 *      LDAP server. If valid, an array of user information will be returned
 *      to be stored in SESSION. 
 * 
 *      If credentials not successful, we return the user to the login screen
 *       with an error message.
 * 
 * ------------------------------------------------------------------------PRSC
 */
        
	public function validate()
	{
		// Get the user credentials though post
		$credentials = $this->input->post(null, true);

		// modify they user name to add the domain so the user doesn't have to type it.
		$user_name = $credentials['user_name'];

		// HRP
		if (strpos($user_name, 'hrp\\') !== false)
		{
			$user_name = str_replace("hrp\\", "", $user_name);
			
			// ENVIRONMENT is set in index.php at root of application
			if(ENVIRONMENT == 'production') 
			{
				$host_name = '71.7.141.101'; // msrodc11.hrp.halifax.ca
				$base = 'OU=HRP Users,OU=Halifax Regional Police,DC=hrp,DC=halifax,DC=ca';
			}
			// dev environment
			else
			{
				$host_name = 'msaddns05.hrp.halifax.ca'; 	// IP: 172.30.110.95
				$base = 'DC=hrp,DC=halifax,DC=ca';
			}
			 				
		} // HRM & Library
		elseif (strpos($user_name, 'hrm\\') !== false)
		{
			$user_name = str_replace("hrm\\", "", $user_name);

			// ENVIRONMENT is set in index.php at root of application
			if(ENVIRONMENT == 'production') 
			{
				$host_name = '71.7.141.100'; // msrodc01.hrm.halifax.ca
				$base = 'DC=hrm,DC=halifax,DC=ca';
			}
			// dev environment
			else
			{
				$host_name = 'msaddns02.hrm.halifax.ca'; 	// IP: 172.25.210.95
				$base = 'OU=HRM Users,OU=Halifax Regional Municipality,DC=hrm,DC=halifax,DC=ca'; 
			}

		}
		
		// Set up the config array for the LDAP library
		$config = array(
			'port' 		=> '389', // 389 = standard | 636 = secure
			'host_name'     => $host_name, 
			'base' 		=> $base, 
			'rdn' 		=> $credentials['user_name'], 
			'password' 	=> $credentials['password'], 
		);


		// Load LDAP and pass it the configuration array.
		$this->load->library('ldap', $config);

		// Call the validate user function and pass it the filter, in 
                // this instance we want to filter by account name (i.e. the
                //  user name the typed in the login form)
		$user_data = $this->ldap->validate_user("(samaccountname=".$user_name.")");


		// if the user is not validated.
		if (!$user_data)
		{
			$this->session->set_flashdata('error', 'Invalid credentials.');
			redirect('login');
		}
		
		$user_data['rdn'] = $credentials['user_name'];
	
		// Other wise store the user data in session and redirect the 
                // user to the home controller, with the session set the user
                // will now gain access to the web site.
                
		$user_data['user'] = FALSE;
		$user_data['admin'] = FALSE;

		for ($i=0; $i < $user_data['groups']['count']; $i++) 
		{ 
			if (strpos($user_data['groups'][$i], 'AppAccountabilityReportingAdmin') !== false)
			{
				$user_data['user'] = TRUE;
				$user_data['admin'] = TRUE;
				break; // exit for loop
			}
			if (strpos($user_data['groups'][$i],'AppAccountabilityReporting') !== false) 
			{
				$user_data['user'] = TRUE;
				$business_units = $this->main_model->get_business_units();
				foreach ($business_units as $business_unit)
				{
					if (strpos($user_data['groups'][$i], 'AppAccountabilityReporting'.$business_unit['BusinessUnitCode']) !== false)
					{
						$user_data['division'] = $business_unit['BusinessUnitCode'];
						break; // exit for loop
					}
				}
			}
		}

		// user doesn't have pemission
		if (!$user_data['user'])
		{
			$this->session->set_flashdata('error', 'You do not have permission access to this system. Contact the Service Desk for support.');
			redirect('login');
		}

		$data = array(
			// 'UserID'				=> $user_data['user']['UserID'],
			'UserName'  			=> $user_data['first_name']." ".$user_data['last_name'],
			'UserNameShort' 		=> $user_data['first_name'],
			'UserEmail'    			=> $user_data['email_address'],
			'UserBusinessUnitCode'  => $user_data['division'],
			'UserBusinessUnitID' 	=> $this->main_model->get_business_unit_id($user_data['division']),
			'UserAdminFlag'			=> $user_data['admin'],
			'UserFlag'				=> $user_data['user'],
			'logged_in' 			=> TRUE
		);
		$this->session->set_userdata($data);



		// DEBUG VIA EMAIL
		//=============================================
		// $output = "<html><body>\n";
		// $output .=  "<p>".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."</p>";
		// $output .=  "<h1>".$user_name."</h1>";

		// // User array
		// $output .=  "<h2>User Data Array</h2>";
		// foreach ($user_data as $key => $value) {
		//     if(is_array($value))
		//     {
		//     	$output .=  "<p>$key</p>\n<ul>\n";
		//     	foreach ($value as $key2 => $value2) {
		//     		// if (strpos($value2,'AppAccountabilityReporting') !== false) 
		//     			$output .=  "<li>$key2: $value2</li>\n";
		//     	}
		//     	$output .=  "</ul>\n";
		//     }
		//     else $output .=  "<p>$key: $value</p>\n";
		// }

		// // Data array
		// $output .=  "<h2>Data Array</h2>";
		// foreach ($data as $key => $value) {
		//     if(is_array($value))
		//     {
		//     	$output .=  "<p>$key</p>\n<ul>\n";
		//     	foreach ($value as $key2 => $value2) {
		//     		// if (strpos($value2,'AppAccountabilityReporting') !== false) 
		//     			$output .=  "<li>$key2: $value2</li>\n";
		//     	}
		//     	$output .=  "</ul>\n";
		//     }
		//     else $output .=  "<p>$key: $value</p>\n";
		// }
		// // print_r($this->session->all_userdata());
		// $output .= "</body></html>";

		// $config['charset']  = 'utf-8';			// added
		// $config['mailtype'] = 'html';
		// $this->email->set_newline( "\r\n" );	// added
		// $this->email->set_crlf( "\r\n" );		// added
		// $this->email->initialize($config);
		// $this->email->clear();					// added

		// $this->email->from('no-reply@halifax.ca', 'Halifax Regional Municipality');
		// $this->email->to('pompilg@halifax.ca'); 
		// $this->email->subject('Accountability Debuging');
		// $this->email->message($output);	
		// $this->email->send();
		//=============================================


		redirect('');	// loads deliverables() (default function), pointing to root keeps the url clean
	}

/*           
============================================================================
 login
----------------------------------------------------------------------------
 Checks POST data for a valid email and password.
----------------------------------------------------------------------------
 On success: sets session and redirects back to index to display a list of 
 all strategic deliverables.
 On fail: return to login screen.
============================================================================
*/
	public function login()
	{	
		$data['page'] = "pages/login";
		$this->load->view('template/master', $data);

		// // Set validation rules and wrap errors with bootstrap text-error style		
		// $this->form_validation->set_rules('email_address', 'email address', 'required|trim|valid_email|max_length[50]');
		// $this->form_validation->set_rules('password', 'password', 'required|trim|max_length[10]');

		// // If it fails load index().
		// if ($this->form_validation->run() == FALSE)
		// {			
		// 	$data['page'] = "pages/login";
		// 	$this->load->view('template/master', $data);
		// }
		// // Otherwise check that the user exists, create a session and redirect the user to the deliverables page.
		// // If the user doesn't exit send them back to the login screen with the appropriate error
		// else
		// {
		// 	$email			= $this->input->post('email_address',TRUE);
		// 	$password 		= $this->input->post('password',TRUE);
		// 	$data['user'] 	= $this->main_model->is_user($email, $password);
			
		// 	// user exists
		// 	if(!empty($data['user']))
		// 	{
		// 		$user_data = array(
		// 			'UserID'				=> $data['user']['UserID'],
		// 			'UserName'  			=> $data['user']['UserName'],
		// 			'UserEmail'    			=> $email,
		// 			'UserBusinessUnitID' 	=> $data['user']['UserBusinessUnitID'],
		// 			'UserBusinessUnitCode'  => $data['user']['UserBusinessUnitCode'],
		// 			'UserAdminFlag'			=> $data['user']['UserAdminFlag'],
		// 			'logged_in' 			=> TRUE
		// 		);
		// 		$this->session->set_userdata($user_data);
		// 		redirect('', 'refresh');	// loads deliverables() (default function), pointing to root keeps the url clean
		// 	}
		// 	// user doesn't exist
		// 	else
		// 	{
		// 		$this->session->set_flashdata('error', 'Invalid credentials. Please try again.');
		// 		redirect('login', 'refresh');
		// 	}
		// }
	}
/*           
============================================================================
 logout
----------------------------------------------------------------------------
 Destroys session and returns user to the login screen
============================================================================
*/
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}
/*           
============================================================================
 view($id)
----------------------------------------------------------------------------
 Accepts an ID if the deliverable from POST.
----------------------------------------------------------------------------
 Loads the edit deliverable view for users with access to a the deliverable 
 being requested, otherwise it sends them back to the main listings.
============================================================================
*/
	public function deliverable($id)
	{

		$data['deliverable'] 					= $this->main_model->md_get_status_deliverable($id);
		$parentID = ($data['deliverable']['DeliverableTypeID'] == 2 ? $data['deliverable']['DeliverableParentID'] : NULL);
		$data['supporting_deliverables'] 		= $this->main_model->get_supporting_deliverables($id, $data['deliverable']['DeliverableTypeID'], $parentID );

		// for side menu
		// $data['deliverable_types'] 				= $this->main_model->get_deliverable_types();
		// $data['business_units'] 				= $this->main_model->get_business_units();
		// $data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
		// // $data['focus_area_outcomes'] 			= $this->main_model->get_focus_area_outcomes();
		// // $data['priority_outcomes'] 				= $this->main_model->get_priority_outcomes();
		// $data['risks'] 							= $this->main_model->md_get_risks();
		// $data['priorities'] 					= $this->main_model->get_priorities();
		$data['statuses'] 						= $this->main_model->get_statuses();

		$data['page'] = "pages/deliverable";
		$this->load->view('template/tpl_MODREC_All', $data);
	}

	
/*           
============================================================================
 
 		FUNCTION:	show_deliverables()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the nw controller->director function that sets up
 		necessary values to display the "list of deliverables" view file.
 		This was originally contained in the Index() function by Gian,
 		but I moved it so make it a proper component based call for the
 		new menus and to make it easier to reposition in the future.(PRSC)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/	
	
	public function show_deliverables()
	{
		// check for session
		if($this->session->userdata('UserName'))
		{
		
                        /*------------------------------------------------------------------
                         *  Call main_model.php DB Access routines that get app data references 
                         *  used to later build the drop down menu lists for search header.
                         *  Based on used by the "FILTER" settings in the web page search parms.
                         *------------------------------------------------------------------PRSC  
                         */
                        
			$filter = $this->input->post(null,true);                // Get the User Search Parms

			$data['deliverables'] 					= $this->main_model->md_get_deliverables($filter);
			
//			if($filter['DeliverableTypeID'] != 3)
//				$data['supporting_deliverables']                = $this->main_model->get_supporting_deliverables($filter);

			// for reporting menus
			$data['deliverable_types'] 				= $this->main_model->get_deliverable_types();
//			$data['business_unit_service_areas']    = $this->main_model->get_business_unit_service_areas();
			//$data['focus_area_outcomes']          = $this->main_model->get_focus_area_outcomes();
			$data['focus_areas'] 					= $this->main_model->get_focus_areas();
			$data['admin_pillars'] 					= $this->main_model->md_get_adminpillars();
			$data['priority_outcomes'] 				= $this->main_model->get_priority_outcomes();
			$data['risks'] 							= $this->main_model->md_get_risks();
			$data['priorities'] 					= $this->main_model->get_priorities();
			$data['statuses'] 						= $this->main_model->md_get_statusratings();
			$data['risks'] 							= $this->main_model->md_get_risks();
			$data['business_units'] 				= $this->main_model->get_business_units();
			$data['publishedinfo']					= $this->main_model->md_get_published();
			$data['sources']						= $this->main_model->md_get_sources();
			$data['services']						= $this->main_model->md_get_services();
			$data['serviceareas']    				= $this->main_model->get_service_areas();

			$data['adminpriorities'] 				= $this->main_model->md_get_adminpriorities();
			
			
			$data['services_BUSASV']    			= $this->main_model->md_get_services_WITH_BU_SA_SV();
			
					/*
                         *------------------------------------------------------
                         *      Determine CI template page to use based on
                         *      View type choice entered by the user
                         *-------------------------------------------------PRSC 
                         */
                        
   /*                     
			if($filter['ViewType'] == 2) // focus area view
			{
				$data['page'] = "pages/focus-area-results";
			}
			elseif($filter['ViewType'] == 3) // risk view
			{
				$data['page'] = "pages/risk-results";
			}
			elseif($filter['ViewType'] == 4) // business unit view
			{
				$data['page'] = "pages/business-unit-results";
			}
			elseif($filter['ViewType'] == 5) // admin pillar view
			{
				$data['page'] = "pages/admin-pillar-results";
			}
			else // default view // $filter['ViewType'] == 1
			{
//				$data['page'] = "pages/index";			
				$data['page'] = "pages/pg_show_deliverables";			
			}
*/

			
			$data['page'] = "pages/pg_show_base_deliverables";			
			
			
			$data['ddfilters']  =  $filter; 		
            /*
             *------------------------------------------------------
             *      Load selected template as the current master
             *      and show with header and footer as main.
             *-------------------------------------------------PRSC 
             */
			
			
			$this->load->view('template/master', $data);

		}
		
                   /*
                    *------------------------------------------------------
                    *      IF HERE then user was not defined
                    *      so force user to login screen.
                    *-------------------------------------------------PRSC 
                    */
		
		
		else $this->load->view('pages/login');
	}
	

	
	
/*           
============================================================================
 
 		FUNCTION:	show_deliverables()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the new "get everything" filter that was
 		rewritten to allow for the filtered version of the views.(PRSC)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/	
	
	public function show_full_deliverables()
	{
		// check for session
		if($this->session->userdata('UserName'))
		{
			
			
                   /*
                    *------------------------------------------------------
                    *      CI Define the dropdowns for Filter
                    *-------------------------------------------------PRSC 
                    */

			// ensures select inputs get repopulated
			$this->form_validation->set_rules('DeliverableTypeID', 'type', 'trim');
			$this->form_validation->set_rules('BusinessUnitID', 'business unit', 'trim');
			$this->form_validation->set_rules('ServiceArea'.$this->input->post('BusinessUnitID',TRUE), 'service area', 'trim');
			// $this->form_validation->set_rules('OutcomeID', 'outcome', 'trim');
			$this->form_validation->set_rules('FocusAreaID', 'focus area', 'trim');
			$this->form_validation->set_rules('AdminPillarID', 'administrative pillar', 'trim');
			$this->form_validation->set_rules('RiskID', 'risk', 'trim');
			$this->form_validation->set_rules('PriorityID', 'priority', 'trim');
			$this->form_validation->set_rules('StatusID', 'status', 'trim');
			$this->form_validation->set_rules('ViewType', 'view', 'trim');

			// execute form validation for the purpose of reloading the form options
			$this->form_validation->run();
			// if ($this->form_validation->run() == FALSE){}
			// else{}

                        /*------------------------------------------------------------------
                         *  Call main_model.php DB Access routines that get app data references 
                         *  used to later build the drop down menu lists for search header.
                         *  Based on used by the "FILTER" settings in the web page search parms.
                         *------------------------------------------------------------------PRSC  
                         */
                        
			$filter = $this->input->post(null,true);                // Get the User Search Parms

			$data['deliverables'] 					= $this->main_model->get_deliverables($filter);
			
			if($filter['DeliverableTypeID'] != 3)
				$data['supporting_deliverables']                = $this->main_model->get_supporting_deliverables($filter);

			// for reporting menus
			$data['deliverable_types'] 				= $this->main_model->get_deliverable_types();
			$data['business_unit_service_areas']    = $this->main_model->get_business_unit_service_areas();
			//$data['focus_area_outcomes']          = $this->main_model->get_focus_area_outcomes();
			$data['focus_areas'] 					= $this->main_model->get_focus_areas();
			$data['admin_pillars'] 					= $this->main_model->md_get_adminpillars();
			//$data['priority_outcomes'] 			= $this->main_model->get_priority_outcomes();
			$data['risks'] 							= $this->main_model->md_get_risks();
			$data['priorities'] 					= $this->main_model->get_priorities();
			$data['statuses'] 						= $this->main_model->md_get_statusratings();
			$data['risks'] 							= $this->main_model->md_get_risks();
			$data['business_units'] 				= $this->main_model->get_business_units();
			$data['publishedinfo']					= $this->main_model->md_get_published();
			$data['sources']						= $this->main_model->md_get_sources();
			$data['services']						= $this->main_model->md_get_services();
			$data['serviceareas']    				= $this->main_model->get_service_areas();
			
			$data['services_BUSASV']    			= $this->main_model->md_get_services_WITH_BU_SA_SV();
			
					/*
                         *------------------------------------------------------
                         *      Determine CI template page to use based on
                         *      View type choice entered by the user
                         *-------------------------------------------------PRSC 
                         */
                        
                        
			if($filter['ViewType'] == 2) // focus area view
			{
				$data['page'] = "pages/focus-area-results";
			}
			elseif($filter['ViewType'] == 3) // risk view
			{
				$data['page'] = "pages/risk-results";
			}
			elseif($filter['ViewType'] == 4) // business unit view
			{
				$data['page'] = "pages/business-unit-results";
			}
			elseif($filter['ViewType'] == 5) // admin pillar view
			{
				$data['page'] = "pages/admin-pillar-results";
			}
			else // default view // $filter['ViewType'] == 1
			{
//				$data['page'] = "pages/index";			
				$data['page'] = "pages/pg_show_deliverables";			
			}


			$data['ddfilters']  =  $filter; 		
            /*
             *------------------------------------------------------
             *      Load selected template as the current master
             *      and show with header and footer as main.
             *-------------------------------------------------PRSC 
             */
			
			
			$this->load->view('template/master', $data);

		}
		
                   /*
                    *------------------------------------------------------
                    *      IF HERE then user was not defined
                    *      so force user to login screen.
                    *-------------------------------------------------PRSC 
                    */
		
		
		else $this->load->view('pages/login');
	}
	
	
	
/*           
============================================================================
 edit
----------------------------------------------------------------------------
 Accepts an ID if the deliverable from POST.
----------------------------------------------------------------------------
 Loads the edit deliverable view for users with access to a the deliverable 
 being requested, otherwise it sends them back to the main listings.
============================================================================
*/
	// public function edit()
	// {
	// 	$id = $this->input->post('id',true);

	// 	if(!empty($id))
	// 	{
	// 		$data['deliverable'] 					= $this->main_model->get_deliverable($id);
	// 		// $data['focus_area_outcomes'] 			= $this->main_model->get_focus_area_outcomes();
	// 		$data['statuses'] 						= $this->main_model->get_statuses();
	// 		// $data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
					
	// 		$data['page'] = "pages/edit";
	// 		$this->load->view('template/master', $data);
	// 	}
	// 	else $this->index();
	// }
/*           
============================================================================
 update
----------------------------------------------------------------------------
 Accepts an ID if the deliverable from POST.
----------------------------------------------------------------------------
 Validates the submitted data. 
 If valid, updates that deliverable and returns the user to the URL listing 
 view they were on before they clicked edit. 
 If invalid, returns the user to the edit page.
============================================================================
*/
	public function update_deliverable()
	{
		// Set validation rules and wrap errors with bootstrap text-error style		
		$this->form_validation->set_rules('StatusUpdate', 'status update', 'trim|required|max_length[5000]');
		$this->form_validation->set_rules('ApproxComplete', 'approximate completion', 'trim|required');
		$this->form_validation->set_rules('ApproxEndDate', 'approximate end date', 'trim|required');
		$this->form_validation->set_rules('StatusID', 'status', 'trim|required');

		$data = $this->input->post(NULL,TRUE);

		$id = $this->input->post('DeliverableID',TRUE);

		if ($this->form_validation->run() == FALSE)
		{
			$data['deliverable'] 					= $this->main_model->get_deliverable($id);
			$parentID = ($data['deliverable']['DeliverableTypeID'] == 2 ? $data['deliverable']['DeliverableParentID'] : NULL);
			$data['supporting_deliverables'] 		= $this->main_model->get_supporting_deliverables($id, $data['deliverable']['DeliverableTypeID'], $parentID );
			$data['statuses'] 						= $this->main_model->get_statuses();

				
			$data['page'] = "pages/pg_add_deliverable";
			$this->load->view('template/tpl_MODREC_All', $data);
		}
		// Otherwise, save the submitted data and send a confirmation email to user
		else
		{
			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->update_deliverable_status($data))	
			{
					$data['data_state'] = "Risk Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Risk Record";
			}
					
						
//			$data['deliverable'] 					= $this->main_model->get_deliverable($id);
			$parentID = ($data['deliverable']['DeliverableTypeID'] == 2 ? $data['deliverable']['DeliverableParentID'] : NULL);
			$data['supporting_deliverables'] 		= $this->main_model->get_supporting_deliverables($id, $data['deliverable']['DeliverableTypeID'], $parentID );
			$data['statuses'] 						= $this->main_model->get_statuses();

			// load required dropdown data

			$data['deliverable_types'] 				= $this->main_model->get_deliverable_types();
			// $data['strategic_deliverables'] 		= $this->main_model->get_strategic_deliverables();
			$data['strategic_deliverables'] 		= $this->main_model->get_deliverables($strategic);
			$data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
			// $data['focus_area_outcomes'] 		= $this->main_model->get_focus_area_outcomes();
			$data['priority_outcomes'] 				= $this->main_model->get_priority_outcomes();
			$data['admin_priorities'] 				= $this->main_model->md_get_adminpriorities();

			$data['priorities'] 					= $this->main_model->get_priorities();
			$data['risks'] 							= $this->main_model->md_get_risks();
			$data['publishedinfo']					= $this->main_model->md_get_published();
			$data['sources']						= $this->main_model->md_get_sources();
			$data['services']						= $this->main_model->md_get_services();
			$data['workingyears']					= $this->main_model->md_get_working_years();
			$data['business_units'] 				= $this->main_model->get_business_units();
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['deliverable'] 		= $this->main_model->md_get_deliverable($id);
//			$data['page'] = "pages/pg_add_standard";
//			$this->load->view('template/tpl_MODREC_All', $data);
			
			
			$data['page'] = "pages/pg_add_deliverable";
			$this->load->view('template/tpl_MODREC_All', $data);

			// redirect($this->session->flashdata('uri'));	// set in the view
		}
	}


/*           
 * 
============================================================================
 download
----------------------------------------------------------------------------
 $type = sort type (focus-area/business-unit/status)
 $section and $category will depend on if type is focus-area or business-unit.
 $section = focus area code or business unit code (respectfully)
 $categofy = outcome code or service area id (respectfully)
----------------------------------------------------------------------------
 Validates the submitted data. 
 If valid, updates that deliverable and returns the user to the URL listing 
 view they were on before they clicked edit. 
 If invalid, returns the user to the edit page.
============================================================================
*/
	public function download()
	{
		// load file and download helper classes
		$this->load->helper('file');
		$this->load->helper('download');

		// Create a temporary file and open it with write permissions
		$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/test.csv';
		$file = fopen($path, 'w');

		// fetch data
		$filter = $this->input->post(null,true);
		$deliverables = $this->main_model->get_deliverables($filter);

 		
		$business_units 				= $this->main_model->md_get_business_units();
		$services						= $this->main_model->md_get_services();
		$serviceareas   				= $this->main_model->md_get_service_areas();
		$publishedinfo					= $this->main_model->md_get_published();
		$sources						= $this->main_model->md_get_sources();
		
		// set default header columns
		$headers = array('Business Unit', 'Service Area', 'Business Plan Number',
		 'Business Plan Deliverable', 'Business Plan Deliverable Description', 'Focus Area', 
		 'Priority Outcome', 'Administrative Priority', 'Administrative Pillar', 'Associated Risk',
		 'Priority Status', 'Status', 'Status Update', 'Percent Complete', 'Expected Completion',
		 'Last Updated', 'Last Updated By','Source','PublishedYN',
		 'BU - Performing','ServArea - Performing','Service Performing Initiative',
		 'BU - Serv Impact By','ServArea - Serv Impact By','Service Impact By' );
//		$headers = array('Wha', 'Na');
		
		// for operational deliverables, fetch supporting strategic data and add additional header columns
		if($filter['DeliverableTypeID'] == 2) 
		{
			$supporting_deliverables = $this->main_model->get_supporting_deliverables($filter);
			array_push($headers, 'Strategic Business Unit', 'Strategic Business Plan Number', 'Strategic Initiative');
		}
		
		fputcsv($file, $headers);	// add headers to file

		// loop through deliverables
		foreach ($deliverables as $dt) 
		{
			$values[0] = $dt['BusinessUnitCode'];
			$values[1] = $dt['ServiceAreaName'];
			$values[2] = $dt['DeliverableCode'];
			$values[3] = $dt['DeliverableDescShort'];
			$values[4] = strip_tags($dt['DeliverableDesc']);
			$values[5] = $dt['FocusAreaName'];
			$values[6] = $dt['OutcomeName'];
			$values[7] = $dt['AdminPriorityName'];
			$values[8] = $dt['AdminPillarName'];
			$values[9] = $dt['RiskDescShort'];
			$values[10] = $dt['PriorityDesc'];
			$values[11] = $dt['StatusDescription'];
			$values[12] = strip_tags($dt['StatusUpdate']);
			$values[13] = $dt['ApproxComplete'];
			$values[14] = substr($dt['ApproxEndDate'], 0, -13);	// Remove the timestamp
			$values[15] = substr($dt['LastModDate'], 0, -13);		// Remove the timestamp
			$values[16] = $dt['LastModBy'];

			/*
			 * -----------------------------------------------------
			 *     Find Literal Source Val
			 * -----------------------------------------------------
			 */
			
							$realID = 0;
								$realVL	= "";
								$realID = $dt['Source'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($sources as $rt) 
										{
										if($rt['SourceID'] == $realID)
										$realVL = $rt['SourceShortName'];
										}
									}
								}
								else 
									{ $realVL = "N/A"; }
									
									
								$values[17] = $realVL;

									
			
			/*
			 * -----------------------------------------------------
			 *     Find Literal Published Val
			 * -----------------------------------------------------
			 */
			
								$realID = 0;
								$realVL	= "";
								$realID = $dt['PublishedYN'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($publishedinfo as $rt) 
										{
										if($rt['PublishedID'] == $realID)
											$realVL = $rt['PublishedShortName'];
										}
									}
								}
								else 
									{ $realVL = ""; }
								 $realVL;	  
			
								$values[18] = $realVL;

			/*
			 * -----------------------------------------------------
			 *      Now figure out the full Service FOR Names to Show
			 * -----------------------------------------------------
			 */
			$buCD 		= "";						
			$saCD 		= "";
			$servName	= "";			
			
			if(!empty($services) && (!empty($dt['ServiceFOR'])))
				    {
					foreach ($services as $rt)
					{
			
					if($rt['ServiceID'] == $dt['ServiceFOR'])	
					{	
	
					//  Lookup the actual Service Area Code to show to user PRSC					
						$vdt = $rt['ServiceAreaID'];
						foreach ($serviceareas as $st)
						{
							if($vdt == $st['ServiceAreaID'])
							{
								$saCD = $st['ServiceAreaName'];
							}
						}
						 		 
						//  Lookup the actual Business Unit Code to show to user PRSC					
						$vdt = $rt['BusinessUnitID'];
						foreach ($business_units as $st)
						{
						if($vdt == $st['BusinessUnitID'])
							{
							$buCD = $st['BusinessUnitCode'];
							}
						}

					$vl = $rt['ServiceID'];
					$servName = $rt['ServiceShortNM'];
					}
				}
		   }			
					
			$values[19] = $buCD;
			$values[20] = $saCD;
			$values[21] = $servName;


			/*
			 * -----------------------------------------------------
			 *      Now figure out the full Service BY Names to Show
			 * -----------------------------------------------------
			 */
			$buCD 		= "";						
			$saCD 		= "";
			$servName	= "";			
			
//			if(!empty($services) && (!empty($dt['ServiceBY'])))
			if($dt['ServiceBY'] > 0)
			  {
					foreach ($services as $rt)
					{
			
					if($rt['ServiceID'] == $dt['ServiceBY'])
					{	
	
					//  Lookup the actual Service Area Code to show to user PRSC					
						$vdt = $rt['ServiceAreaID'];
						foreach ($serviceareas as $st)
						{
							if($vdt == $st['ServiceAreaID'])
							{
								$saCD = $st['ServiceAreaName'];
							}
						}
						 		 
						//  Lookup the actual Business Unit Code to show to user PRSC					
						$vdt = $rt['BusinessUnitID'];
						foreach ($business_units as $st)
						{
						if($vdt == $st['BusinessUnitID'])
							{
							$buCD = $st['BusinessUnitCode'];
							}
						}

					$vl = $rt['ServiceID'];
					$servName = $rt['ServiceShortNM'];
					}
				}
		   }			
					
			$values[22] = $buCD;
			$values[23] = $saCD;
			$values[24] = $servName;
					
			/*
			 * -----------------------------------------------------
			 *      For Operational Deliverables
			 * -----------------------------------------------------
			 */
			
			if($dt['DeliverableTypeID'] == 2)
			{
				// reset values
				$values[25] = '';	// Strategic Business Unit (CODE)
				$values[26] = '';	// Strategic Business Plan Number
				$values[27] = '';	// Strategic Deliverable

				// for non-admin users
				if(!$this->session->userdata('UserAdminFlag')) 
				{
					// clear status updates for busines units that aren't their own. 
					if($dt['BusinessUnitCode'] != $this->session->userdata('UserBusinessUnitCode')) 
					 $values[12] = '';
				}

				// for operational deliverables with strategic parents, find the match and capture strategic details
				if($dt['DeliverableParentID'] > 0) 
				{
					if(!empty($supporting_deliverables))
					{
						foreach ($supporting_deliverables as $supporting_deliverable) 
						{
							if($dt['DeliverableParentID'] == $supporting_deliverable['DeliverableID']) 
							{
								$values[25] = $supporting_deliverable['BusinessUnitCode'];		// Strategic Business Unit (CODE)
								$values[26] = $supporting_deliverable['DeliverableCode'];		// Strategic Business Plan Number
								$values[27] = $supporting_deliverable['DeliverableDescShort'];	// Strategic Deliverable
							}
						}
					}
				}
			}

			fputcsv($file, $values); // add values to CSV file
//			fputcsv($file, $filter); // diag only

//			$fdest = '/inetpub/wwwroot/AccountabilityReporting2015/temp/ftest.csv';
//		write_file($fdest, $filter);	
		
		}

		fclose($file); // close file for editing
		$data = utf8_decode(file_get_contents($path));  // get data and encode it as UTF8
		$filename = 'Accountability-Reporting-Export-'.date('M-d-Y').'.csv';
		
		
		force_download($filename, $data);	

	}
	
	
/*           
============================================================================
 
 		FUNCTION:	download_services()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		Allows users to export the data into a spreadsheet for this table.
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	
	
	public function download_services()
	{
		// load file and download helper classes
		$this->load->helper('file');
		$this->load->helper('download');

		// Create a temporary file and open it with write permissions
		$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/test.csv';
		$file = fopen($path, 'w');

		// fetch data
		$filter = $this->input->post(null,true);
		$services = $this->main_model->md_get_services_WITH_BU_SA_SV();
		
		// set default header columns
		$headers = array('BUID','BusinessUnitName','BusinessUnitCode',
									'ServiceAreaID', 'ServiceAreaName',
									'ServiceID',
									'ServiceName','ServiceDesc',
									'LastModBy','LastModDate',);
		fputcsv($file, $headers);	// add headers to file

		// loop through deliverables
		foreach ($services as $dt) 
		{
			$values[0] = $dt['BusinessUnitID'];
			$values[1] = $dt['BusinessUnitShortName'];
			$values[2] = $dt['BusinessUnitCode'];
			$values[3] = $dt['ServiceAreaID'];
			$values[4] = $dt['ServiceAreaName'];
			$values[5] = $dt['ServiceID'];
			$values[6] = $dt['ServiceShortNM'];
			$values[7] = strip_tags($dt['ServiceDesc']);
			$values[8] = $dt['LastModBy'];
			$values[9] = substr($dt['LastModDate'], 0, -13);		// Remove the timestamp
			

			fputcsv($file, $values); // add values to CSV file
//			fputcsv($file, $filter); // diag only

//			$fdest = '/inetpub/wwwroot/AccountabilityReporting2015/temp/ftest.csv';
//		write_file($fdest, $filter);	
		
		}

		fclose($file); // close file for editing
		$data = utf8_decode(file_get_contents($path));  // get data and encode it as UTF8
		$filename = 'Accountability-Reporting-2016-Services-Export-'.date('M-d-Y').'.csv';
		
		
		force_download($filename, $data);	

	}	

/*           
============================================================================
 
 		FUNCTION:	download_risks()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		Allows users to export the data into a spreadsheet for this table.
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	
	
	public function download_risks()
	{
		// load file and download helper classes
		$this->load->helper('file');
		$this->load->helper('download');

		// Create a temporary file and open it with write permissions
//		$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/risk.csv';
		
		$uname = $this->session->userdata('UserName');
		$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/' . $uname . '.csv';
		
		$file = fopen($path, 'w');

		// fetch data
		$filter 				= $this->input->post(null,true);
		$risks 					= $this->main_model->md_get_risks();
		$heatmapratings			= $this->main_model->md_get_heatmapratings();
		
		// set default header columns
		$headers = array('RiskID','RiskCode','Risk Short Description',
									'Likelihood',
									'Impact','HeatRating','RiskOwner',
									'LastModBy','LastModDate',
									'Risk Description', 'Risk Statement'
									);
		fputcsv($file, $headers);	// add headers to file

		// loop through risks
		foreach ($risks as $dt) 
		{
			$values[0] = $dt['RiskID'];
			$values[1] = $dt['RiskCode'];
			$values[2] = $dt['RiskDescShort'];
			$values[3] = $dt['Likelihood'];
			$values[4] = $dt['Impact'];

//			$values[5] = $dt['HeatMapRating'];
		/*
			 * -----------------------------------------------------
			 *     Find Literal for HeatMap Ratingl
			 * -----------------------------------------------------
			 */
			
								$realID = 0;
								$realVL	= "";
								$realID = $dt['HeatMapRating'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($heatmapratings as $rt) 
										{
										if($rt['HeatMapRateID'] == $realID)
											$realVL = $rt['HeatMapRateShortName'];
										}
									}
								}
								else 
									{ $realVL = ""; }
								 $realVL;	  
			
								$values[5] = $realVL;
			
			
			
			
			$values[6] = $dt['RiskOwner'];
			$values[7] = $dt['LastModBy'];
//			$values[8] = $dt['LastModDate'];
			$values[8] = substr($dt['LastModDate'], 0, -13);		// Remove the timestamp
			
			$values[9] = strip_tags($dt['RiskDesc']);
			$values[10] = strip_tags($dt['RiskStatement']);
			
			fputcsv($file, $values); // add values to CSV file
//			fputcsv($file, $filter); // diag only

//			$fdest = '/inetpub/wwwroot/AccountabilityReporting2015/temp/ftest.csv';
//		write_file($fdest, $filter);	
		
		}

		fclose($file); // close file for editing
		$data = utf8_decode(file_get_contents($path));  // get data and encode it as UTF8
		$filename = 'Accountability-Reporting-2016-Risks-Export-'.date('M-d-Y').'.csv';
		
		
		force_download($filename, $data);	

	}	

	

/*           
============================================================================
 
 		FUNCTION:	download_standards()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		Allows users to export the data into a spreadsheet for this table.
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	
	
	public function download_standards()
	{
		// load file and download helper classes
		$this->load->helper('file');
		$this->load->helper('download');

		// Create a temporary file and open it with write permissions
		$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/test.csv';
		$file = fopen($path, 'w');

		// fetch data
		$filter 				= $this->input->post(null,true);
		$standards 				= $this->main_model->md_get_standards();
		$services				= $this->main_model->md_get_services();
		
		// set default header columns
		$headers = array('Standard ID','Standard Code','Standard Short Name', 'Standard Description',
									'Service',
									'LastModBy','LastModDate'
									);
		fputcsv($file, $headers);	// add headers to file

		// loop through standards
		foreach ($standards as $dt) 
		{
			$values[0] = $dt['StandardID'];
			$values[1] = $dt['StandardCD'];
			$values[2] = $dt['StandardShortNM'];
			$values[3] = strip_tags($dt['StandardDesc']);
		
		/*
			 * -----------------------------------------------------
			 *     Find Literal for Service Rating
			 * -----------------------------------------------------
			 */
			
								$realID = 0;
								$realVL	= "";
								$realID = $dt['ServiceID'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($services as $rt) 
										{
										if($rt['ServiceID'] == $realID)
											$realVL = $rt['ServiceShortNM'];
										}
									}
								}
								else 
									{ $realVL = ""; }
								 $realVL;	  
			
								$values[5] = $realVL;
			
			
			
			
			$values[7] = $dt['LastModBy'];
//			$values[8] = $dt['LastModDate'];
			$values[8] = substr($dt['LastModDate'], 0, -13);		// Remove the timestamp
			
			fputcsv($file, $values); // add values to CSV file
//			fputcsv($file, $filter); // diag only

//			$fdest = '/inetpub/wwwroot/AccountabilityReporting2015/temp/ftest.csv';
//		write_file($fdest, $filter);	
		
		}

		fclose($file); // close file for editing
		$data = utf8_decode(file_get_contents($path));  // get data and encode it as UTF8
		$filename = 'Accountability-Reporting-2016-Standards-Export-'.date('M-d-Y').'.csv';
		
		
		force_download($filename, $data);	

	}	

	
	

/*           
============================================================================
 
 		FUNCTION:	download_performances()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		Allows users to export the data into a spreadsheet for this table.
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	
	
	public function download_performances()
	{
		// load file and download helper classes
		$this->load->helper('file');
		$this->load->helper('download');

		// Create a temporary file and open it with write permissions
		$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/test.csv';
		$file = fopen($path, 'w');

		// fetch data
		$filter 				= $this->input->post(null,true);
		$performances 				= $this->main_model->md_get_performances();
		$services				= $this->main_model->md_get_services();
		
		// set default header columns
		$headers = array('Performance ID','Performance Code','Performance Short Name', 'Performance Description',
									'Service',
									'LastModBy','LastModDate'
									);
		fputcsv($file, $headers);	// add headers to file

		// loop through performances
		foreach ($performances as $dt) 
		{
			$values[0] = $dt['PerformanceID'];
			$values[1] = $dt['PerformanceCD'];
			$values[2] = $dt['PerformanceShortNM'];
			$values[3] = strip_tags($dt['PerformanceDesc']);
		
		/*
			 * -----------------------------------------------------
			 *     Find Literal for Service Rating
			 * -----------------------------------------------------
			 */
			
								$realID = 0;
								$realVL	= "";
								$realID = $dt['ServiceID'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($services as $rt) 
										{
										if($rt['ServiceID'] == $realID)
											$realVL = $rt['ServiceShortNM'];
										}
									}
								}
								else 
									{ $realVL = ""; }
								 $realVL;	  
			
								$values[5] = $realVL;
			
			
			
			
			$values[7] = $dt['LastModBy'];
//			$values[8] = $dt['LastModDate'];
			$values[8] = substr($dt['LastModDate'], 0, -13);		// Remove the timestamp
			
			fputcsv($file, $values); // add values to CSV file
//			fputcsv($file, $filter); // diag only

//			$fdest = '/inetpub/wwwroot/AccountabilityReporting2015/temp/ftest.csv';
//		write_file($fdest, $filter);	
		
		}

		fclose($file); // close file for editing
		$data = utf8_decode(file_get_contents($path));  // get data and encode it as UTF8
		$filename = 'Accountability-Reporting-2016-Performances-Export-'.date('M-d-Y').'.csv';
		
		
		force_download($filename, $data);	

	}	

	
	

/*           
============================================================================
 
 		FUNCTION:	download_serviceareas()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		Allows users to export the data into a spreadsheet for this table.
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	
	
	public function download_service_areas()
	{
		// load file and download helper classes
		$this->load->helper('file');
		$this->load->helper('download');

		// Create a temporary file and open it with write permissions
		$path = '/inetpub/wwwroot/AccountabilityReporting2015/temp/test.csv';
		$file = fopen($path, 'w');

		// fetch data
		$filter 				= $this->input->post(null,true);
		$serviceareas 			= $this->main_model->md_get_service_areas();
		$businessunits 			= $this->main_model->md_get_business_units();
		
		// set default header columns
		$headers = array('ServiceArea ID','ServiceArea Code','ServiceArea Short Name', 'ServiceArea Description',
									'Business Unit ID',
									'LastModBy','LastModDate'
									);
		fputcsv($file, $headers);	// add headers to file

		// loop through serviceareas
		foreach ($serviceareas as $dt) 
		{
			$values[0] = $dt['ServiceAreaID'];
			$values[1] = $dt['ServiceAreaName'];
			$values[2] = $dt['ServiceAreaShortCD'];
			$values[3] = strip_tags($dt['ServiceAreaDesc']);
		
		/*
			 * -----------------------------------------------------
			 *     Find Literal for Business Unit
			 * -----------------------------------------------------
			 */
			
								$realID = 0;
								$realVL	= "";
								$realID = $dt['BusinessUnitID'];
								if($realID)
								{
									if($realID > 0)
									{
										foreach ($businessunits as $rt) 
										{
										if($rt['BusinessUnitID'] == $realID)
											$realVL = $rt['BusinessUnitCode'];
										}
									}
								}
								else 
									{ $realVL = ""; }
								 $realVL;	  
			
								$values[4] = $realVL;
			
			
			
			
			$values[5] = $dt['LastModBy'];

			$values[6] = substr($dt['LastModDate'], 0, -13);		// Remove the timestamp
			
			fputcsv($file, $values); // add values to CSV file
//			fputcsv($file, $filter); // diag only

//			$fdest = '/inetpub/wwwroot/AccountabilityReporting2015/temp/ftest.csv';
//		write_file($fdest, $filter);	
		
		}

		fclose($file); // close file for editing
		$data = utf8_decode(file_get_contents($path));  // get data and encode it as UTF8
		$filename = 'Accountability-Reporting-2016-ServiceAreas-Export-'.date('M-d-Y').'.csv';
		
		
		force_download($filename, $data);	

	}	
	
	
/*           
============================================================================
 add
----------------------------------------------------------------------------
 Loads the add new deliverable view.
============================================================================
*/
	public function add_deliverable($id = NULL)
	{	
		$strategic['DeliverableTypeID'] = 1;
		if($id)	$data['deliverable'] = $this->main_model->get_deliverable($id);
		$data['data_state'] = "";
		
		// load required dropdown data
	
		$data['deliverable_types'] 				= $this->main_model->get_deliverable_types();

		// The us of get deliverables by way of the filter was Gians short cut solution.
		// I switched it back to actually getting all of the deliverables by way of the oldold
		// method until the DeliverableViews2016 can be fixed.
		
		
		$data['strategic_deliverables'] 		= $this->main_model->get_strategic_deliverables();
		//$data['strategic_deliverables'] 		= $this->main_model->get_deliverables($strategic);

		
		$data['business_units'] 				= $this->main_model->get_business_units();
		$data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
		// $data['focus_area_outcomes'] 		= $this->main_model->get_focus_area_outcomes();
		$data['priority_outcomes'] 				= $this->main_model->get_priority_outcomes();
		$data['admin_priorities'] 				= $this->main_model->md_get_adminpriorities();
		$data['priorities'] 					= $this->main_model->get_priorities();
		$data['risks'] 							= $this->main_model->md_get_risks();
		$data['publishedinfo']					= $this->main_model->md_get_published();
		$data['sources']						= $this->main_model->md_get_sources();
		$data['services']						= $this->main_model->md_get_services();
		$data['serviceareas']					= $this->main_model->md_get_service_areas();
		$data['workingyears']					= $this->main_model->md_get_working_years();
		
		$data['page'] = "pages/pg_add_deliverable";
		
		$this->load->view('template/tpl_MODREC_All', $data);
	
	}

	
		
/*           
============================================================================
 
 		FUNCTION:	add_service()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-service" view file.
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_service_area($id = NULL)
	{	
		
		// This goes to database to get the data used to initialize the 
		// boxes in the "add-service" web page.

		$data['business_units'] 				= $this->main_model->get_business_units();
		$data['data_state'] = "";
		
		if($id)	$data['servicearea'] = $this->main_model->md_get_service_area($id);
		
		$data['page'] = "pages/pg_add_service_area";
		$this->load->view('template/tpl_MODREC_All', $data);
		
	}
	
	
	public function show_service_areas($id = NULL)
	{	
			// Get the other data used to fill the link fields or external refs PRSC
		
			// load required dropdown data
		$data['business_units'] 				= $this->main_model->get_business_units();
		$data['data_state'] = "";
		
		// This shows a list of the Risks for users to reference PRSC
		$data['service_areas'] 			= $this->main_model->md_get_service_areas();
		$data['page']					= "pages/pg_show_service_areas";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	

public function update_service_area()
	{
			
	// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-servicearea";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('ServiceAreaID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
//		$vd = $data['ServiceAreaShortCD'];
//		if($vd == '')
//					$processYN = FALSE;

		$vd = $data['ServiceAreaName'];
		if($vd == '')
					$processYN = FALSE;
	
		// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
					/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_service_area($data))	
			{
					$data['data_state'] = "Service Area Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Service Area Record";
			}
		}	
			
			// This goes to database to get the data used to initialize the 
			// boxes in the "add-service" web page.

			$data['business_units'] 		= $this->main_model->get_business_units();
			
										
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['servicearea'] 		= $this->main_model->md_get_service_area($id);
			$data['page'] = "pages/pg_add_service_area";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
	public function update_service_areaOLD()
	{
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data = $this->input->post(NULL,TRUE);
		$data['data_state'] = "";
		
		$id = $this->input->post('ServiceAreaID',TRUE);

		if($id > 0)
		{
			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_service_area($data))	
			{
					$data['data_state'] = "Service Area Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Service Area";
			}
			
			// This goes to database to get the data used to initialize the 
			// boxes in the "add-service" web page.

			$data['business_units'] 		= $this->main_model->get_business_units();
			
			// Load Current Record and just reshow the page with state
			
			$data['servicearea'] 		= $this->main_model->md_get_service_area($id);
			$data['page'] = "pages/pg_add_service_area";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		}
	}
	
	public function update_service_areaCNIU()
	{
		// Set validation rules and wrap errors with bootstrap text-error style		
//		$this->form_validation->set_rules('StatusUpdate', 'status update', 'trim|required|max_length[5000]');
//		$this->form_validation->set_rules('ApproxComplete', 'approximate completion', 'trim|required');
//		$this->form_validation->set_rules('ApproxEndDate', 'approximate end date', 'trim|required');
//		$this->form_validation->set_rules('StatusID', 'status', 'trim|required');

		$data = $this->input->post(NULL,TRUE);
		$data['data_state'] = "";
		
		$id = $this->input->post('ServiceAreaID',TRUE);

		if ($this->form_validation->run() == FALSE)
		{
			// This goes to database to get the data used to initialize the 
			// boxes in the "add-service" web page.

			$data['business_units'] 		= $this->main_model->get_business_units();
			
			// Load Current Record and just reshow the page with error
			
			$data['servicearea'] 			= $this->main_model->md_get_service_area($id);
			$data['data_state'] = "Data Entered Invalid - Please Try Again";
			$data['page'] = "pages/pg_add_service_area";
			$this->load->view('template/tpl_MODREC_All', $data);
			
		}
		// Otherwise, save the submitted data and send a confirmation email to user
		else
		{
			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			$data['success']  = "";
			$data['fail']	  = "";	
			if($this->main_model->update_service_area($data))	
			{
					$data['data_state'] = "Service Area Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Service Area";
			}
			
			// This goes to database to get the data used to initialize the 
			// boxes in the "add-service" web page.

			$data['business_units'] 		= $this->main_model->get_business_units();
			
			// Load Current Record and just reshow the page with state
			
			$data['servicearea'] 		= $this->main_model->md_get_service_area($id);
			$data['page'] = "pages/pg_add_service_area";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		}
	}
	
	public function create_service_area()
	{	
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-servicearea";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
//		$vd = $data['ServiceAreaShortCD'];
//		if($vd == '')
//					$processYN = FALSE;

		$vd = $data['ServiceAreaName'];
		if($vd == '')
					$processYN = FALSE;
					
						
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_service_area($data))	
			{
					$data['data_state'] = "Service Service_Area Record Created Successfully";
					$this->session->set_flashdata('success', 'service_area was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Service_Aea Record.');
					$data['data_state'] = "Error Creatng Service service_area Record";
			}
		}
			
				// This goes to database to get the data used to initialize the 
			// boxes in the "add-service" web page.

			$data['business_units'] 		= $this->main_model->get_business_units();
											
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['service_area'] 		= $this->main_model->md_get_service_area($id);
			$data['page'] = "pages/pg_add_service_area";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
		
}	
	
	
/*           
============================================================================
 
 		FUNCTION:	add_risk()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-risk" view file.
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	
	
	public function add_risk($id = NULL)
	{	
		$data['data_state'] = "";
	
		// This goes to database to get the data used to initialize the 
		// boxes in the "pg_add_risk" web page.
		$data['impacts'] 			= $this->main_model->md_get_impacts();
		$data['heatmapratings'] 	= $this->main_model->md_get_heatmapratings();
		$data['likelihoods'] 		= $this->main_model->md_get_likelihoods();
	
		if($id)	$data['risk'] = $this->main_model->md_get_risk($id);
		$data['page'] = "pages/pg_add_risk";
		$this->load->view('template/tpl_MODREC_All', $data);
		
	}
	
	
	public function show_risks($id = NULL)
	{	
		// This goes to database to get the data used to initialize the 
		// boxes in the "pg_add_risk" web page.
		$data['impacts'] 			= $this->main_model->md_get_impacts();
		$data['heatmapratings'] 	= $this->main_model->md_get_heatmapratings();
		$data['likelihoods'] 		= $this->main_model->md_get_likelihoods();
		
		
		// This shows a list of the Risks for users to reference
		$data['risks'] 			= $this->main_model->md_get_risks();
		$data['page']			= "pages/pg_show_risks";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	
	public function update_risk()
	{
			
	// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-risk";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('RiskID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['RiskCode'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['RiskDescShort'];
		if($vd == '')
					$processYN = FALSE;
	
 	// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
					/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_risk($data))	
			{
					$data['data_state'] = "Risk Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Risk Record";
			}
			
		}	
		
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_risk" web page.
			$data['impacts'] 			= $this->main_model->md_get_impacts();
			$data['heatmapratings'] 	= $this->main_model->md_get_heatmapratings();
			$data['likelihoods'] 		= $this->main_model->md_get_likelihoods();
			
			
			// Load Current Record and just reshow the page with state
			
			$data['risk'] 		= $this->main_model->md_get_risk($id);
			$data['page'] 		= "pages/pg_add_risk";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
	

	public function create_risk()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-risk";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['RiskCode'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['RiskDescShort'];
		if($vd == '')
					$processYN = FALSE;
					
					
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_risk($data))	
			{
					$data['data_state'] = "Risk Record Created Successfully";
					$this->session->set_flashdata('success', 'Risk was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Risk Record.');
					$data['data_state'] = "Error Creatng Risk Record";
			}
		}
		
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_risk" web page.
			$data['impacts'] 			= $this->main_model->md_get_impacts();
			$data['heatmapratings'] 	= $this->main_model->md_get_heatmapratings();
			$data['likelihoods'] 		= $this->main_model->md_get_likelihoods();
											
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['standard'] 		= $this->main_model->md_get_risk($id);
			$data['page'] = "pages/pg_add_risk";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
		
}
	
		
	
/*           
============================================================================
 
 		FUNCTION:	add_service()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-service" view file.
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_service($id = NULL)
	{	
		// This goes to database to get the data used to initialize the 
		// boxes in the "add-service" web page.
		$data['data_state'] = "";
		
		
		$data['business_units'] 				= $this->main_model->get_business_units();
		$data['priority_outcomes'] 				= $this->main_model->get_priority_outcomes();
		$data['serviceareas']					= $this->main_model->md_get_service_areas();
		$data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
		
		if($id)	$data['service'] = $this->main_model->md_get_service($id);
		$data['page'] = "pages/pg_add_service";
		$this->load->view('template/tpl_MODREC_All', $data);
		
	}
	
	
	public function show_services($id = NULL)
	{	
		// Get the other data used to fill the link fields or external refs PRSC
		
		$data['business_units'] 			= $this->main_model->get_business_units();
		$data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
		$data['serviceareas']				= $this->main_model->get_service_areas();
		
			
		// This shows a list of the Risks for users to reference PRSC
		$data['services'] 			= $this->main_model->md_get_services();
		$data['page']				= "pages/pg_show_services";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	
	public function update_service($id = NULL)
	{
	// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-service";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('ServiceID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['ServiceShortCD'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['ServiceShortNM'];
		if($vd == '')
					$processYN = FALSE;
	
 	// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
					/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_service($data))	
			{
					$data['data_state'] = "Service Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Service Record";
			}
		}
		
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_risk" web page - PRSC
		
			$data['business_units'] 			= $this->main_model->get_business_units();
			$data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
			$data['serviceareas']				= $this->main_model->get_service_areas();
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['service'] 		= $this->main_model->md_get_service($id);
			$data['page'] = "pages/pg_add_service";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
		
		
	}
		

	public function create_service()
	{	
		
			// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-service";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['ServiceShortCD'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['ServiceShortNM'];
		if($vd == '')
					$processYN = FALSE;
					
						
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_service($data))	
			{
					$data['data_state'] = "Service Record Created Successfully";
					$this->session->set_flashdata('success', 'Service was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Service Record.');
					$data['data_state'] = "Error Creatng service Record";
			}
		}
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_service" web page.
			$data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
			$data['business_units'] 			= $this->main_model->get_business_units();
			$data['serviceareas']				= $this->main_model->get_service_areas();
														
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['service'] 		= $this->main_model->md_get_service($id);
			$data['page'] = "pages/pg_add_service";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
			
		
		
}		
	
	
	
/*           
============================================================================
 
 		FUNCTION:	add_adminpillar()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-servicestandard" view file.
 		This will allow users to create and enter a new ServiceArea
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_adminpillar($id = NULL)
	{	
		$data['data_state'] = "";
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
	// boxes in the "add-service" web page.
		if($id)	$data['adminpillar'] = $this->main_model->md_get_adminpillar($id);
		
		$data['page'] = "pages/pg_add_adminpillar";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
	
	
	public function show_adminpillars()
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		// This shows a list of the adminpillars for users to reference
		$data['adminpillars'] 			= $this->main_model->md_get_adminpillars();
		
		$data['page']				= "pages/pg_show_adminpillars";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	

	public function update_adminpillar()
	{
			
	// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-service";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('AdminPillarID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['AdminPillarCode'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['AdminPillarName'];
		if($vd == '')
					$processYN = FALSE;

					
 	// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
		
			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */

			
			if($this->main_model->md_update_adminpillar($data))	
			{
					$data['data_state'] = "Admin Pillar Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Admin Pillar Record";
			}
		}		
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_risk" web page - PRSC
		
			
			// Load Current Record and just reshow the page with state - PRSC
				
			$data['adminpillar'] 		= $this->main_model->md_get_adminpillar($id);
			$data['page'] = "pages/pg_add_adminpillar";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
	
	public function create_adminpillar()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-adminpillar";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['AdminPillarCode'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['AdminPillarName'];
		if($vd == '')
					$processYN = FALSE;
					
						
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_adminpillar($data))	
			{
					$data['data_state'] = "Admin Pillar Record Created Successfully";
					$this->session->set_flashdata('success', 'Admin Pillar was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Admin Pillar Record.');
					$data['data_state'] = "Error Creatng Admin Pillar Record";
			}
		}
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_adminpillar" web page.
		
			
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['adminpillar'] 		= $this->main_model->md_get_adminpillar($id);
			$data['page'] = "pages/pg_add_adminpillar";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
}
		
//------------------------------------------------( EO Function Block )


	
/*           
============================================================================
 
 		FUNCTION:	add_adminpriority()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-servicestandard" view file.
 		This will allow users to create and enter a new ServiceArea
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_adminpriority($id = NULL)
	{	
		$data['data_state'] = "";
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		$data['adminpillars'] 			= $this->main_model->md_get_adminpillars();
		
	// boxes in the "add-service" web page.
		if($id)	$data['adminpriority'] = $this->main_model->md_get_adminpriority($id);
		
		$data['page'] = "pages/pg_add_adminpriority";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
	
	
	public function show_adminpriorities($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC

		$data['adminpillars'] 			= $this->main_model->md_get_adminpillars();
		
		// This shows a list of the adminprioritys for users to reference
		$data['adminprioritys'] 			= $this->main_model->md_get_adminpriorities();
		$data['page']				= "pages/pg_show_adminpriorities";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	

	public function update_adminpriority()
	{
			
	// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-adminpriority";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('AdminPriorityID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['AdminPriorityName'];
		if($vd == '')
					$processYN = FALSE;

						
 	// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_adminpriority($data))	
			{
					$data['data_state'] = "Admin Priority Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Admin Priority Record";
			}
		}	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_adminpriorityk" web page - PRSC

			$data['adminpillars'] 			= $this->main_model->md_get_adminpillars();
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['adminpriority'] 		= $this->main_model->md_get_adminpriority($id);
			$data['page'] = "pages/pg_add_adminpriority";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
	
	public function create_adminpriority()
	{	
		
			// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-risk";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['AdminPriorityName'];
		if($vd == '')
					$processYN = FALSE;

		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_adminpriority($data))	
			{
					$data['data_state'] = "Admin Priority Record Created Successfully";
					$this->session->set_flashdata('success', 'Admin Priority was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Admin Priority Record.');
					$data['data_state'] = "Error Creatng Admin Priority Record";
			}
		}
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_adminpriority" web page.

			$data['adminpillars'] 			= $this->main_model->md_get_adminpillars();
			
			
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['adminpriority'] 		= $this->main_model->md_get_adminpriority($id);
			$data['page'] = "pages/pg_add_adminpriority";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view

		
		
}
		
		
//------------------------------------------------( EO Function Block )


	
	
/*           
============================================================================
 
 		FUNCTION:	add_outcome()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-servicestandard" view file.
 		This will allow users to create and enter a new ServiceArea
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_outcome($id = NULL)
	{	
		$data['data_state'] = "";
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		
	// boxes in the "add-service" web page.
		if($id)	$data['outcome'] = $this->main_model->md_get_outcome($id);
		
		$data['page'] = "pages/pg_add_outcome";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
	
	
public function show_outcomes($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		// This shows a list of the outcomes for users to reference
		$data['outcomes'] 			= $this->main_model->md_get_outcomes();
		$data['page']				= "pages/pg_show_outcomes";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	

public function update_outcome()
	{
			
	// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-outcome";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('OutcomeID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['ServiceShortCD'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['ServiceShortNM'];
		if($vd == '')
					$processYN = FALSE;
	
 	// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_outcome($data))	
			{
					$data['data_state'] = "Outcome Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Outcome Record";
			}
		}
		
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_outcomek" web page - PRSC
		
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['outcome'] 		= $this->main_model->md_get_outcome($id);
			$data['page'] = "pages/pg_add_outcome";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
	
	public function create_outcome()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-outcome";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['OutcomeShortName'];
		if($vd == '')
					$processYN = FALSE;

					
						
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_outcome($data))	
			{
					$data['data_state'] = "Outcome Record Created Successfully";
					$this->session->set_flashdata('success', 'Outcome was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Outcome Record.');
					$data['data_state'] = "Error Creatng Outcome Record";
			}
		}
		
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_outcome" web page.
		
			
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['outcome'] 		= $this->main_model->md_get_outcome($id);
			$data['page'] = "pages/pg_add_outcome";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
			
		
		
}
		
		
//------------------------------------------------( EO Function Block )


	
/*           
============================================================================
 
 		FUNCTION:	Sources
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-source" view file.
 		This will allow users to create and enter a new ServiceArea
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_source($id = NULL)
	{	
		$data['data_state'] = "";
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		
	// boxes in the "add-service" web page.
		if($id)	$data['source'] = $this->main_model->md_get_source($id);
		
		$data['page'] = "pages/pg_add_source";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
	
	
	public function show_sources($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		// This shows a list of the sources for users to reference
		$data['sources'] 			= $this->main_model->md_get_sources();
		$data['page']				= "pages/pg_show_sources";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	

	public function update_source()
	{
			
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-source";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('SourceID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['SourceShortName'];
		if($vd == '')
					$processYN = FALSE;

		// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_source($data))	
			{
					$data['data_state'] = "Source Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Source Record";
			}
		}	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_sourcek" web page - PRSC
		
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['source'] 		= $this->main_model->md_get_source($id);
			$data['page'] = "pages/pg_add_source";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
	
	public function create_source()
	{	
		
			// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-source";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['SourceShortName'];
		if($vd == '')
					$processYN = FALSE;

						
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_source($data))	
			{
					$data['data_state'] = "Source Record Created Successfully";
					$this->session->set_flashdata('success', 'Source was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Source Record.');
					$data['data_state'] = "Error Creatng Source Record";
			}
		}
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_source" web page.
		
			
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['source'] 		= $this->main_model->md_get_source($id);
			$data['page'] = "pages/pg_add_source";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view

		
}
		
		
//------------------------------------------------( EO Function Block )


		


	
	
/*           
============================================================================
 
 		FUNCTION:	add_statusrating()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-servicestandard" view file.
 		This will allow users to create and enter a new ServiceArea
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_statusrating($id = NULL)
	{	
		$data['data_state'] = "";
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		
	// boxes in the "add-service" web page.
		if($id)	$data['statusrating'] = $this->main_model->md_get_statusrating($id);
		
		$data['page'] = "pages/pg_add_statusrating";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
	
	
	public function show_statusratings($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		
		// This shows a list of the statusratings for users to reference
		$data['statusratings'] 		= $this->main_model->md_get_statusratings();
		$data['page']				= "pages/pg_show_statusratings";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	

public function update_statusrating()
	{
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-statusrating";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('StatusID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		
		$vd = "";	
		$vd = $data['StatusDescription'];
		if($vd == '')
					$processYN = FALSE;
					
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{

			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_statusrating($data))	
			{
					$data['data_state'] = "Status Rating Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Status Rating Record";
			}
			
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_statusratingk" web page - PRSC
		}		
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['statusrating'] 		= $this->main_model->md_get_statusrating($id);
			$data['page'] = "pages/pg_add_statusrating";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
	
	public function create_statusrating()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-statusrating";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['StatusDescription'];
		if($vd == '')
					$processYN = FALSE;

						
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_statusrating($data))	
			{
					$data['data_state'] = "Status Rating Record Created Successfully";
					$this->session->set_flashdata('success', 'Status Rating was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Status Rating Record.');
					$data['data_state'] = "Error Creatng Status Rating Record";
			}
		}
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_statusrating" web page.
		
			
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['statusrating'] 		= $this->main_model->md_get_statusrating($id);
			$data['page'] = "pages/pg_add_statusrating";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
		
}
		


/*           
============================================================================
 
 		FUNCTION:	add_service_standard()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-servicestandard" view file.
 		This will allow users to create and enter a new ServiceArea
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_standard($id = NULL)
	{	
		$data['data_state'] = "";
		
	// Get the other data used to fill the link fields or external refs PRSC
		
		$data['services'] 			= $this->main_model->md_get_services();

	//Required for the multi-lable Button
		$data['business_units'] 	= $this->main_model->get_business_units();
		$data['serviceareas'] 		= $this->main_model->md_get_service_areas();
		
		
	// boxes in the "add-service" web page.
		if($id)	$data['standard'] = $this->main_model->md_get_standard($id);
		
		$data['page'] = "pages/pg_add_standard";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
		
	public function show_standards($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		$data['services'] 			= $this->main_model->md_get_services();
		$data['business_units'] 	= $this->main_model->md_get_business_units_NORM();
		$data['serviceareas']		= $this->main_model->md_get_service_areas();
		
		// This shows a list of the Risks for users to reference
		$data['standards'] 			= $this->main_model->md_get_standards();
		
		$data['page']				= "pages/pg_show_standards";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	
	
	public function update_standard()
	{
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-standard";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('StandardID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['StandardShortNM'];
		if($vd == '')
					$processYN = FALSE;

							
					
					
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_standard($data))	
			{
					$data['data_state'] = "Service Standard Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Service Standard Record";
			}
		}	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_risk" web page - PRSC
		
			$data['services'] 			= $this->main_model->md_get_services();
	
			$data['business_units'] 			= $this->main_model->md_get_business_units_NORM();
			$data['serviceareas']				= $this->main_model->md_get_service_areas();
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['standard'] 		= $this->main_model->md_get_standard($id);
			$data['page'] = "pages/pg_add_standard";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
			
	
	public function create_standard()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-standard";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['StandardShortNM'];
		if($vd == '')
					$processYN = FALSE;

							
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_standard($data))	
			{
					$data['data_state'] = "Service Standard Record Created Successfully";
					$this->session->set_flashdata('success', 'Standard was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Standard Record.');
					$data['data_state'] = "Error Creatng Service Standard Record";
			}
		}	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_performance" web page.
		
			$data['services'] 			= $this->main_model->md_get_services();
	
			$data['business_units'] 			= $this->main_model->md_get_business_units_NORM();
			$data['serviceareas']				= $this->main_model->md_get_service_areas();
			
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['standard'] 		= $this->main_model->md_get_standard($id);
			$data['page'] = "pages/pg_add_standard";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
		
}
	
	
	
/*           
============================================================================
 
 		FUNCTION:	func_focus_area()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This encloses the controller->director functions that sets up,
		edit, create, or show a list for all of the necessary values
		to display the "focusarea" group of views
.
 		This will allow users to create and enter a new record
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_focus_area($id = NULL)
	{	
		$data['data_state'] = "";
		
	// Get the other data used to fill the link fields or external refs PRSC
		
		$data['focusareatypes'] 			= $this->main_model->md_get_focus_area_types();
		
		
	// boxes in the "add-focusarea" web page.
		if($id)	$data['focusarea'] = $this->main_model->md_get_focus_area($id);
		
		$data['page'] = "pages/pg_add_focus_area";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
		
	public function show_focus_areas($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		$data['focusareatypes'] 			= $this->main_model->md_get_focus_area_types();
		
		// This shows a list of the Risks for users to reference
		$data['focusareas'] 			= $this->main_model->md_get_focus_areas();
		
		$data['page']				= "pages/pg_show_focus_areas";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	
	
	public function update_focus_area()
	{
			
	// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-focusarea";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('FocusAreaID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['FocusAreaCode'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['FocusAreaName'];
		if($vd == '')
					$processYN = FALSE;
	
 	// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
					/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_focus_area($data))	
			{
					$data['data_state'] = "Focus Area Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Focus Area Record";
			}
		}
		
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_risk" web page - PRSC
		
			$data['focusareatypes'] 			= $this->main_model->md_get_focus_area_types();
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['focusarea'] 		= $this->main_model->md_get_focus_area($id);
			$data['page'] = "pages/pg_add_focus_area";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		

	}
			
	
	public function create_focus_area()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-focusarea";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['FocusAreaCode'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['FocusAreaName'];
		if($vd == '')
					$processYN = FALSE;
					
						
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
					/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_focus_area($data))	
			{
					$data['data_state'] = "Focus Area Record Created Successfully";
					$this->session->set_flashdata('success', 'Focus Area Record was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Focus Area Record.');
					$data['data_state'] = "Error Creatng Focus Area Record";
			}
		}	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_focus_area" web page - PRSC
		
			$data['focusareatypes'] 			= $this->main_model->md_get_focus_area_types();
			
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['focusarea'] 		= $this->main_model->md_get_focus_area($id);
			$data['page'] = "pages/pg_add_focus_area";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
			
		
		
}


/*           
============================================================================
 
 		FUNCTION:	func_business_unit()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This encloses the controller->director functions that sets up,
		edit, create, or show a list for all of the necessary values
		to display the "businessunit" group of views
.
 		This will allow users to create and enter a new record
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_business_unit($id = NULL)
	{	
		$data['data_state'] = "";
		
	// Get the other data used to fill the link fields or external refs PRSC
		
		// $data['services'] 			= $this->main_model->md_get_services();
		
		
	// boxes in the "add-businessunit" web page.
		if($id)	$data['businessunit'] = $this->main_model->md_get_business_unit($id);
		
		$data['page'] = "pages/pg_add_business_unit";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
		
	public function show_business_units($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		// $data['services'] 			= $this->main_model->md_get_services();
	
		// This shows a list of the Businss Units for users to reference
		$data['businessunits'] 			= $this->main_model->md_get_business_units();
		
		$data['page']				= "pages/pg_show_business_units";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	
	
	public function update_business_unit()
	{
			
	// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-service";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('BusinessUnitID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['BusinessUnitCode'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['BusinessUnitName'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['BusinessUnitShortName'];
		if($vd == '')
					$processYN = FALSE;
					
					
 	// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
					/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_business_unit($data))	
			{
					$data['data_state'] = "Business Unit Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Business Unit Record";
			}
		}	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_risk" web page - PRSC
		
			//$data['services'] 			= $this->main_model->md_get_services();
										
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['businessunit'] 		= $this->main_model->md_get_business_unit($id);
			$data['page'] = "pages/pg_add_business_unit";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
			
	
	public function create_business_unit()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-risk";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['BusinessUnitCode'];
		if($vd == '')
					$processYN = FALSE;

		$vd = $data['BusinessUnitName'];
		if($vd == '')
					$processYN = FALSE;
					
		$vd = $data['BusinessUnitShortName'];
		if($vd == '')
					$processYN = FALSE;
						
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
					/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_business_unit($data))	
			{
					$data['data_state'] = "Business Unit Record Created Successfully";
					$this->session->set_flashdata('success', 'Business Unit Record was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Business Unit Record.');
					$data['data_state'] = "Error Creatng Business Unit Record";
			}
		}	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_business_unit" web page - PRSC
		
			$data['services'] 			= $this->main_model->md_get_services();
										
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['businessunit'] 		= $this->main_model->md_get_business_unit($id);
			$data['page'] = "pages/pg_add_business_unit";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
			

		
		
}

	
/*           
============================================================================
 
 		FUNCTION:	func_priority_outcome()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This encloses the controller->director functions that sets up,
		edit, create, or show a list for all of the necessary values
		to display the "priorityoutcome" group of views
.
 		This will allow users to create and enter a new record
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_priority_outcome($id = NULL)
	{	
		$data['data_state'] = "";
		
	// Get the other data used to fill the link fields or external refs PRSC
		
		$data['focusareas'] 			= $this->main_model->md_get_focus_areas();
				
		
	// boxes in the "add-priorityoutcome" web page.
		if($id)	$data['priorityoutcome'] = $this->main_model->md_get_priority_outcome($id);
		
		$data['page'] = "pages/pg_add_priority_outcome";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
		
	public function show_priority_outcomes($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		$data['focusareas'] 			= $this->main_model->md_get_focus_areas();
		
		// This shows a list of the Priority Outcomes for users to reference
		$data['priorityoutcomes'] 			= $this->main_model->md_get_priority_outcomes();
		
		$data['page']				= "pages/pg_show_priority_outcomes";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	
	
	public function update_priority_outcome()
	{
			
	// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-priority-outcome";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('OutcomeID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['OutcomeName'];
		if($vd == '')
					$processYN = FALSE;

		// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
					/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_priority_outcome($data))	
			{
					$data['data_state'] = "Priority Outcome Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Priority Outcome Record";
			}
		}	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_risk" web page - PRSC
		
			$data['focusareas'] 			= $this->main_model->md_get_focus_areas();
		
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['priorityoutcome'] 		= $this->main_model->md_get_priority_outcome($id);
			$data['page'] = "pages/pg_add_priority_outcome";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
			
	
	public function create_priority_outcome()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-priorityoutcome";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['OutcomeName'];
		if($vd == '')
					$processYN = FALSE;

						
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
				/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_priority_outcome($data))	
			{
					$data['data_state'] = "Priority Outcome Record Created Successfully";
					$this->session->set_flashdata('success', 'Priority Outcome Record was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Priority Outcome Record.');
					$data['data_state'] = "Error Creatng Priority Outcome Record";
			}
			
		}
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_priority_outcome" web page - PRSC
		
		$data['focusareas'] 			= $this->main_model->md_get_focus_areas();
		
		// Load Current Record and just reshow the page with state - PRSC
			
//			$data['priorityoutcome'] 		= $this->main_model->md_get_priority_outcome($id);
			$data['page'] = "pages/pg_add_priority_outcome";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
		
}
	
	
	
/*           
============================================================================
 
 		FUNCTION:	add_performance()
 		AUTHOR:		R.Stephen Chafe (Zen River Software)
 		CREATED:	20160116
 
 		This function is the controller->director function that sets up
 		necessary values to display the "add-servicestandard" view file.
 		This will allow users to create and enter a new ServiceArea
 		as requested by the 2016 change request documentation (John Spekken)
 		
 		ARGS:		$id
 		
 		RETURNS:	n/a
 
============================================================================
*/
	public function add_performance($id = NULL)
	{	
		$data['data_state'] = "";
		
		// Get the other data used to fill the link fields or external refs PRSC
		
//		$data['services'] 			= $this->main_model->md_get_services();
		$data['services']    		= $this->main_model->md_get_services_WITH_BU_SA_SV();
		
		//Required for the multi-lable Button
		$data['business_units'] 	= $this->main_model->md_get_business_units_NORM();
		$data['serviceareas'] 		= $this->main_model->md_get_service_areas();
		
		
	// boxes in the "add-service" web page.
		if($id)	$data['performance'] = $this->main_model->md_get_performance($id);
		
		$data['page'] = "pages/pg_add_performance";
		$this->load->view('template/tpl_MODREC_All', $data);
		;
	}
	
	
	
	public function show_performances($id = NULL)
	{	
		
		// Get the other data used to fill the link fields or external refs PRSC
		
		$data['services'] 					= $this->main_model->md_get_services();
		$data['business_units'] 			= $this->main_model->get_business_units();
		$data['serviceareas']				= $this->main_model->get_service_areas();

		// This shows a list of the Risks for users to reference
		$data['performances'] 			= $this->main_model->md_get_performances();
		$data['page']				= "pages/pg_show_performances";
		$this->load->view('template/tpl_LIST_All', $data);
		
	}
	

public function update_performance()
	{
			
	// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-performance";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$id = $this->input->post('PerformanceID',TRUE);
		if($id < 0)
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['PerformanceShortNM'];
		if($vd == '')
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['PerformanceCD'];
		if($vd == '')
					$processYN = FALSE;
					
	// If Data Stored Okay then allow call to DB else Fail out - PRSC			
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
		
		if($processYN == TRUE && $id > 0)
		{
					/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_update_performance($data))	
			{
					$data['data_state'] = "Performance Record Updated Successfully";
			} else { 	
					$data['data_state'] = "Error Updating Performance Record";
			}
		}	
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_risk" web page - PRSC
		
			$data['services'] 			= $this->main_model->md_get_services();
			$data['business_units'] 			= $this->main_model->md_get_business_units_NORM();
			$data['serviceareas']				= $this->main_model->md_get_service_areas();
			
			// Load Current Record and just reshow the page with state - PRSC
			
			$data['performance'] 		= $this->main_model->md_get_performance($id);
			$data['page'] = "pages/pg_add_performance";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
	}
	
	public function create_performance()
	{	
		
		// Set validation rules and wrap errors with bootstrap text-error style		

		$data['data_state'] 	= "";
		$processYN 				= TRUE;
		$esc_module				= "deliverables/add-performance";
		$data = $this->input->post(NULL,TRUE);
		
			/*
			 * -------------------------------------------------------
			 * 		Form Validation section (if any)
			 * ---------------------------------------------------PRSC
			 */

		$vd = "";	
		$vd = $data['PerformanceShortNM'];
		if($vd == '')
					$processYN = FALSE;
		$vd = "";	
		$vd = $data['PerformanceCD'];
		if($vd == '')
					$processYN = FALSE;
					
		if($processYN == TRUE)
		{
		  $data["data_state"] = '';	
		}
		else 
		{
		  $data['data_state'] = "WARNING: Form Requires Data. Please Re-enter Fields Correctly.";				
		}
					
		if($processYN == TRUE)
		{
			/*
			 * ---------------------------------------------------------
			 * 		If the FORM Validation worked then try to update
			 * 		the record in the database. 
			 * 
			 * 		Whether successful or not this goes back to the 
			 * 		original entry page, but displays message status
			 * 		at the top of the screen. It will reload the data
			 * 		from the database as well for the display.n
			 * ---------------------------------------------------PRSC
			 */
			
			if($this->main_model->md_create_performance($data))	
			{
					$data['data_state'] = "Performance Record Created Successfully";
					$this->session->set_flashdata('success', 'Performance was created.');
					
			} else { 	
					$this->session->set_flashdata('success', 'Error Creating Performance Record.');
					$data['data_state'] = "Error Creatng performance Record";
			}
		 }
		
			// This goes to database to get the data used to initialize the 
			// boxes in the "pg_add_performance" web page.
		
			$data['services'] 					= $this->main_model->md_get_services();
	
			$data['business_units'] 			= $this->main_model->md_get_business_units_NORM();
			$data['serviceareas']				= $this->main_model->md_get_service_areas();
			
			// Load Current Record and just reshow the page with state - PRSC
			
//			$data['standard'] 		= $this->main_model->md_get_performance($id);
			$data['page'] = "pages/pg_add_performance";
			$this->load->view('template/tpl_MODREC_All', $data);
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		
		
}
		
	
/*	-----------------------------------------------------------------------------------
	create() - Actually creates new deliverables
	-----------------------------------------------------------------------------------*/
	public function create_deliverable()
	{	
		// Set validation rules and wrap errors with bootstrap text-error style		
		$this->form_validation->set_rules('DeliverableTypeID', 'type', 'trim|required|xss_clean');
		if($this->input->post('DeliverableTypeID',TRUE) == 2) 
			$this->form_validation->set_rules('DeliverableParentID', 'Related to', 'trim|xss_clean');
		$this->form_validation->set_rules('BusinessUnitID', 'business unit', 'trim|required|xss_clean');
		$this->form_validation->set_rules('ServiceArea'.$this->input->post('BusinessUnitID',TRUE), 'service area', 'trim|required|xss_clean');
		$this->form_validation->set_rules('OutcomeID', 'outcome', 'trim|required|xss_clean');
		$this->form_validation->set_rules('AdminPriorityID', 'admin priority', 'trim|required|xss_clean');
		$this->form_validation->set_rules('RiskID', 'risk', 'trim|xss_clean');
		$this->form_validation->set_rules('PriorityID', 'priority', 'trim|required|xss_clean');
		$this->form_validation->set_rules('DeliverableCode', 'business plan #', 'trim|required|xss_clean|max_length[5]');
		$this->form_validation->set_rules('DeliverableDescShort', 'short description', 'trim|required|xss_clean|max_length[500]');
		$this->form_validation->set_rules('DeliverableDesc', 'full description', 'trim|required|xss_clean|max_length[5000]');
		// $this->form_validation->set_rules('StatusUpdate', 'Update', 'trim|xss_clean');
		// $this->form_validation->set_rules('StatusUpdate', 'Update', 'trim|xss_clean');
		// $this->form_validation->set_rules('StatusUpdate', 'Status Update', 'trim|xss_clean|required|max_length[5000]');
		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

		if ($this->form_validation->run() == FALSE) 	$this->add_deliverable();
		// Otherwise, save the submitted data and send a confirmation email to user
		else
		{
			$data = $this->input->post(NULL,TRUE);

			// update
			if(!empty($data['DeliverableID']))
			{
				if($this->main_model->md_update_deliverable($data))
					$this->session->set_flashdata('success', 'Deliverable was updated.');
				else 
					$this->session->set_flashdata('danger', 'Error updating deliverable.');
				redirect('edit-deliverable/'.$data['DeliverableID']);
			}
			// create
			else
			{
				if($this->main_model->create_deliverable($data))
					$this->session->set_flashdata('success', 'Deliverable was added.');
				else 
					$this->session->set_flashdata('danger', 'Error adding deliverable.');
				redirect('create-deliverable');
			}
		}

	}
	
/* ---------------------------------------------------------------
 * 
 * 		The Status screen information is split over two tables. The
 * 		reason for this is unclear.  Perhaps it was added scope creep
 * 		or the idea was to have multiple status entries per deliverable.
 * 
 * 		The actual Status Desc is in its own table, while the other
 * 		settings are kept in the main Deliverables Table.
 * 
 * ------------------------------------------------------------ PRSC
 */
	

public function edit_status($id)
	{

		$data['deliverable'] 					= $this->main_model->md_get_status_deliverable($id);
		$parentID = ($data['deliverable']['DeliverableTypeID'] ==
				 2 ? $data['deliverable']['DeliverableParentID'] : NULL);
		$data['supporting_deliverables'] 		=
				 $this->main_model->get_supporting_deliverables($id, $data['deliverable']['DeliverableTypeID'], $parentID );

		// for side menu
		// $data['deliverable_types'] 				= $this->main_model->get_deliverable_types();
		// $data['business_units'] 				= $this->main_model->get_business_units();
		// $data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
		// // $data['focus_area_outcomes'] 			= $this->main_model->get_focus_area_outcomes();
		// // $data['priority_outcomes'] 				= $this->main_model->get_priority_outcomes();
		// $data['risks'] 							= $this->main_model->md_get_risks();
		// $data['priorities'] 					= $this->main_model->get_priorities();
		$data['statuses'] 						= $this->main_model->get_statuses();
		$data['publishedinfo']					= $this->main_model->md_get_published();
		$data['sources']						= $this->main_model->md_get_sources();
		$data['services']						= $this->main_model->md_get_services();
			$data['business_units'] 			= $this->main_model->md_get_business_units_NORM();
			$data['serviceareas']				= $this->main_model->md_get_service_areas();
		
		$data['page'] = "pages/pg_add_status";
		$this->load->view('template/tpl_MODREC_All', $data);
	}

	
	

/*           
============================================================================
 update
----------------------------------------------------------------------------
 Accepts an ID if the deliverable from POST.
----------------------------------------------------------------------------
 Validates the submitted data. 
 If valid, updates that deliverable and returns the user to the URL listing 
 view they were on before they clicked edit. 
 If invalid, returns the user to the edit page.
 
 		This function is used solely now to take the data from the
 		STATUS screen, and save it to the database.
 
============================================================================
*/
	public function update_status($rid = NULL)
	{
		// Set validation rules and wrap errors with bootstrap text-error style		
		$this->form_validation->set_rules('StatusUpdate', 'status update', 'trim|required|max_length[5000]');
		$this->form_validation->set_rules('ApproxComplete', 'approximate completion', 'trim|required');
		$this->form_validation->set_rules('ApproxEndDate', 'approximate end date', 'trim|required');
		$this->form_validation->set_rules('StatusID', 'status', 'trim|required');

		$data = $this->input->post(NULL,TRUE);

//		if($rid)
//		{
//		$id = $rid;
//		}
//		else
//		{
		$id = $this->input->post('DeliverableID',TRUE);			// get the hidden field
//		}

		if ($this->form_validation->run() == FALSE)
		{
			$data['deliverable'] 					= $this->main_model->md_get_status_deliverable($id);
			$parentID = ($data['deliverable']['DeliverableTypeID'] == 2 ?
					 $data['deliverable']['DeliverableParentID'] : NULL);
			$data['supporting_deliverables'] 		= $this->main_model->get_supporting_deliverables($id, 
								$data['deliverable']['DeliverableTypeID'], $parentID );
			$data['statuses'] 						= $this->main_model->get_statuses();

			$data['page'] = "pages/pg_add_status";
//			$this->load->view('template/master', $data);
			$this->load->view('template/tpl_MODREC_All', $data);
			
		}
		// Otherwise, save the submitted data and send a confirmation email to user
		else
		{
			// $this->main_model->update_deliverable($data);
			if($this->main_model->update_deliverable_status($data))
				$data['success'] = 'Deliverable [' . $id . '] was updated.';
			else 	
				$data['fail'] = 'Error updating deliverable [' . $id . ']';
			
			
			$data['deliverable'] 					= $this->main_model->md_get_status_deliverable($id);
			$parentID = ($data['deliverable']['DeliverableTypeID'] == 2 ?
				 $data['deliverable']['DeliverableParentID'] : NULL);
			$data['supporting_deliverables'] 		= $this->main_model->get_supporting_deliverables($id, 
												$data['deliverable']['DeliverableTypeID'], $parentID );
			$data['statuses'] 						= $this->main_model->get_statuses();
			$data['publishedinfo']					= $this->main_model->md_get_published();
			$data['sources']						= $this->main_model->md_get_sources();
			$data['services']						= $this->main_model->md_get_services();
			$data['business_units'] 			= $this->main_model->md_get_business_units_NORM();
			$data['serviceareas']				= $this->main_model->md_get_service_areas();
			
			
			$data['page'] = "pages/pg_add_status";
//			$this->load->view('template/master', $data);
			$this->load->view('template/tpl_MODREC_All', $data);
			
			
			// redirect($this->session->flashdata('uri'));	// set in the view
		}
	}

/*           
============================================================================
 view($id)
----------------------------------------------------------------------------
 
----------------------------------------------------------------------------
 
 	This function is used to display the STATUS data entry screen. 
 	
 	The STATUS entry portion of the software will display the Deliverable
 	in the same layout as what you see on the SHOW all type screens. But
 	at the bottom there is the option to enter other information for the
 	current STATUS, Approximate Date, Time, Outcome ID.
 	
 	Also shows the Related Operational Items at the bottom of the screen (PRSC).
 
	Loads the edit deliverable view for users with access to a the deliverable 
 	being requested, otherwise it sends them back to the main listings.(GIAN)
 	Accepts an ID if the deliverable from POST.
 	
 	Previously this was just called 'deliverable' method.(PRSC)
 
 
============================================================================
*/
	public function deliverable_status($id)
	{

		$data['deliverable'] 					= $this->main_model->get_deliverable($id);
		$parentID = ($data['deliverable']['DeliverableTypeID'] == 2 ? $data['deliverable']['DeliverableParentID'] : NULL);
		$data['supporting_deliverables'] 		= $this->main_model->get_supporting_deliverables($id, $data['deliverable']['DeliverableTypeID'], $parentID );

		// for side menu
		// $data['deliverable_types'] 				= $this->main_model->get_deliverable_types();
		// $data['business_units'] 				= $this->main_model->get_business_units();
		// $data['business_unit_service_areas']	= $this->main_model->get_business_unit_service_areas();
		// // $data['focus_area_outcomes'] 			= $this->main_model->get_focus_area_outcomes();
		// // $data['priority_outcomes'] 				= $this->main_model->get_priority_outcomes();
		// $data['risks'] 							= $this->main_model->get_risks();
		// $data['priorities'] 					= $this->main_model->get_priorities();
		$data['statuses'] 						= $this->main_model->get_statuses();
		$data['publishedinfo']					= $this->main_model->md_get_published();
		$data['sources']						= $this->main_model->md_get_sources();
		$data['services']						= $this->main_model->md_get_services();
			$data['business_units'] 			= $this->main_model->md_get_business_units_NORM();
			$data['serviceareas']				= $this->main_model->md_get_service_areas();
		
		$data['page'] = "pages/deliverable";
		$this->load->view('template/master', $data);
	}
	


}