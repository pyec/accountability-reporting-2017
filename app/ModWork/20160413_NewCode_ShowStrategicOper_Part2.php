<?php
 /*---------------------------------------------------------------------
  * 
  * 	This is used with the Strategic Listings to build a list of 
  * 	links that allows users to update the status information on 
  * 	each of the related Deliverables to this parent Strategic one.
  * 
  * -----------------------------------------------------20160222 PRSC
  */
?>

		
		<?php 
		$XCOND_ShowRelatedParents 		= FALSE;
		$XCOND_ShowRelatedChildren		= FALSE;
		
		// Check if Show the Strategic Values
		if($ddfilters['DeliverableTypeID'] == 1 && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedParents 	= TRUE; }
				
		// Check if Show the Operational Values		
		if($ddfilters['DeliverableTypeID'] == 2 &&$CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedChildren	= TRUE;
				
		// Check if base view requires showing both		
		if($ddfilters['DeliverableTypeID'] == 1 && $CNT_childrenxx > 0)
				{ $XCOND_ShowRelatedParents 	= TRUE; }
		if($ddfilters['DeliverableTypeID'] == 3 &&$CNT_parentsxx > 0)
				{ $XCOND_ShowRelatedChildren	= TRUE;
		?>
				
		<?php		
		/*-----------------------------------------------
		 * 		Strategics Header
		 * -----------------------------------------------
		 */
		?>
		<?php if($XCOND_ShowRelatedParents == TRUE): ?>		
		
		<div class="col-sm-12 supporting_deliverables">
			<p class="">

			<?php 
				echo 'Strategic Initiatives';
			?>

			</p>
		<ul>
		<?php
		/* -------------------------------------------------------------
		 * 			Check if Child Deliverables to show
		 * -------------------------------------------------------------
		 */
		
		foreach ($PRNT_Deliverables as $rcdt): ?>
				<?php
					$label_color = '';
					if($rcdt['statusid'] == 1 ) $label_color = 'success';
					if($rcdt['statusid'] == 2 ) $label_color = 'warning';
					if($rcdt['statusid'] == 3 ) $label_color = 'danger';
					if($deliverable['StatusID'] > 3 ) $label_color = 'info';
					?>
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $rcdt['id'] ?>">
						<?= $rcdt['title'] ?>
					</a>
					
					 <!-- - <?= $rcdt['percent'] ?>% completed 
					    <?= ($rcdt['percent'] < 100) ? '('.$rcdt['status'].
					            ')' : '' ?>  -->
					 - <span title="<?= $rcdt['status'] ?> ">
					          <?= $rcdt['percent'] ?>% completed</span>
				</li>
				<?php endforeach ?>

			</ul>
		</div>
		<?php endif ?>
		
		<!-- EO Display ALL Related Supporting Deliverables - Strategic or Operational -->
	
		<?php		
		/*-----------------------------------------------
		 * 		Strategics Header
		 * -----------------------------------------------
		 */
		if($XCOND_ShowRelatedParents == TRUE):
		?>		
		
		<div class="col-sm-12 supporting_deliverables">
			<p class="">

			<?php 
				echo 'Strategic Initiatives';
			?>

			</p>
		<ul>
		<?php
		/* -------------------------------------------------------------
		 * 			Check if Child Deliverables to show
		 * -------------------------------------------------------------
		 */
		
		foreach ($CHLD_Deliverables as $rcdt): ?>
				<?php
					$label_color = '';
					if($rcdt['statusid'] == 1 ) $label_color = 'success';
					if($rcdt['statusid'] == 2 ) $label_color = 'warning';
					if($rcdt['statusid'] == 3 ) $label_color = 'danger';
					if($deliverable['StatusID'] > 3 ) $label_color = 'info';
					?>
				<li class="text-<?= $label_color ?>">
					<a href="<?= base_url() ?>edit-status/<?= $rcdt['id'] ?>">
						<?= $rcdt['title'] ?>
					</a>
					
					 <!-- - <?= $rcdt['percent'] ?>% completed 
					    <?= ($rcdt['percent'] < 100) ? '('.$rcdt['status'].
					            ')' : '' ?>  -->
					 - <span title="<?= $rcdt['status'] ?> ">
					          <?= $rcdt['percent'] ?>% completed</span>
				</li>
				<?php endforeach ?>

			</ul>
		</div>
		<?php endif ?><!-- EO Display ALL Related Operationals -->
 