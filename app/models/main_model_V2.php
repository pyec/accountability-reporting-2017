<?php
class Main_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//$this->load->database();	// auto-loaded
	}

/*===========================================================================
 * 
 *  FUNCTION:       is_user()
 *  AUTHOR:         Unknown
 *  CREATED:        Unknown
 * 
 * ------------------------------------------------------------------------
 *  This function gets the user credentials and returns it to the calling
 *  parent in an array.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         NIL
 *  OUTPUTS:        array containing user email and passwrd
 * 
 *  MOD HISTRY:
 *  20160104    PRSC    Comments Sections Added
 * 
 *==========================================================================*/
             
	public function is_user($email=FALSE, $password=FALSE)
	{

		if(!empty($email) && !empty($password)) {

			$values = array(
				'UserEmail' 	=> trim(strtolower($email)),
				'UserPassword'	=> trim(strtolower($password))
			);

			$query = $this->db->get_where('UsersView', $values); 
			return $query->row_array();
		}
	}



	// public function get_strategic_deliverables($type = FALSE, $section = FALSE, $category = FALSE)
	// {

	// 	// check if a type is supplied
	// 	if(!empty($type))
	// 	{
	// 		// perform the appropriate action depending on the type provided
	// 		switch ($type)
	// 		{
	// 			case 'mine':
	// 				break;

	// 			case 'focus-area':

	// 				if(!empty($section))
	// 				{
	// 					// return deliverables for the appropriate business unit
	// 					$this->db->where('FocusAreaCode', $section);
	// 					$this->db->order_by("OutcomeName", "asc");
						
	// 					if(!empty($category))
	// 					{
	// 						$this->db->where('OutcomeName', $category);
	// 						$this->db->order_by("DeliverableCode", "asc");
	// 					}
	// 				}

	// 				break;

	// 			case 'business-unit':

	// 				if(!empty($section))
	// 				{
	// 					// return deliverables for the appropriate business unit
	// 					$this->db->where('BusinessUnitCode', $section);
	// 					$this->db->order_by("DeliverableCode", "asc");

	// 					if(!empty($category))
	// 					{
	// 						$this->db->where('ServiceAreaID', $category);
	// 					}
	// 				}

	// 				// order deliverables by business unit name in ascending order
	// 				//$this->db->order_by("DeliverableCode", "asc");

	// 				break;

	// 			case 'status':

	// 				if(!empty($section))
	// 				{
	// 					$this->db->where('StatusID', $section);
	// 					$this->db->order_by("BusinessUnitCode", "asc");
	// 					$this->db->order_by("DeliverableCode", "asc");
	// 				}

	// 				break;
	// 		}
	// 	}

	// 	// otherwise, if no type was supplied
	// 	else
	// 	{
	// 		// order by business plan code
	// 		//$this->db->order_by("StatusID", "desc");
	// 		$this->db->order_by("BusinessUnitCode", "asc");
	// 		$this->db->order_by("DeliverableCode", "asc");
	// 	}

	// 	//$this->db->where('active', 'True');
	// 	//$this->db->order_by("service_category", "asc");
		
	// 	$query = $this->db->get('StrategicDeliverablesView');
	// 	return $query->result_array();
	// }


	// public function get_operational_deliverables()
	// {
	// 	$this->db->select('DeliverableParentID, BusinessUnitCode, DeliverableCode, DeliverableDescShort');
	// 	$this->db->order_by("DeliverableParentID", "asc");
	// 	$this->db->order_by("BusinessUnitCode", "asc");
	// 	$this->db->order_by("DeliverableCode", "asc");
	// 	$query = $this->db->get('OperationalDeliverablesView');
	// 	return $query->result_array();
	// }


/*===========================================================================
 * 
 *  FUNCTION:       md_get_deliverable
 *  AUTHOR:         Unknown
 *  CREATED:        Unknown
 * 
 * ------------------------------------------------------------------------
 *
 *      This function will build a single record display of the deliverables.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	    $id			Key value for ID in question	
 *  OUTPUTS:        	--
 * 
 *  MOD HISTRY:
 *  20160104    PRSC    Comments Sections Added
 * 
 *==========================================================================*/
             	
	
	public function get_deliverable($id = 0)
	{
		$this->db->where('DeliverableID', $id);
//		$query = $this->db->get('DeliverablesView');
		$query = $this->db->get('Deliverables');
		
		if($query->row_array())	return $query->row_array();	
	}

		public function md_get_status_deliverable($id = 0)
	{
		$this->db->where('DeliverableID', $id);
		$query = $this->db->get('DeliverablesView');
//		$query = $this->db->get('Deliverables');
		
		if($query->row_array())	return $query->row_array();	
	}
	
	
	
/*===========================================================================
 * 
 *  FUNCTION:       md_get_deliverables
 *  AUTHOR:         Unknown
 *  CREATED:        Unknown
 * 
 * ------------------------------------------------------------------------
 *
 *      This function will build a display of the deliverables based on 
 *      what search criteria the user enters into the various dropdown
 *      search conditions.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         $filter
 *  OUTPUTS:        --
 * 
 *  MOD HISTRY:
 *  20160104    PRSC    Comments Sections Added
 * 
 *==========================================================================*/
             
	public function get_deliverables($filter)
	{
		if(!empty($filter)) 
		{
			// if(!empty($filter['DeliverableTypeID']))	$this->db->where('DeliverableTypeID', $filter['DeliverableTypeID']);
			if(!empty($filter['DeliverableTypeID']) && $filter['DeliverableTypeID'] != 3)	$this->db->where('DeliverableTypeID', $filter['DeliverableTypeID']);
			if(!empty($filter['BusinessUnitID']))
			{
				$this->db->where('BusinessUnitID', $filter['BusinessUnitID']);
				if(!empty($filter['ServiceArea'.$filter['BusinessUnitID']]))	$this->db->where('ServiceAreaID', $filter['ServiceArea'.$filter['BusinessUnitID']]);
			}				
			// if(!empty($filter['OutcomeID']))			$this->db->where('OutcomeID', $filter['OutcomeID']);
			if(!empty($filter['FocusAreaID']))			$this->db->where('FocusAreaID', $filter['FocusAreaID']);
			if(!empty($filter['AdminPillarID']))		$this->db->where('AdminPillarID', $filter['AdminPillarID']);
			if(!empty($filter['RiskID']))				$this->db->where('RiskID', $filter['RiskID']);
			if(!empty($filter['PriorityID']))			$this->db->where('PriorityID', $filter['PriorityID']);

			
			if(!empty($filter['WorkingYear']))			$this->db->where('WorkingYear', $filter['WorkingYear']);
			if(!empty($filter['ServiceBY']))			$this->db->where('ServiceBY', $filter['ServiceBY']);
			if(!empty($filter['ServiceFOR']))			$this->db->where('ServiceFOR', $filter['ServiceFOR']);
			if(!empty($filter['ImpactedServ']))			$this->db->where('ImpactedServ', $filter['ImpactedServ']);
			if(!empty($filter['Source']))			$this->db->where('Source', $filter['Source']);
			if(!empty($filter['PublishedYN']))			$this->db->where('PublishedYN', $filter['PublishedYN']);
			if(!empty($filter['PerformanceID']))		$this->db->where('PerformanceID', $filter['PerformanceID']);
			if(!empty($filter['StandardID']))			$this->db->where('StandardID', $filter['StandardID']);
			
			
			if(!empty($filter['StatusID']) && $filter['StatusID'] == 4)
				$this->db->where('StatusID >', 1);
			elseif(!empty($filter['StatusID']))	
				$this->db->where('StatusID', $filter['StatusID']);

		}

		// else $this->db->where('DeliverableTypeID', 1);	// default deliverable type is strategic

		// check view type
		if(empty($filter['ViewType']) || $filter['ViewType'] == 1) // default view
		{
			$this->db->order_by("BusinessUnitCode", "ASC");
			$this->db->order_by("DeliverableCode", "ASC");
		}
		elseif($filter['ViewType'] == 2) // focus area view
		{
			$this->db->order_by("FocusAreaName", "ASC");
			$this->db->order_by("OutcomeName", "ASC");
			$this->db->order_by("BusinessUnitCode", "ASC");
			$this->db->order_by("DeliverableCode", "ASC");	
		}
		elseif($filter['ViewType'] == 3) // risk view
		{
			$this->db->order_by("RiskDescShort", "ASC");
			$this->db->order_by("BusinessUnitCode", "ASC");
			$this->db->order_by("DeliverableCode", "ASC");
		}
		elseif($filter['ViewType'] == 4) // business unit view
		{
			$this->db->order_by("BusinessUnitCode", "ASC");
			$this->db->order_by("ServiceAreaName", "ASC");
			$this->db->order_by("DeliverableCode", "ASC");
		}
		elseif($filter['ViewType'] == 5) // administrative pillar view
		{
			$this->db->order_by("AdminPillarName", "ASC");
			$this->db->order_by("AdminPriorityName", "ASC");
			$this->db->order_by("DeliverableCode", "ASC");
		}

//---------------------------------------------------------------------------
//    		This section for debugging of filter selection data - PRSC
//---------------------------------------------------------------------------		
//			 echo "<pre>";
//			 print_r($filter);
//			 echo "</pre>";
		
		
		// $this->db->select('BusinessUnitCode, BusinessUnitName, DeliverableTypeID, DeliverableID, DeliverableParentID, DeliverableCode, DeliverableDescShort, ServiceAreaName, ApproxEndDate, ApproxComplete, DeliverableDesc, OutcomeName, OutcomeDesc, FocusAreaName, FocusAreaDesc, PriorityDesc, RiskDescShort, RiskDesc, StatusUpdate, LastModDate, LastModBy, StatusID, StatusDescription');
		$query = $this->db->get('DeliverablesView2016');
		return $query->result_array();
	}



/*===========================================================================
 * 
 *  FUNCTION:       md_get_supporting_deliverables
 *  AUTHOR:         Unknown
 *  CREATED:        Unknown
 * 
 * ------------------------------------------------------------------------
 *
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         $filter
 *                  $type
 *                  $parentID
 * 
 *  OUTPUTS:        --
 * 
 *  MOD HISTRY:
 *  20160104    PRSC    Comments Sections Added
 * 
 *==========================================================================*/

	public function get_supporting_deliverables($filter, $type = null, $parentID = null)
	{
		if(!empty($filter)) 
		{
			$this->db->order_by("BusinessUnitCode", "ASC");
			$this->db->order_by("DeliverableCode", "ASC");

			// if filter is not an array (eg is an ID)
			if(!is_array($filter))
			{
				// if type = 1 (strategic) then get operational, otherwise get strategic
				if(!empty($type) && $type == 1) 
				{
					$this->db->where('DeliverableParentID', $filter);
					$query = $this->db->get('OperationalDeliverablesView');
				}	
				else if(!empty($parentID))
				{
					$this->db->where('DeliverableID', $parentID);
					$query = $this->db->get('StrategicDeliverablesView');
				}
			}	
			else
			{
				// if(!empty($filter['DeliverableTypeID']))	$this->db->where('DeliverableTypeID', $filter['DeliverableTypeID']);
				// if(!empty($filter['BusinessUnitID']))		$this->db->where('BusinessUnitID', $filter['BusinessUnitID']);
				// if(!empty($filter['OutcomeID']))			$this->db->where('OutcomeID', $filter['OutcomeID']);
				// if(!empty($filter['FocusAreaID']))			$this->db->where('FocusAreaID', $filter['FocusAreaID']);
				// if(!empty($filter['RiskID']))				$this->db->where('RiskID', $filter['RiskID']);
				// if(!empty($filter['PriorityID']))			$this->db->where('PriorityID', $filter['PriorityID']);
				// if(!empty($filter['StatusID']))				$this->db->where('StatusID', $filter['StatusID']);
				// if(!empty($filter['StatusID']) && $filter['StatusID'] == 4)
				// 	$this->db->where('StatusID >', 1);
				// elseif(!empty($filter['StatusID']))	
				// 	$this->db->where('StatusID', $filter['StatusID']);
				
				// if($filter['DeliverableTypeID'] != 3)		$this->db->where('DeliverableTypeID', $filter['DeliverableTypeID']);
				// $query = $this->db->get('DeliverablesView');
				
				// use the opposite view to get the supporting deliverables
				if($filter['DeliverableTypeID'] == 1)		$query = $this->db->get('OperationalDeliverablesView');
				elseif($filter['DeliverableTypeID'] == 2)	$query = $this->db->get('StrategicDeliverablesView');
				else $query = $this->db->get('DeliverablesView');
			}
			return $query->result_array();
		}
		// otherwise, load operational as supporting deliverables
		// else 	$query = $this->db->get('OperationalDeliverablesView');
		// return $query->result_array();
		return false;
	}




	// public function get_deliverables_csv($filter)
	// {
	// 	$this->load->dbutil();

	// 	if(!empty($filter)) 
	// 	{
	// 		// if(!empty($filter['DeliverableTypeID']))	$this->db->where('DeliverableTypeID', $filter['DeliverableTypeID']);
	// 		if(!empty($filter['DeliverableTypeID']) && $filter['DeliverableTypeID'] != 3)	
	// 			$this->db->where('DeliverableTypeID', $filter['DeliverableTypeID']);

	// 		if(!empty($filter['BusinessUnitID'])) 
	// 		{
	// 			$this->db->where('BusinessUnitID', $filter['BusinessUnitID']);

	// 			if(!empty($filter['ServiceArea'.$filter['BusinessUnitID']]))
	// 				$this->db->where('ServiceAreaID', $filter['ServiceArea'.$filter['BusinessUnitID']]);
	// 		}
				
	// 		// if(!empty($filter['OutcomeID']))			$this->db->where('OutcomeID', $filter['OutcomeID']);
	// 		if(!empty($filter['FocusAreaID']))			$this->db->where('FocusAreaID', $filter['FocusAreaID']);
	// 		if(!empty($filter['RiskID']))				$this->db->where('RiskID', $filter['RiskID']);
	// 		if(!empty($filter['PriorityID']))			$this->db->where('PriorityID', $filter['PriorityID']);
	// 		// if(!empty($filter['StatusID']))				$this->db->where('StatusID', $filter['StatusID']);
	// 		if(!empty($filter['StatusID']) && $filter['StatusID'] == 4)
	// 			$this->db->where('StatusID >', 1);
	// 		elseif(!empty($filter['StatusID']))	
	// 			$this->db->where('StatusID', $filter['StatusID']);
	// 	}
	// 	// otherwise get strategic deliverables
	// 	// else $this->db->where('DeliverableTypeID', 1);			

	// 	$this->db->order_by("Business_Unit", "ASC");
	// 	$this->db->order_by("Business_Plan_Number", "ASC");

	// 	$this->db->select('Business_Unit, Service_Area, Business_Plan_Number, Business_Plan_Deliverable, Business_Plan_Deliverable_Description, Focus_Area, Priority_Outcome, Associated_Risk, Priority_Status, Status, Status_Update, Percent_Complete, Expected_Completion, Last_Updated, Last_Updated_By, Deliverable_Type_ID, Deliverable_Type');
	// 	$query = $this->db->get('DeliverablesCSVView');

	// 	$result = $this->dbutil->csv_from_result($query);		// create csv from query results
	// 	return utf8_decode(strip_tags($result));	// strip HTML tags and ensure it's coded as UTF8 to prevent strange characters from being displayed in Excel
	// }






	// public function get_user_business_unit($email_address = null)
	// {
	// 	if(!empty($email_address))	$this->db->where('user_email', $email_address);
	// 	$query = $this->db->get('user_business_unit');
	// 	return $query->result_array();
	// }

        
/*===========================================================================
 * 
 *  FUNCTION:       md_get_deliverable_types
 *  AUTHOR:         Unknown
 *  CREATED:        Unknown
 * 
 * ------------------------------------------------------------------------
 *      This function builds a list of deliverable types from the database
 *      table, and this is used to later fill a drop down box.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         $id
 *  OUTPUTS:        --
 * 
 *  MOD HISTRY:
 *  20160104    PRSC    Comments Sections Added
 * 
 *==========================================================================*/
        
        
	public function get_deliverable_types($id = null)
	{
		if(!empty($id))	$this->db->where('DeliverableTypeID', $id);
		$query = $this->db->get('DeliverableTypes');
		return $query->result_array();
	}
        
               
/*===========================================================================
 * 
 *  FUNCTION:       md_get_statuses
 *  AUTHOR:         Unknown
 *  CREATED:        Unknown
 * 
 * ------------------------------------------------------------------------
 *      This function builds a list of deliverable types from the database
 *      table, and this is used to later fill a drop down box.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         $id
 *  OUTPUTS:        --
 * 
 *  MOD HISTRY:
 *  20160104    PRSC    Comments Sections Added
 * 
 *==========================================================================*/
        

	public function get_statuses($id = null)
	{
		if(!empty($id))	$this->db->where('StatusID', $id);
		$query = $this->db->get('Status');
		return $query->result_array();
	}

	public function get_business_units($id = null)
	{
		if(!empty($id))	$this->db->where('BusinessUnitID', $id);
		$this->db->order_by("BusinessUnitName", "ASC");
		$query = $this->db->get('BusinessUnits');
		return $query->result_array();
	}

	public function get_business_unit_id($code = null)
	{
		if(!empty($code))	$this->db->where('BusinessUnitCode', $code);
		// $this->db->order_by("BusinessUnitCode", "ASC");
		$this->db->select('BusinessUnitID');
		$query = $this->db->get('BusinessUnits');
		if($row = $query->row_array())	return $row['BusinessUnitID'];
	}

		public function get_service_areas($id = null)
	{
		if(!empty($id))	$this->db->where('ServiceAreaID', $id);
		$this->db->order_by("ServiceAreaName", "ASC");
		$query = $this->db->get('ServiceAreas');
		return $query->result_array();
	}
	
		
		
	public function get_priority_outcomes($id = null)
	{
		if(!empty($id))	$this->db->where('OutcomeID', $id);
		$this->db->order_by("OutcomeName", "ASC");
		$query = $this->db->get('PriorityOutcomes');
		return $query->result_array();
	}

	public function get_admin_priorities($id = null)
	{
		if(!empty($id))	$this->db->where('AdminPriorityID', $id);
		$this->db->order_by("AdminPriorityName", "ASC");
		$query = $this->db->get('AdminPriorities');
		return $query->result_array();
	}
	
	
	public function get_priorities($id = null)
	{
		if(!empty($id))	$this->db->where('PriorityID', $id);
		$query = $this->db->get('Priorities');
		return $query->result_array();
	}

	public function get_focus_areas($id = null)
	{
		if(!empty($id))	$this->db->where('FocusAreaID', $id);
		$this->db->order_by("FocusAreaName", "ASC");
		$query = $this->db->get('FocusAreas');
		return $query->result_array();
	}

	public function get_admin_pillars($id = null)
	{
		if(!empty($id))	$this->db->where('AdminPillarID', $id);
		$this->db->order_by("AdminPillarName", "ASC");
		$query = $this->db->get('AdminPillars');
		return $query->result_array();
	}

	
	// public function get_focus_area_outcomes($id = null)
	// {
	// 	$this->db->order_by("FocusAreaName", "ASC");
	// 	$query = $this->db->get('FocusAreaOutcomeView');
	// 	return $query->result_array();
	// }


  	public function get_business_unit_service_areas($id = null)
	{
		$this->db->order_by("BusinessUnitName", "ASC");
		$this->db->order_by("ServiceAreaName", "ASC");
		$query = $this->db->get('BusinessUnitServiceAreaView');
		return $query->result_array();
	}

          
        
        

	public function update_deliverable_status($data)
	{

		$DATETIME	= date("Y-m-d H:i:s");
		//$USER		= 0;

		$values = array(
			//'DeliverableID' 	=> $USER,
			//'DeliverableCode' 	=> $data['servno'],
			//'BusinessUnitID' 	=> strtoupper($data['postal_code']),
			//'ServiceAreaID' 	=> $data['number_of_items'],
			//'OutcomeID' 		=> $data['DeliverableID'],
			//'DeliverableDesc'	=> $data['DeliverableDesc'],
			'StatusUpdate' 		=> trim($data['StatusUpdate']),
			'ApproxEndDate' 	=> date('Y-m-d H:i:s', strtotime($data['ApproxEndDate'])),
			'ApproxComplete' 	=> substr($data['ApproxComplete'],0,-1),
			'StatusID' 			=> $data['StatusID'],
			'LastModBy' 		=> $data['LastModBy'],
			'LastModDate' 		=> $DATETIME
			//'flag_CorpPlan' 	=> $data['item_inside'],
			//'flap_CAO' 			=> $data['item_inside'],
			//'flag_DCAO' 		=> $data['item_inside']

		);

		$this->db->where('DeliverableID', $data['DeliverableID']);
		$result = $this->db->update('Deliverables',$values);

		return $result;

	}



	public function create_deliverable($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;

		$values = array(
			//'DeliverableID' 	=> auto incrementing
			'DeliverableTypeID' => $data['DeliverableTypeID'],
			'DeliverableParentID' => $data['DeliverableParentID'],
			'DeliverableCode' 	=> $data['DeliverableCode'],
			'DeliverableDesc'	=> $data['DeliverableDesc'],
			'DeliverableDescShort'	=> $data['DeliverableDescShort'],
			'BusinessUnitID' 	=> $data['BusinessUnitID'],
			'ServiceAreaID' 	=> $data['ServiceArea'.$data['BusinessUnitID']],
			'OutcomeID' 		=> $data['OutcomeID'],
			'RiskID' 			=> $data['RiskID'],
			'PriorityID' 		=> $data['PriorityID'],
			'AdminPriorityID' 	=> $data['AdminPriorityID'],
			'Source' 			=> $data['Source'],
			'WorkingYear' 		=> $data['WorkingYear'],
			'ServiceFOR' 		=> $data['ServiceFOR'],
			'ServiceBY' 		=> $data['ServiceBY'],
			'ImpactedServ' 		=> $data['ImpactedServ'],
			'PublishedYN' 		=> $data['PublishedYN'],
		
			//'StatusID' 		=> $data['StatusID'],
			//'StatusUpdate' 	=> $data['StatusUpdate'],
			//'ApproxEndDate' 	=> $DATETIME,
			'ApproxComplete' 	=> 0,
		
		
			'LastModBy' 		=> $data['LastModBy'],
			'LastModDate' 		=> $DATETIME
		);

		$result = $this->db->insert('Deliverables', $values);
		if($result)	 	return true;
		else 			return false;
	}

	
	
	
/*===========================================================================
 * 
 *  FUNCTION:       --
 *  AUTHOR:         Unknown
 *  CREATED:        Unknown
 * 
 * ------------------------------------------------------------------------
 *
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         $data
 *  OUTPUTS:        --
 * 
 *  MOD HISTRY:
 *  20160104    PRSC    Comments Sections Added
 * 
 *==========================================================================*/
             
public function md_update_deliverable($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
			//'DeliverableID' 	=> auto incrementing
			'DeliverableTypeID' => $data['DeliverableTypeID'],
			'DeliverableParentID' => $data['DeliverableParentID'],
			'DeliverableCode' 	=> $data['DeliverableCode'],
			'DeliverableDesc'	=> $data['DeliverableDesc'],
			'DeliverableDescShort'	=> $data['DeliverableDescShort'],
			'BusinessUnitID' 	=> $data['BusinessUnitID'],
			'ServiceAreaID' 	=> $data['ServiceArea'.$data['BusinessUnitID']],
			'OutcomeID' 		=> $data['OutcomeID'],
			'RiskID' 			=> $data['RiskID'],
			'PriorityID' 		=> $data['PriorityID'],
			'Source' 			=> $data['Source'],
			'AdminPriorityID' 	=> $data['AdminPriorityID'],
			'WorkingYear' 		=> $data['WorkingYear'],
			'ImpactedServ' 		=> $data['ImpactedServ'],
			'ServiceFOR' 		=> $data['ServiceFOR'],
			'ServiceBY' 		=> $data['ServiceBY'],
			'PublishedYN' 		=> $data['PublishedYN'],
		
			//'StatusID' 		=> $data['StatusID'],
			//'StatusUpdate' 	=> $data['StatusUpdate'],
			//'ApproxEndDate' 	=> $DATETIME,
			// 'ApproxComplete' 	=> 0,
			'LastModBy' 		=> $data['LastModBy'],
			'LastModDate' 		=> $DATETIME
		);

		$this->db->where('DeliverableID', $data['DeliverableID']);
		$result = $this->db->update('Deliverables',$values);

		if($result)	 	return true;
		else 			return false;
	}


		
/*===========================================================================
 * 
 *  FUNCTION:       md_get_servicearea
 *  AUTHOR:         R.Stephen Chafe
 *  CREATED:        2016_01_18			
 * 
 * ------------------------------------------------------------------------
 *
 *      This function will build a single record display of the services.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	    $id			Key value for ID in question	
 *  OUTPUTS:        	--
 * 
 *  MOD HISTRY:
 *  20160119    PRSC    Created from standard shell function 
 *  
 *    
 *==========================================================================*/
             	
	
	public function md_get_service_area($id)
	{
		$this->db->where('ServiceAreaID', $id);
		$query = $this->db->get('ServiceAreas');   // was originally a view
		if($query->row_array())	
				return $query->row_array();	
	}
	
	
/*===========================================================================
 * 
 *  FUNCTION:       md_get_service_areas
 *  AUTHOR:         R.Stephen Chafe
 *  CREATED:        2016_01_18			
 * 
 * ------------------------------------------------------------------------
 *
 *  This function loads the KPI ranges from the Service Area table
 *  for use in filling the drop down boxes for either the search 
 *  FILTER or the data entry screen.
 *  
 *        
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         na
 *  OUTPUTS:        query Results in array
 * 
 *  MOD HISTRY:
 * 
 *==========================================================================*/
	
	
	public function md_get_service_areas($id = null)
	{
		if(!empty($id))	$this->db->where('ServiceAreaID', $id);
		$this->db->order_by("ServiceAreaName", "ASC");
		$query = $this->db->get('ServiceAreas');
		return $query->result_array();
	}
	
	             
	public function md_update_service_area($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
			//'DeliverableID' 	=> auto incrementing
			'ServiceAreaName'	=> $data['ServiceAreaName'],
			'ServiceAreaDesc'	=> $data['ServiceAreaDesc'],
			'BusinessUnitID' 	=> $data['BusinessUnitID'],
//			'LastModBy' 		=> $data['LastModBy'],
			'LastModDate' 		=> $DATETIME
		);

		$this->db->where('ServiceAreaID', $data['ServiceAreaID']);
		$result = $this->db->update('ServiceAreas',$values);

		if($result)	 	return true;
		else 			return false;
	}
	

	public function md_create_service_area($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;
		
		$values = array(
			//'DeliverableID' 	=> auto incrementing
			'ServiceAreaName'	=> $data['ServiceAreaName'],
			'ServiceAreaDesc'	=> $data['ServiceAreaDesc'],
			'BusinessUnitID' 	=> $data['BusinessUnitID'],
//			'LastModBy' 		=> $data['LastModBy'],
			'LastModDate' 		=> $DATETIME
		);

		$result = $this->db->insert('ServiceAreas', $values);
		if($result)	 	return true;
		else 			return false;
	
	}
	
	
	
/*===========================================================================
 * 
 *  FUNCTION:       md_get_risk
 *  AUTHOR:         R.Stephen Chafe
 *  CREATED:        2016_01_18			
 * 
 * ------------------------------------------------------------------------
 *
 *      This function will build a single record display of the risks.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	    $id			Key value for ID in question	
 *  OUTPUTS:        	--
 * 
 *  MOD HISTRY:
 *  20160119    PRSC    Created from standard shell function 
 *  
 *    
 *==========================================================================*/
             	
	
	public function md_get_risk($id = 0)
	{
		$this->db->where('RiskID', $id);
		$query = $this->db->get('Risks');   // was originally a view
		if($query->row_array())	
				return $query->row_array();	
	}
	
/*===========================================================================
 * 
 *  FUNCTION:       md_get_risks
 *  AUTHOR:         Unknown - 
 *  CREATED:        20160119
 * 
 * ------------------------------------------------------------------------
 *
 *      Used to populate the Dropdown box in Filter section.
 *        
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         na
 *  OUTPUTS:        --
 * 
 *  MOD HISTRY:
 * 
 *==========================================================================*/
	
	
	public function md_get_risks($id = null)
	{
		if(!empty($id))	$this->db->where('RiskID', $id);
		$this->db->order_by("RiskDescShort", "ASC");
		$query = $this->db->get('Risks');
		return $query->result_array();
	}
	
	
	public function md_update_risk($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
			'RiskCode'			=> $data['RiskCode'],
			'RiskDescShort'		=> $data['RiskDescShort'],
			'RiskDesc'			=> $data['RiskDesc'],
			'Likelihood' 		=> $data['Likelihood'],
			'Impact' 			=> $data['Impact'],
			'HeatMapRating' 	=> $data['HeatMapRating'],
			'RiskOwner' 		=> $data['RiskOwner'],
			'RiskStatement' 	=> $data['RiskStatement'],
		//			'LastModBy' 		=> $data['LastModBy'],
			'LastModDate' 		=> $DATETIME
		);

		$this->db->where('RiskID', $data['RiskID']);
		$result = $this->db->update('Risks',$values);

		if($result)	 	return true;
		else 			return false;
	}
	
	
public function md_create_risk($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;
		
		$values = array(
			//'DeliverableID' 	=> auto incrementing
			'RiskCode'			=> $data['RiskCode'],
			'RiskDescShort'		=> $data['RiskDescShort'],
			'RiskDesc'			=> $data['RiskDesc'],
			'Likelihood' 		=> $data['Likelihood'],
			'Impact' 			=> $data['Impact'],
			'HeatMapRating' 	=> $data['HeatMapRating'],
			'RiskOwner' 		=> $data['RiskOwner'],
			'RiskStatement' 	=> $data['RiskStatement'],
		//			'LastModBy' 		=> $data['LastModBy'],
			'LastModDate' 		=> $DATETIME
				);

		$result = $this->db->insert('Risks', $values);
		if($result)	 	return true;
		else 			return false;
	
	}	
	
/*===========================================================================
 * 
 *  FUNCTION:       md_get_service
 *  AUTHOR:         R.Stephen Chafe
 *  CREATED:        2016_01_18			
 * 
 * ------------------------------------------------------------------------
 *
 *      This function will build a single record display of the services.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	    $id			Key value for ID in question	
 *  OUTPUTS:        	--
 * 
 *  MOD HISTRY:
 *  20160119    PRSC    Created from standard shell function 
 *  
 *    
 *==========================================================================*/
             	
	
	public function md_get_service($id = 0)
	{
		$this->db->where('ServiceID', $id);
		$query = $this->db->get('Services');   // was originally a view
		if($query->row_array())	
				return $query->row_array();	
	}
	
		
	
/*===========================================================================
 * 
 *  FUNCTION:       md_get_services
 *  AUTHOR:         R.Stephen Chafe
 *  CREATED:        2016_01_18			
 * 
 * ------------------------------------------------------------------------
 *
 *  This function loads the KPI ranges from the Services table
 *  for use in filling the drop down boxes for either the search 
 *  FILTER or the data entry screen.
 *  
 *        
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         na
 *  OUTPUTS:        query Results in array
 * 
 *  MOD HISTRY:
 * 
 *==========================================================================*/
	
	
	public function md_get_services($id = null)
	{
		if(!empty($id))	$this->db->where('ServiceID', $id);
		$this->db->order_by("ServiceShortNM", "ASC");
		$query = $this->db->get('Services');
		return $query->result_array();
	}

	
	public function md_update_service($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
			//'DeliverableID' 	=> auto incrementing
			'ServiceShortNM'	=> $data['ServiceShortNM'],
			'ServiceDESC'		=> $data['ServiceDESC'],
			'BusinessUnitID' 	=> $data['BusinessUnitID'],
			'ServiceAreaID' 	=> $data['ServiceAreaID'],
//			'LastModBy' 		=> $data['LastModBy'],
//			'LastModDate' 		=> $DATETIME
		);

		$this->db->where('ServiceID', $data['ServiceID']);
		$result = $this->db->update('Services',$values);

		if($result)	 	return true;
		else 			return false;
	}
	
	public function md_create_service($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;
		
		$values = array(
			//'DeliverableID' 	=> auto incrementing
			'ServiceShortNM'	=> $data['ServiceShortNM'],
			'ServiceDESC'		=> $data['ServiceDESC'],
			'BusinessUnitID' 	=> $data['BusinessUnitID'],
			'ServiceAreaID' 	=> $data['ServiceAreaID'],
//			'LastModBy' 		=> $data['LastModBy'],
//			'LastModDate' 		=> $DATETIME
						);

		$result = $this->db->insert('Services', $values);
		if($result)	 	return true;
		else 			return false;
	
	}		
	
/*===========================================================================
 * 
 *  FUNCTION:       md_get_service_standard
 *  AUTHOR:         R.Stephen Chafe
 *  CREATED:        2016_01_18			
 * 
 * ------------------------------------------------------------------------
 *
 *      This function will build a single record display of the service_standards.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	$id			Key value for ID in question	
 *  OUTPUTS:        array		query Results
 * 
 *  MOD HISTRY:
 *  20160119    	PRSC    Created from standard shell function 
 *  
 *    
 *==========================================================================*/
             	
	
	public function md_get_standard($id = 0)
	{
		$this->db->where('StandardID', $id);
		$query = $this->db->get('Standards');   // was originally a view
		if($query->row_array())	
				return $query->row_array();	
	}
	
	
/*===========================================================================
 * 
 *  FUNCTION:       md_get_service_standards
 *  AUTHOR:         R.Stephen Chafe
 *  CREATED:        2016_01_18			
 * 
 * ------------------------------------------------------------------------
 *
 *  This function loads the ranges from the Service_Standards table
 *  for use in filling the drop down boxes for either the search 
 *  FILTER or the data entry screen.
 *  
 *        
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         $id			Key value for ID in question	
 *  OUTPUTS:        array		query results
 * 
 *  MOD HISTRY:
 * 
 *==========================================================================*/
	
	
	public function md_get_standards($id = null)
	{
		if(!empty($id))	$this->db->where('StandardID', $id);
		$this->db->order_by("StandardShortNM", "ASC");
		$query = $this->db->get('Standards');
		return $query->result_array();
	}
	
	public function md_update_standard($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
			//'DeliverableID' 	=> auto incrementing
			'StandardShortNM'	=> $data['StandardShortNM'],
			'StandardDesc'		=> $data['StandardDesc'],
			'ServiceID' 		=> $data['ServiceID'],
//			'LastModBy' 		=> $data['LastModBy'],
//			'LastModDate' 		=> $DATETIME
		);

		$this->db->where('StandardID', $data['StandardID']);
		$result = $this->db->update('Standards',$values);

		if($result)	 	return true;
		else 			return false;
	}
		
	public function md_create_standard($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;
		
		$values = array(
		//'DeliverableID' 	=> auto incrementing
			'StandardShortNM'	=> $data['StandardShortNM'],
			'StandardDesc'		=> $data['StandardDesc'],
			'ServiceID' 		=> $data['ServiceID'],
//			'LastModBy' 		=> $data['LastModBy'],
//			'LastModDate' 		=> $DATETIME
								);

		$result = $this->db->insert('Standards', $values);
		if($result)	 	return true;
		else 			return false;
	
	}		
	

/*===========================================================================
 * 
 *  FUNCTION:       md_get_performance
 *  AUTHOR:         R.Stephen Chafe
 *  CREATED:        2016_01_18			
 * 
 * ------------------------------------------------------------------------
 *
 *      This function will build a single record display of the performance.
 * 
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:     	$id			Key value for ID in question	
 *  OUTPUTS:        array		query Results
 * 
 *  MOD HISTRY:
 *  20160119    	PRSC    Created from standard shell function 
 *  
 *    
 *==========================================================================*/
             	
	
	public function md_get_performance($id = 0)
	{
		$this->db->where('PerformanceID', $id);
		$query = $this->db->get('Performances');   // was originally a view
		if($query->row_array())	
				return $query->row_array();	
	}
	
	
/*===========================================================================
 * 
 *  FUNCTION:       md_get_performances
 *  AUTHOR:         R.Stephen Chafe
 *  CREATED:        2016_01_18			
 * 
 * ------------------------------------------------------------------------
 *
 *  This function loads the ranges from the Performance table
 *  for use in filling the drop down boxes for either the search 
 *  FILTER or the data entry screen.
 *  
 *        
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         $id			Key value for ID in question	
 *  OUTPUTS:        array		query results
 * 
 *  MOD HISTRY:
 * 
 *==========================================================================*/
	
	

	public function md_get_performances($id = null)
	{
		if(!empty($id))	$this->db->where('PerformanceID', $id);
		$this->db->order_by("PerformanceShortNM", "ASC");
		$query = $this->db->get('Performances');
		return $query->result_array();
	}

	public function md_update_performance($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");

		$values = array(
			//'DeliverableID' 	=> auto incrementing
			'PerformanceShortNM'	=> $data['PerformanceShortNM'],
			'PerformanceDesc'		=> $data['PerformanceDesc'],
			'ServiceID' 			=> $data['ServiceID'],
//			'LastModBy' 		=> $data['LastModBy'],
//			'LastModDate' 		=> $DATETIME
		);

		$this->db->where('PerformanceID', $data['PerformanceID']);
		$result = $this->db->update('Performances',$values);

		if($result)	 	return true;
		else 			return false;
	}
		
	public function md_create_performance($data)
	{
		$DATETIME	= date("Y-m-d H:i:s");
		$USER		= 0;
		$result 	= false;
		
		$values = array(
		//'DeliverableID' 	=> auto incrementing
			'PerformanceShortNM'	=> $data['PerformanceShortNM'],
			'PerformanceDesc'		=> $data['PerformanceDesc'],
			'ServiceID' 			=> $data['ServiceID'],
//			'LastModBy' 		=> $data['LastModBy'],
//			'LastModDate' 		=> $DATETIME
		);

		$result = $this->db->insert('Performances', $values);
		if($result)	 	return true;
		else 			return false;
	
	}		
		
		
/*===========================================================================
 * 
 *  FUNCTION:       md_get_
 *  AUTHOR:         R.Stephen Chafe
 *  CREATED:        2016_01_18			
 * 
 * ------------------------------------------------------------------------
 *
 *  This function loads the data used in the various dropdowns.
 *         
 * ------------------------------------------------------------------------
 * 
 *  INPUTS:         $id			Key value for ID in question	
 *  OUTPUTS:        array		query results
 * 
 *  MOD HISTRY:
 * 
 *==========================================================================*/
	
	
	public function md_get_likelihoods($id = null)
	{
		if(!empty($id))	$this->db->where('LikelihoodID', $id);
		$this->db->order_by("LikelihoodID", "ASC");
		$query = $this->db->get('Likelihoods');
		return $query->result_array();
	}
	
	public function md_get_impacts($id = null)
	{
		if(!empty($id))	$this->db->where('ImpactID', $id);
		$this->db->order_by("ImpactID", "ASC");
		$query = $this->db->get('Impacts');
		return $query->result_array();
	}
	
	
	public function md_get_heatmapratings($id = null)
	{
		if(!empty($id))	$this->db->where('HeatMapRateID', $id);
		$this->db->order_by("HeatMapRateID", "ASC");
		$query = $this->db->get('HeatMapRatings');
		return $query->result_array();
	}
	
	public function md_get_published($id = null)
	{
		if(!empty($id))	$this->db->where('PublishedID', $id);
		$this->db->order_by("PublishedID", "ASC");
		$query = $this->db->get('Published');
		return $query->result_array();
	}
	
	public function md_get_sources($id = null)
	{
		if(!empty($id))	$this->db->where('SourceID', $id);
		$this->db->order_by("SourceID", "ASC");
		$query = $this->db->get('Sources');
		return $query->result_array();
	}
	
		public function md_get_working_years($id = null)
	{
		if(!empty($id))	$this->db->where('WorkingYearID', $id);
		$this->db->order_by("WorkingYearID", "ASC");
		$query = $this->db->get('WorkingYears');
		return $query->result_array();
	}
	
	
	
}

